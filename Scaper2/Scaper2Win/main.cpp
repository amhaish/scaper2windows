//External Libraries Headers
#include <boost/thread/thread.hpp>
#include <boost/program_options.hpp>
#include <pcl/io/openni2_grabber.h>
#include <pcl/io/pcd_grabber.h>
//Built-in Code Headers
#include <Consts.h>

#include <ExecutionPipeline.h>

#include <PCLEuclideanSegmenter.h>
#include <PCLStatisticalOutlierFilter.h>
#include <PCLPlaneModelsFilter.h>
#include <windows.h>


#include <PCLNormalCalculator.h>
#include <pcl/visualization/histogram_visualizer.h>
#include <algorithm>
#include <boost/filesystem.hpp>

enum GrabberType { DepthSenseGrabber = 0, OpenNIGrabber = 1, OpenNI2Grabber = 2, PCDGrabber = 3 };
int initializeGVariablesThroughCommandLineParameters(int, char**);
//Global variables
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_orig;
boost::shared_ptr<pcl::visualization::ImageViewer> viewer_orig_img;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_planar;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_seg;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_tracker;
boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> viewer_hist;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_selector;


pcl::Grabber* interface;
GrabberType grabberType;
uint units_divisor;
std::string grabber_resolution;
std::string pcd_files_path;//for when using PCDGrabber
boost::filesystem::path pcd_path;
boost::mutex viewer_mutex;
uint segmentSizeThreshold;
bool displayNormals;
double filterStddevMulThresh;
int filterMeanK;

float segmenterDistanceThreshold;
double planeFilterDistanceThreshold;
double planeFilterAngularThreshold;
float  normalCalcMaxDepthChangeFactor;
float  normalSmoothingSize;

bool segment_nan_normals_are_edges;
float  planeFilterMaximumCurvature;
float  planeFilterMinInliers;
float  segmenterAngularThreshold;

float segmentYThreshold;

bool enableStatistcalOutlierFilter;

bool enablPlaneFilter;

char selection_approach;
char tracking_approach;

double selector_scan_distance, selector_scan_time;

double fpfh_radius;
std::string path_to_fpfh_database;

char sound_system_approach;

double sound_x_offset;
double sound_y_offset;
double sound_z_offset;
double exaggerate_pos;
bool joystick;

bool gpu;

bool viz_orig;
bool viz_orig_img;
bool viz_planar;
bool viz_seg;
bool viz_tracker;
bool viz_hist;
bool viz_selector;

bool timing;

ExecutionPipeline* executionPipeline;


int main(int argc, char** argv)
{

	srand(RAND_SEED);
	//==============================
	if (initializeGVariablesThroughCommandLineParameters(argc, argv))
	{
		switch (grabberType)
		{
		case GrabberType::DepthSenseGrabber:
			if (grabber_resolution != "") {
				std::cerr << "Cannot set resolution of DepthSense with this interface: Always 30Hz VGA." << std::endl;
				exit(1);
			}

			interface = new pcl::io::OpenNI2Grabber("");
			break;
		case  GrabberType::PCDGrabber:
			if (grabber_resolution != "") {
				std::cerr << "Cannot set resolution of PCD grabber with this interface." << std::endl;
				exit(1);
			}
			pcd_path = boost::filesystem::path(pcd_files_path);
			if (!boost::filesystem::exists(pcd_path)) {
				std::cerr << "PCD file path does not exist" << std::endl;
				exit(1);
			}
			if (boost::filesystem::is_directory(pcd_path)) {
				std::vector<std::string> pcds;
				std::cout << "-------" << std::endl;
				std::cout << "Files:" << std::endl;
				std::cout << "-------" << std::endl;
				for (boost::filesystem::directory_iterator it(pcd_path), end; it != end; ++it) {
					std::string thisfile;
					thisfile = it->path().string();
					pcds.push_back(thisfile);
					// 	   std::cout<<thisfile<<std::endl;
				}
				std::sort(pcds.begin(), pcds.end());
				for (auto it = pcds.begin(); it != pcds.end(); it++)
					std::cout << *it << std::endl;
				std::cout << "-------" << std::endl;
				interface = new pcl::PCDGrabber<SystemPoint>(pcds, 30, true);
			}
			else {
				interface = new pcl::PCDGrabber<SystemPoint>(pcd_files_path, 30, true);
			}
			break;
		default:
			//interface= new pcl::DepthSenseGrabber("");
			assert(0);
			break;
		}
		executionPipeline = new ExecutionPipeline();
		executionPipeline->setTiming(timing);


		if (viz_orig)
			viewer_orig.reset(new pcl::visualization::PCLVisualizer("Original cloud"));

		if (viz_orig_img)
			viewer_orig_img.reset(new pcl::visualization::ImageViewer("Original cloud depth image"));

		if (viz_planar)
			viewer_planar.reset(new pcl::visualization::PCLVisualizer("Planar segmentation tester"));

		if (viz_seg)
			viewer_seg.reset(new pcl::visualization::PCLVisualizer("Proto-object segmentation tester"));

		if (viz_tracker)
			viewer_tracker.reset(new pcl::visualization::PCLVisualizer("Tracker tester"));

		if (viz_selector)
			viewer_selector.reset(new pcl::visualization::PCLVisualizer("Selector tester"));

		if (viz_hist)
			viewer_hist.reset(new pcl::visualization::PCLHistogramVisualizer());

		if (viz_orig)executionPipeline->AddOrigViewer(viewer_orig, viewer_mutex, displayNormals);
		if (viz_orig_img)executionPipeline->AddOrigImageViewer(viewer_orig_img, viewer_mutex);

		if (enableStatistcalOutlierFilter)
		{
			IPCLFilter* filter = new PCLStatisticalOutlierFilter(NULL, filterMeanK, filterStddevMulThresh);;
			executionPipeline->AddPCLFilter(filter);
		}
		if (enablPlaneFilter)
		{
			IPCLFilter* filter;
			if (viz_planar)

				filter = new PCLPlaneModelsFilter(viewer_planar, planeFilterDistanceThreshold, planeFilterMaximumCurvature, planeFilterMinInliers, planeFilterAngularThreshold, gpu, viewer_mutex);

			else

				filter = new PCLPlaneModelsFilter(NULL, planeFilterDistanceThreshold, planeFilterMaximumCurvature, planeFilterMinInliers, planeFilterAngularThreshold, gpu, viewer_mutex);

			executionPipeline->AddPCLFilter(filter);
		}

		IPCLNormalCalculator* normalCalculator;
		IPCLSegmenter* segmenter;
		//ISegmentTracker* tracker;
		//ISegmentSelector* selector;
		ISegmentSonify* soundSystem;

		normalCalculator = new PCLNormalCalculator(normalCalcMaxDepthChangeFactor, normalSmoothingSize);

		if (viz_seg)
			segmenter = new PCLEuclideanSegmenter(viewer_seg, segmenterDistanceThreshold, segmenterAngularThreshold, segmentYThreshold, segmentSizeThreshold, segment_nan_normals_are_edges, displayNormals, viewer_mutex);
		else
			segmenter = new PCLEuclideanSegmenter(NULL, segmenterDistanceThreshold, segmenterAngularThreshold, segmentYThreshold, segmentSizeThreshold, segment_nan_normals_are_edges, displayNormals, viewer_mutex);

		/*if(tracking_approach=='H')
		  if(viz_tracker)
			tracker=new SegmentTrackerHC(units_divisor,viewer_tracker,viewer_mutex);
		  else
			tracker=new SegmentTrackerHC(units_divisor,NULL,viewer_mutex);
		else if(tracking_approach=='P')
			tracker=new SegmentTrackerPassthrough();
		else if (tracking_approach=='D')
			tracker= new SegmentTrackerAssoc1(units_divisor,viewer_tracker,viewer_mutex);
		else{
		  std::cout<<"UNKNOWN TRACKING APPROACH "<<tracking_approach<<std::endl;
		  exit(1);
		}*/

		/*if(selection_approach=='P')
		{
		 std::cout << "Using null segment selector"<<std::endl;
		 selector = new SegmentSelectorPassthrough();
		}
		else if(selection_approach=='I')
		{
		  std::cout << "Using Hakan & Çağatay's segment selector"<<std::endl;
		  if(viz_selector)
			selector = new SegmentSelectorInteractiveHC(units_divisor,viewer_selector,viewer_mutex);
		  else
			selector = new SegmentSelectorInteractiveHC(units_divisor,NULL,viewer_mutex);
		}
		else if(selection_approach=='A')
		{
		  std::cout << "Using waveform advance segment selector"<<std::endl;
		  std::cout << "selector_scan_distance="<<selector_scan_distance<<" selector_scan_time="<<selector_scan_time<<std::endl;
		  if(viz_selector)
			selector = new SegmentSelectorAdvance(selector_scan_distance,selector_scan_time,viewer_selector,viewer_mutex);
		  else
			selector = new SegmentSelectorAdvance(selector_scan_distance,selector_scan_time,NULL,viewer_mutex);
		}
		else
		{
		  std::cerr<<"I don't know the segment selection approach "<<selection_approach<<"."<<std::endl;
		  exit(1);
		}*/

		/////////////////////////////////////////////////////////////////////////7    
			/*if(sound_system_approach=='T'){
			  soundSystem =  new TestSegmentSonify(exaggerate_pos);
			   soundSystem=tss;
			}
			else if(sound_system_approach== 'S')
			{
			 soundSystem=new SegmentSonify(exaggerate_pos);
			   if(viz_hist)..............
			 }else if(sound_system_approach== 'C')
			 {}
			 }
			else
			{

			  if(viz_hist)
			soundSystem=new SegmentSonifyFPFH(fpfh_radius,viewer_hist,viewer_mutex,path_to_fpfh_database,exaggerate_pos);
			  else
			soundSystem=new SegmentSonifyFPFH(fpfh_radius,NULL,viewer_mutex,path_to_fpfh_database,exaggerate_pos);
			}*/

		executionPipeline->SetPCLNormalCalculator(normalCalculator);
		executionPipeline->SetPCLSegmenter(segmenter);
		//executionPipeline->SetSegmentTracker(tracker);
		//executionPipeline->SetSegmentSelector(selector);
		//executionPipeline->SetSoundSonificationApproach(soundSystem);

		boost::function<void(const pcl::PointCloud<SystemPoint>::ConstPtr&)> callBackFunction = boost::bind(&ExecutionPipeline::ExecutePipeline, executionPipeline, _1);
		interface->registerCallback(callBackFunction);
		interface->start();
		bool st = false;
		while (!st) {
			if (viz_orig) {
				if (viewer_orig->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_orig->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}
			if (viz_orig_img) {
				if (viewer_orig_img->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_orig_img->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}
			if (viz_planar) {
				if (viewer_planar->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_planar->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}
			if (viz_seg) {
				if (viewer_seg->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_seg->spinOnce(0.0001);
					viewer_seg->setBackgroundColor(255, 255, 255);
					//sleep(1000000);
					viewer_mutex.unlock();
				}
			}
			if (viz_tracker) {
				if (viewer_tracker->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_tracker->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}

			if (viz_selector) {
				if (viewer_selector->wasStopped())st = true;
				if (viewer_mutex.try_lock()) {
					viewer_selector->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}

			if (viz_hist) {
				// 	    if(viewer_hist->wasStopped())st=true;
				if (viewer_mutex.try_lock()) {
					viewer_hist->spinOnce(0.0001);
					viewer_mutex.unlock();
				}
			}

			Sleep(100);


		}
		std::cout << "Closing the program --- thanks for playing." << std::endl;
		interface->stop();
	}
	return 0;
}
int initializeGVariablesThroughCommandLineParameters(int ac, char** av) {
	// Declare the supported options.
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "display segmenter tester usage info")
		("grabber", boost::program_options::value<char>()->default_value('D'), "set Grabber type if its  DepthSense Grabber(D) or OpenNI Grabber(O), OpenNI2 Grabber(P), File (PCD) Grabber(F). (D) is the default value")
		("grabber_resolution", boost::program_options::value<std::string>()->default_value(""), "set Grabber resolution - important for speed vs accuracy")
		("pcd_files_path", boost::program_options::value<std::string>()->default_value("PATH TO PCD FILES NOT SET"), "If using File (PCD) Grabber in --grabber option, the path of the PCD file OR a path to a directory containing *only* PCD files")
		("planeFilter", boost::program_options::value<char>()->default_value('D'), "Enable(E) or disable(D) the filteration stage")
		("filterMeanK", boost::program_options::value<uint>()->default_value(10), "Set filter MeanK value")
		("filterStddevMulThreshold", boost::program_options::value<float>()->default_value(1.0), "Set filter standard deviation multiplier threshold")
		("segmentYThreshold", boost::program_options::value<float>()->default_value(0.6), "The threshold of y value for segments normals")
		("thresholdSegmentsByY", boost::program_options::value<char>()->default_value('D'), "Whether or not to threshold normals")
		("normalCalcMaxDepthChangeFactor", boost::program_options::value<float>()->default_value(0.5f), "Set the plane filter and segmenter max Depth Change Factor for normal calculations")
		("normalSmoothingSize", boost::program_options::value<float>()->default_value(30.0f), "Set the plane filter and segmenter normal Smoothing Size for normal calculations")
		("segment_nan_normals_are_edges", boost::program_options::value<char>()->default_value('Y'), "set segmenter to use treat NAN normals as edges (Y) or ignore the normals at such points (N). Default is Y.")
		("enableStatistcalOutlierFilter", boost::program_options::value<char>()->default_value('D'), "Enable(E) or disable(D) the filteration stage")
		("planeFilterDistanceThreshold", boost::program_options::value<float>()->default_value(0.02), "Set the plane filter distance threshold")
		("planeFilterAngularThreshold", boost::program_options::value<float>()->default_value(3.96), "Set the plane filter angular Threshold")
		("planeFilterMaximumCurvature", boost::program_options::value<float>()->default_value(0.2), "Set the plane filter maximum Curvature")
		("planeFilterMinInliers", boost::program_options::value<float>()->default_value(1000), "Set the plane filter minimum Inliers")
		("segmenterDistanceThreshold", boost::program_options::value<float>()->default_value(0.08), "Set distance threshold for the segmenter")
		("segmenterAngularThreshold", boost::program_options::value<float>()->default_value(3.96), "Set the segmenter angular Threshold")
		("segmentSizeThreshold", boost::program_options::value<uint>()->default_value(50), "Set segment size threshold for the segmenter")
		("tracking_approach", boost::program_options::value<char>()->default_value('P'), "Approach to tracking segments. P=Passthrough, H=Hakan's approach.")
		("selection_approach", boost::program_options::value<char>()->default_value('P'), "Approach to choosing segments. P=Pass-through, A=Advance, I=Interactive (from Hakan & Çağatay).")
		("selector_scan_distance", boost::program_options::value<double>()->default_value(4.0), "Parameter for the Advance (A) type selector.")
		("selector_scan_time", boost::program_options::value<double>()->default_value(4.0), "Parameter for the Advance (A) type selector.")
		("fpfh_radius", boost::program_options::value<double>()->default_value(0.08), "FPFH max radius when searching neighbouring points")
		("sound_system_approach", boost::program_options::value<char>()->default_value('F'), "select sound system approach: FPFH sound system 'F',  Contour sound system 'C', SineWave sound system 'S', or a test of the new interpolation 'T'.")
		("path_to_fpfh_database", boost::program_options::value<std::string>()->default_value("../data/FPFH-Dataset/cloudOfFPFHs/"), "Path to FPFH database")
		("units_divisor", boost::program_options::value<uint>()->default_value(1), "For Xtion-sourced images this should be 10.")
		("sound_x_offset", boost::program_options::value<float>()->default_value(0.0), "Amount by which to offset sounds relative to sensor origin.")
		("sound_y_offset", boost::program_options::value<float>()->default_value(0.0), "Amount by which to offset sounds relative to sensor origin.")
		("sound_z_offset", boost::program_options::value<float>()->default_value(0.0), "Amount by which to offset sounds relative to sensor origin.")
		("exaggerate_pos", boost::program_options::value<float>()->default_value(1.0), "Multiplier by which to exaggerate sound position")
		("displayNormals", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') displaying normals.")
		("viz_orig", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of original point cloud.")
		("viz_orig_img", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of original point cloud -- as a depth image.")
		("viz_planar", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of planar background segmentation.")
		("viz_seg", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of segmentation.")
		("viz_tracker", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of tracking.")
		("viz_hist", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of histogram.")
		("viz_selector", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') visualisation of selecting.")
		("timing", boost::program_options::value<char>()->default_value('D'), "Enable('E') or disable('D') timing.")
		("gpu", boost::program_options::value<char>()->default_value('E'), "Enable('E') or disable('D') GPU processing mode.")
		("joystick", boost::program_options::value<char>()->default_value('E'), "Enable('E') or disable('D') joystick.")
		;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(ac, av, desc), vm);
	boost::program_options::notify(vm);
	if (vm.count("help")) {
		cout << desc << "\n";
		return 0;
	}
	if (vm.count("grabber")) {
		switch (vm["grabber"].as<char>()) {
		case 'D':
			grabberType = GrabberType::DepthSenseGrabber;
			break;
		case 'O':
			grabberType = GrabberType::OpenNIGrabber;
			break;
		case 'P':
			grabberType = GrabberType::OpenNI2Grabber;
			break;
		case 'F':
			grabberType = GrabberType::PCDGrabber;
			pcd_files_path = vm["pcd_files_path"].as<std::string>(); //e.g. ~/software/pcl/test/
			break;
		default:
			assert(0);
			break;
		}
	}
	grabber_resolution = vm["grabber_resolution"].as<std::string>();
	fpfh_radius = vm["fpfh_radius"].as<double>();
	path_to_fpfh_database = vm["path_to_fpfh_database"].as<std::string>();
	//     sound_system_approach=(vm["sound_system_approach"].as<char>()=='S'?true:false);
	sound_system_approach = vm["sound_system_approach"].as<char>();
	units_divisor = vm["units_divisor"].as<uint>();
	sound_x_offset = vm["sound_x_offset"].as<float>();
	sound_y_offset = vm["sound_y_offset"].as<float>();
	sound_z_offset = vm["sound_z_offset"].as<float>();
	enablPlaneFilter = (vm["planeFilter"].as<char>() == 'E' ? true : false);
	gpu = (vm["gpu"].as<char>() == 'E' ? true : false);
	enableStatistcalOutlierFilter = (vm["enableStatistcalOutlierFilter"].as<char>() == 'E' ? true : false);
	segmenterDistanceThreshold = vm["segmenterDistanceThreshold"].as<float>();
	planeFilterDistanceThreshold = vm["planeFilterDistanceThreshold"].as<float>();
	planeFilterAngularThreshold = vm["planeFilterAngularThreshold"].as<float>();
	normalCalcMaxDepthChangeFactor = vm["normalCalcMaxDepthChangeFactor"].as<float>();
	normalSmoothingSize = vm["normalSmoothingSize"].as<float>();
	segment_nan_normals_are_edges = (vm["segment_nan_normals_are_edges"].as<char>() == 'Y' ? true : false);
	planeFilterMaximumCurvature = vm["planeFilterMaximumCurvature"].as<float>();
	planeFilterMinInliers = vm["planeFilterMinInliers"].as<float>();
	segmentYThreshold = vm["segmentYThreshold"].as<float>();
	if (vm["thresholdSegmentsByY"].as<char>() != 'E')segmentYThreshold = -1;
	segmenterAngularThreshold = vm["segmenterAngularThreshold"].as<float>();
	filterMeanK = vm["filterMeanK"].as<uint>();
	filterStddevMulThresh = vm["filterStddevMulThreshold"].as<float>();
	segmentSizeThreshold = vm["segmentSizeThreshold"].as<uint>();
	selection_approach = vm["selection_approach"].as<char>();
	tracking_approach = vm["tracking_approach"].as<char>();
	selector_scan_distance = vm["selector_scan_distance"].as<double>();
	selector_scan_time = vm["selector_scan_time"].as<double>();
	exaggerate_pos = vm["exaggerate_pos"].as<float>();
	displayNormals = (vm["displayNormals"].as<char>() == 'E' ? true : false);
	viz_orig = (vm["viz_orig"].as<char>() == 'E' ? true : false);
	viz_orig_img = (vm["viz_orig_img"].as<char>() == 'E' ? true : false);
	viz_planar = (vm["viz_planar"].as<char>() == 'E' ? true : false);
	viz_seg = (vm["viz_seg"].as<char>() == 'E' ? true : false);
	viz_tracker = (vm["viz_tracker"].as<char>() == 'E' ? true : false);
	viz_hist = (vm["viz_hist"].as<char>() == 'E' ? true : false);
	viz_selector = (vm["viz_selector"].as<char>() == 'E' ? true : false);
	timing = (vm["timing"].as<char>() == 'E' ? true : false);
	joystick = (vm["joystick"].as<char>() == 'E' ? true : false);
	//std::cout<<"joystick"<<joystick<<std::endl;
	return 1;
}
