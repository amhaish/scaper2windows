﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Scaper2Wrapper;

namespace Scaper2Manager
{
    public partial class MainForm : Form
    {
        private bool engineStarted = false;
        private Scaper2Engine engine;

        public MainForm()
        {
            InitializeComponent();
            engine = new Scaper2Engine(WrapperGrabberType.DepthSenseGrabber);
        }

        private void btnToogleEngineStatus_Click(object sender, EventArgs e)
        {
            if (!engineStarted)
            {
                try
                {
                    engine.ConfigureEngineVisualizers(viz_orig.Checked, viz_orig_img.Checked, viz_planar.Checked, viz_seg.Checked, viz_tracker.Checked, false, false,false, dispalyNormals.Checked);
                    engine.ConfigurePipelineComponents(false, true, (sbyte)'P', (sbyte)'P', (sbyte)'F');
                    engine.ConfigurePipelineParameters(
                        float.Parse(MaxDepthChangeFactor.Text),
                        float.Parse(NormalSmoothingSize.Text),
                        float.Parse(segmenterDistanceThreshold.Text),
                        float.Parse(segmenterAngularThreshold.Text),
                        float.Parse(segmentYThreshold.Text),
                        float.Parse(segmenterCurvatureThreshold.Text),
                        int.Parse(segmentSizeThreshold.Text),
                        segment_nan_normals_are_edges.Checked,
                        double.Parse(planesFilterDistanceThreshold.Text),
                        double.Parse(planesFilterAngiularThreshold.Text),
                        float.Parse(planesFilterMaxCurvature.Text),
                        float.Parse(planesFilterMinInliers.Text)
                        );
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error in setting the parameters, please check them again.");
                    return;
                }
                engineStarted = true;
                EngineStatusViewChanges();
                engine.StartPipeline();
            }
            else
            {
                engine.StopPipeline();
                engineStarted = false;
                EngineStatusViewChanges();
            }

        }

        private void EngineStatusViewChanges()
        {
            if (engineStarted)
            {
                btnToogleEngineStatus.Text = "Stop Engine";
                foreach (Control c in groupBox1.Controls)
                {
                    c.Enabled = false;
                }
                foreach (Control c in groupBox2.Controls)
                {
                    c.Enabled = false;
                }
            }
            else
            {
                btnToogleEngineStatus.Text = "Start Engine";
                foreach (Control c in groupBox1.Controls)
                {
                    c.Enabled = true;
                }
                foreach (Control c in groupBox2.Controls)
                {
                    c.Enabled = true;
                }
            }
        }

        private void checkboxesCheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
