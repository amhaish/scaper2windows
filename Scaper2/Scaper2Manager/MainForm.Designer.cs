﻿namespace Scaper2Manager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnToogleEngineStatus = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dispalyNormals = new System.Windows.Forms.CheckBox();
            this.viz_tracker = new System.Windows.Forms.CheckBox();
            this.viz_seg = new System.Windows.Forms.CheckBox();
            this.viz_planar = new System.Windows.Forms.CheckBox();
            this.viz_orig_img = new System.Windows.Forms.CheckBox();
            this.viz_orig = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.segmentSizeThreshold = new System.Windows.Forms.TextBox();
            this.segmentYThreshold = new System.Windows.Forms.TextBox();
            this.segmenterAngularThreshold = new System.Windows.Forms.TextBox();
            this.segmenterDistanceThreshold = new System.Windows.Forms.TextBox();
            this.segment_nan_normals_are_edges = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NormalSmoothingSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.MaxDepthChangeFactor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.segmenterCurvatureThreshold = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.planesFilterMinInliers = new System.Windows.Forms.TextBox();
            this.planesFilterMaxCurvature = new System.Windows.Forms.TextBox();
            this.planesFilterAngiularThreshold = new System.Windows.Forms.TextBox();
            this.planesFilterDistanceThreshold = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnToogleEngineStatus
            // 
            this.btnToogleEngineStatus.Location = new System.Drawing.Point(615, 24);
            this.btnToogleEngineStatus.Name = "btnToogleEngineStatus";
            this.btnToogleEngineStatus.Size = new System.Drawing.Size(119, 36);
            this.btnToogleEngineStatus.TabIndex = 0;
            this.btnToogleEngineStatus.Text = "Start Engine";
            this.btnToogleEngineStatus.UseVisualStyleBackColor = true;
            this.btnToogleEngineStatus.Click += new System.EventHandler(this.btnToogleEngineStatus_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dispalyNormals);
            this.groupBox1.Controls.Add(this.viz_tracker);
            this.groupBox1.Controls.Add(this.viz_seg);
            this.groupBox1.Controls.Add(this.viz_planar);
            this.groupBox1.Controls.Add(this.viz_orig_img);
            this.groupBox1.Controls.Add(this.viz_orig);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 609);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vizualizers Configurations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Vizualizers options";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Vizualizers types";
            // 
            // dispalyNormals
            // 
            this.dispalyNormals.AutoSize = true;
            this.dispalyNormals.Location = new System.Drawing.Point(6, 189);
            this.dispalyNormals.Name = "dispalyNormals";
            this.dispalyNormals.Size = new System.Drawing.Size(101, 17);
            this.dispalyNormals.TabIndex = 5;
            this.dispalyNormals.Text = "Dispaly Normals";
            this.dispalyNormals.UseVisualStyleBackColor = true;
            // 
            // viz_tracker
            // 
            this.viz_tracker.AutoSize = true;
            this.viz_tracker.Location = new System.Drawing.Point(6, 137);
            this.viz_tracker.Name = "viz_tracker";
            this.viz_tracker.Size = new System.Drawing.Size(105, 17);
            this.viz_tracker.TabIndex = 4;
            this.viz_tracker.Text = "Vizualize Tracker";
            this.viz_tracker.UseVisualStyleBackColor = true;
            this.viz_tracker.CheckedChanged += new System.EventHandler(this.checkboxesCheckedChanged);
            // 
            // viz_seg
            // 
            this.viz_seg.AutoSize = true;
            this.viz_seg.Location = new System.Drawing.Point(6, 114);
            this.viz_seg.Name = "viz_seg";
            this.viz_seg.Size = new System.Drawing.Size(121, 17);
            this.viz_seg.TabIndex = 3;
            this.viz_seg.Text = "Vizualize Segmenter";
            this.viz_seg.UseVisualStyleBackColor = true;
            this.viz_seg.CheckedChanged += new System.EventHandler(this.checkboxesCheckedChanged);
            // 
            // viz_planar
            // 
            this.viz_planar.AutoSize = true;
            this.viz_planar.Location = new System.Drawing.Point(6, 91);
            this.viz_planar.Name = "viz_planar";
            this.viz_planar.Size = new System.Drawing.Size(105, 17);
            this.viz_planar.TabIndex = 2;
            this.viz_planar.Text = "Vizualize Planner";
            this.viz_planar.UseVisualStyleBackColor = true;
            this.viz_planar.CheckedChanged += new System.EventHandler(this.checkboxesCheckedChanged);
            // 
            // viz_orig_img
            // 
            this.viz_orig_img.AutoSize = true;
            this.viz_orig_img.Location = new System.Drawing.Point(6, 68);
            this.viz_orig_img.Name = "viz_orig_img";
            this.viz_orig_img.Size = new System.Drawing.Size(138, 17);
            this.viz_orig_img.TabIndex = 1;
            this.viz_orig_img.Text = "Vizualize Original Image";
            this.viz_orig_img.UseVisualStyleBackColor = true;
            this.viz_orig_img.CheckedChanged += new System.EventHandler(this.checkboxesCheckedChanged);
            // 
            // viz_orig
            // 
            this.viz_orig.AutoSize = true;
            this.viz_orig.Location = new System.Drawing.Point(6, 45);
            this.viz_orig.Name = "viz_orig";
            this.viz_orig.Size = new System.Drawing.Size(194, 17);
            this.viz_orig.TabIndex = 0;
            this.viz_orig.Text = "Vizualize Original Depth Point Cloud";
            this.viz_orig.UseVisualStyleBackColor = true;
            this.viz_orig.CheckedChanged += new System.EventHandler(this.checkboxesCheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.planesFilterMinInliers);
            this.groupBox2.Controls.Add(this.planesFilterMaxCurvature);
            this.groupBox2.Controls.Add(this.planesFilterAngiularThreshold);
            this.groupBox2.Controls.Add(this.planesFilterDistanceThreshold);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.segmenterCurvatureThreshold);
            this.groupBox2.Controls.Add(this.segmentSizeThreshold);
            this.groupBox2.Controls.Add(this.segmentYThreshold);
            this.groupBox2.Controls.Add(this.segmenterAngularThreshold);
            this.groupBox2.Controls.Add(this.segmenterDistanceThreshold);
            this.groupBox2.Controls.Add(this.segment_nan_normals_are_edges);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.NormalSmoothingSize);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.MaxDepthChangeFactor);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(269, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(330, 609);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pipeline Options";
            // 
            // segmentSizeThreshold
            // 
            this.segmentSizeThreshold.Location = new System.Drawing.Point(148, 237);
            this.segmentSizeThreshold.Name = "segmentSizeThreshold";
            this.segmentSizeThreshold.Size = new System.Drawing.Size(154, 20);
            this.segmentSizeThreshold.TabIndex = 20;
            this.segmentSizeThreshold.Text = "50";
            // 
            // segmentYThreshold
            // 
            this.segmentYThreshold.Location = new System.Drawing.Point(148, 186);
            this.segmentYThreshold.Name = "segmentYThreshold";
            this.segmentYThreshold.Size = new System.Drawing.Size(154, 20);
            this.segmentYThreshold.TabIndex = 19;
            this.segmentYThreshold.Text = "0.6";
            // 
            // segmenterAngularThreshold
            // 
            this.segmenterAngularThreshold.Location = new System.Drawing.Point(148, 160);
            this.segmenterAngularThreshold.Name = "segmenterAngularThreshold";
            this.segmenterAngularThreshold.Size = new System.Drawing.Size(154, 20);
            this.segmenterAngularThreshold.TabIndex = 18;
            this.segmenterAngularThreshold.Text = "3.96";
            // 
            // segmenterDistanceThreshold
            // 
            this.segmenterDistanceThreshold.Location = new System.Drawing.Point(148, 134);
            this.segmenterDistanceThreshold.Name = "segmenterDistanceThreshold";
            this.segmenterDistanceThreshold.Size = new System.Drawing.Size(154, 20);
            this.segmenterDistanceThreshold.TabIndex = 17;
            this.segmenterDistanceThreshold.Text = "0.08";
            // 
            // segment_nan_normals_are_edges
            // 
            this.segment_nan_normals_are_edges.AutoSize = true;
            this.segment_nan_normals_are_edges.Location = new System.Drawing.Point(148, 263);
            this.segment_nan_normals_are_edges.Name = "segment_nan_normals_are_edges";
            this.segment_nan_normals_are_edges.Size = new System.Drawing.Size(137, 17);
            this.segment_nan_normals_are_edges.TabIndex = 8;
            this.segment_nan_normals_are_edges.Text = "Nan Normals are edges";
            this.segment_nan_normals_are_edges.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Size Threshold";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 193);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Vertical Threshold";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Angular Threshold";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Distance Threshold";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Segmentation options";
            // 
            // NormalSmoothingSize
            // 
            this.NormalSmoothingSize.Location = new System.Drawing.Point(148, 72);
            this.NormalSmoothingSize.Name = "NormalSmoothingSize";
            this.NormalSmoothingSize.Size = new System.Drawing.Size(154, 20);
            this.NormalSmoothingSize.TabIndex = 11;
            this.NormalSmoothingSize.Text = "30";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Normal Smoothing Size";
            // 
            // MaxDepthChangeFactor
            // 
            this.MaxDepthChangeFactor.Location = new System.Drawing.Point(148, 38);
            this.MaxDepthChangeFactor.Name = "MaxDepthChangeFactor";
            this.MaxDepthChangeFactor.Size = new System.Drawing.Size(154, 20);
            this.MaxDepthChangeFactor.TabIndex = 9;
            this.MaxDepthChangeFactor.Text = "0.5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Max Depth Change Factor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Normals calculation options";
            // 
            // segmenterCurvatureThreshold
            // 
            this.segmenterCurvatureThreshold.Location = new System.Drawing.Point(148, 212);
            this.segmenterCurvatureThreshold.Name = "segmenterCurvatureThreshold";
            this.segmenterCurvatureThreshold.Size = new System.Drawing.Size(154, 20);
            this.segmenterCurvatureThreshold.TabIndex = 21;
            this.segmenterCurvatureThreshold.Text = "0.04";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 219);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Curvature Threshold";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 295);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Planes filter options";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(87, 401);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Min Inliers";
            // 
            // planesFilterMinInliers
            // 
            this.planesFilterMinInliers.Location = new System.Drawing.Point(148, 394);
            this.planesFilterMinInliers.Name = "planesFilterMinInliers";
            this.planesFilterMinInliers.Size = new System.Drawing.Size(154, 20);
            this.planesFilterMinInliers.TabIndex = 30;
            this.planesFilterMinInliers.Text = "1000";
            // 
            // planesFilterMaxCurvature
            // 
            this.planesFilterMaxCurvature.Location = new System.Drawing.Point(148, 368);
            this.planesFilterMaxCurvature.Name = "planesFilterMaxCurvature";
            this.planesFilterMaxCurvature.Size = new System.Drawing.Size(154, 20);
            this.planesFilterMaxCurvature.TabIndex = 29;
            this.planesFilterMaxCurvature.Text = "0.2";
            // 
            // planesFilterAngiularThreshold
            // 
            this.planesFilterAngiularThreshold.Location = new System.Drawing.Point(148, 342);
            this.planesFilterAngiularThreshold.Name = "planesFilterAngiularThreshold";
            this.planesFilterAngiularThreshold.Size = new System.Drawing.Size(154, 20);
            this.planesFilterAngiularThreshold.TabIndex = 28;
            this.planesFilterAngiularThreshold.Text = "3.96";
            // 
            // planesFilterDistanceThreshold
            // 
            this.planesFilterDistanceThreshold.Location = new System.Drawing.Point(148, 316);
            this.planesFilterDistanceThreshold.Name = "planesFilterDistanceThreshold";
            this.planesFilterDistanceThreshold.Size = new System.Drawing.Size(154, 20);
            this.planesFilterDistanceThreshold.TabIndex = 27;
            this.planesFilterDistanceThreshold.Text = "0.02";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 375);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Maximum Curvature";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(48, 349);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Angular Threshold";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(44, 323);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "Distance Threshold";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 633);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnToogleEngineStatus);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnToogleEngineStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox viz_tracker;
        private System.Windows.Forms.CheckBox viz_seg;
        private System.Windows.Forms.CheckBox viz_planar;
        private System.Windows.Forms.CheckBox viz_orig_img;
        private System.Windows.Forms.CheckBox viz_orig;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox dispalyNormals;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NormalSmoothingSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox MaxDepthChangeFactor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox segmentSizeThreshold;
        private System.Windows.Forms.TextBox segmentYThreshold;
        private System.Windows.Forms.TextBox segmenterAngularThreshold;
        private System.Windows.Forms.TextBox segmenterDistanceThreshold;
        private System.Windows.Forms.CheckBox segment_nan_normals_are_edges;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox segmenterCurvatureThreshold;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox planesFilterMinInliers;
        private System.Windows.Forms.TextBox planesFilterMaxCurvature;
        private System.Windows.Forms.TextBox planesFilterAngiularThreshold;
        private System.Windows.Forms.TextBox planesFilterDistanceThreshold;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}