#include<ISoundGenerator.h>
#include <al.h>
#include<soundgen/SoundFeature.h>
//#include<soundgen/SoundInterpolator.h>
// #include <soundgen/SoundSourceGenerator.h>
#include <soundgen/SoundSourceFullWaveformGenerator.h>
#include <soundgen/SoundSourceWaveformInterpolator.h>
#include <soundgen/SoundSourceOutput.h>
// #include <vector>

#ifndef SoundGeneratorClass
#define SoundGeneratorClass
class SoundGenerator: public ISoundGenerator{
public:	
  SoundGenerator();
  double randd();
  virtual void Execute(std::vector<PCLObject::Ptr>);
  virtual void setExaggeration(double multiplier);
  
private:
  
// WHY YOU NO WORK???  std::vector<PCLObject::Ptr> last_objects;
  
  double exaggerate_pos;
//   SoundInterpolator si;
    std::vector<std::shared_ptr<SoundSourceFullWaveformGenerator> > ssg;
    std::vector<std::vector<short> > full_buffer;
    std::vector<std::shared_ptr<SoundSourceWaveformInterpolator> > ssi;
    std::vector<CircularBuffer<short> > consuming_buffer;
    std::vector<std::shared_ptr<SoundSourceOutput> > sso;
    
  //Sound Generator Parameters
//   int garbage[24]; //because valgrind --leak-check=yes ./segmentertester2 !!!???
  std::vector<Eigen::Vector4f> last_pos;
  
  std::vector<SoundFeature> last_played;
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  
};
#endif