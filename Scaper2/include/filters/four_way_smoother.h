#ifndef FOUR_WAY_SMOOOTHER_H_
#define FOUR_WAY_SMOOOTHER_H_

#include <limits>
// #include <pcl/filters/filter.h>
// #include <pcl/search/pcl_search.h>
#include <pcl/common/centroid.h>
#include <pcl/segmentation/boost.h>


/** \brief 4 pass smoother
  * \note Works on organised clouds. Smooths, reduces NAN (does not eliminate).
  * 		Could be made faster wrt cache misses by changing the data structure used from 5 arrays of points to 1 array of 5-point structs.
  * \author Damien Jade Duff
  */
template<typename PointT>
class FourWaySmoother //: public Filter<PointT>
{
    const int verb=4;
//     using Filter<PointT>::input_;
//     using Filter<PointT>::indices_;
//     typedef typename Filter<PointT>::PointCloud PointCloud;
//     typedef typename pcl::search::Search<PointT>::Ptr KdTreePtr;

    typedef typename pcl::PointCloud<PointT> PointCloud;
    typedef typename pcl::PointCloud<PointT>::Ptr PointCloudPtr;
    typedef typename pcl::PointCloud<PointT>::ConstPtr PointCloudConstPtr;

public:

    typedef boost::shared_ptr< FourWaySmoother<PointT> > Ptr;
    typedef boost::shared_ptr< const FourWaySmoother<PointT> > ConstPtr;


    /** \brief Constructor.
      */
    FourWaySmoother (double max_depth_change_proportion=0.2) : max_depth_change_proportion_ (max_depth_change_proportion)
    {
    }

    /** \brief Filter the input data and store the results into output
      * \param[out] output the resultant point cloud message
      */
    void
    applyFilter (PointCloud &output)
    {

        if(verb>4)std::cout<<"setting output size"<<std::endl;
        size_set(output);
        size_set(added_);
        for(size_t i=0; i<4; i++) {
            if(verb>4)std::cout<<"setting cloud "<<i<<" size"<<std::endl;
            size_set(tclouds_[i]);
// 	uint col_start;
// 	uint col_little_step;
// 	uint col_big_step;
// 	uint row_start;
// 	uint row_little_step;
// 	uint row_big_step;
// 	if(i==0)//up
// 	{
// 	  col_start=0;
// 	  col_little_step=1;
// 	  col_big_step=0;
// 	  row_start=output.height-1;
// 	  row_little_step=
// 	}
            if(i==0) {
                if(verb>4)std::cout<<"filling up"<<std::endl;
                for(size_t row=output.height-1; row>0; row--) {
                    if(verb>5)std::cout<<"row "<<row<<std::endl;
                    if((double)rand()/RAND_MAX>0.5) {
                        for(size_t col=0; col<output.width-1; col++) {
                            if(verb>6)std::cout<<"col+ "<<col<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col-1);
                            prop(tclouds_[i],row,col,row-1,col);
                            prop(tclouds_[i],row,col,row-1,col+1);
                        }
                    } else {
                        for(size_t col=output.width-1; col>0; col--) {
                            if(verb>6)std::cout<<"col- "<<col<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col-1);
                            prop(tclouds_[i],row,col,row-1,col);
                            prop(tclouds_[i],row,col,row-1,col+1);
                        }
                    }
                }
            }

            if(i==1) {
                if(verb>4)std::cout<<"filling down"<<std::endl;
                for(size_t row=0; row<output.height-1; row++) {
                    if(verb>5)std::cout<<"row "<<row<<std::endl;
                    if((double)rand()/RAND_MAX>0.5) {
                        for(size_t col=0; col<output.width-1; col++) {
                            if(verb>6)std::cout<<"col+ "<<col<<std::endl;
                            prop(tclouds_[i],row,col,row+1,col-1);
                            prop(tclouds_[i],row,col,row+1,col);
                            prop(tclouds_[i],row,col,row+1,col+1);
                        }
                    } else {
                        for(size_t col=output.width-1; col>0; col--) {
                            if(verb>6)std::cout<<"col- "<<col<<std::endl;
                            prop(tclouds_[i],row,col,row+1,col-1);
                            prop(tclouds_[i],row,col,row+1,col);
                            prop(tclouds_[i],row,col,row+1,col+1);
                        }
                    }
                }
            }


            if(i==2) {
                if(verb>4)std::cout<<"filling left"<<std::endl;
                for(size_t col=output.width-1; col>0; col--) {
                    if(verb>5)std::cout<<"col "<<col<<std::endl;
                    if((double)rand()/RAND_MAX>0.5) {
                        for(size_t row=0; row<output.height-1; row++) {
                            if(verb>6)std::cout<<"row+ "<<row<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col-1);
                            prop(tclouds_[i],row,col,row,col-1);
                            prop(tclouds_[i],row,col,row+1,col-1);
                        }
                    } else {
                        for(size_t row=output.height-1; row>0; row--) {
                            if(verb>6)std::cout<<"row- "<<row<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col-1);
                            prop(tclouds_[i],row,col,row,col-1);
                            prop(tclouds_[i],row,col,row+1,col-1);
                        }
                    }
                }
            }


            if(i==3) {
                if(verb>4)std::cout<<"filling right"<<std::endl;
                for(size_t col=0; col<output.width; col++) {
                    if(verb>5)std::cout<<"col "<<col<<std::endl;
                    if((double)rand()/RAND_MAX>0.5) {
                        for(size_t row=0; row<output.height-1; row++) {
                            if(verb>6)std::cout<<"row+ "<<row<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col+1);
                            prop(tclouds_[i],row,col,row,col+1);
                            prop(tclouds_[i],row,col,row+1,col+1);
                        }
                    } else {
                        for(size_t row=output.height-1; row>0; row--) {
                            if(verb>6)std::cout<<"row- "<<row<<std::endl;
                            prop(tclouds_[i],row,col,row-1,col+1);
                            prop(tclouds_[i],row,col,row,col+1);
                            prop(tclouds_[i],row,col,row+1,col+1);
                        }
                    }
                }
            }

        }
        if(verb>4)std::cout<<"averaging"<<std::endl;
        uint num_changes=0;
// 	uint num_too_far=0;
        uint num_rejections=0;
        uint num_nan=0;
        for(size_t row=0; row<output.height; row++) {
            if(verb>5)std::cout<<"row "<<row<<std::endl;
            for(size_t col=0; col<output.width; col++) {
                if(pcl::isFinite<PointT>((*input_)(col,row))) {
                    output(col,row)=(*input_)(col,row);
                } else {
                    double maxdist=0;
// 		    double avgx=0;
// 		    double avgy=0;
// 		    double avgz=0;
// 		    double numz=0;
                    pcl::CentroidPoint<PointT> cp;
                    for(size_t i=0; i<4; i++) {
                        if(std::isfinite(tclouds_[i](col,row).z)) {
// 			numz++;
// 			avgx+=std::fabs(tclouds_[i](col,row).x);
// 			avgy+=std::fabs(tclouds_[i](col,row).y);
// 			avgz+=std::fabs(tclouds_[i](col,row).z);
                            cp.add(tclouds_[i](col,row));
                            for(size_t j=i+1; j<4; j++) {
                                if(std::isfinite(tclouds_[j](col,row).z)) {
                                    double dist = std::fabs(tclouds_[i](col,row).z - tclouds_[j](col,row).z);
                                    if (dist>maxdist)maxdist=dist;
                                }
                            }
                        }
                    }
//                     avgz/=numz;
                    PointT centroid;
                    cp.get(centroid);
                    if(cp.getSize()>1) { // could be 0 but let's be a bit careful
		        double proportion=1-std::min(maxdist/centroid.z,centroid.z/maxdist);
                        if(proportion<max_depth_change_proportion_) {
                            output(col,row)=centroid;
                            added_(col,row)=centroid;
                            num_changes++;
                        } else {
// 			std::cout<<maxdist<<"/"<<centroid.z<<"<"<<max_depth_change_proportion_<<std::endl;
                            output(col,row)=(*input_)(col,row);
                            num_nan++;
                            num_rejections++;
                        }
                    } else {
                        num_nan++;
                    }
                }

            }
        }
        if(verb>3)std::cout<<"Num changes: "<<num_changes<<std::endl;
        if(verb>3)std::cout<<"Num NaNs not fixed: "<<num_nan<<std::endl;
        if(verb>3)std::cout<<"Num attempts but depth change rejections: "<<num_rejections<<std::endl;
        if(verb>4)std::cout<<"done"<<std::endl;
        if(num_nan>0)output.is_dense=false;
        else output.is_dense=true;
    }



    void setInputCloud(PointCloudConstPtr input)
    {
        input_=input;
    }

    std::vector<PointCloudPtr> getTClouds() {
        std::vector<PointCloudPtr> vec;
        for(int i=0; i<4; i++)
            vec.push_back(boost::make_shared<PointCloud>(tclouds_[i]));
        return vec;
    }

    PointCloudPtr getAddedCloud() {
        return boost::make_shared<PointCloud>(added_);
    }

private:

    void prop(PointCloud &cloud,size_t srow,size_t scol,size_t trow,size_t tcol) {
//       static int attempted=0;
//       static int didfromoriginal=0;
//       static int didfromprop=0;
//       attempted++;
        if(trow>=0 && trow < cloud.height && tcol>=0 && tcol<cloud.width) {
            if(pcl::isFinite<PointT>((*input_)(scol,srow))) {
                if(!pcl::isFinite<PointT>((*input_)(tcol,trow))) {
		  if(!pcl::isFinite<PointT>(cloud(tcol,trow))) {
                    cloud(tcol,trow)=(*input_)(scol,srow);
		  }else{
		    
		    //this is aimed at making it smoother but only iff under limits
		    //works because during a propagation in one direction, each point is targeted max 2 times.
		    double proportion=1-std::min((*input_)(scol,srow).z/cloud(tcol,trow).z,cloud(tcol,trow).z/(*input_)(scol,srow).z);
		    if(proportion < max_depth_change_proportion_){
		     pcl::CentroidPoint<PointT> cp;
		     cp.add(cloud(tcol,trow));
		     cp.add((*input_)(scol,srow));
		     PointT centroid;
		     cp.get(centroid);
		     cloud(tcol,trow)=centroid;
		    }else{
// 		      std::cerr<<"whoops"<<proportion<<" "<<cloud(scol,srow).z<<" "<<cloud(tcol,trow).z<<std::endl;
		      cloud(tcol,trow).x=std::numeric_limits<double>::quiet_NaN();
		      cloud(tcol,trow).y=std::numeric_limits<double>::quiet_NaN();
		      cloud(tcol,trow).z=std::numeric_limits<double>::quiet_NaN();
		    }
		  }
		  
                }
            }
            else if(pcl::isFinite<PointT>(cloud(scol,srow))) {
                if(!pcl::isFinite<PointT>((*input_)(tcol,trow))) {
		  if(!pcl::isFinite<PointT>(cloud(tcol,trow))) {
                    cloud(tcol,trow)=cloud(scol,srow);
		  }else{

//this is aimed at making it smoother but only iff under limits
		    //works because during a propagation in one direction, each point is targeted max 2 times.
		    double proportion=1-std::min(cloud(scol,srow).z/cloud(tcol,trow).z,cloud(tcol,trow).z/cloud(scol,srow).z);
		    if(proportion < max_depth_change_proportion_){
		     pcl::CentroidPoint<PointT> cp;
		     cp.add(cloud(tcol,trow));
		     cp.add(cloud(scol,srow));
		     PointT centroid;
		     cp.get(centroid);
		     cloud(tcol,trow)=centroid;
		    }else{
// 		      std::cerr<<"whoops"<<proportion<<" "<<cloud(scol,srow).z<<" "<<cloud(tcol,trow).z<<std::endl;
		      cloud(tcol,trow).x=std::numeric_limits<double>::quiet_NaN();
		      cloud(tcol,trow).y=std::numeric_limits<double>::quiet_NaN();
		      cloud(tcol,trow).z=std::numeric_limits<double>::quiet_NaN();
		    }
		    
		  }
                }
            }

        }
    }

    void size_set(PointCloud &toset)
    {
        toset.resize(input_->width*input_->height);
        toset.width=input_->width;
        toset.height=input_->height;
        for(int i=0; i<toset.width*toset.height; i++) {
            toset.points[i].x=std::numeric_limits<double>::quiet_NaN();
            toset.points[i].y=std::numeric_limits<double>::quiet_NaN();
            toset.points[i].z=std::numeric_limits<double>::quiet_NaN();
        }
    }

    /** \brief How much depth has to change between sweeps before we declare them incompatible. */
    double max_depth_change_proportion_;

    PointCloudConstPtr input_;
    /*
    PointCloud cloud_up_;
        PointCloud cloud_down_;
        PointCloud cloud_left_;
        PointCloud cloud_right_;*/

    PointCloud tclouds_[4];
    PointCloud added_;
};


#endif //FOUR_WAY_SMOOOTHER_H_