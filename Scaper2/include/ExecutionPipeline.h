#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <IPCLFilter.h>
#include <PCLObject.h>
#include <PointCloudWithNormals.h>
#include <IPCLSegmenter.h>
#include <ISegmentTracker.h>
#include <ISegmentSelector.h>
#include <ISegmentSonify.h>
#include <IPCLNormalCalculator.h>
#include <common/Timer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>

#ifndef ExecutionPipelineClass
#define ExecutionPipelineClass

class ExecutionPipeline{
 private:
   //Pipline Algorithm Objects
   std::vector<IPCLFilter*> pclFilters;
   IPCLNormalCalculator* pclNormalCalculator;
   IPCLSegmenter* pclSegmenter;
   ISegmentSelector* segmentSelector;
   ISegmentTracker* segmentTracker;
   ISegmentSonify* segmentSonification;

   //Pipeline Data
   bool timing;

   Timer filter_timer;
   Timer seg_timer;
   Timer tracker_timer;
   Timer selector_timer;
   Timer extract_timer;
   Timer sound_timer;
   Timer pipeline_timer;
   Timer callback_timer;

   boost::shared_ptr<pcl::visualization::PCLVisualizer> orig_viewer;
   boost::shared_ptr<pcl::visualization::ImageViewer> orig_viewer_img;

   std::vector<float> orig_viewer_img_data;
   bool displayNormals;
   boost::mutex* viewer_mutex;
   float img_viewer_min,img_viewer_max;
   int img_viewer_num_viewed;

   void visualise(PointCloudWithNormals::ConstPtr cloudWithNormals);
   void visualise_img(PointCloudWithNormals::ConstPtr cloudWithNormals);

   static int verbosity;

 public:

    void ExecutePipeline(const pcl::PointCloud<SystemPoint>::ConstPtr&);
    ExecutionPipeline();
    ~ExecutionPipeline();

    void setTiming(bool val);

    void AddOrigViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex &viewer_mutex);
    void AddOrigImageViewer(boost::shared_ptr<pcl::visualization::ImageViewer> viewer_orig_img, boost::mutex &viewer_mutex);
    
    void AddPCLFilter(IPCLFilter*);
    void ClearPCLFilters();
    void SetPCLNormalCalculator(IPCLNormalCalculator*);
 
    void SetPCLSegmenter(IPCLSegmenter*);
    void SetSegmentSelector(ISegmentSelector*);
    void SetSegmentTracker(ISegmentTracker*);

    void SetSoundSonificationApproach(ISegmentSonify*);

};
#endif
