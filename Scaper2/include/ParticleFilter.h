#include <Consts.h>
#include <IState.h>
#include<IPCLObjectsTracker.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>
#include <IDistrubution.h>
#include <PCLObject.h>

#ifndef ParticleFilterClass
#define ParticleFilterClass


class ParticleFilter:public IPCLObjectsTracker{
private:
  std::vector<IState*> States;
  
  void _resample(int);
  void _filterStates(int);
  std::vector<double> _normalize(std::vector<IState*>);// To make sure that our weights are converted to a probability measure
  void _effectiveParticleCount(double[]);
public:
  ParticleFilter();
  virtual std::vector<PCLObject::Ptr> Execute(pcl::PointCloud<SystemPoint>::CloudVectorType);
  void Initialize(int,std::vector<IDistribution*>);
  void Predict(float effectiveCountMinRatio);
  void Update(IState* measure);
};


#endif