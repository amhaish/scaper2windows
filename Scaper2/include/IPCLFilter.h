#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <PointCloudWithNormals.h>

#ifndef IPCLFilterClass
#define IPCLFilterClass

class IPCLFilter{
public:	
  virtual void Execute(PointCloudWithNormals::Ptr&)=0;
private:
};
#endif