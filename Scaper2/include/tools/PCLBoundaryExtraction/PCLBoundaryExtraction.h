
#include <pcl/point_types.h>
#include <Consts.h>
#include <pcl/pcl_base.h>
#include <pcl/PointIndices.h>
#include <pcl/segmentation/comparator.h>



#ifndef PCLBoundaryExtractionClass
#define PCLBoundaryExtractionClass


class PCLBoundaryExtraction {
  
public:
//       PCLBoundaryExtraction();
      pcl::PointIndices BoundaryExtraction (const pcl::PointCloud<SystemPoint>::Ptr &);      

};

#endif
