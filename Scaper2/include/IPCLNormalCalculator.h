#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include<PointCloudWithNormals.h>

#ifndef IPCLNormalCalculatorClass
#define IPCLNormalCalculatorClass

class IPCLNormalCalculator{
public:
  virtual PointCloudWithNormals::Ptr Execute(pcl::PointCloud<SystemPoint>::Ptr&)=0;
private:
};
#endif