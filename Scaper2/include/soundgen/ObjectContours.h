#include<PCLObject.h>
#include <Consts.h>
#include <pcl/point_types.h>
#include <tools/PCLBoundaryExtraction/PCLBoundaryExtraction.h>
#include <pcl/common/geometry.h>
#include <Eigen/Geometry>
#include <fstream>


#ifndef ObjectContoursFile
#define ObjectContoursFile

namespace contours {

class Contour {

public:
    Contour();
    Contour(PCLObject::Ptr);
    std::vector<float> NormalizedRotaryContour ( );
    std::vector<float> NormalizedRotaryContourX();
    std::vector<float> UnnormalizedRotaryContour();

    std::vector <float> InternalContourHorizontalBar();
    std::vector <float> InternalContourVerticalBar();

    std::vector<float> HorizontalContour();
    std::vector<float> VerticalContour();
    std::vector<float> NormalizeValues(std::vector<float>);
    std::vector<float> NormalizeValues1(std::vector<float>,bool verbosep=false);
    std::vector<float> CentreAtZero(std::vector< float > vals);
    std::vector<float> SquishTo1(std::vector< float > vals);
    std::vector<float> Negate(std::vector< float > vals);

    std::vector<float> normalized_rcontour;
    std::vector<float> normalized_rcontourX;
    std::vector<float> unnormalized_rcontour;
    std::vector<float> normalized_hcontour;
    std::vector<float> normalized_vcontour;
    std::vector<float> normalized_hcontour_neg;
    std::vector<float> normalized_vcontour_neg;

    std::vector<float> internal_contour_horizontal_bar;
    std::vector<float> internal_contour_vertical_bar;



private:


    std::vector<float> normalizedContour;
    std::vector<float> unnormalizedRotaryDistances;
    std::vector<float> normalizedRotaryDistances;
    std::vector<float> normalizedHorizontalDistances;
    std::vector<float> normalizedHorizontalDistances2;
    std::vector<float> normalizedVerticalDistances;
    std::vector<float> normalizedVerticalDistances2;
    std::vector<float> normalizedInternalDepth;
    std::vector<float> depthWave;
    std::vector<float> depth;
    std::vector<float> depthWaveProcessing;

    SystemPoint centroid;
    float distanceToTheCentroid;
    float averageDistance;
    float sumOfDistances ;
    pcl::PointIndices boundary_indices;
    std::vector<float> rotaryContour;
    PCLBoundaryExtraction extractor;

    std::vector<float> horizontal_distance;
    int FindFinitePointInCloudRightToLeft(int);
    int FindFinitePointInCloudLeftToRight(int);

    std::vector<float> vertical_distance;
    int FindFinitePointInCloudToptoBottom(int);
    int FindFinitePointInCloudBottomToTop(int);

    pcl::PointCloud<SystemPoint>::Ptr cloud;


    std::ofstream debugfile1,debugfile2;

};

};

#endif //ObjectContoursFile