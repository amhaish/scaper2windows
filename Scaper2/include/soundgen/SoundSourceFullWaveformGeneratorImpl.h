#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>
#include <common/RateTracker.h>
#include <vector>
#include <chrono>

#ifndef __SoundSourceFullWaveformGeneratorImpl2
#define __SoundSourceFullWaveformGeneratorImpl2

class SoundSourceFullWaveformGeneratorImpl {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundSourceFullWaveformGeneratorImpl (); // unfortunately, high hz does not lead to good results
    virtual ~SoundSourceFullWaveformGeneratorImpl ();

    void generate_sound(SoundFeature sfeat,std::vector<short> &output);

    /* Call this every time you don't call play_feature */
//     void play_nothing();
    SoundFeature get_last_played_sound_feature();
private:

  
/* For passing to the fill_sample routine*/
struct wave_properties {
    double starttime;
    double endtime;
    double startfreq;
    double endfreq;
    double start_attenuation;
//     double attack_duration;
//     double release_onset;
    double end_attenuation;
    uint num_samples;
};

  
  double amplitude_sine(double freq,double t);
  void fill_sample_add(std::vector<short> &output,wave_properties &wp,size_t start_ind=0);
    SoundFeature last_played;

    RateTracker rate_tracker;

    static int verbosity;
  
};

#endif //__SoundSourceFullWaveformGeneratorImpl2
