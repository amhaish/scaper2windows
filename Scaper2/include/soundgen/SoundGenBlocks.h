#include <boost/shared_ptr.hpp>
#include "Consts.h"

#include <math.h>

#ifndef SoundGenBlocksFile
#define SoundGenBlocksFile

namespace SoundBlocks{

  class Tickable{
  public:
    virtual float tick()=0;
  };
  /*
  class Loopable{
  public:
    virtual int loopsize()=0;
  };*/

  class Sine:public Tickable{
  public:
    Sine(float freq);
    virtual float tick();
  private:
    float freq_;
    float t_;
  };

  class Noise:public Tickable{
  public:
    Noise();
    virtual float tick();
  };


  class InterpolatorGenerator: public Tickable
 {
  public:
    InterpolatorGenerator(std::vector<float>& ,float );
    virtual float tick();
    int getSize();
  private:
    std::vector<float> contour;
    std::vector<float> interpolated_contour;
    int scale;
    float tick_value;
    int curr_index;
    int prev_size;
    int interpolated_size;
    float interval_size;
};

  //////////////////////////////////////
  class AmplitudeModulator:public Tickable {
  public:
   AmplitudeModulator(Tickable *carrier,Tickable *modulator, float modulator_index);
//     AmplitudeModulator(Tickable *carrier,Tickable *modulator);
    virtual float tick();
  private:
    Tickable *carrier_;
    Tickable *modulator_;
    float modulator_effect_;
    float carrier_value;
    float modulator_value;
    float tick_value;


    static int verbosity__;
  };

  //////////////////////////////////////
  class FrequencyModulator:public Tickable{
  public:
    FrequencyModulator(Tickable *carrier,Tickable *modulator,float peak_frequency_deviation);
    virtual float tick();
  private:
    Tickable *carrier_;
    Tickable *modulator_;
    float peak_frequency_deviation_;
    float curr_carrier_ind_;
    int prev_carrier_ind_;
    float prev_carrier_sample_;
    float next_carrier_sample_;
    static int verbosity__;
  };

  class Add:public Tickable{
  public:
    Add(std::vector<Tickable*> addends,std::vector<float> mults);
    virtual float tick();
  private:
    std::vector<Tickable*> addends_;
    std::vector<float> mults_;
  };

}

#endif //SoundGenBlocksFile
