#include<Consts.h>
#include <boost/shared_ptr.hpp>
// #include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>

#ifndef __SoundSourceInterpolator
#define __SoundSourceInterpolator

class SoundSourceWaveformInterpolatorImpl;


class SoundSourceWaveformInterpolator{
public:

  enum InterpolationApproach {RestartFadeStart,RestartFadeStartEnd,ChangeWaveAndSizeLayered,GranulateNewIn};
//   enum InterpolationApproach {RestartFade,RestartSharp,ChangeWave,ChangeWaveAndSize};
  
  SoundSourceWaveformInterpolator(); // unfortunately, high hz does not lead to good results
  virtual ~SoundSourceWaveformInterpolator();
  
  void interpolate_sound(const std::vector<short> &full_sound,CircularBuffer<short> &csb, InterpolationApproach approach=RestartFadeStart); 

private:
  
  boost::shared_ptr<SoundSourceWaveformInterpolatorImpl> impl;

};

#endif //__SoundSourceInterpolator
