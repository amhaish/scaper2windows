#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <vector>
#include <deque>
#include <chrono>

#ifndef SoundInterpolatorImplClass
#define SoundInterpolatorImplClass

typedef unsigned int ALuint;

class SoundInterpolatorImpl {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundInterpolatorImpl(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
    virtual ~SoundInterpolatorImpl();

    bool play_feature(SoundFeature sfeat);

    void play_nothing();
private:

  void add_feature_to_fill_buffer(SoundFeature &sfeat);
  bool fill_buffers_to_al_buffers();
  void al_play();
  
    /* Unqueue buffers if they need unqueuing */
    uint unqueue_al_buffers();
    bool load_to_al_buffer(ALuint buffer_no,short *samples,size_t fill_buffer_size,size_t start_ind,size_t num_to_load);
    void calc_timing();
    
    /* The approximate frequency at which this will be played - should be set lower than actual hz */
    double approx_hz;
//     double grain_length;
    double rise_time;

    /* Number of samples in each sound - calculated from hz */
//     size_t grain_num_samples;

    double avg_frame_rate;
    double avg_frame_rate_err_secs;
    
    double curr_frame_rate;
    
    /* Not used so much in current approach */
//   double last_time;

    /* The last feature that was played, kept for interpolation */
    //SoundFeature last_sfeat;

    /* The array to contain the waveform as we build it*/
    std::vector<short> fill_buffer;
    size_t fill_buffer_start;

//     struct
    
    std::deque<ALuint> playing_al_buffer_ids;
    std::deque<std::chrono::high_resolution_clock::time_point> playing_al_buffer_queued_time;
    std::deque<uint> playing_al_buffer_size;
    
    std::deque<ALuint> available_al_buffer_ids;

    std::chrono::high_resolution_clock::time_point last_time_play_request_received;
    
    uint all_playing_al_buffer_size;
    
    //std::vector<short> samples_building;

    /* Keeping track of OpenAL pointers and what buffers are playing */
//   ALuint buffer_id[2];
    //size_t last_played_buffer_ind;
    ALuint source_id;
    size_t num_queued_or_playing_al;

    bool played_last;

    uint frames_processed;
    ALfloat last_offset;
    
};

#endif //SoundInterpolatorImplClass
