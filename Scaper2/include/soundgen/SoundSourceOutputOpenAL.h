#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <deque>
#include <chrono>
#include <common/CircularBuffer.h>
#include <common/RateTracker.h>
#include <Eigen/StdVector>

#ifndef SoundSourceOutputImplClass
#define SoundSourceOutputImplClass

typedef unsigned int ALuint;

class SoundSourceOutputImpl {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundSourceOutputImpl (const double approx_hz=15.0); // unfortunately, high hz does not lead to good results
    virtual ~SoundSourceOutputImpl ();

    bool output(CircularBuffer<short> &csb,const Eigen::Vector4f pos);
    
    bool output(CircularBuffer<short> &csb);
    
//     bool play(CircularBuffer<short> &csb,const Eigen::Vector4f pos);
    
private:

    /* Unqueue buffers if they need unqueuing */
    uint unqueue_al_buffers();
  
    bool fill_buffers_to_al_buffers(CircularBuffer<short> &csb);
    bool load_to_al_buffer(ALuint buffer_no,CircularBuffer<short> &csb,size_t num_to_load);
    
    void al_play();
  
    
    
//     void calc_timing();
    
    /* The approximate frequency at which this will be played - should be set lower than actual hz */
//     double approx_hz;


    /* Number of samples in each sound - calculated from hz */
//     size_t grain_num_samples;

//     double avg_frame_rate;
//     double avg_frame_rate_err_secs;
    
//     double curr_frame_rate;
    
    RateTracker rate_tracker;
    
    /* Not used so much in current approach */
//   double last_time;

    /* The last feature that was played, kept for interpolation */
    //SoundFeature last_sfeat;

//     struct
    
    std::deque<ALuint> playing_al_buffer_ids;
    std::deque<std::chrono::high_resolution_clock::time_point> playing_al_buffer_queued_time;
    std::deque<uint> playing_al_buffer_size;
    
    std::deque<ALuint> available_al_buffer_ids;

//     std::chrono::high_resolution_clock::time_point last_time_play_request_received;
    
    uint all_playing_al_buffer_size;
    
    //std::vector<short> samples_building;

    /* Keeping track of OpenAL pointers and what buffers are playing */
//   ALuint buffer_id[2];
    //size_t last_played_buffer_ind;
    ALuint source_id;
    size_t num_queued_or_playing_al;

    bool played_last;

//     uint frames_processed;
    ALfloat last_offset;

    static bool si_alut_initialised;
    static int si_num_sinterpolators;
    static int verbosity;

    std::shared_ptr<std::ofstream> streaming_file;

};

#endif //SoundSourceOutputImplClass
