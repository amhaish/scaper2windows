#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>
#include <common/RateTracker.h>
#include <vector>
#include <chrono>

#include <soundgen/SoundSourceWaveformInterpolator.h>

#ifndef __SoundSourceWaveformInterpolatorImpl2
#define __SoundSourceWaveformInterpolatorImpl2

class SoundSourceWaveformInterpolatorImpl {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundSourceWaveformInterpolatorImpl (); // unfortunately, high hz does not lead to good results
    virtual ~SoundSourceWaveformInterpolatorImpl ();

    void interpolate_sound(const std::vector<short> &full_sound, CircularBuffer<short> &csb,SoundSourceWaveformInterpolator::InterpolationApproach approach); 

private:
    
//   void adjust_length(double new_length,std::vector<short> &full_sound);
//   void do_interpolate_without_size_change(std::vector<short> &full_sound);
  
//     bool played_last;
    
  static int verbosity;
  int prev_length;
//   int new_length;

  int num_frames_played_sound;
  RateTracker rate_tracker;

//   CircularBuffer<short> &csb;
//   SoundSourceOutput sso;
  
};

#endif //__SoundSourceWaveformInterpolatorImpl2

