#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>
#include <common/RateTracker.h>
#include <vector>
#include <chrono>

#ifndef SoundSourceGeneratorImplClass
#define SoundSourceGeneratorImplClass

class SoundSourceGeneratorImpl {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundSourceGeneratorImpl (double approx_hz=15.0); // unfortunately, high hz does not lead to good results
    virtual ~SoundSourceGeneratorImpl ();

    void generate_sound(SoundFeature sfeat,CircularBuffer<short> &csb);

    /* Call this every time you don't call play_feature */
//     void play_nothing();
    SoundFeature get_last_played_sound_feature();
private:

//   void add_feature_to_fill_buffer(SoundFeature &sfeat,CircularBuffer<short> &csb);
    void calc_timing();
    
    /* The approximate frequency at which this will be played - should be set lower than actual hz */
//     double approx_hz;

    /* Number of samples in each sound - calculated from hz */
//     size_t grain_num_samples;
//     double grain_length;
//     double rise_time;

    RateTracker rate_tracker;
    
//     double avg_frame_rate;
//     double avg_frame_rate_err_secs;
    
//     double curr_frame_rate;
    
    /* The last feature that was played, kept for interpolation */
    //SoundFeature last_sfeat;

    /* The array to contain the waveform as we build it*/

//     std::chrono::high_resolution_clock::time_point last_time_play_request_received;
    
    bool played_last;
    
    SoundFeature last_played;

    
    
};

#endif //SoundSourceGeneratorImplClass
