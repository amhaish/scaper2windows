#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>

#ifndef SoundInterpolatorClass
#define SoundInterpolatorClass

class SoundInterpolatorImpl;

class SoundInterpolator{
public:
  /* Construct sound interpolation object, and intitialise back-end if necessary */
  SoundInterpolator(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
  virtual ~SoundInterpolator();
  
  bool play_feature(SoundFeature &sfeat);
  /** Call this even if you have nothing to play to make sure sounds are buffered right **/
  void play_nothing(); 
  
private:
  
  boost::shared_ptr<SoundInterpolatorImpl> impl;
  
};

#endif //SoundInterpolatorClass
