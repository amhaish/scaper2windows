#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>

#ifndef SoundSourceFullWaveformGeneratorClass
#define SoundSourceFullWaveformGeneratorClass

class SoundSourceFullWaveformGeneratorImpl;

class SoundSourceFullWaveformGenerator{
public:
  
  SoundSourceFullWaveformGenerator(); 
//   SoundSourceFullWaveformGenerator(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
  virtual ~SoundSourceFullWaveformGenerator();
  
  void generate_sound(SoundFeature &sfeat,std::vector<short> &output); 
  
  /* Call this every time you don't call play_feature */
//   void play_nothing();
  SoundFeature get_last_played_sound_feature();
  
private:
  
  boost::shared_ptr<SoundSourceFullWaveformGeneratorImpl> impl;
  
};

#endif //SoundSourceFullWaveformGeneratorClass
