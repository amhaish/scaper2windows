#include<Consts.h>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include<string>

#ifndef SoundFeatureClass
#define SoundFeatureClass
class SoundFeature {

public:
    SoundFeature();
    Eigen::Vector4f pos;
    std::vector<double> frequencies;
    std::vector<double> multipliers;
    double spread_mult; //how long the sound persists (in seconds). Default 1.0.
    double energy_mult; //how much sound (approx) to provide. Default 1.0.
    std::string objectType;

    //only dealt with in newer code (SoundSourceFullWaveformGenerator)
    double attack_duration; //if negative, attack will be calculated automatically
    double hold_duration; //default to 0 (time after attack) before release
    double release_duration; //if negative, release will be calculated automatically

    //let's put lots more things in here, like attack, release, pulse, various timbre elements
    //should we subclass? so that soundinterpolator doesn't have to know every feature

    // alternative: use a different sound generation tookit.
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    
};
/* Print out to a stream the SoundFeature object. */
std::ostream& operator<<(std::ostream& stream,const SoundFeature& sfeat);
#endif