#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <vector>

#ifndef SoundInterpolatorImplClass
#define SoundInterpolatorImplClass

typedef unsigned int ALuint;  

class SoundInterpolatorImpl{
public:
  /* Construct sound interpolation object, and intitialise back-end if necessary */
  SoundInterpolatorImpl(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
  virtual ~SoundInterpolatorImpl();
  
  bool play_feature(SoundFeature &sfeat);
  void play_nothing();
  
private:
  
  /* Unqueue buffers if they need unqueuing */
  unsigned int unqueue_buffers(); 
  
  /* The approximate frequency at which this will be played - should be set lower than actual hz */
  double approx_hz;

  /* Number of samples in each sound - calculated from hz */
  size_t grain_num_samples;
  
  /* Not used so much in current approach */
  double last_time;
  
  /* The last feature that was played, kept for interpolation */
  SoundFeature last_sfeat;
  
  /* The array to contain the waveform as we build it*/
  std::vector<short> samples_building;
  
  /* Keeping track of OpenAL pointers and what buffers are playing */
  ALuint buffer_id[2];
  size_t last_played_buffer_ind;
  ALuint source_id;
  size_t num_queued_or_playing;
  
};

#endif //SoundInterpolatorImplClass
