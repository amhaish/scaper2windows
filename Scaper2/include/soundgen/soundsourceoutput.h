#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <common/CircularBuffer.h>
#include <Eigen/StdVector>

#ifndef SoundSourceOutputClass
#define SoundSourceOutputClass

class SoundSourceOutputImpl;

class SoundSourceOutput{
public:
  /* Construct sound interpolation object, and intitialise back-end if necessary */
  SoundSourceOutput(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
  virtual ~SoundSourceOutput();
  
  /** Call this even if you have nothing to play to make sure sounds are buffered right **/
    
    bool output(CircularBuffer<short> &csb,Eigen::Vector4f &pos);
    
  
private:
  
  bool output(CircularBuffer<short> &csb);
  
  boost::shared_ptr<SoundSourceOutputImpl> impl;
  
};

#endif //SoundSourceOutputClass
