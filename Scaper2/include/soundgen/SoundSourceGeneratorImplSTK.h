#include<Consts.h>
#include "SineWave.h" 
#include "Blit.h"
#include "BlitSaw.h"
#include "BlitSquare.h"
#include "Noise.h"
#include "ADSR.h"
#include "Asymp.h"
#include "Envelope.h"
#include "Granulate.h"
#include"Modulate.h"
//#include "SingWave.h"

//////////// instrument 

#include "BandedWG.h"
#include "BlowBotl.h"
#include "BlowHole.h"
#include "Bowed.h"
#include "Clarinet.h"
#include "Drummer.h"
#include "FM.h"
#include "Resonate.h"
#include "Moog.h"
#include "Shakers.h"
#include "Simple.h"
#include "Sitar.h"
#include "StifKarp.h"
#include "PercFlut.h"

#include "BeeThree.h"
#include "HevyMetl.h"
#include "PercFlut.h"
#include "Rhodey.h"

#include "Instrmnt.h"
#include "Whistle.h"



#include "Stk.h"

#include "BiQuad.h"
#include "Iir.h"
#include "OnePole.h"

#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>
#include <common/RateTracker.h>
#include <vector>
#include <chrono>
#include <string>

#ifndef SoundSourceGeneratorImplSTKClass
#define SoundSourceGeneratorImplSTKClass


using namespace stk;
using namespace std;

/* For passing to the fill_sample routine*/
struct wave_properties {
    double starttime;
    double endtime;
    double startfreq;
    double endfreq;
    double start_attenuation;
//     double attack_duration;
//     double release_onset;
    double end_attenuation;
    uint num_samples;
};

class SoundSourceGeneratorImplSTK {
public:
    /* Construct sound interpolation object, and intitialise back-end if necessary */
    SoundSourceGeneratorImplSTK (double approx_hz=15.0); // unfortunately, high hz does not lead to good results
    virtual ~SoundSourceGeneratorImplSTK ();

    void generate_sound(SoundFeature sfeat,CircularBuffer<short> &csb);
    void fill_sample_add(CircularBuffer<short> &csb,wave_properties &wp,size_t start_ind,string object_type,size_t frequency_ind);
    SoundFeature get_last_played_sound_feature();
    
    
    /* Call this every time you don't call play_feature */
//     void play_nothing();
    
private:

// sound Generators
stk::SineWave sineWv;
stk::Blit blit;
stk::BlitSaw blit_saw;
stk::BlitSquare blit_square;
stk::Noise noise;
stk::ADSR adsr;
stk::Asymp Asym;
stk::Envelope env;
stk::Granulate granulate;
stk::Modulate modulate;
//stk::SingWave sing;



StkFloat lowFrequency=50;

// instruments
stk::BandedWG bandWG;
stk::BlowBotl blowBotl;
stk::Bowed bowed;
stk::Drummer drummer;
stk::BeeThree beeThree;
stk::Resonate resonate;
stk::Moog moog;
stk::Shakers shakers;
stk::Simple simple;
stk::Sitar sitar;
stk::StifKarp krap;


stk::PercFlut flut;
stk::HevyMetl hvymtl;

stk::Rhodey rhodey;


stk::Whistle whistle;


// filters
stk::Iir iir_fil;
stk::BiQuad bi_fil;
stk::OnePole onep_fil;

stk::Envelope envlp;


StkFloat sampleRate;

//   void add_feature_to_fill_buffer(SoundFeature &sfeat,CircularBuffer<short> &csb);
    void calc_timing();
    
    /* The approximate frequency at which this will be played - should be set lower than actual hz */
//     double approx_hz;

    /* Number of samples in each sound - calculated from hz */
//     size_t grain_num_samples;
//     double grain_length;
//     double rise_time;

    RateTracker rate_tracker;
    
//     double avg_frame_rate;
//     double avg_frame_rate_err_secs;
    
//     double curr_frame_rate;
    
    /* The last feature that was played, kept for interpolation */
    //SoundFeature last_sfeat;

    /* The array to contain the waveform as we build it*/

//     std::chrono::high_resolution_clock::time_point last_time_play_request_received;
    
    bool played_last;

     SoundFeature last_played;
     int aaa;
    
};

#endif //SoundSourceGeneratorImplClass
