#include<Consts.h>
#include <boost/shared_ptr.hpp>
#include <soundgen/SoundFeature.h>
#include <common/CircularBuffer.h>

#ifndef SoundSourceGeneratorSTKClass
#define SoundSourceGeneratorSTKClass

class SoundSourceGeneratorImplSTK;

class SoundSourceGeneratorSTK{
public:
  
  SoundSourceGeneratorSTK(double approx_hz=15.0); // unfortunately, high hz does not lead to good results
  virtual ~SoundSourceGeneratorSTK();
  
  void generate_sound(SoundFeature &sfeat,CircularBuffer<short> &csb); 
  
  /* Call this every time you don't call play_feature */
//   void play_nothing();
  SoundFeature get_last_played_sound_feature();
  
private:
  
  boost::shared_ptr<SoundSourceGeneratorImplSTK> implSTK;
  
};

#endif //SoundSourceGeneratorClass
