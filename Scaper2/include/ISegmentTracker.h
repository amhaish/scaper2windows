#include <Consts.h>
#include <PCLObject.h>
#include <pcl/point_types.h>

#include <PointCloudWithNormals.h>

#ifndef ISegmentTrackerClass
#define ISegmentTrackerClass

class ISegmentTracker{
public:	
  virtual std::pair<std::vector<PCLObject::Ptr>, std::vector<PCLObject::Ptr>> Execute(PointCloudWithNormals::Vector)=0;
private:
};
#endif //ISegmentTrackerClass