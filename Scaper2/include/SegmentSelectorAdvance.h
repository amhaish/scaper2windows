#ifndef SegmentSelectorAdvanceClass
#define SegmentSelectorAdvanceClass

#include <ISegmentSelector.h>
#include <chrono>
// #include <Consts.h>

// #include <pcl/point_types.h>

#include <pcl/visualization/pcl_visualizer.h>
// #include<PCLObject.h>


class SegmentSelectorAdvance: public ISegmentSelector{
public:	
//   SegmentSelectorAdvance(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m);
  SegmentSelectorAdvance(double scan_distance,double scan_time,boost::shared_ptr<pcl::visualization::PCLVisualizer>,boost::mutex&);
  virtual std::vector<PCLObject::Ptr> Execute(std::vector<PCLObject::Ptr> &segments);
  
private:
  double max_scan_distance;
  double scan_time;
  
  typedef std::chrono::high_resolution_clock clock;
  std::chrono::time_point<clock> last_clock;
  double last_scan_depth;
  
  static int verbosity;
//   static int verbosity;
  
  boost::mutex* viewer_mutex;
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  unsigned int last_size;
  
  
};
#endif //SegmentSelectorAdvanceClass