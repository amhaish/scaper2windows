#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <PCLObject.h>
#include <ISegmentSelector.h>

#ifndef SegmentSelectorInteractiveClass
#define SegmentSelectorInteractiveClass

class SegmentSelectorInteractiveHC: public ISegmentSelector{
public:	
  SegmentSelectorInteractiveHC(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer>,boost::mutex&);
  virtual std::vector<PCLObject::Ptr> Execute(std::vector<PCLObject::Ptr> &segments);
private:
  boost::mutex* viewer_mutex;
  void displayCloudsColoured(std::vector< PCLObject::Ptr > &sorted_primitives,boost::mutex*);
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  void removePreviousDataFromScreen();
  //Tracker parameters
  unsigned int last_size;
  uint units_divisor;

};

#endif //SegmentSelectorInteractiveClass