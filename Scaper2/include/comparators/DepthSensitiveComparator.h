#include <pcl/segmentation/comparator.h>
#include <pcl/point_types.h>

 #ifndef DepthSensitiveComparatorClass
 #define DepthSensitiveComparatorClass
 
template<typename PointT, typename PointNT, typename PointLT>
  class DepthSensitiveComparator: public pcl::Comparator<PointT>
  {
    public:
      typedef typename pcl::Comparator<PointT>::PointCloud PointCloud;
      typedef typename pcl::Comparator<PointT>::PointCloudConstPtr PointCloudConstPtr;
      
      typedef typename pcl::PointCloud<PointNT> PointCloudN;
      typedef typename PointCloudN::Ptr PointCloudNPtr;
      typedef typename PointCloudN::ConstPtr PointCloudNConstPtr;
      
      typedef typename pcl::PointCloud<PointLT> PointCloudL;
      typedef typename PointCloudL::Ptr PointCloudLPtr;
      typedef typename PointCloudL::ConstPtr PointCloudLConstPtr;

      typedef boost::shared_ptr<DepthSensitiveComparator<PointT, PointNT, PointLT> > Ptr;
      typedef boost::shared_ptr<const DepthSensitiveComparator<PointT, PointNT, PointLT> > ConstPtr;

      using pcl::Comparator<PointT>::input_;
      
      /** \brief Empty constructor for EuclideanClusterComparator. */
      DepthSensitiveComparator ()
      {
      }
      
      /** \brief Destructor for EuclideanClusterComparator. */
      virtual
      ~DepthSensitiveComparator ()
      {
      }

      virtual void 
      setInputCloud (const PointCloudConstPtr& cloud)
      {
        input_ = cloud;
        Eigen::Matrix3f rot = input_->sensor_orientation_.toRotationMatrix ();
        z_axis_ = rot.col (2);
      }
      
      /** \brief Provide a pointer to the input normals.
        * \param[in] normals the input normal cloud
        */
      inline void
      setInputNormals (const PointCloudNConstPtr &normals)
      {
        normals_ = normals;
      }

      /** \brief Get the input normals. */
      inline PointCloudNConstPtr
      getInputNormals () const
      {
        return (normals_);
      }

      /** \brief Set the tolerance in radians for difference in normal direction between neighboring points, to be considered part of the same plane.
        * \param[in] angular_threshold the tolerance in radians
        */
      virtual inline void
      setAngularThreshold (float angular_threshold)
      {
        angular_threshold_ = cosf (angular_threshold);
      }
      
      /** \brief Get the angular threshold in radians for difference in normal direction between neighboring points, to be considered part of the same plane. */
      inline float
      getAngularThreshold () const
      {
        return (acos (angular_threshold_) );
      }

      /** \brief Set the tolerance in meters for difference in perpendicular distance (d component of plane equation) to the plane between neighboring points, to be considered part of the same plane.
        * \param[in] distance_threshold the tolerance in meters 
        * \param depth_dependent
        */
      inline void
      setDistanceThreshold (float distance_threshold, bool depth_dependent)
      {
        distance_threshold_ = distance_threshold;
        depth_dependent_ = depth_dependent;
      }

      /** \brief Get the distance threshold in meters (d component of plane equation) between neighboring points, to be considered part of the same plane. */
      inline float
      getDistanceThreshold () const
      {
        return (distance_threshold_);
      }

      /** \brief Set label cloud
        * \param[in] labels The label cloud
        */
      void
      setLabels (PointCloudLPtr& labels)
      {
        labels_ = labels;
      }

      /** \brief Set labels in the label cloud to exclude.
        * \param exclude_labels a vector of bools corresponding to whether or not a given label should be considered
        */
      void
      setExcludeLabels (std::vector<bool>& exclude_labels)
      {
        exclude_labels_ = boost::make_shared<std::vector<bool> >(exclude_labels);
      }

      void setNANNormalsAreEdges(bool value){
	nan_normals_are_edges_ = value;
      }
      
      /** \brief Compare points at two indices by their plane equations.  True if the angle between the normals is less than the angular threshold,
        * and the difference between the d component of the normals is less than distance threshold, else false
        * \param idx1 The first index for the comparison
        * \param idx2 The second index for the comparison
        */
      virtual bool
      compare (int idx1, int idx2) const
      {
        int label1 = labels_->points[idx1].label;
        int label2 = labels_->points[idx2].label;
        
        if (label1 == -1 || label2 == -1)
          return false;
        
        if ( (*exclude_labels_)[label1] || (*exclude_labels_)[label2])
          return false;
        
        float dist_threshold = distance_threshold_;
        if (depth_dependent_)
        {
          Eigen::Vector3f vec = input_->points[idx1].getVector3fMap ();
          float z = vec.dot (z_axis_);
          dist_threshold *= z * z;
        }

        float dx = input_->points[idx1].x - input_->points[idx2].x;
        float dy = input_->points[idx1].y - input_->points[idx2].y;
        float dz = input_->points[idx1].z - input_->points[idx2].z;
        float dist = sqrtf (dx*dx + dy*dy + dz*dz);
// 	std::cout<<"angular_threshold_ :"<<angular_threshold_ <<std::endl;
	if(!(dist < dist_threshold))return false;
	if(nan_normals_are_edges_){
	  if(!pcl::isFinite(normals_->points[idx1])||!pcl::isFinite(normals_->points[idx2]))return false;
	}
	else{
	  if(!pcl::isFinite(normals_->points[idx1])||!pcl::isFinite(normals_->points[idx2]))return true;
	}
	if(!(normals_->points[idx1].getNormalVector3fMap ().dot (normals_->points[idx2].getNormalVector3fMap()) > angular_threshold_ ) )return false;
	//note: angular_threshold is the cosine of the angular threshold sent as a parameter
	
	//cout<<comparisonResult<<':'<<dist<<'/'<<distance_threshold_<<'|'<<normals_->points[idx1].getNormalVector3fMap ().dot (normals_->points[idx2].getNormalVector3fMap())<<'/'<<angular_threshold_<<'\n';
	
        return true;
      }
      
    protected:
      PointCloudNConstPtr normals_;
      PointCloudLPtr labels_;

      boost::shared_ptr<std::vector<bool> > exclude_labels_;
      float angular_threshold_;
      float distance_threshold_;
      bool depth_dependent_;
      Eigen::Vector3f z_axis_;
      bool nan_normals_are_edges_;
  };
#endif