#include <pcl/segmentation/comparator.h>
#include <comparators/DepthSensitiveComparator.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/plane_coefficient_comparator.h>


#ifndef CurvatureBasedComparatorClass
#define CurvatureBasedComparatorClass

template<typename PointT, typename PointNT, typename PointLT>
class CurvatureBasedComparator : public DepthSensitiveComparator<PointT, PointNT, PointLT>
{
public:
	typedef typename pcl::Comparator<PointT>::PointCloud PointCloud;
	typedef typename pcl::Comparator<PointT>::PointCloudConstPtr PointCloudConstPtr;

	typedef typename pcl::PointCloud<PointNT> PointCloudN;
	typedef typename PointCloudN::Ptr PointCloudNPtr;
	typedef typename PointCloudN::ConstPtr PointCloudNConstPtr;

	typedef typename pcl::PointCloud<PointLT> PointCloudL;
	typedef typename PointCloudL::Ptr PointCloudLPtr;
	typedef typename PointCloudL::ConstPtr PointCloudLConstPtr;

	typedef boost::shared_ptr<CurvatureBasedComparator<PointT, PointNT, PointLT> > Ptr;
	typedef boost::shared_ptr<const CurvatureBasedComparator<PointT, PointNT, PointLT> > ConstPtr;

	using pcl::Comparator<PointT>::input_;


	/** \brief Empty constructor for PlaneCoefficientComparator. */
	CurvatureBasedComparator()
	{
	}

	/** \brief Empty constructor for PlaneCoefficientComparator.
	  * \param[in] distance_map the distance map to use
	  */
	CurvatureBasedComparator(const float *distance_map) :
		distance_map_(distance_map)
	{
	}

	/** \brief Destructor for PlaneCoefficientComparator. */
	virtual
		~CurvatureBasedComparator()
	{
	}

	/** \brief Set a distance map to use. For an example of a valid distance map see
	  * \ref OrganizedIntegralImageNormalEstimation
	  * \param[in] distance_map the distance map to use
	  */
	inline void
		setDistanceMap(const float *distance_map)
	{
		distance_map_ = distance_map;
	}


	inline void
		setCurvatureThreshold(float curvature_threshold)
	{
		curvature_threshold_ = curvature_threshold;
	}


	/** \brief Return the distance map used. */
	const float*
		getDistanceMap() const
	{
		return (distance_map_);
	}


protected:
	/** \brief Compare two neighboring points, by using normal information, curvature, and euclidean distance information.
	  * \param[in] idx1 The index of the first point.
	  * \param[in] idx2 The index of the second point.
	  */
	bool
		compare(int idx1, int idx2) const
	{
		float dx = input_->points[idx1].x - input_->points[idx2].x;
		float dy = input_->points[idx1].y - input_->points[idx2].y;
		float dz = input_->points[idx1].z - input_->points[idx2].z;
		float dist = sqrtf(dx*dx + dy*dy + dz*dz);

		bool normal_ok = (normals_->points[idx1].getNormalVector3fMap().dot(normals_->points[idx2].getNormalVector3fMap()) > angular_threshold_);
		bool dist_ok = (dist < distance_threshold_);
		bool curvature_ok = sqrtf(normals_->points[idx1].curvature * normals_->points[idx1].curvature - normals_->points[idx2].curvature * normals_->points[idx2].curvature) < curvature_threshold_;

		return (dist_ok && normal_ok && curvature_ok);
	}

protected:
	const float* distance_map_;
	float curvature_threshold_;
};
#endif