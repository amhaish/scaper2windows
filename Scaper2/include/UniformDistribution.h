#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <PointCloudWithNormals.h>

#ifndef UniformDistributionClass
#define UniformDistributionClass

class UniformDistribution{
public:
  double Generate();
  UniformDistribution(int maxValue);
private:
  int maxValue;
};
#endif