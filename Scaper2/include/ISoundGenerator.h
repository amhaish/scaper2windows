
#include<PCLObject.h>

#ifndef ISoundGeneratorClass
#define ISoundGeneratorClass

class ISoundGenerator{
public:	
  virtual void Execute(std::vector<PCLObject::Ptr>)=0;
  virtual void setExaggeration(double multiplier)=0;
};
#endif