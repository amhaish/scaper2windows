#include <Consts.h>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>

#ifndef PCLObjectClass
#define PCLObjectClass
class PCLObject{
public:
  typedef boost::shared_ptr<PCLObject> Ptr;
  
  int track_id;
  bool isMatched; //Hakan
  int key;
  float width;
  float height;
  float depth;
  float size;
  float distance;
  //float firstMoment;
  //std::vector <float> secondMoment;
  Eigen::Vector4f centroid;
  pcl::PointCloud<SystemPoint>::Ptr objectCloud;
  pcl::PointCloud<pcl::Normal>::Ptr normalCloud;
  
  pcl::PointCloud<SystemPoint>::Ptr modified_cloud;
  pcl::PointCloud<pcl::Normal>::Ptr modified_normals;
  pcl::FPFHSignature33 fpfh ;
   
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
};

std::ostream& operator<<(std::ostream& stream,
                         const PCLObject& obj) ;



#endif