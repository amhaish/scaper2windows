//TODO: rename this as it does more than Euclidean segmentation

#include <IPCLSegmenter.h>
#include <PointCloudWithNormals.h>
#include <pcl/segmentation/comparator.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/boost.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <comparators/DepthSensitiveComparator.h>
#include <comparators/CurvatureBasedComparator.h>

 #ifndef PCLEuclideanSegmenterClass
 #define PCLEuclideanSegmenterClass

class PCLEuclideanSegmenter: public IPCLSegmenter{
public:
  PCLEuclideanSegmenter(boost::shared_ptr<pcl::visualization::PCLVisualizer> v,float distanceThreshold,float angularThreshold, float curvatureThreshold,float segmentYThreshold,uint segmentSizeThreshold,bool nan_normals_are_edges,boost::mutex& m);

  virtual PointCloudWithNormals::Vector Execute(PointCloudWithNormals::Ptr&);

protected:
  inline void removePreviousDataFromScreen ();
  inline void displayEuclideanClusters();
  inline bool CheckSegmentDirection(pcl::PointCloud<pcl::Normal>::Ptr&,pcl::PointIndices*);
  inline bool CheckSegmentSize(pcl::PointIndices*);
private:
	void Visualize();

	pcl::PointIndices boundary_indices;
	float distanceToTheCentroid;
	float sumOfDistances;
	float averageDistance;
	Eigen::Vector4f cluster_centroid;
	SystemPoint centroid2;
	SystemPoint centroid;
	pcl::PointXYZ boundary_point;


	boost::mutex* viewer_mutex;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	PointCloudWithNormals::Vector clusters;


	//Segmenter Configuration
	float DistanceThreshold;
	float AngularThreshold;
	float CurvatureThreshold;
	uint SegmentSizeThreshold;
	float segmentYThreshold;

	int last_clusters_size;
	int last_normals_size;
	bool displayNormals;

	bool nan_normals_are_edges;

	bool use_curvature;
};
#endif