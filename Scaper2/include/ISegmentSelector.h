
#ifndef ISegmentSelectorClass
#define ISegmentSelectorClass

#include <PCLObject.h>

class ISegmentSelector{
public:	
  virtual std::vector<PCLObject::Ptr> Execute(std::vector<PCLObject::Ptr> &segments)=0;
private:
  
};

#endif //ISegmentSelectorClass