#include "PCLObject.h"
#include<consts.h>

#include <pcl/visualization/pcl_visualizer.h>
// #include<PCLObject.h>


#include <pcl/point_types.h>

#include <boost/shared_ptr.hpp>

#ifndef PCLToolsFile
#define PCLToolsFile
namespace PCLTools{

  void displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer,boost::mutex* viewer_mutex,std::string basename,int last_size);
  const int verbosity=5;
};



#endif //PCLToolsFile