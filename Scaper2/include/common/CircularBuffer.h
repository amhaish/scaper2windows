
#ifndef CircularBufferClass
#define CircularBufferClass
#include<consts.h>
#include <assert.h>
#include <sstream>
#include <boost/iterator/iterator_concepts.hpp>

// #define ALLOW_NEGATIVE_IND // for debugging
template <class T> class CircularBuffer {

public:
    CircularBuffer(size_t sizep):m_size(sizep),m_ptr(0),m_num_consumed(0){
      assert(sizep>0);
      m_buffer = new T[sizep];
      zero_all();
    }
    
    CircularBuffer(const CircularBuffer &obj){
      m_size=obj.m_size;
      m_ptr=obj.m_ptr;
      m_buffer = new T[m_size];
      for(int i=0;i<m_size;i++)
        m_buffer[i]=obj.m_buffer[i];
      m_num_consumed=obj.m_num_consumed;
    }
    
    ~CircularBuffer(){
      delete[] m_buffer;
    }
//     !!!make this ind be negative as possibilit
    T& operator[](int ind){
//       T& operator[](size_t ind){
      int add;
      if(ind+m_ptr>=0)add = (ind+m_ptr)%m_size;
#ifdef ALLOW_NEGATIVE_IND
      else add = (m_size+ind+m_ptr)%m_size;
#else
      else assert(0);//better to use write datatype? harder to catch overflow
#endif
      return m_buffer[add];
    }
    
    T consume(bool zero=true){
	T val = m_buffer[m_ptr];
        if(zero)
          m_buffer[m_ptr]=0;
	m_ptr++;
	if(m_ptr>=m_size)m_ptr-=m_size;
	m_num_consumed++;
	return val;
    }
    
    void zero_all(){
      for(size_t i=0;i<m_size;i++){
	m_buffer[i]=0;
      }
    }    

    size_t size(){
	return m_size;
    }   
    
    std::string str(){
	return str(0,m_size);
    }
    
    std::string str(int start,int end){
//       std::string str(size_t start,size_t end){
//       assert(end<=m_size);
      std::stringstream ss;
      ss<<"[CircularBuffer["<<start<<"-"<<end<<"](size="<<m_size<<",ptr="<<m_ptr<<",num_consumed="<<m_num_consumed<<": ";
      for(int i=start;i<end;i++){
	if (i!=0)ss<<",";
	ss<<"["<<i+m_ptr<<"]:"<<m_buffer[(i+m_ptr)%m_size];
      }
//       if(num_from_end>0){
// 	ss<<"...";
// 	for(int i=std::max(size_to_output,m_size-num_from_end);i<m_size;i++){
// 	  if (i!=0)ss<<",";
// 	  ss<<"["<<i+m_ptr<<"]:"<<m_buffer[(i+m_ptr)%m_size];	  
// 	}
//       }
      ss<<"]";
      return ss.str();
    }

    void reset_num_consumed(){
      m_num_consumed=0;
    }

    size_t get_num_consumed(){
     return m_num_consumed;
    }

private:
  
  T *m_buffer;
  size_t m_size;
  size_t m_ptr; 
  size_t m_num_consumed;
//   size_t m_num_consumed;
    
};

// std::ostream& operator<<(std::ostream& stream,const CircularBuffer & buf);

#endif //CircularBufferClass