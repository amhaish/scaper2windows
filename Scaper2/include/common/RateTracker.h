#include <chrono>
#include <string>
#include<consts.h>
#ifndef RateTrackerClass
#define RateTrackerClass

class RateTracker{
public:
  RateTracker(std::string name="",double initial_hz=15,double avg_frame_weight_multiplier=0.2);
  
  /** Call this periodically to average intervals over which it is called */
  void update(); 
  
  inline double get_current_period(){return current_period;}
  inline double get_tracked_period(){return tracked_period;}
  inline double get_tracked_hz(){return tracked_hz;}
  inline double get_tracked_period_error(){return tracked_period_error;}
  inline double get_num_updates(){return num_updates;}
  void print_status();
  
private:
  std::chrono::high_resolution_clock::time_point last_time_point;
  double current_period;
  double tracked_period;
  double tracked_hz;
  double tracked_period_error;
  uint num_updates;
  
  std::string name;
  double avg_frame_weight_multiplier;
  
};


#endif //RateTrackerClass