#include<consts.h>

#ifndef TimerClass
#define TimerClass

#include <chrono>
#include <string>

class Timer
{
public:
    Timer(std::string name,uint print_precision=6);
    Timer (const Timer &timer);
    void reset();
    double elapsed() const;
    void print_elapsed();
    void accumulate();
    double accumulated_average() const;
    void print_accumulated_average();
    void accumulate_and_print_elapsed();
    void accumulate_and_print_accumulated_average();
    
private:
    typedef std::chrono::high_resolution_clock clock;
    typedef std::chrono::duration<double, std::ratio<1> > second;
    std::chrono::time_point<clock> start;
    std::string name;
    double accum_sec;
    uint accum_cnt;
    uint print_precision;
};

#endif // TimerClass