#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <PCLObject.h>

#ifndef ISegmentSonifyClass
#define ISegmentSonifyClass

class ISegmentSonify {
  
public:
   virtual void Execute(std::vector<PCLObject::Ptr>)=0;
  
};
#endif