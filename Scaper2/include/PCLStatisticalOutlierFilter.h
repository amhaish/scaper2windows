#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <IPCLFilter.h>

#ifndef PCLStatisticalOutlierFilterClass
#define PCLStatisticalOutlierFilterClass
//clock_t clk;
//#define isDepthValid(x) (!(boost::math::isnan)(x.z))
//#define BENCHMARK(x)  clk = clock(); {x} printf("Line %d took %lums to execute\n", __LINE__, clock()-clk);
class PCLStatisticalOutlierFilter: public IPCLFilter{
public:	
  PCLStatisticalOutlierFilter(boost::shared_ptr<pcl::visualization::PCLVisualizer>,int,double);
  virtual void Execute(PointCloudWithNormals::Ptr &);
private:
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  
  //Filter parameters
  double StddevMulThresh;
  int FilterMeanK;
};
#endif