#include <Consts.h>
#include <pcl/point_types.h>

#ifndef IStateClass
#define IStateClass

class IState{

public:
  virtual IState* Copy()=0;
  virtual void Diffuse()=0;
  double Weight;
private:
  
  
};


#endif