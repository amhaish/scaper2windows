#include <Consts.h>
#include <vector>
#include <memory>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>


#ifndef PointCloudWithNormalsClass
#define PointCloudWithNormalsClass

class PointCloudWithNormals{
 
public:
    typedef std::vector<PointCloudWithNormals> Vector; // PointCloudWithNormals::Vector
    typedef std::shared_ptr<PointCloudWithNormals> Ptr; // PointCloudWithNormals::Ptr
    typedef std::shared_ptr<const PointCloudWithNormals> ConstPtr; // PointCloudWithNormals::Ptr

    PointCloudWithNormals();
    pcl::PointCloud<SystemPoint>::Ptr cloud;
    pcl::PointCloud<pcl::Normal>::Ptr normals;
    pcl::PointCloud<SystemPoint>::Ptr boundary;
	
	boost::shared_ptr<std::vector<int>> full_cloud_indices;
	pcl::PointCloud<SystemPoint>::Ptr full_cloud;
	pcl::PointCloud<pcl::Normal>::Ptr full_normals;
};
#endif