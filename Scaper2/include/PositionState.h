#include <Consts.h>
#include <pcl/point_types.h>
#include <IState.h>

#ifndef PositionStateClass
#define PositionStateClass

class PositionState:public IState{
  
public:
  IState* Copy();
  void Diffuse();
  static IState* FromArray(double[]);
private:
  SystemPoint Centriod;
  double Variance;
};

#endif