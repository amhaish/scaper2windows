#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/cloud_viewer.h>
#include <IPCLFeaturesExporter.h>
#include <pcl/visualization/histogram_visualizer.h>

#include <pcl/filters/random_sample.h>


#ifndef PCLFeaturesExtractorClass
#define PCLFeaturesExtractorClass
class PCLFeaturesExtractor: public IPCLFeaturesExporter{
public:	

  PCLFeaturesExtractor ();
//   PCLFeaturesExtractor (double fpfh_radius,boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>,boost::mutex& m);
//   PCLFeaturesExporter(boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>,boost::mutex& );
  virtual void Execute(std::vector<PCLObject::Ptr>);
private:  

//   double fpfh_radius;
//   
//   boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> hist_viewer;
//   pcl::FPFHSignature33 CalculateFPFH(PCLObject::Ptr);
//   void DisplayHistogram(pcl::FPFHSignature33);
//   boost::mutex* viewer_mutex;
  //   uint hist_view_cntr;
  
  //Exporter parameters
  float prev_width;
  float prev_height;
  float prev_depth;

};
#endif