#include<ISoundGenerator.h>
#include <al.h>
#include<soundgen/SoundFeature.h>
//#include<soundgen/SoundInterpolator.h>
#include <soundgen/SoundSourceGenerator.h>
#include <soundgen/SoundSourceOutput.h>
#include <pcl/visualization/histogram_visualizer.h>

// #include <vector>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/fpfh.h>
#include <sstream>
#ifndef SoundGeneratorClass
#define SoundGeneratorClass
class SoundGenerator: public ISoundGenerator {
public:	
//   SoundGenerator(std::string path_to_fpfh_database, boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>, boost::mutex& );
  SoundGenerator();
  double randd();
  virtual void Execute(std::vector<PCLObject::Ptr>);
  virtual void setExaggeration(double multiplier);
 
private:
  
    // WHY YOU NO WORK???  std::vector<PCLObject::Ptr> last_objects;
//   std::string path_to_fpfh_database;
  double exaggerate_pos;
    //   SoundInterpolator si;
    std::vector<SoundSourceGenerator> ssg;
    std::vector<SoundSourceOutput> sso;
    std::vector<CircularBuffer<short> > csb;
     //Sound Generator Parameters
     //   int garbage[24]; //because valgrind --leak-check=yes ./segmentertester2 !!!???
  std::vector<Eigen::Vector4f> last_pos;
  std::vector<SoundFeature> last_played;
  
//   boost::mutex* viewer_mutex;
//   boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> hist_viewer;
//   void DisplayHistogram(pcl::PointCloud<pcl::FPFHSignature33>::Ptr,std::string,std::string);
//   
//   std::vector<std::string> object_names_list;
//   pcl::PointCloud<pcl::FPFHSignature33>::Ptr FPFHDataset;
//   
//   pcl::KdTreeFLANN<pcl::FPFHSignature33> kdtree;;
  
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  
};
#endif