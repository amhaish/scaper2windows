 
#include <ISegmentSonify.h>
#include <soundsubsystems/SIUSoundSystem/PCLFeaturesExtractor.h>
#include <soundsubsystems/SIUSoundSystem/SoundGenerator.h>

#ifndef SegmentSonifyClass
#define SegmentSonifyClass

class SegmentSonify: public ISegmentSonify {
public:
//     SegmentSonify(double, boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>, boost::mutex& , std::string, double);
    SegmentSonify(double);
    virtual void Execute(std::vector<PCLObject::Ptr>);

private:
   /* double fpfh_radius;
    boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> hist_viewer;
    boost::mutex* viewer_mutex;
    std::string path_to_fpfh_database;
   */ 
    double multiplier;

    IPCLFeaturesExporter* extractor;
    ISoundGenerator* soundGen;


};
#endif
