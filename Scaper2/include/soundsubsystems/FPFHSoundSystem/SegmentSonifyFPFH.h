#include <ISegmentSonify.h>
#include <soundsubsystems/FPFHSoundSystem/PCLFeaturesExtractorFPFH.h>
#include <soundsubsystems/FPFHSoundSystem/SoundGeneratorFPFH.h>

#ifndef SegmentSonifyFPFHClass
#define SegmentSonifyFPFHClass

class SegmentSonifyFPFH: public ISegmentSonify {
public:
    SegmentSonifyFPFH (double, boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>, boost::mutex& , std::string, double);
    virtual void Execute(std::vector<PCLObject::Ptr>);

private:
    double fpfh_radius;
    boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> hist_viewer;
    boost::mutex* viewer_mutex;
    std::string path_to_fpfh_database;
    double multiplier;

    IPCLFeaturesExporter* extractor;
    ISoundGenerator* soundGen;


};
#endif
