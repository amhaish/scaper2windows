#include<ISoundGenerator.h>
#include<soundgen/SoundFeature.h>
#include <soundgen/SoundSourceFullWaveformGenerator.h>
#include <soundgen/SoundSourceWaveformInterpolator.h>
#include <soundgen/SoundSourceOutput.h>
#include <soundgen/ObjectContours.h>
#include <pcl/common/geometry.h>
#include "soundgen/SoundGenBlocks.h"

#include <sstream>
#ifndef SoundGeneratorContourClass
#define SoundGeneratorContourClass

class SoundGeneratorContour: public ISoundGenerator {
public:
    SoundGeneratorContour();
    SoundGeneratorContour(bool);
    virtual void Execute(std::vector<PCLObject::Ptr>);
    virtual void setExaggeration(double multiplier);
    virtual void setZOffset(double offset);

private:

    size_t created_objects;

    const int verbosity = 0;

    double z_offset_;
    double exaggerate_pos;
    float factor;
    int current_samples;
    int total_samples;
    int interval_size;
    bool depth_normalization;
    float scale1, scale2, scale3;

//     std::vector<std::shared_ptr<SoundSourceFullWaveformGenerator> > ssg;
//   std::vector<std::vector<short> > full_buffer;
    std::vector<std::shared_ptr<SoundSourceWaveformInterpolator> > ssi;
    std::vector<CircularBuffer<short> > csb;
    std::vector<std::shared_ptr<SoundSourceOutput> > sso;


    std::vector<Eigen::Vector4f> last_pos;
    std::vector<SoundFeature> last_played;

    std::vector<float> normalized_rcontour;
    std::vector<float> normalized_hcontour;
    std::vector<float> normalized_vcontour;

    std::map<int,size_t> trackid2buffer;
    std::set<size_t> free_buffers;

    std::map<int,uint> times_seen;


    SoundBlocks::Tickable* rgenerator;
    SoundBlocks::Tickable* hgenerator;
    SoundBlocks::Tickable* vgenerator;
    SoundBlocks::Tickable* hfreqmod;
    SoundBlocks::Tickable* vmodhmod;

    float tick_value;

//   SystemPoint centroid;
//   float distanceToTheCentroid;
//   float averageDistance;
//   float sumOfDistances ;
//   pcl::PointIndices boundary_indices;
//   PCLBoundaryExtraction extractor;

    Eigen::Vector4f exaggerate(Eigen::Vector4f);

    void debug();


public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW


};
#endif