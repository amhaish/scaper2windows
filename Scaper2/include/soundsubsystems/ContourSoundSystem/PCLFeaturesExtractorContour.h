#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
// #include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <IPCLFeaturesExporter.h>
#include <pcl/visualization/histogram_visualizer.h>

#include <pcl/filters/random_sample.h>


#ifndef PCLFeaturesExtractorContourClass
#define PCLFeaturesExtractorContourClass
class PCLFeaturesExtractorContour: public IPCLFeaturesExporter{
public:

  PCLFeaturesExtractorContour ();
//   PCLFeaturesExporter(boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>,boost::mutex& );
  virtual void Execute(std::vector<PCLObject::Ptr>);
private:
////NOT sure if we need these
  //Exporter parameters
  float prev_width;
  float prev_height;
  float prev_depth;

};
#endif