// if we dont want to visualize the boundary wave form
#include <ISegmentSonify.h>
#include <soundsubsystems/ContourSoundSystem/PCLFeaturesExtractorContour.h>
#include <soundsubsystems/ContourSoundSystem/SoundGeneratorContour.h>

#ifndef SegmentSonifyContourClass
#define SegmentSonifyContourClass


class SegmentSonifyContour: public ISegmentSonify {
 public:
    SegmentSonifyContour (double,double sound_z_offset,bool depth_normalization);
    virtual void Execute(std::vector<PCLObject::Ptr>);

 private:
    double multiplier_;
    double sound_z_offset_;
    bool depth_normalization_;

    IPCLFeaturesExporter* extractor;
    ISoundGenerator* soundGen;


};
#endif
