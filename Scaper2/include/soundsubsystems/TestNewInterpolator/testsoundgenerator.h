#include<ISoundGenerator.h>
#include <al.h>
#include<soundgen/SoundFeature.h>
//#include<soundgen/SoundInterpolator.h>
#include <soundgen/SoundSourceFullWaveformGenerator.h>
#include <soundgen/SoundSourceWaveformInterpolator.h>
#include <soundgen/SoundSourceOutput.h>
#include <pcl/visualization/histogram_visualizer.h>

// #include <vector>

#include <common/Timer.h>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/fpfh.h>
#include <sstream>
#ifndef TestSoundGeneratorClass
#define TestSoundGeneratorClass
class TestSoundGenerator: public ISoundGenerator {
public:	
//   SoundGenerator(std::string path_to_fpfh_database, boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer>, boost::mutex& );
  TestSoundGenerator();
  double randd();
  virtual void Execute(std::vector<PCLObject::Ptr>);
  virtual void setExaggeration(double multiplier);
 
private:
  
const int verbosity = 4;
    // WHY YOU NO WORK???  std::vector<PCLObject::Ptr> last_objects;
//   std::string path_to_fpfh_database;
  double exaggerate_pos;
    //   SoundInterpolator si;

    std::vector<std::shared_ptr<SoundSourceFullWaveformGenerator> > ssg;
    std::vector<std::vector<short> > full_buffer;
    std::vector<std::shared_ptr<SoundSourceWaveformInterpolator> > ssi;
    std::vector<CircularBuffer<short> > csb;
    std::vector<std::shared_ptr<SoundSourceOutput> > sso;    
    
     //Sound Generator Parameters
     //   int garbage[24]; //because valgrind --leak-check=yes ./segmentertester2 !!!???
  std::vector<Eigen::Vector4f> last_pos;
  std::vector<SoundFeature> last_played;

//   Timer timer;

  
//   boost::mutex* viewer_mutex;
//   boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> hist_viewer;
//   void DisplayHistogram(pcl::PointCloud<pcl::FPFHSignature33>::Ptr,std::string,std::string);
//   
//   std::vector<std::string> object_names_list;
//   pcl::PointCloud<pcl::FPFHSignature33>::Ptr FPFHDataset;
//   
//   pcl::KdTreeFLANN<pcl::FPFHSignature33> kdtree;;
  
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  
};
#endif //TestSoundGeneratorClass