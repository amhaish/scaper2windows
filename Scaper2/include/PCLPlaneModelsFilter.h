#include <Consts.h>
#include <boost/thread/thread.hpp>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <limits>
#include <IPCLFilter.h>


#ifndef PCLPlaneModelsFilterClass
#define PCLPlaneModelsFilterClass
class PCLPlaneModelsFilter : public IPCLFilter {
public:

	PCLPlaneModelsFilter(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis, float distanceThreshold, float maximumCurvature, float minInliers, float, bool gpu, boost::mutex& m);
	virtual void Execute(PointCloudWithNormals::Ptr &);
private:
	void Visualize(PointCloudWithNormals::Ptr cloudWithNormal, pcl::PointCloud<SystemPoint>::CloudVectorType my_clusters);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	boost::mutex* viewer_mutex;

	//Filter parameters
	float distanceThreshold;
	float  maxDepthChangeFactor;
	float  normalSmoothingSize;
	float  maximumCurvature;
	float  minInliers;
	float  angularThreshold;
	bool gpuProcessing;
	int last_clusters_size;
};
#endif