#include <Consts.h>
#include <vector>
#include <memory>
#include <pcl/features/integral_image_normal.h>
#include <IPCLNormalCalculator.h>


#ifndef PCLNormalCalculatorClass
#define PCLNormalCalculatorClass

class PCLNormalCalculator : public IPCLNormalCalculator { 

public:
  
    PCLNormalCalculator(float,float);
    ~PCLNormalCalculator();
    virtual PointCloudWithNormals::Ptr Execute(pcl::PointCloud<SystemPoint>::Ptr&);
   
    pcl::PointCloud<pcl::Normal>::Ptr calculateNormals(pcl::PointCloud<SystemPoint>::Ptr &);

    
private:
     float MaxDepthChangeFactor;
     float NormalSmoothingSize;
};

#endif