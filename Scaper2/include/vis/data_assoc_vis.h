#ifndef __DATAASSOCVIS__
#define __DATAASSOCVIS__

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <PCLObject.h>

class DataAssocVisualizer{
public:
  unsigned int last_size;
  DataAssocVisualizer(boost::mutex& , boost::shared_ptr<pcl::visualization::PCLVisualizer>, std::vector<PCLObject::Ptr> &clouds1,   std::vector<PCLObject::Ptr> &clouds2,std::vector<std::pair<int,int> > &matches);
//   {
//       
//     unsigned char red [12] = {255,   0,   0, 255, 255,   0,128,   0,   0, 128, 128,   0};
//     unsigned char grn [12] = {  0, 255,   0, 255,   0, 255,0, 128,   0, 128,   0, 128};
//     unsigned char blu [12] = {  0,   0, 255,   0, 255, 255, 0,   0, 128,   0, 128, 128};
//       
//       int v1(0);
//     viewer->createViewPort(0.0, 0.0, 1.0, 0.5, v1);
//     viewer->setBackgroundColor (255, 255, 255,v1);
//     
//     int v2(0);
//     viewer->createViewPort(0.0, 0.5, 1.0, 1.0, v2);
//     viewer->setBackgroundColor (255, 255, 255,v2);
//     
//     
//     pcl::PointCloud<pcl::PointXYZ>::Ptr centroids1(new pcl::PointCloud<pcl::PointXYZ>);
//     pcl::PointCloud<pcl::PointXYZ>::Ptr centroids2(new pcl::PointCloud<pcl::PointXYZ>);
//     
//     for(int i=0;i<clouds1.size();i++){
//       pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color(clouds1[i],red[i%12] ,grn[i%12],blu[i%12]);
//       std::stringstream ss,tt;
//       ss<<"C"<<i;
//       tt<<"c"<<i;
//       viewer->addPointCloud<pcl::PointXYZ> (clouds1[i], color, ss.str(), v1);  
//       Eigen::Vector4f centroid;
//       pcl::compute3DCentroid(*clouds1[i],centroid);
//       pcl::PointXYZ centroidp;
//       centroidp.x=centroid[0];centroidp.y=centroid[1];centroidp.z=centroid[2];
//       viewer->addText3D(tt.str(), centroidp,1.0,1.0,0.0,0.0,tt.str(),v1);
//       centroids1->points.push_back(centroidp);
//       centroids1->height++;
//     }
//     for(int i=0;i<clouds2.size();i++){
//       pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color(clouds2[i],red[(i+6)%12] ,grn[(i+3)%12],blu[i%12]);
//       std::stringstream ss,tt;
//       ss<<"K"<<i;
//       tt<<"k"<<i;
//       viewer->addPointCloud<pcl::PointXYZ> (clouds2[i], color, ss.str(), v2);  
//       Eigen::Vector4f centroid;
//       pcl::compute3DCentroid(*clouds2[i],centroid);
//       pcl::PointXYZ centroidp;
//       centroidp.x=centroid[0];centroidp.y=centroid[1];centroidp.z=centroid[2];
//       viewer->addText3D(tt.str(), centroidp,1.0,1.0,0.0,0.0,tt.str(),v2);
//       centroids2->points.push_back(centroidp);
//       centroids2->height++;
//     }  
   /*
    
    viewer->addText("Radius: 0.01", 10, 10, "v1 text", v1);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud1", v1);
*/

//   viewer->spin();
//   }
  
 
  
private:
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;

  bool cloud1on;
  boost::mutex* viewer_mutex;
  bool cloud2on;
  bool stopme;
  /*
  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr > clouds1;
  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr > clouds2;*/
  std::vector<pcl::PointCloud<SystemPoint>::Ptr> clouds1;
  std::vector<pcl::PointCloud<SystemPoint>::Ptr> clouds2;
  pcl::PointCloud<SystemPoint>::Ptr centroids1;
  pcl::PointCloud<SystemPoint>::Ptr centroids2;  
  
// protected:
  void mouseCB (const pcl::visualization::MouseEvent &event,
                         void* viewer_void);
  inline void removePreviousDataFromScreen();
  void addLeftClouds();
  void addRightClouds();
  void remLeftClouds();
  void remRightClouds();
  
  
};



#endif //__DATAASSOCVIS__