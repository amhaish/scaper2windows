#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>
/*keyboard*/
#include <sys/time.h>
#include <sys/types.h>
/*keyboard*/

using namespace std;

#ifndef ParameterTakingClass
#define ParameterTakingClass

class ParameterTaking {
public:
	static ParameterTaking *GetSerialPort();
	void ArduinoParameterUpdate();
	void KeyboardParameterUpdate();
	int *GetJoystickData();
	int GetSelectionType();
	int GetRotaryEncoder();
	int BuffertoInt(char *);
	bool isSerialPortRunning();
	void setEnabled(bool en);
	void JoystickSelection();
	void ImuSelection();

private:
	ParameterTaking();
	static ParameterTaking *sp;
	int joystickData[3]; // 0. index for X, 1. index for Y, 2. index for button 
	int buttonsData[2];
	int rotaryEncoderData;
	int imuData[3]; 
	int fd;
	int selectionType;
	bool serialPortRunning;
	bool enabled;
};
#endif //ParameterTakingClass