#ifndef SegmentTrackerBaselineClass
#define SegmentTrackerBaselineClass

#include <Consts.h>

#include<ISegmentTracker.h>

#include <pcl/visualization/pcl_visualizer.h>

class SegmentTrackerBaseline: public ISegmentTracker{
public:	
  SegmentTrackerBaseline();

  virtual std::pair<std::vector<PCLObject::Ptr>,std::vector<PCLObject::Ptr>> Execute(PointCloudWithNormals::Vector);

private:

  std::vector<PCLObject::Ptr> previous;

  std::map< std::pair<int,int>,double> memoized_match_score;
  double match_score(int i,PCLObject::Ptr a,int j,PCLObject::Ptr b);
  std::vector<std::string> last_visualised_names;

  static int verbosity;

};
#endif //SegmentTrackerBaselineClass