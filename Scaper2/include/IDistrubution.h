#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <PointCloudWithNormals.h>

#ifndef IDistributionClass
#define IDistributionClass

class IDistribution{
public:
  double Generate();
};
#endif