#include <Consts.h>
#include <boost/thread/thread.hpp>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/common/centroid.h>
#include <limits>
#include <pcl/visualization/cloud_viewer.h>
#include <PCLObject.h>
#include <ISegmentTracker.h>
#include <vis/data_assoc_vis.h>
#include <cmath>

#ifndef SegmentTrackerAssoc1Class
#define SegmentTrackerAssoc1Class


class SegmentTrackerAssoc1: public ISegmentTracker{
public:	
  SegmentTrackerAssoc1(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer>,boost::mutex&);
  virtual std::vector<PCLObject::Ptr> Execute(PointCloudWithNormals::Vector);
private:
  uint units_divisor;
  boost::mutex* viewer_mutex;
//   void displayCloudsColoured(std::vector< PCLObject::Ptr > &sorted_primitives,boost::mutex*);
  void displayCloudsColoured(std::vector< PCLObject::Ptr > &sorted_primitives,boost::mutex*);

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  void removePreviousDataFromScreen();
  //Tracker parameters
  unsigned int last_size;
};
#endif