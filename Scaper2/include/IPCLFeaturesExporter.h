#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <PCLObject.h>

#ifndef IPCLFeaturesExporterClass
#define IPCLFeaturesExporterClass

class IPCLFeaturesExporter{
public:	
  virtual void Execute(std::vector<PCLObject::Ptr>)=0;
private:
};
#endif