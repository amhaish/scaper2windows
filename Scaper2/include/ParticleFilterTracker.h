#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include<PCLObject.h>
#include<IPCLObjectsTracker.h>

#ifndef PCLObjectsTrackerClass
#define PCLObjectsTrackerClass

class ParticleFilterTracker: public IPCLObjectsTracker{
public:	
  ParticleFilterTracker(boost::shared_ptr<pcl::visualization::PCLVisualizer>,boost::mutex&);
  
  virtual std::vector<PCLObject::Ptr> Execute(pcl::PointCloud<SystemPoint>::CloudVectorType);
private:
  boost::mutex* viewer_mutex;
  void displayCloudsColoured(std::vector< PCLObject::Ptr > &sorted_primitives,boost::mutex*);
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  void removePreviousDataFromScreen();
  //Tracker parameters
  unsigned int last_size;
};
#endif