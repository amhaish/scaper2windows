#ifndef SegmentSelectorPassthroughClass
#define SegmentSelectorPassthroughClass

#include <ISegmentSelector.h>

// #include <Consts.h>

// #include <pcl/point_types.h>

#include <pcl/visualization/pcl_visualizer.h>
// #include<PCLObject.h>


class SegmentSelectorPassthrough: public ISegmentSelector{
public:	
//   SegmentSelectorPassthrough(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m);
  SegmentSelectorPassthrough();
  virtual std::vector<PCLObject::Ptr> Execute(std::vector<PCLObject::Ptr> &segments);
  
private:

//   static int verbosity;
};
#endif //SegmentSelectorPassthroughClass