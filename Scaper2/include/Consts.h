//clock_t clk;
//#define isDepthValid(x) (!(boost::math::isnan)(x.z))
//#define BENCHMARK(x)  clk = clock(); {x} printf("Line %d took %lums to execute\n", __LINE__, clock()-clk);

#ifndef ConstsFile
#define ConstsFile

#include <pcl/point_types.h>

#ifndef DEBUG_MODE
#define DEBUG_MODE True
#endif

#define SAMPLE_RATE 44100
#define BYTES_PER_SAMPLE 2

const int RAND_SEED=34469;

typedef typename pcl::PointXYZRGBA SystemPoint;
typedef typename unsigned int uint;
#endif //ConstsFile