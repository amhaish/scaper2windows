#include <Consts.h>
#include <PCLObject.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#ifndef IPCLObjectsTrackerClass
#define IPCLObjectsTrackerClass

class IPCLObjectsTracker{
public:	
  virtual std::vector<PCLObject::Ptr> Execute(pcl::PointCloud<SystemPoint>::CloudVectorType)=0;
private:
};
#endif