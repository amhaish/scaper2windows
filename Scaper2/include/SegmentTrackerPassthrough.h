#include <Consts.h>

#include<ISegmentTracker.h>

#ifndef SegmentTrackerPassthroughClass
#define SegmentTrackerPassthroughClass

class SegmentTrackerPassthrough: public ISegmentTracker{
public:	
  SegmentTrackerPassthrough();
  
  virtual std::vector<PCLObject::Ptr> Execute(PointCloudWithNormals::Vector);
  
private:
    float findSize(PCLObject::Ptr obj);
  
};
#endif //SegmentTrackerPassthroughClass