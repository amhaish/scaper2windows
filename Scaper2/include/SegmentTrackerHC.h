#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include<PCLObject.h>
#include<ISegmentTracker.h>

#ifndef SegmentTrackerHCClass
#define SegmentTrackerHCClass

class SegmentTrackerHC: public ISegmentTracker{
public:	
  SegmentTrackerHC(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer>,boost::mutex&);
  
  virtual std::vector<PCLObject::Ptr> Execute(PointCloudWithNormals::Vector);
  
private:
  boost::mutex* viewer_mutex;
  void displayCloudsColoured(std::vector< PCLObject::Ptr > &sorted_primitives,boost::mutex*);
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  void removePreviousDataFromScreen();
  float findSize(PCLObject::Ptr obj);
  uint units_divisor;

  //Tracker parameters
unsigned int last_size;
};
#endif //SegmentTrackerHC