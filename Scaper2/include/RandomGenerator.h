#include <stdlib.h> 

#ifndef RandomGeneratorClass
#define RandomGeneratorClass

class RandomGenerator{
public:	
  static double NextDouble(int min,int max);
};

#endif