#include <Consts.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include<PointCloudWithNormals.h>

#ifndef IPCLSegmenterClass
#define IPCLSegmenterClass

class IPCLSegmenter{
public:	
  virtual PointCloudWithNormals::Vector Execute(PointCloudWithNormals::Ptr&)=0;
private:
};
#endif