/**
 * Adapted from sim_test_simple.cpp
 *
 */
#include <boost/algorithm/string.hpp>
#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <boost/shared_ptr.hpp>

#include <GL/glew.h>

# include <GL/gl.h>
# include <GL/glu.h>
# include <GL/glut.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "pcl/common/common.h"
#include "pcl/common/transforms.h"

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

// define the following in order to eliminate the deprecated headers warning
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl/io/vtk_lib_io.h>

#include "pcl/simulation/camera.h"
#include "pcl/simulation/model.h"
#include "pcl/simulation/scene.h"
#include "pcl/simulation/range_likelihood.h"

#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>

// RangeImage:
#include <pcl/range_image/range_image_planar.h>

// Pop-up viewer
#include <pcl/visualization/cloud_viewer.h>
#include <boost/thread/thread.hpp>

using namespace Eigen;
using namespace pcl;
using namespace pcl::console;
using namespace pcl::io;
using namespace pcl::simulation;

Scene::Ptr scene_;
Camera::Ptr camera_;
RangeLikelihood::Ptr range_likelihood_;

int window_width_;
int window_height_;
bool paused_;

std::ifstream poses_file_stream;
std::string poses_file_name;
std::string target_pcd_dir;
std::string target_pcd_basename;


std::string model_name;


int width;
int height;
int fx;
int fy;
int cx;
int cy;
double znear;
double zfar;


int poseno;

int num_interp;

Eigen::Isometry3d last_pose;

void printHelp (int, char **argv)
{
    print_error ("Syntax is: %s <poses filename> <target PCD dir> <target PCD basename> <num interpolations between each pose> <num interpolations>\n", argv[0]);
    print_info ("acceptable filenames include vtk, obj and ply. ply can support colour\n");
}

void wait ()
{
    std::cout << "Press enter to continue";
    getchar();
    std::cout << "\n\n";
}

Eigen::Isometry3d get_isometry(double x,double y,double z,double roll,double pitch,double yaw) {
    Eigen::Matrix3d m;
    m = AngleAxisd (yaw, Vector3d::UnitZ ())
        * AngleAxisd (pitch, Vector3d::UnitY ())
        * AngleAxisd (roll, Vector3d::UnitX ());

    Eigen::Isometry3d pose;

    pose.setIdentity ();
    pose.rotate(m);

    Vector3d v;
    v << x, y, z;
    pose.translate(v);
    return pose;
}
/*
boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addCoordinateSystem (1.0, "reference");
    viewer->initCameraParameters ();
    return (viewer);
}*/

// Read in a 3D model
void load_PolygonMesh_model (std::string polygon_file)
{
    pcl::PolygonMesh mesh;	// (new pcl::PolygonMesh);
    pcl::io::loadPolygonFile (polygon_file, mesh);
    pcl::PolygonMesh::Ptr cloud (new pcl::PolygonMesh (mesh));

    PolygonMeshModel::Ptr model = PolygonMeshModel::Ptr (new PolygonMeshModel (GL_POLYGON, cloud));
    scene_->add (model);

    std::cout << "Just read " << polygon_file << std::endl;
    std::cout << mesh.polygons.size () << " polygons and "
              << mesh.cloud.data.size () << " triangles\n";

}


int
main (int argc, char** argv)
{

    print_info ("From a model and a list of poses, reconstruct the PCDs. For more information, use: %s -h\n", argv[0]);

    if (argc < 5)
    {
        printHelp (argc, argv);
        return (-1);
    }

    poses_file_name = argv[1];

    poses_file_stream.open(poses_file_name);

    target_pcd_dir=argv[2];
    target_pcd_basename=argv[3];

    num_interp=std::stoi(argv[4]);

    std::string input;
    std::vector<std::string> inputs;
    std::getline(poses_file_stream ,input);
    assert(input=="Synthetic pose list");

//   std::cout<<"Got "<<input<<std::endl;
    std::getline(poses_file_stream ,input);
//   std::cout<<"Got "<<input<<std::endl;

    inputs.clear();
    boost::split(inputs, input, boost::is_any_of(","));
    assert(inputs[0]=="Version");
    assert(inputs[1]=="1.10");
    model_name=inputs[3];

    std::cout<<"Will read "<<model_name<<std::endl;

    std::getline(poses_file_stream ,input);
    assert(input=="Intrinsic params");
//   std::cout<<"Got "<<input<<std::endl;

    std::getline(poses_file_stream ,input);
    assert(input=="width,height,fx,fy,cx,cy,znear,zfar");


    std::getline(poses_file_stream ,input);

//   std::cout<<"Got "<<input<<std::endl;

    inputs.clear();
    boost::split(inputs, input, boost::is_any_of(","));
    width=std::stoi(inputs[0]);
    height=std::stoi(inputs[1]);
    fx=std::stod(inputs[2]);
    fy=std::stod(inputs[3]);
    cx=std::stod(inputs[4]);
    cy=std::stod(inputs[5]);
    znear=std::stod(inputs[6]);
    zfar=std::stod(inputs[7]);

    window_width_=width*2;
    window_height_=height*2;


    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);// was GLUT_RGBA
    glutInitWindowPosition (10, 10);
    glutInitWindowSize (window_width_, window_height_);
    glutCreateWindow ("OpenGL range likelihood");

    GLenum err = glewInit ();
    if (GLEW_OK != err)
    {
        std::cerr << "Error: " << glewGetErrorString (err) << std::endl;
        exit (-1);
    }

    std::cout << "Status: Using GLEW " << glewGetString (GLEW_VERSION) << std::endl;

    if (glewIsSupported ("GL_VERSION_2_0"))
        std::cout << "OpenGL 2.0 supported" << std::endl;
    else
    {
        std::cerr << "Error: OpenGL 2.0 not supported" << std::endl;
        exit(1);
    }
    std::cout << "GL_MAX_VIEWPORTS: " << GL_MAX_VIEWPORTS << std::endl;

    camera_ = Camera::Ptr (new Camera ());
    scene_ = Scene::Ptr (new Scene ());

    //range_likelihood_ = RangeLikelihoodGLSL::Ptr(new RangeLikelihoodGLSL(1, 1, height, width, scene_, 0));

    const GLubyte* version = glGetString (GL_VERSION);
    std::cout << "OpenGL Version: " << version << std::endl;


    range_likelihood_ = RangeLikelihood::Ptr (new RangeLikelihood (2, 2, height/2, width/2, scene_));

    range_likelihood_->setCameraIntrinsicsParameters (width,height, fx,fy, cx, cy);

    int a,b;
    float c,d,e,f;
    range_likelihood_->getCameraIntrinsicsParameters(a,b,c,d,e,f);

    
    range_likelihood_->setZNear(znear);
    range_likelihood_->setZFar(zfar);


    range_likelihood_->setComputeOnCPU (true);
    range_likelihood_->setSumOnCPU (true);
    range_likelihood_->setUseColor (true);

    cout << "About to read: " << model_name << endl;
    load_PolygonMesh_model (model_name);

    std::getline(poses_file_stream ,input);
    std::cout<<"Got "<<input<<std::endl;
    assert(input=="Camera params");

    std::getline(poses_file_stream ,input);
    std::cout<<"Got "<<input<<std::endl;
    assert(input=="x,y,z,roll,pitch,yaw");

    poseno=0;

    glutPostRedisplay();


    while(true) {


        std::vector<float> scores;
        std::string input;
        std::vector<std::string> inputs;

        std::vector<double> inputsd;


        if(!std::getline(poses_file_stream ,input)) {
            std::cout<<"Finished."<<std::endl;
            exit(0);
        }
        if(input=="") {
            std::cout<<"Finished."<<std::endl;
            exit(0);
	}

	std::cout<<"Got "<<input<<std::endl;
        inputs.clear();
        boost::split(inputs, input, boost::is_any_of(","));

        inputsd.resize(inputs.size());

        std::transform(inputs.begin(),inputs.end(),inputsd.begin(),[](const std::string& str) {
            return std::stod(str);
        });

//     Eigen::Isometry3d pose=get_isometry(inputsd[0],inputsd[1],inputsd[2],inputsd[3],inputsd[4],inputsd[5]);

        camera_->set(inputsd[0],inputsd[1],inputsd[2],inputsd[3],inputsd[4],inputsd[5]);

        Eigen::Isometry3d this_pose;
        if(poseno==0) {
            last_pose=camera_->getPose();
            poseno++;
            continue;
//             return 0;
        }
        else {
            this_pose = camera_->getPose();
        }


        //just used for calculating match scores for simulation matching, which we are not using.
        float* reference = new float[range_likelihood_->getRowHeight() * range_likelihood_->getColWidth()];


//     const float* depth_buffer = range_likelihood_->getDepthBuffer();
        for (int i=0, n=0; i<range_likelihood_->getRowHeight(); ++i)
        {
            for (int j=0; j<range_likelihood_->getColWidth(); ++j)
            {
                reference[n++] = 1;//depth_buffer[i*range_likelihood_->getWidth() + j];
            }
        }


        for (int ino=0; ino<num_interp; ino++) {
	  
	    std::cout<<"Interpolating pose no "<<(poseno-1)<<" and "<<poseno<<"."<<std::endl;
	  
            std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > poses;
            Eigen::Isometry3d interp_pose;

            double last_prop = ((double)ino)/(num_interp-1);
            interp_pose.setIdentity();
            Eigen::Quaterniond interp_rot;
            Eigen::Quaterniond last_rot(last_pose.rotation());
            Eigen::Quaterniond this_rot(this_pose.rotation());
            interp_rot = last_rot.slerp(last_prop,this_rot);

            interp_pose.setIdentity();
            Eigen::Vector3d last_trans=last_pose.translation();
            Eigen::Vector3d this_trans=this_pose.translation();
            Eigen::Vector3d interp_trans = last_prop*last_trans +  (1-last_prop)*this_trans;
            interp_pose.translate(interp_trans);
            interp_pose.rotate(interp_rot);
            poses.push_back(interp_pose);

            range_likelihood_->computeLikelihoods (reference, poses, scores);

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc_out (new pcl::PointCloud<pcl::PointXYZRGB>);

            range_likelihood_->getPointCloud (pc_out,true,camera_->getPose (),true);

// 	    Eigen::Matrix4f cameraT = camera_->getPose ().matrix ().cast<float> ();
    
	    pc_out->sensor_origin_ = Eigen::Vector4f(0,0,0,0);//cameraT.rightCols (1);
// 	    Eigen::Quaternion<float> quat (cameraT.block<3,3> (0,0));
	    pc_out->sensor_orientation_ = Eigen::Quaternionf(1,0,0,0);//quat;
	    
	    std::stringstream pcd_fname;
	    pcd_fname << target_pcd_dir << "/" << target_pcd_basename << std::setw(3) << std::setfill('0') << poseno << "_" << std::setw(3) << std::setfill('0') << ino << ".pcd";
            pcl::PCDWriter writer;
//             std::string pcd_fname = target_pcd_dir +"/"+target_pcd_basename + std::to_string(poseno) +"_"+std::to_string(ino)+".pcd";
            writer.write (pcd_fname.str(), *pc_out,false);



        }
        poseno++;

        delete[] reference;

        last_pose=this_pose;

    }

//     exit(0);

    return 0;
}
