// This code reads all of orgnized point clouds from a directory (pcd_files_path), estimates normal for each cloud, removes NaN and infinite points,
// estimates FPFH for central point of each point cloud and saves the FPFH. Finally it makes a cloud of FPFHs and saves it as a CloudofFPFHs.pcd file.
#include <iostream>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <boost/program_options.hpp>
#include <sstream>
#include <boost/shared_ptr.hpp>
#include <ostream>
#include <fstream>
using namespace std;
float MaxDepthChangeFactor=0.4f;
float NormalSmoothingSize=10.0f;
int number_of_files=355;
std::string pcd_files_path="/home/software/scaper2/data/FPFH-Dataset/Organized_pcd";
boost::filesystem::path pcd_path;


 int main (int argc, char **argv){
   ofstream myfile;
   myfile.open ("NameList.txt");
   pcl::PointCloud<pcl::FPFHSignature33>::Ptr CloudofFPFHs (new pcl::PointCloud<pcl::FPFHSignature33> ());
   CloudofFPFHs->width = number_of_files; //nomber of input point clouds
   CloudofFPFHs->height = 1;
   //read PCD files from directory one by one
   pcd_path=boost::filesystem::path(pcd_files_path);
   if(!boost::filesystem::exists(pcd_path))
   {
      std::cerr << "PCD file path does not exist"<<std::endl;
      exit(1);
   }
   std::vector<std::string> pcds;
   std::vector<std::string> saveFPFH;

   for(boost::filesystem::directory_iterator it(pcd_path),end;it!=end;++it)
   {
     
      std::string thisfile;
      std::string filenames;
      thisfile = it->path().string();
      filenames = it->path().filename().string();
      pcds.push_back(thisfile);
      saveFPFH.push_back(filenames);
      myfile <<filenames<< "\n";
   }
   myfile.close();
      
   int in=0;
   for(auto it=pcds.begin();it!=pcds.end();it++)
   {
      std::cout<<*it<<std::endl;
      std::cout<<"saved fpfh name will be"<<saveFPFH[in]<<std::endl;
      pcl::PCDReader reader;
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
      // Read in the cloud data
      reader.read (*it, *cloud);
      std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;
      // caculate normal
      pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>());
      pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
      ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
      ne.setMaxDepthChangeFactor(MaxDepthChangeFactor);
      ne.setNormalSmoothingSize(NormalSmoothingSize);
      ne.setInputCloud(cloud);
      ne.compute(*cloud_normals);

      std::cout<< "Normals are calculated \n";
      std::cout<<"\nsize of main cloud is:"<<cloud->points.size();
      std::cout<<"\nsize of main normal is:"<<cloud_normals->points.size();
  
      pcl::PointCloud<pcl::PointXYZ>::Ptr modified_cloud (new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::Normal>::Ptr modified_normals(new pcl::PointCloud<pcl::Normal>);
      //Remove NaN and infinite points
      for (size_t i = 0; i < cloud->points.size(); i++)
      {
	  if (pcl::isFinite<pcl::Normal>(cloud_normals->points[i]) && pcl::isFinite<pcl::PointXYZ>(cloud->points[i]))
	  { 
	    modified_normals->points.push_back(cloud_normals->points[i]);
	    modified_cloud->points.push_back(cloud->points[i]);
	  }
      }
   
      std::cout<<"\nsize of modified_cloud is:"<<modified_cloud->points.size();
      std::cout<<"\nsize of modifie normal is:"<<modified_normals->points.size();
      //Find the centroid
      Eigen::Vector4f modified_centroid;
      pcl::compute3DCentroid(*modified_cloud, modified_centroid);
      std::cout<<"\nmodified_centroid is:"<<modified_centroid;
      //Estimate FPFH
      pcl::FPFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
      fpfh.setSearchSurface(modified_cloud);
      fpfh.setInputNormals (modified_normals);
      pcl::PointCloud<pcl::PointXYZ>::Ptr keypoint (new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointXYZ key;
      key.x=modified_centroid[0];
      key.y=modified_centroid[1];
      key.z=modified_centroid[2];
      keypoint->points.push_back(key);
      fpfh.setInputCloud (keypoint);
      pcl::PointCloud<pcl::FPFHSignature33>::Ptr fpfh_output (new pcl::PointCloud<pcl::FPFHSignature33> ());
      fpfh.setRadiusSearch (1);
      fpfh.compute (*fpfh_output);
      //Add FPFH to Cloud of FPFHs
      CloudofFPFHs->points.push_back(fpfh_output->points[0]);   
      //save the FPFH
      std::stringstream ss;
      ss<<saveFPFH[in]; 
      in ++;
      pcl::io::savePCDFileASCII<>(ss.str(),*fpfh_output);
      std::cout<< "\nFPFH is saved\n";
   }

   //Save Cloud of FPFHS
   std::stringstream sss;
   sss<<"CloudOfFPFHs";
   pcl::io::savePCDFileASCII<>(sss.str(),*CloudofFPFHs);
   std::cout<< "\ncloudofFPFHs is saved\n";
   return 0;
 } 
