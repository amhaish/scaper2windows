//based on tutorial code
#include <pcl/io/openni_grabber.h>
#include <pcl/io/grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#ifdef WITH_DEPTHSENSE_DS
#include "depth_sense_grabber.h"
#endif
#ifdef WITH_DEPTHSENSE_PCL
#include <pcl/io/depth_sense_grabber.h>
#endif

#include <boost/program_options.hpp>
#include <deque>          
#include <PCLEuclideanSegmenter.h>
#include <memory>
#include <pcl/io/pcd_io.h>


#include <sstream>

#include <boost/shared_ptr.hpp>

namespace po = boost::program_options;

 class SimpleDepthSenseRecorder
 {
   public:
     SimpleDepthSenseRecorder (   std::string base_filename,double num_frames,double buffer_size) : viewer ("PCL DepthSenhse Viewer (Recorder)"),base_filename(base_filename),num_frames(num_frames),buffer_size(buffer_size),frames_written(0) {}

     void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud)
     {
       
       if(num_frames<=frames_written)return;
       
       if (!viewer.wasStopped())
         viewer.showCloud (cloud);
       
       buffer.push_front(cloud);
       if(buffer.size()>=buffer_size){
	write(buffer.back());
	buffer.pop_back();
	 
       }
       if(frames_written+buffer.size()>=num_frames){
	 interface->stop();
	  while(!buffer.empty()){
	    write(buffer.back());
	    buffer.pop_back();
	  }
	  
       }
       
     }
     
     void write(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud){
       
       std::stringstream ss;
       ss<<base_filename<<"_"<<setfill('0') << setw(6) << frames_written<<".pcd";
       pcl::io::savePCDFileBinaryCompressed(ss.str(),*cloud);
       frames_written++;
       
     }

     void run ()
     {
       interface.reset(new pcl::OpenNIGrabber);

       boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f =
         boost::bind (&SimpleDepthSenseRecorder::cloud_cb_, this, _1);

       interface->registerCallback (f);

       interface->start ();

       while (!viewer.wasStopped())
       {
         boost::this_thread::sleep (boost::posix_time::seconds (1));
       }

       interface->stop ();
     }

     pcl::visualization::CloudViewer viewer;
   std::string base_filename;
    
       double num_frames;
    double buffer_size;
    
    std::deque<pcl::PointCloud<pcl::PointXYZ>::ConstPtr> buffer;
    int frames_written;
    
     boost::shared_ptr<pcl::Grabber> interface;
    
     
 };

 
 
po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Proto Viewer" );
    desc.add_options()
    ( "help,h","Get help. You probably need it. But not that kind of help." )
    ( "verbose,v",po::value<int>()->default_value ( 0 ),"Verbosity level." )
    
    ( "base_filename",po::value<std::string>()->default_value (""),"Base of filename to generate PCD files from." )
    
    ( "num_frames",po::value<int>()->default_value ( 15 ),"Number of frames to record." )
    ( "buffer_size",po::value<int>()->default_value ( -1 ),"Number of frames to buffer." )
    

    ;

    po::positional_options_description p;
     p.add ( "base_filename", 1 );
//      p.add ( "filenames", 15 );
   p.add("num_frames", 1);
//    p.add("buffer_size", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm );

    return vm;
}


 
 int main (int argc, char **argv)
 {
    po::variables_map vm=args ( argc,argv );
   std::string base_filename=vm["base_filename"].as<std::string>();
    if ( base_filename =="" ) {
        std::cerr<<"Need base filename"<<std::endl;
	return 1;
    }
       double num_frames = vm["num_frames"].as<int>();
    double buffer_size = vm["buffer_size"].as<int>();
    if(buffer_size<0)buffer_size=num_frames;
   
   SimpleDepthSenseRecorder v(base_filename,num_frames,buffer_size);
   v.run ();
   return 0;
 }
