
srcFiles = dir('/software/scaper2/data/FPFH-Dataset/Original_dataset/*.pcd');  % the folder in which pcd exists
for i = 1 : length(srcFiles)
    filename = strcat('/software/scaper2/data/FPFH-Dataset/Original_dataset/',srcFiles(i).name); 
    filename
cloud = readPcd (filename);
    ocloud = NaN(480,640,3); % forget colour
    cloud_size = size(cloud);
    for ind = 1:cloud_size(1)
        point = cloud(ind,:);
        X=point(5);
        Y=point(6);
        ocloud(Y,X,:)=point(1:3);
    end

    %now check how many non-nan there are
    size(find(~isnan(ocloud(:,:,1))))
    cloud_size

    %now save to disk
    finalfilenameT = strcat('/software/scaper2/data/FPFH-Dataset/Organized_pcd/',srcFiles(i).name);

    savepcd(finalfilenameT,ocloud,'ascii') % save ascii
    
    
end
