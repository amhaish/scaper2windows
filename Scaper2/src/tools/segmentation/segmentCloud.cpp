#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <limits>
#include <boost/thread/thread.hpp>
#include <pcl/segmentation/euclidean_cluster_comparator.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/centroid.h>
#include "depth_sense_grabber.h"


#include <pcl/filters/statistical_outlier_removal.h>


#define isDepthValid(x) (!(boost::math::isnan)(x.z))




clock_t clk;
#define BENCHMARK(x)  clk = clock(); {x} printf("Line %d took %lums to execute\n", __LINE__, clock()-clk);


 class SegmentCloud
 {
   public:
     SegmentCloud () : viewer ("PCL OpenNI Viewer") {
     	statisticalFilterIsEnabled = true;
     	viewerIsEnabled = false;
     }

     void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&);
     

     void run ();
     
     void enableStatisticalFilter() {
     	statisticalFilterIsEnabled = true;
     }

     void enableViewer() {
     	viewerIsEnabled = true;
     }

  private:
	bool statisticalFilterIsEnabled;
	bool viewerIsEnabled;  	
     //boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    pcl::visualization::PCLVisualizer viewer;
    boost::mutex cloud_mutex;

     // pcl::visualization::CloudViewer viewer;
 };

void SegmentCloud::cloud_cb_ (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &constCloud)
{
  
	static pcl::PointCloud<pcl::PointXYZ>::Ptr background(new pcl::PointCloud<pcl::PointXYZ>(*constCloud));

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(*constCloud));
/*
    for (int i = 0; i < cloud->width; i++)
    {
      for (int j = 0; j < cloud->height; j++)
      {
        if (((background->at(i, j).z * 0.8 < cloud->at(i, j).z) && isDepthValid(background->at(i, j))) || !isDepthValid(cloud->at(i, j)))
        {
          // Background

          // Point more distant that current background, set as new background
          if (background->at(i, j).z < cloud->at(i, j).z)
            background->at(i, j).z = cloud->at(i, j).z;

          // Set point x and z-value to NaN, indicating it is not part of the foreground, but keeping the cloud organized
          cloud->at(i, j).x = std::numeric_limits<float>::quiet_NaN();
          cloud->at(i, j).z = std::numeric_limits<float>::quiet_NaN();
           
        }
        else
        {
          // Foreground

          if (!isDepthValid(background->at(i, j)))
            background->at(i, j).z = cloud->at(i, j).z;
        }
      }
    }
*/
    boost::shared_ptr<std::vector<pcl::PointIndices>> labelsIndices(new std::vector<pcl::PointIndices>());
    pcl::PointCloud<pcl::Label>::Ptr labels(new pcl::PointCloud<pcl::Label>());

    pcl::Label ltmp;
    ltmp.label=0;

    labels->points.resize(cloud->points.size(),ltmp);
    std::vector<bool> exclude_labels;
    exclude_labels.resize(1,false);
    
    //-------------Statistical Filtering------------------------
    
    if (statisticalFilterIsEnabled){	
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	    sor.setKeepOrganized(true);
	    sor.setInputCloud(cloud);
	    sor.setMeanK(10);
	    sor.setStddevMulThresh(1.0);
	    sor.filter(*cloud);
    }
    pcl::PointCloud<pcl::PointXYZ>::CloudVectorType clusters;


    pcl::EuclideanClusterComparator<pcl::PointXYZ, pcl::Normal, pcl::Label>::Ptr
      comparator(new pcl::EuclideanClusterComparator<pcl::PointXYZ, pcl::Normal, pcl::Label>());
    comparator->setDistanceThreshold(0.08f, true);
    comparator->setInputCloud(cloud);
    comparator->setLabels(labels);
    comparator->setExcludeLabels (exclude_labels);

    pcl::OrganizedConnectedComponentSegmentation<pcl::PointXYZ, pcl::Label> segmenter(comparator);

	pcl::PointCloud<pcl::Label>::Ptr elabels(new pcl::PointCloud<pcl::Label>());
	boost::shared_ptr<std::vector<pcl::PointIndices>> elabelsIndices(new std::vector<pcl::PointIndices>());

    segmenter.setInputCloud(cloud);
  //  BENCHMARK(segmenter.segment(*labels, *labelsIndices);)
    segmenter.segment(*elabels, *elabelsIndices);


//    pcl::PointCloud<pcl::PointXYZ>::Ptr labelledCloud(new pcl::PointCloud<pcl::PointXYZ>()); 


    cout << "elabelsIndices->size(): " << elabelsIndices->size() << endl;
    cout << "----------------elabelsIndices-----------------------------------" << endl;
    for (int i = 0; i < elabelsIndices->size(); i++){
      if ((*elabelsIndices)[i].indices.size () > 25 )
        cout << "elabelsIndices "  << i << " size: " << (*elabelsIndices)[i].indices.size () << endl;
        
    }
    cout << "---------------------------------------------------" << endl;

    for (size_t i = 0; i < elabelsIndices->size(); i++)
    {
       if ((*elabelsIndices)[i].indices.size () > 25){   
          pcl::PointCloud<pcl::PointXYZ> cluster;
          pcl::copyPointCloud(*cloud, (*elabelsIndices)[i].indices, cluster);
          clusters.push_back(cluster);
       }

    }
    
    Eigen::Matrix< float, 4, 1 > centroid;

   
  //   cout << "cluster size " << clusters.size() << endl;
    
      //cout << centroid.x << endl;
    
    
    if (viewerIsEnabled) {

	    char name[1024];
	    unsigned char red [6] = {255,   0,   0, 255, 255,   0};
	    unsigned char grn [6] = {  0, 255,   0, 255,   0, 255};
	    unsigned char blu [6] = {  0,   0, 255,   0, 255, 255};

	   // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	  
	    //for (size_t i = 0; i < clusters.size (); i++)
	    if (clusters.size() > 0)
	      cout << "cluster size " << clusters.size() << endl;
	    cloud_mutex.lock();

	    for (size_t i = 0; i < clusters.size(); i++)
	    {
	      pcl::PointCloud<pcl::PointXYZ>::Ptr ccc=boost::make_shared<pcl::PointCloud<pcl::PointXYZ> >(clusters[i]);
	 //     pcl::compute3DCentroid ( clusters[i], centroid );
 
	      /*
	      cout << clusters[i].points[10].x << endl;
	      cout << clusters[i].points[10].y << endl;
	      cout << clusters[i].points[10].z << endl;
	*/      
	      //cout << name << " " <<  int (i) << endl;
	      //cout << "cloud_mutex cb: " << endl;
	      std::stringstream ss;
	      ss<<"pc"<<i;
	     // cout << "cloud_mutex after lock in cb: "  << endl;
	      pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color0(ccc,red[i%6],grn[i%6],blu[i%6]);
	      if (!viewer.updatePointCloud (ccc,color0,ss.str()))
	        viewer.addPointCloud (ccc,color0,ss.str());
	      viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, ss.str());
	      viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, ss.str());
	      
	     // cout << "cloud_mutex after unlock in cb: " << endl;
	    }
	    	cloud_mutex.unlock();
	  //  if (!viewer.wasStopped())
	  //    viewer.showCloud (cloud);
     }
     

  }


void SegmentCloud::run()
{
	pcl::Grabber* interface = new pcl::DepthSenseGrabber("");
	// pcl::Grabber* interface = new pcl::OpenNIGrabber();
	boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f =
	 boost::bind (&SegmentCloud::cloud_cb_, this, _1);



	interface->registerCallback (f);

	interface->start ();

	while (!viewer.wasStopped() && viewerIsEnabled){

	//viewer.spinOnce (100);
	// cout << "cloud_mutex run: "  << endl;
	if(cloud_mutex.try_lock ()){
	 

	//  cout << "cloud_mutex after lock in run: "  << endl;
	viewer.spinOnce (10);
	cloud_mutex.unlock ();
	}
	// cout << "cloud_mutex after unlock in run: " << endl;
	// boost::this_thread::sleep (boost::posix_time::seconds (1));
	}

	interface->stop ();
}

 int main ()
 {
   SegmentCloud v;
   v.enableViewer();
   v.run ();
   return 0;
 }