#include <PCLObject.h>
#include <iostream>

std::ostream& operator<<(std::ostream& stream,
                         const PCLObject& obj) {
    stream<<"<<PCLObject: key:"<<std::setw(5)<<obj.key;
    stream<<" width:"<<obj.width;
    stream<<" height:"<<obj.height;
    stream<<" depth:"<<obj.depth;
    stream<<" size:"<<obj.size;
    stream<<" distance:"<<obj.distance;
    stream<<" centroid:"<<obj.centroid[0]<<","<<obj.centroid[1]<<","<<obj.centroid[2];
    stream<<" cloud-size:"<<obj.objectCloud->size();
    stream<<">>";
    return stream;

}
