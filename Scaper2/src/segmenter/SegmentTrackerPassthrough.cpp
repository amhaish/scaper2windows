#include <SegmentTrackerPassthrough.h>
#include <pcl/common/centroid.h>

SegmentTrackerPassthrough::SegmentTrackerPassthrough(){}

float SegmentTrackerPassthrough::findSize(PCLObject::Ptr obj){
    float objDist = sqrt(obj->centroid[0]*obj->centroid[0] + obj->centroid[1]*obj->centroid[1] + obj->centroid[2]*obj->centroid[2]);
    obj->distance=objDist;
    float objCloudSize = obj->objectCloud->size();
    return objDist*objCloudSize;
}

std::vector<PCLObject::Ptr> SegmentTrackerPassthrough::Execute(PointCloudWithNormals::Vector clouds){

  std::vector<PCLObject::Ptr> objects;

  for(int i=0;i<clouds.size();i++){
    PCLObject::Ptr obj(new PCLObject());
    obj->track_id = i;
    obj->isMatched = false;
    obj->objectCloud = clouds[i].cloud;//boost::make_shared<pcl::PointCloud<SystemPoint> >(clouds[i]);
    obj->normalCloud = clouds[i].normals;
    pcl::compute3DCentroid(*(clouds[i].cloud),obj->centroid);
    obj->size = findSize(obj);
    objects.push_back(obj);
  }

  return objects;
}