#include <SegmentTrackerAssoc1.h>
#include <vis/data_assoc_vis.h>
#include <PCLEuclideanSegmenter.h>
#include <algorithm>
#include <pcl/PointIndices.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/segmentation/comparator.h>
#include <PCLBoundaryExtraction.h>
#include <pcl/geometry/geometry.h>
#include <SegmentTrackerAssoc1.h>
#define NUM_OBJECTS 3	


int stvvvv=3;

#define DIVVV 10
SegmentTrackerAssoc1::SegmentTrackerAssoc1(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m){
  this->viewer_mutex=&m;
  this->units_divisor=units_divisor;
  last_size=0;
  this->viewer=vis;//new pcl::visualization::PCLVisualizer("Objects Tracker Visualizer");
  //this->viewer->setBackgroundColor(1.0,1.0,1.0); //test this: should look better when printed to paper
}

void SegmentTrackerAssoc1::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::mutex* viewer_mutex)
{
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};

  for (size_t i = 0; i < clouds.size(); i++)
  {
    viewer_mutex->lock();
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = "TRK" + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
    viewer_mutex->unlock();
  }
  /*
  for(unsigned int i=clouds.size();i<last_size;i++){
    std::string s = "TRK" + std::to_string(i);
    viewer->removePointCloud(s.c_str());
  }
  */
  last_size=clouds.size();
}

inline void SegmentTrackerAssoc1::removePreviousDataFromScreen()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }
}

/*float CalcDistanceBetweenSegments(PCLObject::Ptr cloud1 , Eigen::Vector4f centroid){
   pcl::OrganizedConnectedComponentSegmentation<SystemPoint, pcl::Label>::ComparatorPtr comparator;
   DepthSensitiveComparator<SystemPoint, pcl::Normal, pcl::Label>::Ptr ds_comparator(new DepthSensitiveComparator<SystemPoint, pcl::Normal, pcl::Label>());
//    ds_comparator->setDistanceThreshold(this->DistanceThreshold, true);
//    ds_comparator->setAngularThreshold(pcl::deg2rad(this->AngularThreshold));
//    ds_comparator->setInputNormals();
   ds_comparator->setInputCloud(cloud1->objectCloud);
//    ds_comparator->setNANNormalsAreEdges(this->nan_normals_are_edges); 
   comparator=ds_comparator;
  
  pcl::OrganizedConnectedComponentSegmentation<SystemPoint, pcl::Label> segmentationAlgorithm(comparator);
   pcl::PointCloud<pcl::Label>::Ptr elabels(new pcl::PointCloud<pcl::Label>());
   boost::shared_ptr<std::vector<pcl::PointIndices> > elabelsIndices(new std::vector<pcl::PointIndices>());
   
   segmentationAlgorithm.setInputCloud(cloud1->objectCloud);
   segmentationAlgorithm.segment(*elabels, *elabelsIndices);
   this->clusters.clear();
 
 	 PointCloudWithNormals cluster;
 	 pcl::copyPointCloud(cloud1->objectCloud, (*elabelsIndices)[i].indices, *cluster.cloud);
 	 pcl::copyPointCloud(cloud1->normalCloud, (*elabelsIndices)[i].indices, *cluster.normals);
 ////////////////////////////////////////////////////////////////////////////////////////////////
	 float distanceToTheCentroid= 0 ;
	 pcl::PointIndices boundary_indices;
	 float maxDistance = 0 ; 
 	 segmentationAlgorithm.findLabeledRegionBoundary((*elabelsIndices)[i].indices[0],elabels,boundary_indices);
 	 for(size_t j=0 ; j<boundary_indices.indices.size(); j++)
 	 { 
 	    distanceToTheCentroid=(cloud1->objectCloud->points[boundary_indices.indices[j]]-centroid).norm();
 	    if (distanceToTheCentroid > maxDistance)
	      maxDistance == distanceToTheCentroid;
 	 }
 	 return maxDistance;
 }
*/
bool AreSegmentsCloseEnough (pcl::PointCloud<SystemPoint>::Ptr cloud1, pcl::PointCloud<SystemPoint>::Ptr cloud2 ,Eigen::Vector4f centroid1, Eigen::Vector4f centroid2 ){
  float DistanceTreshhold = 0.005; 
  float Distnace = 0;
  float BestDistance =std::numeric_limits<float>::max();
  pcl::PointIndices boundary_indices1;
  pcl::PointIndices boundary_indices2;
  PCLBoundaryExtraction boundary;
  boundary_indices1=boundary.BoundaryExtraction(cloud1);
  boundary_indices2=boundary.BoundaryExtraction(cloud2);
  for (int i=0 ;i< boundary_indices1.indices.size(); i++){
  SystemPoint key;
  key.x=centroid2[0];
  key.y=centroid2[1];
  key.z=centroid2[2];
  SystemPoint key2;
  key2.x=cloud1->points[boundary_indices1.indices[i]].x;
  key2.y=cloud1->points[boundary_indices1.indices[i]].y;
  key2.z=cloud1->points[boundary_indices1.indices[i]].z;
  Distnace=sqrt((key2.x-key.x)*(key2.x-key.x) + (key2.y-key.y)*(key2.y-key.y) + (key2.z-key.z)*(key2.z-key.z));
  if (Distnace < BestDistance)
    BestDistance = Distnace;
   }
  float cloud1ToCentroid2Dis = BestDistance;
  std::cout<<"cloud1ToCentroid2Dis is : "<<cloud1ToCentroid2Dis<<std::endl;
  BestDistance =std::numeric_limits<float>::max();
  for (int i=0 ;i< boundary_indices1.indices.size(); i++){
  SystemPoint key;
  key.x=centroid1[0];
  key.y=centroid1[1];
  key.z=centroid1[2];
  SystemPoint key2;
  key2.x=cloud2->points[boundary_indices1.indices[i]].x;
  key2.y=cloud2->points[boundary_indices1.indices[i]].y;
  key2.z=cloud2->points[boundary_indices1.indices[i]].z;
  Distnace=sqrt((key2.x-key.x)*(key2.x-key.x) + (key2.y-key.y)*(key2.y-key.y) + (key2.z-key.z)*(key2.z-key.z));
  if (Distnace < BestDistance)
    BestDistance = Distnace;
   }
  float cloud2ToCentroid1Dis = BestDistance;
  std::cout<<"cloud2ToCentroid1Dis is : "<<cloud2ToCentroid1Dis<<std::endl;
  std::cout<<"distance is : "<<abs(cloud1ToCentroid2Dis-cloud2ToCentroid1Dis)<<std::endl;
  if(abs(cloud1ToCentroid2Dis-cloud2ToCentroid1Dis)<=DistanceTreshhold)
    return true;
  else return false;
}



float CalcFirstMoment(PCLObject::Ptr obj){
    float objDist = sqrt(obj->centroid[0]*obj->centroid[0] + obj->centroid[1]*obj->centroid[1] + obj->centroid[2]*obj->centroid[2]);
    obj->distance=objDist;
    float objCloudSize = obj->objectCloud->size();
    
    return objDist*objCloudSize;
}

std::vector <float> CalcSecondMoment(PCLObject::Ptr obj){
    pcl::MomentOfInertiaEstimation <pcl::PointXYZRGBA> feature_extractor;
    feature_extractor.setInputCloud (obj->objectCloud);
    feature_extractor.compute ();
    std::vector <float> moment_of_inertia;
    feature_extractor.getMomentOfInertia (moment_of_inertia);
    return moment_of_inertia;
}

std::vector<PCLObject::Ptr> SegmentTrackerAssoc1::Execute(PointCloudWithNormals::Vector clouds){
  int findex = 0;
  static int lastFrameIndex; // not sure about this ???? 
    struct Features
  {
  pcl::PointCloud<SystemPoint>::Ptr cloud;
  Eigen::Vector4f centroid;
  float firstMoment;
  bool combined = false;
  std::pair<int,int> objectsNo;
  int objectNo;
  };	
  
  std::vector<Features> features;
  static std::vector<Features> lastFramefeatures;
  std::vector<PCLObject::Ptr> objects;
  std::vector<PCLObject::Ptr> selectedObjects;
  static std::vector<PCLObject::Ptr> lastFrameBuf;
  //static std::vector<PCLObject::Ptr> lastFrameCombinations;

  std::vector<PCLObject::Ptr> primitives;
  std::vector <std::pair<int,int> > matches;
  
  for (size_t i = 0; i < clouds.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject());
    obj->track_id = -1;
    obj->isMatched = false;
    obj->objectCloud = clouds[i].cloud;//boost::make_shared<pcl::PointCloud<SystemPoint> >(clouds[i]);
    obj->normalCloud = clouds[i].normals;
    pcl::compute3DCentroid(*(clouds[i].cloud),obj->centroid);

//  objFirstMom = CalcFirstMoment(obj);
//  obj->firstMoment = objFirstMom;
 /* objSecondMom = CalcSecondMoment(obj);
    obj->secondMoment= objSecondMom;
    std::cout<< "\n size of cloud is :" <<obj->objectCloud->size()<<std::endl;
    std::cout<< "\n size of vector is :" <<objSecondMom.size()<<std::endl;
    std::cout<< "\n second momen is :" <<std::endl;
    for (std::vector<float>::const_iterator i = objSecondMom.begin(); i != objSecondMom.end(); ++i)
      std::cout << *i << ' ';*/ 
  float zoomAmount = ((float)zoomPercent/100)*35;
    obj->centroid[0] = obj->centroid[0] - zoomAmount;
//     obj->centroid[1] = obj->centroid[1] - zoomAmount;
//     obj->centroid[2] = obj->centroid[2] - zoomAmount;
    if(zoomPercent == 0){
    //  if(objSize > 300)
	  objects.push_back(obj);
    }
    else{
      if(/*objSize > 3000 &&*/ obj->centroid[2] > 0.8 * units_divisor)
	  objects.push_back(obj);
    }
     
    //double this_salence = objSize / obj->centroid.norm() ;
    
    //salience.push_back(this_salience);
    //if(objFirstMom > 300 && obj->centroid[2] > 0.420*units_divisor) 
    

  }
  
  // this is a really basic way of trying to get some kind of consistency in data association.
  // a better way would be to check the closeness of all objects to each other, use some kind of threshold to
  // disocciate ones that are not close, then choose the optimal match
  
  int n=0;
/*
  while(objects.size()>0&&primitives.size()<NUM_OBJECTS){
      unsigned int besti=0;
      double bestf = 0;//0=std::numeric_limits<double>::max();
      for(unsigned int i=0;i<objects.size();i++){
	  if(salience[i]>bestf){
	   bestf=salience[i];
	   cout << "bestf = " << bestf << endl;
	   besti=i;
	   cout << "besti = " << besti << endl;
	  }
      }
      objects[besti]->track_id=n++;
      cout << "object track id = " << objects[besti]->track_id << endl;
      primitives.push_back(objects[besti]);
       objects.erase(objects.begin()+besti);
      salience.erase(salience.begin()+besti);
  }
*/

// --------------------------------------------------------------------------------------------------------------------------------------------------
  int indexes[3];
  int x = 0;
  // Selecting 3 closest object to sensor for comparing origin difference
  while(objects.size() > 0 && selectedObjects.size() < 3  && x < objects.size()){
    float bestScore = std::numeric_limits<float>::max();
    int index;
    float currentScore;
    for(unsigned int i = 0; i < objects.size(); i++){
      if(objects[i]->isMatched == true) continue;
      currentScore= sqrt(objects[i]->centroid[0]*objects[i]->centroid[0] + objects[i]->centroid[1]*objects[i]->centroid[1] + objects[i]->centroid[2]*objects[i]->centroid[2]);
      //currentScore = abs(objects[i]->centroid[0]*objects[i]->centroid[1]*objects[i]->centroid[2]);
      if(currentScore < bestScore){
	bestScore = currentScore;
	index = i;
      }
    }
    selectedObjects.push_back(objects[index]);
    indexes[x] = index;
    objects[index]->isMatched = true;
    //calculate features for selected objects
    Features f;    
    f.cloud= objects[index]->objectCloud;
    f.centroid=objects[index]->centroid;
    f.firstMoment= CalcFirstMoment(objects[index]);
    f.objectNo = x;
    f.combined = false;
    
   // if(f.firstMoment>=300)
    features.push_back(f);
    findex ++;
    x++;
  }
  findex+=1; //because I said so
  for(unsigned int i = 0; i < objects.size(); i++){
      objects[i]->isMatched = false;
  }
  // examine merging----------------------------------------------------------------------------------
  std::vector<std::pair<float,float>> distAndMoment;
  float bestCost=std::numeric_limits<float>::max();
  float cost = 0;
  float costTreshold = 0.1;
  float distance=0;
  float moment=0;
  std::vector<int> permutation;
  std::vector<int> LastPermutation;
  std::vector<int> BestPermutation;
  std::vector<float> BestPermutationCosts;
  std::vector <std::pair<int,int> > matche;
  int * myints; 
  float c = 0 ;
  int lastFrameSize =lastFramefeatures.size(); //lastFrameBuf.size();
  int thisFrameSize = 0 ;
  Features ff;
  bool wrongAssoc = false;
  static int aa= 0;
// calcualating combined features
  int faeturesSize = features.size();
  if(faeturesSize>1){
    for(size_t i=0;i<faeturesSize;i++) 
     for (size_t j=i+1;j<faeturesSize;j++){
       if (i!=j && AreSegmentsCloseEnough(features[i].cloud,features[j].cloud ,features[i].centroid, features[j].centroid)){
	 ff.firstMoment = features[i].firstMoment+features[j].firstMoment;
	 ff.centroid = (features[i].centroid+features[j].centroid)/2;
	 ff.objectsNo = {i,j};
	 ff.combined = true;
	 features.push_back(ff);
      }
    }
  }
 thisFrameSize = features.size();
 int counter=0;
  if(lastFrameSize>0 && thisFrameSize >0 && lastFrameSize >= thisFrameSize )
   {
      myints= new int [lastFrameSize];
      for (size_t i = 0; i<lastFrameSize;i++)
	myints[i] = i;
      
      do {
          permutation.erase(permutation.begin(), permutation.end());
	  for(size_t i= 0; i<thisFrameSize; i++)
	    permutation.push_back(myints[i]); 
	  
	  if(permutation != LastPermutation)
	  {
	    wrongAssoc = false;
	    for(size_t i= 0; i<thisFrameSize; i++)
	    {
	      if(features[i].combined==true && lastFramefeatures[myints[i]].combined==true)
          	wrongAssoc = true;
// 	      std::cout<<"myints "<<i<<"in run "<<counter<<"is "<< myints[i]<<std::endl;
 	      distance=(features[i].centroid-lastFramefeatures[myints[i]].centroid).norm();
	      moment=abs(features[i].firstMoment-lastFramefeatures[myints[i]].firstMoment);
	      moment/=10000;
	      std::cout<< "distance is : "<<distance<<std::endl;
      	      std::cout<< "moment is   : "<< moment<<std::endl;

//  	      std::cout<< "distanece andd momentas are "<< distance<< "  "<<moment<<std::endl;	
	      c=distance*distance + moment*moment;
// 	      BestPermutationCosts.push_back(c);
	      std::cout<< "c is : "<< c<<std::endl;
	      if (c >= costTreshold){
		permutation[i] = lastFrameSize + 1;
	      }else{
		cost+=c;
	      }
		
	    }
	    if(cost<bestCost && wrongAssoc == false)
	    {
	    BestPermutation = permutation;
	    bestCost=cost;  
	    }
	   else{
	     BestPermutationCosts.erase(BestPermutationCosts.begin() , BestPermutationCosts.end());
	    }
	    cost = 0;
	    counter++;
	    LastPermutation.erase(LastPermutation.begin(), LastPermutation.end());
	    for(size_t i= 0; i<thisFrameSize; i++)
	    {  
	      LastPermutation.push_back(myints[i]);
	    }
	  }
      } while ( std::next_permutation(myints,myints+lastFrameSize) );
      
    
      std::cout<<"last frame size :"<<lastFrameSize<<" this frame size is : "<<thisFrameSize<<std::endl;
      std::cout<<"best cost is : " << bestCost<<std::endl;
      std::cout<<"best association is :"<<std::endl;
      for(size_t i=0; i<BestPermutation.size(); i++)
	std::cout<<BestPermutation[i]<<" ";
      std::cout<<std::endl; 
  }
  else if(thisFrameSize>0 && lastFrameSize >0  && thisFrameSize >lastFrameSize)
  {
      myints= new int [thisFrameSize];
      for (size_t i = 0; i<thisFrameSize;i++)
	myints[i] = i;
      
      do { 
          permutation.erase(permutation.begin(), permutation.end());
	  for(size_t i= 0; i<lastFrameSize; i++)
	    permutation.push_back(myints[i]); 
	  
	  if(permutation != LastPermutation)
	  {
	    wrongAssoc = false;
	    for(size_t i= 0; i<lastFrameSize; i++)
	    {
	      if(lastFramefeatures[i].combined==true && features[myints[i]].combined==true)
          	wrongAssoc = true;
//  	      std::cout<<"myints "<<i<<"in run "<<counter<<"is "<< myints[i]<<std::endl;
 	      distance=(lastFramefeatures[i].centroid-features[myints[i]].centroid).norm();
	      moment=abs(lastFramefeatures[i].firstMoment-features[myints[i]].firstMoment);
	      moment/=10000;
//  	      std::cout<< "distanece andd momentas are "<< distance<< "  "<<moment<<std::endl;	
	      c=distance*distance + moment*moment;
// 	      BestPermutationCosts.push_back(c);
 	      if (c >= costTreshold){
		permutation[i] = thisFrameSize + 1;
	      }else{
		cost+=c;
	      }
	      
	    }
//    	    std::cout<<counter<<"cost is :" << cost<<std::endl;
	    if(cost<bestCost && wrongAssoc == false)
	    {
	    BestPermutation = permutation;
	    bestCost=cost;  
	    }
	   else{
	     BestPermutationCosts.erase(BestPermutationCosts.begin() , BestPermutationCosts.end());
	    }
	    cost = 0;
	    counter++;
	    LastPermutation.erase(LastPermutation.begin(), LastPermutation.end());
	    for(size_t i= 0; i<thisFrameSize; i++)
	    {  
	      LastPermutation.push_back(myints[i]);
	    }
	  }
      } while ( std::next_permutation(myints,myints+thisFrameSize) );
      
      std::cout<<"last features size :"<<lastFrameSize<<" this frame size is : "<<thisFrameSize<<std::endl;
      std::cout<<"best cost is : " << bestCost<<std::endl;
      std::cout<<"best association is :"<<std::endl;
      for(size_t i=0; i<BestPermutation.size(); i++)
	std::cout<<BestPermutation[i]<<" ";
      std::cout<<std::endl;
  }   
    
  if(BestPermutation.size()>0)
  {
    if(lastFrameSize >= thisFrameSize){
       for (size_t i= 0; i < BestPermutation.size(); i++)
      {
	if(BestPermutation[i] != lastFrameSize + 1){
	  if(features[i].combined==false){
 	  std::cout<<features[i].objectNo<<" is mached to " ;
	  if(lastFramefeatures[BestPermutation[i]].combined == false){
 	    std::cout<<lastFramefeatures[BestPermutation[i]].objectNo <<std::endl;
	    matche.push_back(std::pair<int,int>(features[i].objectNo,lastFramefeatures[BestPermutation[i]].objectNo));
	  }else{
 	    std::cout<<lastFramefeatures[BestPermutation[i]].objectsNo.first<<" and "<<lastFramefeatures[BestPermutation[i]].objectsNo.second<<std::endl;
	    matche.push_back(std::pair<int,int>(features[i].objectNo,lastFramefeatures[BestPermutation[i]].objectsNo.first));
	    matche.push_back(std::pair<int,int>(features[i].objectNo,lastFramefeatures[BestPermutation[i]].objectsNo.second));
	  }
	  }else{
          std::cout<<features[i].objectsNo.first<<" and "<<features[i].objectsNo.second<<" are mached to ";
	  if(lastFramefeatures[BestPermutation[i]].combined == false){
 	    std::cout<<lastFramefeatures[BestPermutation[i]].objectNo <<std::endl;
	    matche.push_back(std::pair<int,int>(features[i].objectsNo.first,lastFramefeatures[BestPermutation[i]].objectNo));
	    matche.push_back(std::pair<int,int>(features[i].objectsNo.second,lastFramefeatures[BestPermutation[i]].objectNo));
	  }else{
	    std::cout<< "errrrrrrrrrrrrrror"<<std::endl;
// 	    std::cout<<lastFramefeatures[BestPermutation[i]].objectsNo.first<<" and "<<lastFramefeatures[BestPermutation[i]].objectsNo.second<<std::endl;
//          matche.push_back(std::pair<int,int>(features[i].objectsNo.first,lastFramefeatures[BestPermutation[i]].objectsNo.first));
//          matche.push_back(std::pair<int,int>(i,lastFramefeatures[BestPermutation[i]].objectsNo.second));	
	  }
	 }
       } 
     }
    }
    else{
      for (size_t i= 0; i < BestPermutation.size(); i++)
      {
	if(BestPermutation[i] != thisFrameSize + 1){
	  if(features[BestPermutation[i]].combined==false){
 	  std::cout<<features[BestPermutation[i]].objectNo<<" is mached to " ;
	  if(features[BestPermutation[i]].combined == false){
 	    std::cout<<lastFramefeatures[i].objectNo <<std::endl;
	    matche.push_back(std::pair<int,int>(features[BestPermutation[i]].objectNo,lastFramefeatures[i].objectNo));
	  }else{
 	    std::cout<<lastFramefeatures[i].objectsNo.first<<" and "<<lastFramefeatures[i].objectsNo.second<<std::endl;
	    matche.push_back(std::pair<int,int>(features[BestPermutation[i]].objectNo,lastFramefeatures[i].objectsNo.first));
	    matche.push_back(std::pair<int,int>(features[BestPermutation[i]].objectNo,lastFramefeatures[i].objectsNo.second));
	  }
	  }else{
          std::cout<<features[BestPermutation[i]].objectsNo.first<<" and "<<features[BestPermutation[i]].objectsNo.second<<" are mached to ";
	  if(lastFramefeatures[i].combined == false){
 	    std::cout<<lastFramefeatures[i].objectNo <<std::endl;
	    matche.push_back(std::pair<int,int>(features[BestPermutation[i]].objectsNo.first, lastFramefeatures[i].objectNo));
	    matche.push_back(std::pair<int,int>(features[BestPermutation[i]].objectsNo.second,lastFramefeatures[i].objectNo));
	  }else{
	    std::cout<< "errrrrrrrrrrrrrror"<<std::endl;
// 	    std::cout<<lastFramefeatures[BestPermutation[i]].objectsNo.first<<" and "<<lastFramefeatures[BestPermutation[i]].objectsNo.second<<std::endl;
//          matche.push_back(std::pair<int,int>(features[i].objectsNo.first,lastFramefeatures[BestPermutation[i]].objectsNo.first));
//          matche.push_back(std::pair<int,int>(i,lastFramefeatures[BestPermutation[i]].objectsNo.second));	
	  }
	}
      }
    }
   }
  }
  
//   for(int i=0; i< matche.size();i++){
//     std::cout<<"maches are " <<matche[i].first <<"  "<< matche[i].second <<std::endl;
//   }

/*  
  float dist;
  int bestj;
  
  // Tracking selected 3 objects
  if(selectedObjects.size() > 0 && lastFrameBuf.size() > 0){
      float smallestDist = std::numeric_limits<float>::max();	
      
      for(unsigned int i = 0; i < selectedObjects.size(); i++){
// 	  if(selectedObjects[i]->isMatched == true) continue;
	  
	  for(unsigned int j = 0; j < lastFrameBuf.size(); j++){
	      if(lastFrameBuf[j]->isMatched == true) continue;
	      
	      //dist = sqrt( (selectedObjects[i]->centroid[0]-lastFrameBuf[j]->centroid[0])*(selectedObjects[i]->centroid[0]-lastFrameBuf[j]->centroid[0]) + (selectedObjects[i]->centroid[1]-lastFrameBuf[j]->centroid[1])*(selectedObjects[i]->centroid[1]-lastFrameBuf[j]->centroid[1]) +
	      //	     (selectedObjects[i]->centroid[2]-lastFrameBuf[j]->centroid[2])*(selectedObjects[i]->centroid[2]-lastFrameBuf[j]->centroid[2]) );
	      
	      dist = (selectedObjects[i]->centroid - lastFrameBuf[j]->centroid).norm();
boundary_indices
	      //score = dist * ( 1 / findSize(selectedObjects[i]) ) * selectedObjects[i]->centroid.norm();
	      //changingRating = abs( ( findSize(selectedObjects[i]) - findSize(lastFrameBuf[j]) ) / findSize(lastFrameBuf[j]) );
	      
	      if(dist < smallestDist){
		  smallestDist = dist;
		  bestj = j;
	      }
	  }
	  
          if(lastFrameBuf[bestj]->track_id == -1) objects[indexes[i]]->track_id = n++;
	  else objects[indexes[i]]->track_id = lastFrameBuf[bestj]->track_id;
	  lastFrameBuf[bestj]->isMatched = true;
	  primitives.push_back(selectedObjects[i]);
	  matches.push_back(std::pair<int,int>(indexes[i],bestj));
      }
  }  
  */

//   DataAssocVisualizer vis(objects,lastFrameBuf,matches);
//   lastFrameBuf.erase(lastFrameBuf.begin(), lastFrameBuf.end());
//   lastFrameBuf.resize(objects.size());
  
//   lastFramefeatures.erase(lastFramefeatures.begin(), lastFramefeatures.end());
//   lastFramefeatures.resize(features.size());
  lastFramefeatures=features;
  lastFrameIndex=findex;
  
  for(unsigned int i = 0; i < lastFrameBuf.size(); i++){
      lastFrameBuf[i]->isMatched = false;
  }
  
  if(this->viewer){
//      viewer_mutex->lock();
//      removePreviousDataFromScreen();
//     viewer_mutex->unlock();	
     if(lastFrameBuf.size() > 0){

	if(selectedObjects.size() > 0 && lastFrameBuf.size() > 0){
	// displayCloudsColoured(primitives,viewer_mutex);
//         viewer_mutex->lock();
        cout<<"s"<<selectedObjects.size()<<" "<<lastFrameBuf.size()<<endl;
        if(aa>=1) DataAssocVisualizer viss(*viewer_mutex,viewer, selectedObjects,lastFrameBuf,matche);
//         viewer_mutex->unlock();
       }
    }
  }
  aa++;
  lastFrameBuf = selectedObjects; 
 // delete myints;
  return primitives;
}