#include <UniformDistribution.h>
#include <RandomGenerator.h>

UniformDistribution::UniformDistribution(int maxValue){
  this->maxValue = maxValue;
}

double UniformDistribution::Generate(){
  return RandomGenerator::NextDouble(0,maxValue);
}