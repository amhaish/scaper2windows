//TODO: rename this as it does more than Euclidean segmentation
#include <Consts.h>
#include <PCLEuclideanSegmenter.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/segmentation/euclidean_cluster_comparator.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <Consts.h>
#include <math.h>
#include <pcl/common/angles.h>
#include <boost/thread/thread.hpp>
#include <pcl/filters/voxel_grid.h>
#include <filters/sw_extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <limits>

#include <pcl/common/geometry.h>
#include <pcl/filters/extract_indices.h>

int verbosity_segmenter = 0;

PCLEuclideanSegmenter::PCLEuclideanSegmenter(boost::shared_ptr<pcl::visualization::PCLVisualizer> v, float distanceThreshold, float angularThreshold, float curvatureThreshold,float segmentYThreshold, uint segmentSizeThreshold, bool nan_normals_are_edges, boost::mutex& m)
{
	this->viewer = v;
	this->DistanceThreshold = distanceThreshold;
	this->AngularThreshold = angularThreshold;
	this->CurvatureThreshold = curvatureThreshold;
	this->viewer_mutex = &m;
	this->SegmentSizeThreshold = segmentSizeThreshold;
	this->last_clusters_size = 0;
	this->last_normals_size = 0;
	this->segmentYThreshold = segmentYThreshold;
	this->nan_normals_are_edges = nan_normals_are_edges;
	
	//if (v)
	//this->viewer->setBackgroundColor(0, 0, 0); //test this: should look better when printed to paper
	this->sumOfDistances = 0;
}

inline void PCLEuclideanSegmenter::removePreviousDataFromScreen()
{
	std::string name;
	for (int i = 0; i < last_clusters_size; i++)
	{
		name = "pc" + std::to_string(i);
		viewer->removePointCloud(name.c_str());
	}
	for (int i = 0; i < last_normals_size; i++)
	{
		name = "no" + std::to_string(i);
		viewer->removePointCloud(name.c_str());
	}
}

inline void PCLEuclideanSegmenter::displayEuclideanClusters()
{
	unsigned char red[12] = { 255,   0,   0, 255, 255,   0,128,   0,   0, 128, 128,   0 };
	unsigned char grn[12] = { 0, 255,   0, 255,   0, 255,0, 128,   0, 128,   0, 128 };
	unsigned char blu[12] = { 0,   0, 255,   0, 255, 255, 0,   0, 128,   0, 128, 128 };

	for (size_t i = 0; i < clusters.size(); i++)
	{
		pcl::PointCloud<SystemPoint>::Ptr ccc = clusters[i].cloud;

		std::string s = "pc" + std::to_string(i);
		char const *pchar = s.c_str();
		pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc, red[i % 12], grn[i % 12], blu[i % 12]);
		if (!viewer->updatePointCloud(ccc, color0, pchar))
			viewer->addPointCloud(ccc, color0, pchar);
		//viewer->setPosition(900, 0);
		viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
		//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
		if (displayNormals)
		{
			pcl::PointCloud<pcl::Normal>::Ptr cccNorms = clusters[i].normals;
			s = "no" + std::to_string(i);
			//       char const *pchar2 = s.c_str();
			viewer->addPointCloudNormals<SystemPoint, pcl::Normal>(ccc, cccNorms, 1, 0.02f, s);
			//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, s);
		}
	}
}

inline bool PCLEuclideanSegmenter::CheckSegmentDirection(pcl::PointCloud<pcl::Normal>::Ptr &normals, pcl::PointIndices* indices) {
	if (this->segmentYThreshold < 0)return true;
	float ySum = 0;
	for (size_t i = 0; i < (indices->indices).size(); i++) {
		ySum += (*normals).points[indices->indices[i]].normal_y;
	}
	ySum = ySum / (indices->indices).size();
	//cout<<ySum;
	if (ySum < this->segmentYThreshold) {
		return true;
	}
	else {
		return false;
	}
}

inline bool PCLEuclideanSegmenter::CheckSegmentSize(pcl::PointIndices* indices) {
	if (indices->indices.size() > this->SegmentSizeThreshold) {
		return true;
	}
	else {
		return false;
	}
}

PointCloudWithNormals::Vector PCLEuclideanSegmenter::Execute(PointCloudWithNormals::Ptr &cloud_with_normals)
{
	boost::shared_ptr<std::vector<pcl::PointIndices>> labelsIndices(new std::vector<pcl::PointIndices>());
	pcl::PointCloud<pcl::Label>::Ptr labels(new pcl::PointCloud<pcl::Label>());
	pcl::Label ltmp;
	ltmp.label = 0;
	labels->points.resize(cloud_with_normals->cloud->points.size(), ltmp);
	std::vector<bool> exclude_labels;
	exclude_labels.resize(1, false);
	pcl::OrganizedConnectedComponentSegmentation<SystemPoint, pcl::Label>::ComparatorPtr comparator;
	//if (use_curvature) {
	CurvatureBasedComparator<SystemPoint, pcl::Normal, pcl::Label>::Ptr ds_comparator(new CurvatureBasedComparator<SystemPoint, pcl::Normal, pcl::Label>());
	ds_comparator->setDistanceThreshold(this->DistanceThreshold, true);
	ds_comparator->setAngularThreshold(pcl::deg2rad(this->AngularThreshold));
	ds_comparator->setCurvatureThreshold(this->CurvatureThreshold);
	ds_comparator->setInputNormals(cloud_with_normals->normals);
	ds_comparator->setInputCloud(cloud_with_normals->cloud);
	ds_comparator->setLabels(labels);
	ds_comparator->setExcludeLabels(exclude_labels);
	ds_comparator->setNANNormalsAreEdges(this->nan_normals_are_edges);
	comparator = ds_comparator;
	//}
	//else {
	//pcl::EuclideanClusterComparator<SystemPoint, pcl::Normal, pcl::Label>::Ptr e_comparator(new pcl::EuclideanClusterComparator<SystemPoint, pcl::Normal, pcl::Label>());
	//e_comparator->setDistanceThreshold(this->DistanceThreshold, true);
	//e_comparator->setAngularThreshold(pcl::deg2rad(this->AngularThreshold));//not used
	//e_comparator->setInputNormals(cloud_with_normals->normals);//not used
	//e_comparator->setInputCloud(cloud_with_normals->cloud);
	//e_comparator->setLabels(labels);
	//e_comparator->setExcludeLabels(exclude_labels);
	//comparator = e_comparator;

	pcl::OrganizedConnectedComponentSegmentation<SystemPoint, pcl::Label> segmentationAlgorithm(comparator);
	pcl::PointCloud<pcl::Label>::Ptr elabels(new pcl::PointCloud<pcl::Label>());
	boost::shared_ptr<std::vector<pcl::PointIndices> > elabelsIndices(new std::vector<pcl::PointIndices>());
	segmentationAlgorithm.setInputCloud(cloud_with_normals->cloud);
	segmentationAlgorithm.segment(*elabels, *elabelsIndices);
	this->clusters.clear();
	for (size_t i = 0; i < elabelsIndices->size(); i++)
	{
		if (CheckSegmentSize(&((*elabelsIndices)[i]))) {
			//if (CheckSegmentDirection(cloud_with_normals->normals, &((*elabelsIndices)[i]))) {
			PointCloudWithNormals cluster;
			cluster.full_cloud_indices = boost::make_shared<std::vector<int> >((*elabelsIndices)[i].indices);
			cluster.full_cloud = cloud_with_normals->cloud;
			cluster.full_normals = cloud_with_normals->normals;
			pcl::SWExtractIndices<SystemPoint> extractp;
			extractp.setIndices(cluster.full_cloud_indices);
			extractp.setNegative(false);
			extractp.setKeepOrganized(true);
			extractp.setSubWindow(true);
			extractp.setInputCloud(cloud_with_normals->cloud);
			extractp.filter(*cluster.cloud); //Get the planes
			pcl::SWExtractIndices<pcl::Normal> extractn;
			extractn.setIndices(cluster.full_cloud_indices);
			extractn.setNegative(false);
			extractn.setKeepOrganized(true);
			extractn.setInputCloud(cloud_with_normals->normals);
			extractn.filter(*cluster.normals);
			/*
			pcl::copyPointCloud(*cloud_with_normals->cloud, (*elabelsIndices)[i].indices, *cluster.cloud);
			pcl::copyPointCloud(*cloud_with_normals->normals, (*elabelsIndices)[i].indices, *cluster.normals);*/
			////////////////////////////////////////////////////////////////////////////////////////////////
			//pcl::compute3DCentroid(*cluster.cloud, cluster_centroid);
			//centroid.x = cluster_centroid[0];
			//centroid.y = cluster_centroid[1];
			//centroid.z = cluster_centroid[2];
			//segmentationAlgorithm.findLabeledRegionBoundary((*elabelsIndices)[i].indices[0], elabels, boundary_indices);
			//for (size_t j = 0; j < boundary_indices.indices.size(); j++)
			//{
			//	distanceToTheCentroid = pcl::geometry::distance(cloud_with_normals->cloud->points[boundary_indices.indices[j]], centroid);
			//	sumOfDistances += distanceToTheCentroid;
			//}
			//averageDistance = sumOfDistances / boundary_indices.indices.size();
			//// 	       pcl::copyPointCloud(*cluster.cloud, boundary_indices.indices[j], *cluster.boundary);
			//// 	       std::cout<< "cluster size is: "<< cluster.cloud->size()<< std::endl;
			//// 	       std::cout<< "the average distance is: "<< cluster.boundary->size()<< std::endl;
			//std::cout << "the average distance is: " << averageDistance << std::endl;
			////////////////////////////////////////////////////////////////////////////////////////////////
			this->clusters.push_back(cluster);
		}
	}
	Visualize();
	return clusters;
}

void PCLEuclideanSegmenter::Visualize() {
	if (this->viewer) {
		viewer_mutex->lock();
		//removePreviousDataFromScreen();
		displayEuclideanClusters();
		viewer_mutex->unlock();
	}
	last_clusters_size = this->clusters.size();
	last_normals_size = this->clusters.size();
}