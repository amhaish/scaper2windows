#include <SegmentSelectorAdvance.h>
#include <Consts.h>
#include <common/PCLTools.h>

int SegmentSelectorAdvance::verbosity = 6;

SegmentSelectorAdvance::SegmentSelectorAdvance(double sd,double st,boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m):max_scan_distance(sd),scan_time(st){
  last_clock=clock::now();
  last_scan_depth=0;
  viewer_mutex=&m;
  viewer=vis;
  last_size=0;
}

void min_max_z(pcl::PointCloud<SystemPoint>::ConstPtr cloud,double &min,double &max){
      min=std::numeric_limits<double>::infinity();
      max=-std::numeric_limits<double>::infinity();
      for(uint i=0;i<cloud->points.size();i++){
	bool in_segment = pcl::isFinite(cloud->points[i]);
	if(in_segment){
 	      double z = cloud->points[i].z;
	      if(z<min)min=z;
	      if(z>max)max=z;
	}
      }
}

void min_max_range(pcl::PointCloud<SystemPoint>::ConstPtr cloud,double &min,double &max){
      min=std::numeric_limits<double>::infinity();
      max=-std::numeric_limits<double>::infinity();
      for(uint i=0;i<cloud->points.size();i++){
	bool in_segment = pcl::isFinite(cloud->points[i]);
	if(in_segment){
 	      double dist = sqrt(cloud->points[i].x * cloud->points[i].x + cloud->points[i].y * cloud->points[i].y + cloud->points[i].z * cloud->points[i].z);
	      if(dist<min)min=dist;
	      if(dist>max)max=dist;
	}
      }
}

std::vector<PCLObject::Ptr> SegmentSelectorAdvance::Execute(std::vector<PCLObject::Ptr> &segments){
  std::chrono::time_point<clock> this_clock=clock::now();
  std::chrono::duration<double> dur = std::chrono::duration<double>(this_clock-last_clock);

  double scan_depth = last_scan_depth + dur.count()*max_scan_distance/scan_time;
  while(scan_depth>max_scan_distance)scan_depth-=max_scan_distance;
  if(verbosity>3)std::cout<<"Scanning for selection of objects at distance "<<scan_depth<<std::endl;

  std::vector<PCLObject::Ptr> selected;

  for(uint i=0;i<segments.size();i++){
      double min_obj_depth,max_obj_depth;
      min_max_z(segments[i]->objectCloud,min_obj_depth,max_obj_depth);
      if(verbosity>5)std::cout<<"Checking: min_obj_depth("<<min_obj_depth<<")<=scan_depth("<<scan_depth<<") && max_obj_depth ("<<max_obj_depth<<") >=scan_depth("<<scan_depth<<")...";
      if(min_obj_depth<=scan_depth && max_obj_depth>=scan_depth){
	selected.push_back(segments[i]);
        if(verbosity>5)std::cout<<"Yes."<<std::endl;
      }else{
        if(verbosity>5)std::cout<<"No."<<std::endl;
      }
  }

  if(viewer)PCLTools::displayCloudsColoured(selected,viewer,viewer_mutex,"select_adv",last_size);
  last_size=selected.size();

  return selected;
}

