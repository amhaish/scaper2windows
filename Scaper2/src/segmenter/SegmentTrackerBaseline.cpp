#include <SegmentTrackerBaseline.h>
#include <limits>
#include <pcl/common/centroid.h>
#include <algorithm>
#include <set>
#include <iterator>

#include <boost/make_shared.hpp>

int SegmentTrackerBaseline::verbosity = 0;

SegmentTrackerBaseline::SegmentTrackerBaseline(){
}


double SegmentTrackerBaseline::match_score(int i,PCLObject::Ptr a,int j,PCLObject::Ptr b){
  std::pair<int,int> ind(i,j);
  std::map< std::pair<int,int>,double>::iterator it = memoized_match_score.find(ind);
  if(it==memoized_match_score.end()){
    double score=0;
    std::vector<int> intersection;
    std::set_intersection (a->full_cloud_indices->begin(), a->full_cloud_indices->end(), b->full_cloud_indices->begin(), b->full_cloud_indices->end(), std::back_inserter(intersection));
    score=((double)intersection.size())/(a->full_cloud_indices->size()+b->full_cloud_indices->size());
    memoized_match_score[ind]=score;
    if(verbosity>3)std::cout<<"SegmentTrackerBaseline: match_score (C):("<<i<<","<<j<<")-->"<<score<<std::endl;
    return score;
  }
  else{
    double score=(*it).second;
    if(verbosity>4)std::cout<<"SegmentTrackerBaseline: match_score (M):("<<i<<","<<j<<")-->"<<score<<std::endl;
    return score;
  }
}

std::pair<std::vector<PCLObject::Ptr>,std::vector<PCLObject::Ptr>> SegmentTrackerBaseline::Execute(PointCloudWithNormals::Vector clouds){

  memoized_match_score.clear();

  for(size_t  i=0;i<clouds.size();i++){
    std::sort(clouds[i].full_cloud_indices->begin(),clouds[i].full_cloud_indices->end());
  }


  std::vector<PCLObject::Ptr> tracked;

  for (size_t i = 0; i < clouds.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject());
    obj->track_id = -1;
    obj->full_cloud_indices = clouds[i].full_cloud_indices;
    obj->isMatched = false;
    obj->objectCloud = clouds[i].cloud;//boost::make_shared<pcl::PointCloud<SystemPoint> >(clouds[i]);
    obj->normalCloud = clouds[i].normals;
    pcl::compute3DCentroid(*(clouds[i].cloud),obj->centroid);
//     objSize = findSize(obj);
//     obj->size = objSize;

//     float zoomAmount = ((float)zoomPercent/100)*35;
    tracked.push_back(obj);
  }

  std::set<int> available_prev;
  for(size_t j=0;j<previous.size();j++){
    available_prev.insert(j);
  }

  std::set<int> available_new;
  for(size_t i=0;i<tracked.size();i++){
    available_new.insert(i);
  }

  std::set<int> used_track_ids;

  if(verbosity>2)std::cout<<"SegmentTrackerBaseline: Looking to associate track ids."<<std::endl;

  while(available_new.size()>0 && available_prev.size() >0){

    double best=std::numeric_limits<int>::min();
    int besti;
    int bestj;

    for(std::set<int>::iterator new_obj_ind_iterator=available_new.begin();new_obj_ind_iterator!=available_new.end();new_obj_ind_iterator++){
      int i=*new_obj_ind_iterator;

      for(std::set<int>::iterator prev_obj_ind_iterator=available_prev.begin();prev_obj_ind_iterator!=available_prev.end();prev_obj_ind_iterator++){
        int j=*prev_obj_ind_iterator;

        double this_score=match_score(i,tracked[i],j,previous[j]);
        if(this_score>best){
          best=this_score;
          besti=i;
          bestj=j;
        }

      }

    }
    if(best<=0){
      if(verbosity>2)std::cout<<"SegmentTrackerBaseline: no more matches."<<std::endl;
      break;
    }
    tracked[besti]->track_id=previous[bestj]->track_id;
    available_new.erase(besti);
    available_prev.erase(bestj);
    used_track_ids.insert(tracked[besti]->track_id);
    if(verbosity>2)std::cout<<"SegmentTrackerBaseline: Associating "<<besti<<" with "<<bestj<<" (track id "<<previous[bestj]->track_id<<")."<<std::endl;
  }

  for(std::set<int>::iterator it=available_prev.begin();it!=available_prev.end();it++){
      used_track_ids.insert(previous[*it]->track_id);
  }

  int next_track_id=0;
  for(std::set<int>::iterator it=available_new.begin();it!=available_new.end();it++){
    while(used_track_ids.find(next_track_id)!=used_track_ids.end())next_track_id++;
    used_track_ids.insert(next_track_id);
    tracked[*it]->track_id=next_track_id;
    if(verbosity>2)std::cout<<"SegmentTrackerBaseline: Starting new object with track id "<<next_track_id<<std::endl;
  }
  
  std::pair<std::vector<PCLObject::Ptr>,std::vector<PCLObject::Ptr>> output;
  output.first = tracked;
  output.second =  previous;
  
  previous=tracked;
  
  return output;
}