#include <PCLPlaneModelsFilter.h>
#include <Consts.h>
#include <chrono>

#include <pcl/segmentation/boost.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/segmentation/plane_coefficient_comparator.h>
#include <pcl/segmentation/euclidean_plane_coefficient_comparator.h>
#include <pcl/segmentation/rgb_plane_coefficient_comparator.h>
#include <pcl/segmentation/edge_aware_plane_comparator.h>
#include <pcl/segmentation/euclidean_cluster_comparator.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/segmentation/organized_multi_plane_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include "..\..\include\PCLPlaneModelsFilter.h"

void timing() {
	static std::chrono::high_resolution_clock::time_point last;
	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> dur = std::chrono::duration_cast<std::chrono::duration<float>>(now - last);
	//     float durf=dur.count();
	std::cout << "" << dur.count() << " seconds since last call" << std::endl;
	last = now;
}

PCLPlaneModelsFilter::PCLPlaneModelsFilter(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis, float distanceThreshold, float maximumCurvature, float minInliers, float angularThreshold, bool gpu, boost::mutex& m)
{
	this->viewer = vis;
	this->distanceThreshold = distanceThreshold;
	this->maximumCurvature = maximumCurvature;
	this->minInliers = minInliers;
	this->angularThreshold = angularThreshold;
	this->gpuProcessing = gpu;
	this->last_clusters_size = 0;
	this->viewer_mutex = &m;
	//if(viewer)
	   //this->viewer->setBackgroundColor(1.0,1.0,1.0); 
}

void PCLPlaneModelsFilter::Execute(PointCloudWithNormals::Ptr &cloudWithNormal)
{
	pcl::PointCloud<SystemPoint>::CloudVectorType my_clusters;
	pcl::EuclideanPlaneCoefficientComparator<SystemPoint, pcl::Normal>::Ptr comparator;
	comparator.reset(new pcl::EuclideanPlaneCoefficientComparator<SystemPoint, pcl::Normal>());
	pcl::PointCloud<SystemPoint>::Ptr cloud_f(new pcl::PointCloud<SystemPoint>);
	pcl::PointCloud<pcl::Normal>::Ptr cloud_fn(new pcl::PointCloud<pcl::Normal>);
	std::vector<pcl::ModelCoefficients> model_coefficients;
	std::vector<pcl::PointIndices> inliers;
	pcl::OrganizedMultiPlaneSegmentation<SystemPoint, pcl::Normal, pcl::Label> mps;
	mps.setMinInliers(this->minInliers);
	mps.setAngularThreshold(pcl::deg2rad(this->angularThreshold)); //3 degrees
	mps.setDistanceThreshold(this->distanceThreshold); //2cm
	mps.setInputNormals(cloudWithNormal->normals);
	mps.setComparator(comparator);
	mps.setMaximumCurvature(this->maximumCurvature);
	mps.setInputCloud(cloudWithNormal->cloud);
	mps.segment(model_coefficients, inliers);
	pcl::PointIndices::Ptr all_inliers(new pcl::PointIndices);
	for (size_t i = 0; i < inliers.size(); i++) {
		pcl::PointIndices::Ptr fInliers(new  pcl::PointIndices(inliers[i]));
		pcl::PointCloud<SystemPoint> cluster;
		pcl::copyPointCloud(*(cloudWithNormal->cloud), fInliers->indices, cluster);
		my_clusters.push_back(cluster);
		pcl::PointCloud<SystemPoint> temp_pc;
		pcl::PointCloud<pcl::Normal> temp_pcn;
		all_inliers->indices.insert(all_inliers->indices.end(), fInliers->indices.begin(), fInliers->indices.end());
	}
	pcl::ExtractIndices<SystemPoint> extract;
	extract.setIndices(all_inliers);
	extract.setNegative(true); // Removes part_of_cloud from full cloud  and
	extract.setKeepOrganized(true);
	extract.setInputCloud(cloudWithNormal->cloud);
	extract.filter(*cloud_f); //Get the planes
	pcl::ExtractIndices<pcl::Normal> extractn;
	extractn.setIndices(all_inliers);
	extractn.setNegative(true); // Removes part_of_cloud from full cloud  and
	extractn.setKeepOrganized(true);
	extractn.setInputCloud(cloudWithNormal->normals);
	extractn.filter(*cloud_fn);
	*cloudWithNormal->cloud = *cloud_f;
	*cloudWithNormal->normals = *cloud_fn;
	Visualize(cloudWithNormal,my_clusters);
}

void PCLPlaneModelsFilter::Visualize(PointCloudWithNormals::Ptr cloudWithNormal,pcl::PointCloud<SystemPoint>::CloudVectorType my_clusters)
{
	unsigned char red[12] = { 255,   0,   0, 255, 255,   0,128,   0,   0, 128, 128,   0 };
	unsigned char grn[12] = { 0, 255,   0, 255,   0, 255,0, 128,   0, 128,   0, 128 };
	unsigned char blu[12] = { 0,   0, 255,   0, 255, 255, 0,   0, 128,   0, 128, 128 };
	if (this->viewer) {
		viewer_mutex->lock();
		//std::string name;
		//name = "cloud without planes";
		//viewer->removePointCloud(name.c_str());
		//for (int i = 0; i < last_clusters_size; i++)
		//{
		//	name = "pe_pc" + std::to_string(i);
		//	viewer->removePointCloud(name.c_str());
		//}
		//std::stringstream ss;
		//ss << "cloud without planes";
		//pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(cloudWithNormal->cloud);
		//if (!viewer->updatePointCloud(cloudWithNormal->cloud, color0, ss.str()))
			//viewer->addPointCloud(cloudWithNormal->cloud, color0, ss.str());
		//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, ss.str());
		//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, ss.str());
		for (size_t i = 0; i < my_clusters.size(); i++)
		{
			pcl::PointCloud<SystemPoint>::Ptr ccc = boost::make_shared<pcl::PointCloud<SystemPoint> >(my_clusters[i]);
			std::string s = "pe_pc" + std::to_string(i);
			char const *pchar = s.c_str();
			pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc, red[i % 12], grn[i % 12], blu[i % 12]);
			if (!viewer->updatePointCloud(ccc, color0, pchar))
				viewer->addPointCloud(ccc, color0, pchar);
			viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
			viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
		}
		viewer_mutex->unlock();
		last_clusters_size = my_clusters.size();
	}
}

