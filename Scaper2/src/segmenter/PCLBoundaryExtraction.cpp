#include <PCLBoundaryExtraction.h>
pcl::PointIndices PCLBoundaryExtraction::BoundaryExtraction (pcl::PointCloud<SystemPoint>::Ptr &cloud)
{
   pcl::PointIndices boundary_indices;
   boundary_indices.indices.clear ();
  int start_idx = 0;
//   std::cout << "cloud size in BoundaryExtraction code is : " <<cloud->size() << std::endl;
  
  for (int i=0 ; i<cloud->width ; i++)
    for (int j=0 ; j< cloud->height ; j++){
      if (pcl::isFinite<SystemPoint>(cloud->points[j*cloud->width+i]))
      {
	start_idx = j*cloud->width+i;
	i = cloud -> width;
	j = cloud -> height;
      }
    }
  
  int curr_idx = start_idx;
  int curr_x   = start_idx % cloud->width;
  int curr_y   = start_idx / cloud->width;
  
  struct Neighbor
  {
     Neighbor (int dx, int dy, int didx)
     : d_x (dx)
     , d_y (dy)
     , d_index (didx)
     {}
        
     int d_x;
     int d_y;
     int d_index; // = dy * width + dx: pre-calculated
    
  };
  // fill lookup table for next points to visit
  Neighbor directions [8] = {Neighbor(-1,  0,                 -1),
                             Neighbor(-1, -1, -cloud->width - 1), 
                             Neighbor( 0, -1, -cloud->width    ),
                             Neighbor( 1, -1, -cloud->width + 1),
                             Neighbor( 1,  0,                  1),
                             Neighbor( 1,  1,  cloud->width + 1),
                             Neighbor( 0,  1,  cloud->width    ),
                             Neighbor(-1,  1,  cloud->width - 1)};
  
  // find one pixel with other label in the neighborhood -> assume thats the one we came from
  int direction = -1;
  int x;
  int y;
  int index;
  for (unsigned dIdx = 7; dIdx >= 0; --dIdx)
  {
    x = curr_x + directions [dIdx].d_x;
    y = curr_y + directions [dIdx].d_y;
    index = curr_idx + directions [dIdx].d_index;
    if (x >= 0 && x < int(cloud->width) && y >= 0 && y < int(cloud->height) && pcl::isFinite<SystemPoint>(cloud->points[index]))
    {
      direction = dIdx;
      break;
    }
  }
  // no connection to outer regions => start_idx is not on the border
  if (direction == -1){
    
   std::fill(boundary_indices.indices.begin(), boundary_indices.indices.end(), 0);
   std::cout<<" cant find the boundary"<<std::endl;
   return boundary_indices;
    
  }
  direction = (direction + 4) & 7;
  boundary_indices.indices.push_back (start_idx);
  do {
    unsigned nIdx;
    for (unsigned dIdx = 1; dIdx <= 8; ++dIdx)
    {
      nIdx = (direction + dIdx) & 7;
      
      x = curr_x + directions [nIdx].d_x;
      y = curr_y + directions [nIdx].d_y;
      index = curr_idx + directions [nIdx].d_index;
      if (x >= 0 && x < int(cloud->width) && y >= 0 && y < int(cloud->height) && pcl::isFinite<SystemPoint>(cloud->points[index]))
        break; // mistake on PCL: the second "x" is mistakely "y" in original PCL code
    }
    // update the direction
    direction = (nIdx + 4) & 7;
    curr_idx += directions [nIdx].d_index;
    curr_x   += directions [nIdx].d_x;
    curr_y   += directions [nIdx].d_y;
    boundary_indices.indices.push_back(curr_idx);
  } while ( curr_idx != start_idx);
  
  // printing an aski image for debugging 
  /*
  for(int y=0;y<cloud->height;y++){
    for(int x=0;x<cloud->width;x++){
      int ind = x + cloud->width * y;
      bool f=false;
      for(int i=0;i<boundary_indices.indices.size();i++){
	if(ind==boundary_indices.indices[i]){
	 f=true;
	 break;
	}
      }
      if(f)std::cout<<"X";
     // else if(x==cloud->width-1)std::cout<<"|";
     // else if(y==cloud->height-1)std::cout<<"-";
      else if(pcl::isFinite<SystemPoint>(cloud->points[x+cloud->width*y]))std::cout<<"O";
      else std::cout <<".";
    }
    std::cout<<std::endl;
  }    */  

  return boundary_indices;
}