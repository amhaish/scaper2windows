#include <PCLNormalCalculator.h>

PCLNormalCalculator::PCLNormalCalculator(float maxDepthChangeFactor, float normalSmoothingSize)
{
  this->MaxDepthChangeFactor=maxDepthChangeFactor;
  this->NormalSmoothingSize=normalSmoothingSize;

}

 PointCloudWithNormals::Ptr PCLNormalCalculator::Execute(pcl::PointCloud< SystemPoint >::Ptr &cloud)
 { 
   PointCloudWithNormals::Ptr cloudWithNormals(new PointCloudWithNormals());
   cloudWithNormals->cloud=cloud;
   cloudWithNormals->normals=calculateNormals(cloudWithNormals->cloud);
   return cloudWithNormals;

 }
 
pcl::PointCloud<pcl::Normal>::Ptr PCLNormalCalculator::calculateNormals(pcl::PointCloud< SystemPoint >::Ptr &PointCloud)
{
// estimate normals
	pcl::PointCloud<pcl::Normal>::Ptr normal (new pcl::PointCloud<pcl::Normal>);
	pcl::IntegralImageNormalEstimation<SystemPoint, pcl::Normal> ne;
	ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
	ne.setMaxDepthChangeFactor(this->MaxDepthChangeFactor);
	ne.setNormalSmoothingSize(this->NormalSmoothingSize);
	normal->points.resize(PointCloud->points.size());
	normal->width = PointCloud->width;
	normal->height = PointCloud->height;
	ne.setInputCloud(PointCloud);
	ne.compute(*normal);
	return normal;
}



	