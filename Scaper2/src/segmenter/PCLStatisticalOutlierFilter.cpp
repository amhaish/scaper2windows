#include <PCLStatisticalOutlierFilter.h>

PCLStatisticalOutlierFilter::PCLStatisticalOutlierFilter(boost::shared_ptr<pcl::visualization::PCLVisualizer> v,int meanK,double stdDevMulThreshold)
{
  this->viewer=v;
  if(v){
   std::cerr<<"NOT IMPL"<<std::endl; 
  }
  this->FilterMeanK=meanK;
  this->StddevMulThresh=stdDevMulThreshold;
}



void PCLStatisticalOutlierFilter::Execute(PointCloudWithNormals::Ptr &cloudWithNormal)
{
  /*pcl::StatisticalOutlierRemoval<SystemPoint> sor;
  sor.setKeepOrganized(true);
  sor.setInputCloud(cloudWithNormal->cloud);
  sor.setMeanK(FilterMeanK);
  sor.setStddevMulThresh(StddevMulThresh);
  sor.filter(*cloud);*/
  pcl::RadiusOutlierRemoval<SystemPoint> outrem;
  // build the filter
  outrem.setInputCloud(cloudWithNormal->cloud);
  outrem.setRadiusSearch(2);
  outrem.setMinNeighborsInRadius (2);
  // apply filter
  outrem.filter (*(cloudWithNormal->cloud));
}
