

int test=3;
const int UPPER_LIMIT = 525;
const int LOWER_LIMIT = 490;

ParameterTaking* ParameterTaking::sp = NULL;

ParameterTaking* ParameterTaking::GetSerialPort()
{
	if(!sp)
		sp = new ParameterTaking();
	  
	return sp;
}

ParameterTaking::ParameterTaking():enabled(true)
{
	/* Initializing part */
	struct termios toptions;
	
	/*keyboard*/
	struct termios oldSettings, newSettings;
	tcgetattr( fileno( stdin ), &oldSettings );
	newSettings = oldSettings;
	newSettings.c_lflag &= (~ICANON & ~ECHO);
	tcsetattr( fileno( stdin ), TCSANOW, &newSettings );
	/*keyboard*/
	
	/* file descriptor */
	fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY);
	if(fd < 0) fd = open("/dev/ttyACM1", O_RDWR | O_NOCTTY); //added by celenkah
	if(fd < 0) serialPortRunning = false;
	else serialPortRunning = true;
	
	/* wait for the Arduino to reboot */
	usleep(3000); // microseconds

	/* get current options for serial pplport */
	tcgetattr(fd, &toptions);
	/* set 57600 baud both ways */
	cfsetispeed(&toptions, B57600);
	cfsetospeed(&toptions, B57600);
	/* 8 bits, no parity, no stop bits */
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	toptions.c_cflag |= (CLOCAL | CREAD);
	/* Canonical mode */
	toptions.c_lflag |= ICANON;
	/* commit the serial port settings */
	tcsetattr(fd, TCSANOW, &toptions);
	
	/* initial values of sensors*/
	joystickData[0] = 0;
	joystickData[1] = 0;
	joystickData[2] = 0;
	selectionType = 0;
	
	buttonsData[0] = 0;
	buttonsData[1] = 0;
	rotaryEncoderData = 0;
	imuData[0] = 0;
	imuData[1] = 0;
	imuData[2] = 0;
}

bool ParameterTaking::isSerialPortRunning()
{
  return serialPortRunning && enabled;
}

void ParameterTaking::KeyboardParameterUpdate()
{
	fd_set set;
	struct timeval tv;

	tv.tv_sec = 0;
	tv.tv_usec = 0.0001;

	FD_ZERO( &set );
	FD_SET( fileno( stdin ), &set );

	int res = select( fileno( stdin )+1, &set, NULL, NULL, &tv );

	if( res > 0 )
	{
            char ch;
            read( fileno( stdin ), &ch, 1 );
	    
	    if(ch == 'b'){
	      if(test>2)printf("biggest\n");
	      selectionType = 2;
	    }
	    else if(ch == 'n'){
	      if(test>2)printf("nearest\n");
	      selectionType = 1;
	    }
	    else if(ch == 'l'){
	      if(test>2)printf("left\n");
	      selectionType = 3;
	    }
	    else if(ch == 'r'){
	      if(test>2)printf("right\n");
	      selectionType = 4;
	    }
	    else selectionType = 0;
	    
	}
	else if( res < 0 )
	{
	    perror( "select error" );
	}
	else
	{
	}
}

void ParameterTaking::ArduinoParameterUpdate()
{
	int n;
	char *buf = new char[48];
	float valueofX = 0, valueofY = 0;
	bool seektoX;

	tcflush(fd, TCIOFLUSH);

	seektoX = true;
	
	/* cleanning at the beginning*/
	for(n=0;n<10;n++)read(fd, buf, 48);
	
	/* seeking to 'X' */
	do {
		/* reading 1 byte for each loop until 'X' */
		n = read(fd, buf, 1);
		if (n <= 0) {
			seektoX = false;
		}
	} while (n > 0 && buf[0] != 'X'); 
	
	if (seektoX) {
		/* reading 4 byte X value */
		n = read(fd, buf, 47);
		if (test > 2)cout << "Buffer: " << buf << endl;

		valueofX = BuffertoInt(buf);
		joystickData[0] = valueofX;
		
		/* button state clicked: 1 */
		joystickData[2] = (int)buf[5] - '0';

		/* seeking to 'Y' */
		char *bufY;

		bufY = buf + 7;

		valueofY = BuffertoInt(bufY);
		joystickData[1] = valueofY;
		
		/* button state clicked: 1 */
		buttonsData[0] = (int)buf[13] - '0';
		buttonsData[1] = (int)buf[15] - '0';
		
		/* seeking to 'R' */
		char *bufRotaryEncoder;
		bufRotaryEncoder = buf + 18;
		
		rotaryEncoderData = BuffertoInt(bufRotaryEncoder);
		
		/* seeking to 'Imu Yaw'*/
		char *bufImuYaw;
		bufImuYaw = buf + 24;
		
		imuData[0] = BuffertoInt(bufImuYaw);
		
		/* seeking to 'Imu Pitch'*/
		char *bufImuPitch;
		bufImuPitch = buf + 32;

		imuData[1] = BuffertoInt(bufImuPitch);
		
		/* seeking to 'Imu Roll'*/
		char *bufImuRoll;
		bufImuRoll = buf + 40;
		
		imuData[2] = BuffertoInt(bufImuRoll);
		
		/* consuming 1 byte '\n' */
		n = read(fd, buf, 1);
	}
	/* flush buffer then wait for new coming data */
	tcflush(fd, TCIOFLUSH);
	//usleep(20000);
	
	/*Output*/
	if (test > 2) {
		/*Joystick Output*/
		cout << endl;
		cout << "Joystick Output\n";
		cout << "  Joystick x: " << joystickData[0] << endl;
		cout << "  Joystick y: " << joystickData[1] << endl;
		cout << "  Button state: " << joystickData[2] << endl;
		
		/*Buttons Output*/
		cout << "Buttons Output\n";
		cout << "  Button 1: " << buttonsData[0] << endl;
		cout << "  Button 2: " << buttonsData[1] << endl;
		
		/*Rotary Encoder Output*/
		cout << "Rotary Encoder Output\n";
		cout << "  Rotary Encoder: " << rotaryEncoderData << endl;
		
		/*Imu Output*/
		cout << "Imu Output\n";
		cout << "  Yaw  : " << imuData[0] << endl;
		cout << "  Pitch: " << imuData[1] << endl;
		cout << "  Roll : " << imuData[2] << endl;
		cout << endl;
	}
	
	JoystickSelection();
	
	ImuSelection();
		
	delete[] buf;
}

int *ParameterTaking::GetJoystickData()
{
	return joystickData;
}

int ParameterTaking::GetSelectionType()
{
	if(test>2){
	  if(fd >= 0) cout << "Parameters are taken from serial port." << endl;
	  else cout << "Parameters are taken from keyboard" << endl;
	}
	if(test>2) cout << "isSerialPortConnected= " << serialPortRunning << " && " << "isSerialPortEnabled= " << enabled << endl;
	
	return selectionType;
}

int ParameterTaking::GetRotaryEncoder()
{
    return rotaryEncoderData;
}

int ParameterTaking::BuffertoInt(char* buf)
{
	int toReturn = 0;
	int i = 0;
	bool negative = false;

	while (i < 4) {
		/* checking number or not */
		if ( ((int)buf[i] >= 48 && (int)buf[i] <= 57) || (int)buf[i] == '-') {
			if (buf[i] == '0') {
				i++;
				if (i == 4) toReturn = 0;
			}
			else if (buf[i] == '-') {
				negative = true;
				i++;
			}
			else {
				for (int j = i; j < 4; j++) {
					toReturn += (buf[j] - '0')*pow(10, 4 - j - 1);
				}
				break;
			}
		}
		else {std::cout<<"Euch charlie "<<buf<<std::endl; break;}//added by celenkah
	}
	(negative) ? toReturn *= -1 : toReturn;
	return toReturn;
}

void ParameterTaking::setEnabled(bool en)
{

  enabled=en;
  
}

void ParameterTaking::JoystickSelection()
{
	/*
	 * Selection type for X:
	 * Default(initial): 0
	 * Left position: SelectLeftSegment(): 3 
	 * Right position: SelectRightSegment(): 4
	 * 
	 * Selection type for Y:
	 * Default(initial): 0
	 * Down position: Nearest object: 1
	 * Up position: Biggest object: 2
	 * 
	 */
	
	/* Joystick - value of X */
	if (test > 2)cout << "Joystick" << endl;
	if (test > 2)cout << " Position of X:     ";
	if (joystickData[0] < UPPER_LIMIT && joystickData[0] > LOWER_LIMIT && joystickData[1] < UPPER_LIMIT && joystickData[1] > LOWER_LIMIT) {
		selectionType = 0;
	}
	if (joystickData[0] < UPPER_LIMIT && joystickData[0] > LOWER_LIMIT){
		if (test > 2)cout << "Initial"; 
	}
	else if (joystickData[0] < LOWER_LIMIT) {
		selectionType = 3;
		//SelectLeftSegment();
		if (test > 2)cout << "Left";
	}
	else if (joystickData[0] > UPPER_LIMIT) {
		selectionType = 4;
		//SelectRightSegment();
		if (test > 2)cout << "Right";
	}
	else {
		selectionType = 0;
		if (test > 2)cout << "Error";
	}
	if (test > 2)cout << endl;

	/* Joystick - value of Y */
	if (test > 2)cout << " Position of Y:     ";
	if (joystickData[1] < UPPER_LIMIT && joystickData[1] > LOWER_LIMIT) {
		if (test > 2)cout << "Initial";
	}
	else if (joystickData[1] < LOWER_LIMIT) {
		selectionType = 1;
		//SelectNearestObject();
		if (test > 2)cout << "Down";
	}
	else if (joystickData[1] > UPPER_LIMIT) {
		selectionType = 2;
		//SelectBiggestObject();
		if (test > 2)cout << "Up";
	}
	else {
		selectionType = 0;
		if (test > 2)cout << "Error";
	}
	if (test > 2) cout << endl;
	
	/* Joystick - value of Button */
	if (test > 2)cout << " Status of Button: ";
	if(joystickData[2] == 1)if (test > 2) cout << " Clicked";
	if(joystickData[2] == 0)if (test > 2) cout << " Not-clicked";
	if (test > 2) cout << endl;
}

void ParameterTaking::ImuSelection()
{
	/* Range of values coming from Imu
	 * Value of Yaw: -180 0 180
	 * Value of Pitch: 0 90 0 -90 0	  
	 * Value of Raw: 0 180 -180 0
	 * */
	
	/* Imu - value of Yaw */
	if (test > 2)cout << "Imu" << endl;
	if (test > 2)cout << " Position of Yaw: ";
	if(imuData[0] > 0 && imuData[0] < 180){
		if (test > 2) cout << "Head turned to right";
		if(imuData[0] > 90) cout << " back";
		else cout << " front";
		}
	else if(imuData[0] < 0 && imuData[0] > -180){
		if (test > 2) cout << "Head turned to left";
		if(imuData[0] < -90) cout << " back";
		else cout << " front";
		}
	else
		if (test > 2) cout << "Initial position";
		
	if (test > 2) cout << endl;
	
	/* Imu - value of Pitch */
	if (test > 2)cout << " Position of Pitch: ";
	if(imuData[1] > 0 && imuData[1] < 90){
		if (test > 2) cout << "Head loooks up";
		}
	else if(imuData[1] < 0 && imuData[1] > -90){
		if (test > 2) cout << "Head loooks down";
		}
	else
		if (test > 2) cout << "Initial position";
		
	if (test > 2) cout << endl;
	
	/* Imu - value of Roll */
	if (test > 2)cout << " Position of Roll: ";
	if(imuData[2] > 0 && imuData[2] < 180){
		if (test > 2) cout << "Head tilted to the right";
		}
	else if(imuData[2] < 0 && imuData[2] > -90){
		if (test > 2) cout << "Head tilted to the left";
		}
	else
		if (test > 2) cout << "Initial position";
		
	if (test > 2) cout << endl << endl;
}