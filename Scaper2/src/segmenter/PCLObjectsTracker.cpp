#include <PCLObjectsTracker.h>
#include <limits>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/common/centroid.h>

#include <boost/thread/thread.hpp>

#define NUM_OBJECTS 1

int otvvv=3;

PCLObjectsTracker::PCLObjectsTracker(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m)
{
  this->viewer_mutex=&m;
  last_size=0;
  this->viewer=vis;//new pcl::visualization::PCLVisualizer("Objects Tracker Visualizer");
  //this->viewer->setBackgroundColor(1.0,1.0,1.0); //test this: should look better when printed to paper
}


void PCLObjectsTracker::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::mutex* viewer_mutex)
{
  
//   static 
  
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};
  
  for (size_t i = 0; i < clouds.size (); i++)
  {viewer_mutex->lock();
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = "tr" + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
    viewer_mutex->unlock();
  }  
  
//   for(unsigned int i=clouds.size();i<last_size;i++){
    
//    viewer_mutex->lock();
//     std::string s = "tr" + std::to_string(i);
//     viewer->removePointCloud(s.c_str());
//     viewer_mutex->unlock();
//   }
//   
  
  last_size=clouds.size();
//   usleep(10000000);
}

inline void PCLObjectsTracker::removePreviousDataFromScreen ()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }

}



std::vector<PCLObject::Ptr> PCLObjectsTracker::Execute(PointCloudWithNormals::Vector clouds_with_normals){
  std::vector<PCLObject::Ptr> objects;
  std::vector<double> salience; // TODO: - make it responsive to user controls
  
  //std::vector<PCLObject::Ptr> currentResult;
  int nearestObjIndex;
  int nearestObjZ=std::numeric_limits< int >::max();

  
  int biggestObjIndex;
//   int selectedObjIndex;
  int objDist;
  int objSize;
  int biggestObjSize = -1;
  for (size_t i = 0; i < clouds_with_normals.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject());

    obj->objectCloud=clouds_with_normals[i].cloud;
    obj->normalCloud=clouds_with_normals[i].normals;
 
    pcl::compute3DCentroid(*clouds_with_normals[i].cloud,obj->centroid);

    if(obj->centroid.norm() < nearestObjZ)
    {
      nearestObjZ=obj->centroid.norm();
      nearestObjIndex=i;

      if(otvvv>2)cout << "nearestObjIndex: " << nearestObjIndex << endl;

    }
    
    objDist = sqrt(obj->centroid[0]*obj->centroid[0] + obj->centroid[1]*obj->centroid[1] + obj->centroid[2]*obj->centroid[2]);
    obj->distance=objDist;
    objSize = clouds_with_normals[i].cloud->size();

    if(biggestObjSize == -1 || objDist*objSize > biggestObjSize ){
      biggestObjSize = objDist*objSize;

      if(otvvv>2)cout << "biggestObjSize: " << biggestObjSize << endl;
      biggestObjIndex = i;
      if(otvvv>2)cout << "biggestObjIndex: " << biggestObjIndex << endl;

    }
    
    //this is where object selection would go - e.g. rightness, leftness, bigness, closenes.
    double this_salience = objSize / obj->centroid.norm() ;
    
    salience.push_back(this_salience);
    objects.push_back(obj);
  }
  
//   if(objectSelectionType == 0 || objectSelectionType == 1){
//     selectedObjIndex = nearestObjIndex;
//   }
//   else if(objectSelectionType == 2){
//     selectedObjIndex = biggestObjIndex;
//   }
//   else{
//    exit(1); 
//   }
  

  //if(otvvv>2)cout << "selectedObjIndex: " << selectedObjIndex << endl;

  
  //currentResult.push_back(objects[selectedObjIndex]);
//   if(viewer){
//     if(objects.size()>0){
//     if(otvvv>4)std::cout<<"VISUALISING TRACKED OBJECT - SIZE "<<objects[0]->objectCloud->size()<<std::endl;
//     
//     pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(objects[0]->objectCloud,255,255,255);
//     if (!viewer->updatePointCloud(objects[0]->objectCloud,color0,"Tracked"))
// 	  viewer->addPointCloud (objects[0]->objectCloud,color0,"Tracked");
//       viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "Tracked");
//       viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, "Tracked");
//     }
//   }
  
  // this is a really basic way of trying to get some kind of consistency in data association.
  // a better way would be to check the closeness of all objects to each other, use some kind of threshold to
  // disocciate ones that are not close, then choose the optimal match
  
  std::vector<PCLObject::Ptr> sorted_primitives;
  
  int n=0;
  while(objects.size()>0&&sorted_primitives.size()<NUM_OBJECTS){
      unsigned int besti=0;
      double bestf = 0;//0=std::numeric_limits<double>::max();
      for(unsigned int i=0;i<objects.size();i++){
	  if(salience[i]>bestf){
	   bestf=salience[i];
	   besti=i;
	  }
      }
      objects[besti]->track_id=n++;
      sorted_primitives.push_back(objects[besti]);
      objects.erase(objects.begin()+besti);
      salience.erase(salience.begin()+besti);
  }
  
  if(this->viewer){
    viewer_mutex->lock();
    removePreviousDataFromScreen();
    viewer_mutex->unlock();
    if(sorted_primitives.size()>0)
    {
      displayCloudsColoured(sorted_primitives,viewer_mutex);
      
    }
  }
  
  return sorted_primitives;
}