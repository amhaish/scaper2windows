#include <ParticleFilter.h>
#include <PositionState.h>
#include <RandomGenerator.h>
#include <math.h>   

std::vector<PCLObject::Ptr> ParticleFilter::Execute(pcl::PointCloud<SystemPoint>::CloudVectorType clouds){
    
}
  
ParticleFilter::ParticleFilter(){
    
}
  
void ParticleFilter::Initialize(int numberOfParticles,std::vector<IDistribution*> distributions)
{
    int nDim = distributions.size();
    for (int i = 0; i < numberOfParticles; i++)
    {
       double* randomParam = new double[nDim];
       for (int dimIdx = 0; dimIdx < nDim; dimIdx++)
       {
          randomParam[dimIdx] = distributions[dimIdx]->Generate();
       }
       IState* p = PositionState::FromArray(randomParam);
       p->Weight = 1 / numberOfParticles;
       States.push_back(p);
    }
}

void ParticleFilter::Predict(float effectiveCountMinRatio){
  std::vector<IState*> new_states;
  double effectiveCountRatio = (double)_effectiveParticleCount(_normalize(States)) / States.size();
  if (effectiveCountRatio > std::numeric_limits<T>::epsilon() && effectiveCountRatio < effectiveCountMinRatio)
  {
    new_states = _resample(States.size()).ToList();
  }
  else
  {
      for(int j=0 ; j < States.size() ; j++)
      {
	new_states[j].push_back(States[j].Copy());
      }
  }
  for(int j=0 ; j < new_states.size() ; j++)
  {
       new_states[j].Diffuse();
  }
  States = new_states;
}


void ParticleFilter::_resample(int samplesCount){
  std::vector<IState*> resampledParticles;
  std::vector<IState*> filteredParticles = _filterStates(States.size());
  for (int i=0 ; i < filteredParticles.size(); i++)
  {
      IState* newP = filteredParticles[i].Copy();
      newP->Weight = 1 / States.size();
      resampledParticles.push_back(newP);
  }
  return resampledParticles;
}

std::vector<IState*> ParticleFilter::_filterStates(int sampleCount)
{
  double cumulativeWeights[] = new double[States.size()];
  int cumSumIdx = 0;
  double cumSum = 0;
  for (int i=0; i < States.size(); i++)
  {
      cumSum += States[i]->Weight;
      cumulativeWeights[cumSumIdx++] = cumSum;
  }
  double maxCumWeight = cumulativeWeights[States.size() - 1];
  double minCumWeight = cumulativeWeights[0];

  std::vector<IState*> filteredParticles;

  double initialWeight = 1 / States.size();
            
  for (int i = 0; i < sampleCount; i++)
  {
     double randWeight = minCumWeight + RandomGenerator.NextDouble(1) * (maxCumWeight - minCumWeight);
            
     int particleIdx = 0;
     while (cumulativeWeights[particleIdx] < randWeight) 
     {
          particleIdx++;
     }
            
     IState* p = States[particleIdx];
     filteredParticles.push_back(p);
  }

  return filteredParticles;
  double cumulativeWeights[] = new double[States.size()];
            
  int cumSumIdx = 0;
  double cumSum = 0;
  for(int i=0 ; i < States.size(); i++)
  {
     cumSum += States[i].Weight;
     cumulativeWeights[cumSumIdx++] = cumSum;
  }

  double maxCumWeight = cumulativeWeights[States.size() - 1];
  double minCumWeight = cumulativeWeights[0];

  double filteredParticles = new List<FeatureParticle>();

  double initialWeight = 1 / States.size();
  RandomProportional rg;
  for (int i = 0; i < sampleCount; i++)
  {
	  double randWeight = minCumWeight + rg.NextDouble(1) * (maxCumWeight - minCumWeight);
            
     int particleIdx = 0;
     while (cumulativeWeights[particleIdx] < randWeight) 
     {
         particleIdx++;
     }
            
	 IState* p = States[particleIdx];
     filteredParticles.Add(p);
  }
  return filteredParticles;
}
        
std::vector<double> ParticleFilter::_normalize(std::vector<IState*> _states){
  std::vector<double> normalizedWeights;
  double weightSum;
  for(int i=0 ; i < _states.size() ; i++){
      weightSum+=_states[i].Weight;
  }
  weightSum+=std::numeric_limits<T>::epsilon();
  for(int i=0 ; i < _states.size() ; i++){
    double normalizedWeight = _states[i]->Weight / weightSum;
    normalizedWeights.push_back(normalizedWeight);
  }
  return normalizedWeights;
}

double ParticleFilter::_effectiveParticleCount(double weights[])
{
    double sumSqr;
    double sum;
    for(int i=0 ; i < weights.length ; i++){
      sumSqr+=weights[i] * weights[i];
      sum+=weights[i];
    }
    sumSqr+= std::numeric_limits<long>::epsilon();
    return sum / sumSqr;
}

void ParticleFilter::Update(IState* measure)
{
   for (int i=0;i<States.size();i++)
   {
      int dX = States[i].Position.X - measure->Position.X;
      int dY = States[i].Position.Y - measure->Position.Y;
      States[i].Weight = 1 / (std::Sqrt(dX * dX + dY * dY));
   }
}