#include <ParticleFilterTracker.h>
#include <ParticleFilter.h>
#include <limits>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/common/centroid.h>

#include <boost/thread/thread.hpp>

#define NUM_OBJECTS 1

ParticleFilterTracker::ParticleFilterTracker(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m)
{
  this->viewer_mutex=&m;
  last_size=0;
  this->viewer=vis;
}


void ParticleFilterTracker::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::mutex* viewer_mutex)
{
  
//   static 
  
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};
  
  for (size_t i = 0; i < clouds.size (); i++)
  {
    viewer_mutex->lock();
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = "tr" + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
    viewer_mutex->unlock();
  }   
  
  last_size=clouds.size();

}

inline void ParticleFilterTracker::removePreviousDataFromScreen ()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }

}



std::vector<PCLObject::Ptr> ParticleFilterTracker::Execute(pcl::PointCloud<SystemPoint>::CloudVectorType clouds_with_normals){
  std::vector<PCLObject::Ptr> objects;

  for (size_t i = 0; i < clouds_with_normals.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject());

    obj->objectCloud=clouds_with_normals[i].cloud;
    obj->normalCloud=clouds_with_normals[i].normals;
    pcl::compute3DCentroid(*clouds_with_normals[i].cloud,obj->centroid);
    
    
    

    objects.push_back(obj);
  }
  

  

  
  if(this->viewer){
    viewer_mutex->lock();
    removePreviousDataFromScreen();
    viewer_mutex->unlock();
    if(objects.size()>0)
    {
      displayCloudsColoured(objects,viewer_mutex);
    }
  }
  
  //return sorted_primitives;

}