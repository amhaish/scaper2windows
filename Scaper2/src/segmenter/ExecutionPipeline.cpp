#include <Consts.h>
#include <ExecutionPipeline.h>
#include <common/Timer.h>
#include <algorithm>

int ExecutionPipeline::verbosity = 4;

ExecutionPipeline::ExecutionPipeline() :
	pclNormalCalculator(0),
	pclSegmenter(0),
	segmentSelector(0),
	segmentTracker(0),
	segmentSonification(0),
	filter_timer("Cloud Filter Timer"),
	seg_timer("Segmenter Timer"),
	tracker_timer("Tracker Timer"),
	selector_timer("Selector Timer"),
	extract_timer("Feature Extraction Timer"),
	sound_timer("Sound Generation Timer"),
	pipeline_timer("Execution Pipeline Timer"),
	callback_timer("Callback Rate Timer"),
	img_viewer_min(0.0), img_viewer_max(0.0), img_viewer_num_viewed(0) {}


void ExecutionPipeline::setTiming(bool val) {
	this->timing = val;
}

void ExecutionPipeline::visualise(PointCloudWithNormals::ConstPtr  cloudWithNormals) {
	viewer_mutex->lock();

	boost::shared_ptr< pcl::visualization::PointCloudColorHandler<SystemPoint> > ch;
	bool has_rgb = pcl::traits::has_field<SystemPoint, pcl::fields::rgb>::value;
	if (has_rgb) {
		ch.reset(new pcl::visualization::PointCloudColorHandlerRGBField<SystemPoint>(cloudWithNormals->cloud));
	}
	else {
		ch.reset(new pcl::visualization::PointCloudColorHandlerCustom<SystemPoint>(cloudWithNormals->cloud, 255, 255, 255));
	}

	if (!orig_viewer->updatePointCloud(cloudWithNormals->cloud, *ch, "orig_cloud"))
		orig_viewer->addPointCloud(cloudWithNormals->cloud, *ch, "orig_cloud");

	orig_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "orig_cloud");
	orig_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, "orig_cloud");
	orig_viewer->addCoordinateSystem(0.1);

	if (displayNormals)
	{
		orig_viewer->removePointCloud("orig_normals");
		orig_viewer->addPointCloudNormals<SystemPoint, pcl::Normal>(cloudWithNormals->cloud, cloudWithNormals->normals, 1, 0.02f, "orig_normals");
		orig_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "orig_normals");
	}
	viewer_mutex->unlock();

	if (verbosity > 6) {
		std::cout << "Added cloud to viewer" << std::endl;
		std::cout << "Size of cloud is " << cloudWithNormals->cloud->points.size() << std::endl;
		int abc = 0;
		for (uint i = 0; i < cloudWithNormals->cloud->points.size(); i++) {
			if (pcl::isFinite(cloudWithNormals->cloud->points[i])) {
				abc++;
			}
		}
		std::cout << "Non-NAN points are " << abc << std::endl;
	}
}

void ExecutionPipeline::visualise_img(PointCloudWithNormals::ConstPtr  cloudWithNormals) {

	const int stretch_width = 2;
	const int stretch_height = 2;
	int stretch = stretch_width*stretch_height;

	size_t img_len = cloudWithNormals->cloud->size();
	size_t img_width = cloudWithNormals->cloud->width;
	size_t img_height = cloudWithNormals->cloud->height;
	assert(img_len == img_width*img_height);

	orig_viewer_img_data.resize(img_len*stretch);
	for (size_t r = 0; r < img_height; r++)
		for (size_t c = 0; c < img_width; c++)
			for (size_t i = 0; i < stretch_height; i++)
				for (size_t j = 0; j < stretch_width; j++)
					orig_viewer_img_data[(r*stretch_height + i)*img_width*stretch_width + c*stretch_width + j] = cloudWithNormals->cloud->points[r*img_width + c].z;

	struct nan_comparator_min {
		bool operator () (float x, float y) const
		{
			return std::isnan(y) ? true : x < y;
		}
	};

	struct nan_comparator_max {
		bool operator () (float x, float y) const
		{
			return std::isnan(x) ? true : x < y;
		}
	};

	float this_min = *std::min_element(orig_viewer_img_data.begin(), orig_viewer_img_data.end(), nan_comparator_min());
	float this_max = *std::max_element(orig_viewer_img_data.begin(), orig_viewer_img_data.end(), nan_comparator_max());

	img_viewer_min = img_viewer_min*img_viewer_num_viewed / (img_viewer_num_viewed + 1) + this_min * 1 / (img_viewer_num_viewed + 1);
	img_viewer_max = img_viewer_max*img_viewer_num_viewed / (img_viewer_num_viewed + 1) + this_max * 1 / (img_viewer_num_viewed + 1);
	img_viewer_num_viewed++;
	if (img_viewer_num_viewed > 10)img_viewer_num_viewed = 10; // stay dynamic

	if (verbosity > 5) {
		std::cout << "Min depth: " << this_min << " --> " << img_viewer_min << " ";
		std::cout << "Max depth: " << this_max << " --> " << img_viewer_max << std::endl;
	}

	viewer_mutex->lock();

	orig_viewer_img->addFloatImage(&orig_viewer_img_data[0], img_width*stretch_width, img_height*stretch_height, /*float min_value*/img_viewer_min, /*max_value*/img_viewer_max, /*grayscale*/false, "orig_float_image", /*opacity*/1.0);

	viewer_mutex->unlock();

}

void ExecutionPipeline::ExecutePipeline(const pcl::PointCloud<SystemPoint>::ConstPtr &constCloud) {

	if (timing)callback_timer.accumulate_and_print_accumulated_average();
	if (timing)callback_timer.reset();
	if (timing)pipeline_timer.reset();

	pcl::PointCloud<SystemPoint>::Ptr cloud(new pcl::PointCloud<SystemPoint>(*constCloud));
	//Normal calculation step==============================================================================
	 PointCloudWithNormals::Ptr cloudWithNormals = pclNormalCalculator->Execute(cloud);
	//=============================================================================================

	if (orig_viewer) {
		visualise(cloudWithNormals);
	}
	if (orig_viewer_img) {
		visualise_img(cloudWithNormals);
	}

	//Filtering step==============================================================================
	if (timing)filter_timer.reset();
	for (std::vector<IPCLFilter*>::size_type i = 0; i != this->pclFilters.size(); i++) {
		pclFilters[i]->Execute(cloudWithNormals);
	}
	if (timing)filter_timer.accumulate_and_print_accumulated_average();
	//=============================================================================================


	//Segmentation step==============================================================================
	PointCloudWithNormals::Vector segments;
	if (timing)seg_timer.reset();
	if (this->pclSegmenter) {
		segments = pclSegmenter->Execute(cloudWithNormals);
	}
	if (timing)seg_timer.accumulate_and_print_accumulated_average();
	//=============================================================================================


	std::pair<std::vector<PCLObject::Ptr>, std::vector<PCLObject::Ptr>> objects;
	PointCloudWithNormals::Vector selected;
	//if(segments.size()>0){
	//  if(timing)tracker_timer.reset();
	//  if(this->segmentTracker){
	//    objects=segmentTracker->Execute(segments);
	//    if(verbosity>3) std::cout<<objects.size()<<" object(s) tracked."<<std::endl;
	//  }
	//  else{
	//   assert(0); 
	//  }
	//  if(timing)tracker_timer.accumulate_and_print_accumulated_average();
	//  
	//  if(timing)selector_timer.reset();
	//
	//  if(this->segmentSelector){
	//    selected=segmentSelector->Execute(objects);
	//    if(verbosity>3) std::cout<<selected.size()<<" object(s) selected."<<std::endl;
	//  }
	//  else{
	//   assert(0); 
	//  }
	//  
	//  if(timing)selector_timer.accumulate_and_print_accumulated_average();
	//  
	//  if(timing)sound_timer.reset();
	//  if(this->segmentSonification){
	//     this->segmentSonification->Execute(selected);
	//  }
	//  else{
	//      assert(0);
	//  }
	//  if(timing)sound_timer.accumulate_and_print_accumulated_average();
	//  
	//} else{
	//  
	//  std::cout<<"No objects returned from segmenter"<<std::endl;
	//  if(timing)sound_timer.reset();
	//  if(this->segmentSonification){
	//     this->segmentSonification->Execute(selected);
	//  }
	//  /*if(this->soundGenerator){
	//    this->soundGenerator->Execute(objects);
	//  }*/
	//  
	//  if(timing)sound_timer.accumulate_and_print_accumulated_average();
	//}

	//if (timing)pipeline_timer.accumulate_and_print_accumulated_average();
}

void  ExecutionPipeline::AddOrigViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer> vis, boost::mutex& viewer_mutex) {
	orig_viewer = vis;
	this->viewer_mutex = &viewer_mutex;
}

void  ExecutionPipeline::AddOrigImageViewer(boost::shared_ptr<pcl::visualization::ImageViewer> vis, boost::mutex& viewer_mutex) {
	orig_viewer_img = vis;
	this->viewer_mutex = &viewer_mutex;
}

void ExecutionPipeline::SetPCLNormalCalculator(IPCLNormalCalculator* normalCalculator)
{
	this->pclNormalCalculator = normalCalculator;
}

void ExecutionPipeline::SetPCLSegmenter(IPCLSegmenter* segmenter) {
	this->pclSegmenter = segmenter;
}

void ExecutionPipeline::SetSegmentSelector(ISegmentSelector* segmentSelector) {
	this->segmentSelector = segmentSelector;
}

void ExecutionPipeline::SetSegmentTracker(ISegmentTracker* segmentTracker) {
	this->segmentTracker = segmentTracker;
}

void ExecutionPipeline::SetSoundSonificationApproach(ISegmentSonify* segmentSonification) {
	this->segmentSonification = segmentSonification;
}

ExecutionPipeline::~ExecutionPipeline() {
}

void  ExecutionPipeline::AddPCLFilter(IPCLFilter* filter) {
	this->pclFilters.push_back(filter);
}

void ExecutionPipeline::ClearPCLFilters() {
	this->pclFilters.clear();
}