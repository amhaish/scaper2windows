#include <SegmentTrackerHC.h>
#include <vis/data_assoc_vis.h>


#include <limits>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/common/centroid.h>

#include <boost/thread/thread.hpp>


#define NUM_OBJECTS 3

int stvvv=3;

SegmentTrackerHC::SegmentTrackerHC(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m){
  this->viewer_mutex=&m;
  this->units_divisor=units_divisor;
  last_size=0;
  this->viewer=vis;//new pcl::visualization::PCLVisualizer("Objects Tracker Visualizer");
  //this->viewer->setBackgroundColor(1.0,1.0,1.0); //test this: should look better when printed to paper
}

void SegmentTrackerHC::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::mutex* viewer_mutex)
{
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};

  for (size_t i = 0; i < clouds.size(); i++)
  {
    viewer_mutex->lock();
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = "TRK" + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
    viewer_mutex->unlock();
  }
  /*
  for(unsigned int i=clouds.size();i<last_size;i++){
    std::string s = "TRK" + std::to_string(i);
    viewer->removePointCloud(s.c_str());
  }
  */
  last_size=clouds.size();
}

inline void SegmentTrackerHC::removePreviousDataFromScreen()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }
}

float SegmentTrackerHC::findSize(PCLObject::Ptr obj){
    float objDist = sqrt(obj->centroid[0]*obj->centroid[0] + obj->centroid[1]*obj->centroid[1] + obj->centroid[2]*obj->centroid[2]);
    obj->distance=objDist;
    float objCloudSize = obj->objectCloud->size();
    
    return objDist*objCloudSize;
}

std::vector<PCLObject::Ptr> SegmentTrackerHC::Execute(PointCloudWithNormals::Vector clouds){
  
  std::vector<PCLObject::Ptr> objects;
  std::vector<PCLObject::Ptr> selectedObjects;
  static std::vector<PCLObject::Ptr> lastFrameBuf;
  std::vector<PCLObject::Ptr> primitives;
  std::vector <std::pair<int,int> > matches;
  
  float objSize;
  
  int zoomPercent = 0;
  ParameterTaking *sp = ParameterTaking::GetSerialPort();
  
  if(sp->isSerialPortRunning())
      zoomPercent = sp->GetRotaryEncoder();
  
  for (size_t i = 0; i < clouds.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject());
    obj->track_id = -1;
    obj->isMatched = false;
    obj->objectCloud = clouds[i].cloud;//boost::make_shared<pcl::PointCloud<SystemPoint> >(clouds[i]);
    obj->normalCloud = clouds[i].normals;
    pcl::compute3DCentroid(*(clouds[i].cloud),obj->centroid);
    objSize = findSize(obj);
    obj->size = objSize;
    
    float zoomAmount = ((float)zoomPercent/100)*35;
    //obj->centroid[2] = obj->centroid[2] - zoomAmount;
     
    //double this_salience = objSize / obj->centroid.norm() ;
    
    //salience.push_back(this_salience);
    if(objSize > 3000)
      objects.push_back(obj);
  }
  
  // this is a really basic way of trying to get some kind of consistency in data association.
  // a better way would be to check the closeness of all objects to each other, use some kind of threshold to
  // disocciate ones that are not close, then choose the optimal match
  
  int n=0;
/*
  while(objects.size()>0&&primitives.size()<NUM_OBJECTS){
      unsigned int besti=0;
      double bestf = 0;//0=std::numeric_limits<double>::max();
      for(unsigned int i=0;i<objects.size();i++){
	  if(salience[i]>bestf){
	   bestf=salience[i];
	   cout << "bestf = " << bestf << endl;
	   besti=i;
	   cout << "besti = " << besti << endl;
	  }
      }
      objects[besti]->track_id=n++;
      cout << "object track id = " << objects[besti]->track_id << endl;
      primitives.push_back(objects[besti]);
      objects.erase(objects.begin()+besti);
      salience.erase(salience.begin()+besti);
  }
*/

// --------------------------------------------------------------------------------------------------------------------------------------------------

  int indexes[3];
  int x = 0;
  // Selecting 3 closest object to sensor for comparing origin difference
  while(objects.size() > 0 && selectedObjects.size() < 3 && x < objects.size()){
    float bestScore = std::numeric_limits<float>::max();
    int index;
    float currentScore;
    for(unsigned int i = 0; i < objects.size(); i++){
      if(objects[i]->isMatched == true) continue;
      
      currentScore = abs(objects[i]->centroid[0]*objects[i]->centroid[1]*objects[i]->centroid[2]);
      if(currentScore < bestScore){
	bestScore = currentScore;
	index = i;
      }
    }
    selectedObjects.push_back(objects[index]);
    indexes[x] = index;
    x++;
    objects[index]->isMatched = true;
  }
  
  for(unsigned int i = 0; i < objects.size(); i++){
      objects[i]->isMatched = false;
  }
  
  float dist;
  int bestj;
  
  // Tracking selected 3 objects
  if(selectedObjects.size() > 0 && lastFrameBuf.size() > 0){
      float smallestDist = std::numeric_limits<float>::max();
      
      for(unsigned int i = 0; i < selectedObjects.size(); i++){
// 	  if(selectedObjects[i]->isMatched == true) continue;
	  
	  for(unsigned int j = 0; j < lastFrameBuf.size(); j++){
	      if(lastFrameBuf[j]->isMatched == true) continue;
	      
	      //dist = sqrt( (selectedObjects[i]->centroid[0]-lastFrameBuf[j]->centroid[0])*(selectedObjects[i]->centroid[0]-lastFrameBuf[j]->centroid[0]) + (selectedObjects[i]->centroid[1]-lastFrameBuf[j]->centroid[1])*(selectedObjects[i]->centroid[1]-lastFrameBuf[j]->centroid[1]) +
	      //	     (selectedObjects[i]->centroid[2]-lastFrameBuf[j]->centroid[2])*(selectedObjects[i]->centroid[2]-lastFrameBuf[j]->centroid[2]) );
	      
	      dist = (selectedObjects[i]->centroid - lastFrameBuf[j]->centroid).norm();

	      //score = dist * ( 1 / findSize(selectedObjects[i]) ) * selectedObjects[i]->centroid.norm();
	      //changingRating = abs( ( findSize(selectedObjects[i]) - findSize(lastFrameBuf[j]) ) / findSize(lastFrameBuf[j]) );
	      
	      if(dist < smallestDist){
		  smallestDist = dist;
		  bestj = j;
	      }
	  }
	  
	  if(lastFrameBuf[bestj]->track_id == -1) objects[indexes[i]]->track_id = n++;
	  else objects[indexes[i]]->track_id = lastFrameBuf[bestj]->track_id;
	  lastFrameBuf[bestj]->isMatched = true;
	  primitives.push_back(selectedObjects[i]);
	  matches.push_back(std::pair<int,int>(indexes[i],bestj));
      }
  }

//   DataAssocVisualizer vis(objects,lastFrameBuf,matches);

  lastFrameBuf.erase(lastFrameBuf.begin(), lastFrameBuf.end());
  lastFrameBuf = objects;
  
  for(unsigned int i = 0; i < lastFrameBuf.size(); i++){
      lastFrameBuf[i]->isMatched = false;
  }
  
  if(this->viewer){
    viewer_mutex->lock();
    removePreviousDataFromScreen();
    viewer_mutex->unlock();
    if(primitives.size() > 0){
      displayCloudsColoured(primitives,viewer_mutex);
    }
  }
  
  return primitives;
}