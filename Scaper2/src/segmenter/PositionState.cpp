  #include <PositionState.h>
  #include <RandomGenerator.h>
  
  IState* PositionState::Copy(){
     PositionState* po=new PositionState();
     po->Centriod=this->Centriod;
     //po->Variance=this->Variance;
     po->Weight=this->Weight;
     return po;
  }
      
  void PositionState::Diffuse(){
    this->Centriod.x = this->Centriod.x + RandomGenerator::NextDouble(-Variance,Variance);
    this->Centriod.y = this->Centriod.y + RandomGenerator::NextDouble(-Variance,Variance);
    this->Centriod.z = this->Centriod.z + RandomGenerator::NextDouble(-Variance,Variance);
  }
  
  IState* PositionState::FromArray(double arr[]){
    PositionState* po=new PositionState();
    po->Centriod.x = arr[0];
    po->Centriod.y = arr[1];
    po->Centriod.z = arr[2];
    return po;
  }