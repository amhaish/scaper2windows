#include<SoundGenerator2.h>
// #include <boost/graph/graph_concepts.hpp>

int sgvvv = 0;

#define DEFAULT_FPS 15
#define BUFFER_SIZE 44100*10
#define NUM_OBJECTS 2


//TODO: put it somewhere better
std::ostream& operator<<(std::ostream& stream,
                         const PCLObject& obj) {
    stream<<"<<PCLObject: key:"<<std::setw(5)<<obj.key;
    stream<<" width:"<<obj.width;
    stream<<" height:"<<obj.height;
    stream<<" depth:"<<obj.depth;
    stream<<" size:"<<obj.size;
    stream<<" distance:"<<obj.distance;
    stream<<" centroid:"<<obj.centroid[0]<<","<<obj.centroid[1]<<","<<obj.centroid[2];
    stream<<" cloud-size:"<<obj.objectCloud->size();
    stream<<">>";
    return stream;

}


SoundGenerator::SoundGenerator() { // 44100*10 = 10 sec worth
  
    
    while(ssg.size()<NUM_OBJECTS){
      std::shared_ptr<SoundSourceFullWaveformGenerator> ssgx(new SoundSourceFullWaveformGenerator);
      ssg.push_back(ssgx);
      full_buffer.push_back(std::vector<short>(BUFFER_SIZE));
      std::shared_ptr<SoundSourceWaveformInterpolator> ssix(new SoundSourceWaveformInterpolator);
      ssi.push_back(ssix);
      consuming_buffer.push_back(CircularBuffer<short>(BUFFER_SIZE));
      std::shared_ptr<SoundSourceOutput> ssox(new SoundSourceOutput(DEFAULT_FPS));
      sso.push_back(ssox);
      last_pos.push_back(Eigen::Vector4f());
    }

}
void SoundGenerator::setExaggeration(double multiplier) {
    exaggerate_pos=multiplier;
}

double SoundGenerator::randd() {
    return ((double)rand())/RAND_MAX;
}
void SoundGenerator::Execute(std::vector<PCLObject::Ptr> objects) {
    Eigen::Vector4f pos_before_exaggeration(0,0,0,0);
    Eigen::Vector4f pos_after_exaggeration(0,0,0,0);
    
//     while(objects.size()>ssg.size() && ssg.size()<10){
//       ssg.push_back(SoundSourceGenerator(DEFAULT_FPS));
//       sso.push_back(SoundSourceOutput(DEFAULT_FPS));
//       csb.push_back(CircularBuffer<short>(BUFFER_SIZE));
//       last_pos.push_back(Eigen::Vector4f());
//     }
//     std::vector<SoundFeature> new_last_played;
    if(objects.size()>0) {
      for(unsigned int i=0;i<objects.size() ;i++)
      {
	if(sgvvv>2)std::cout<<"Object["<<i<<"]:"<<*objects[i]<<std::endl;
// 	if(objects[i]->width * objects[i]->height > 0.00001 && objects[i]->size < 0.1)
// 	{
	  if(objects[i]->track_id>=NUM_OBJECTS){
	    
	    if(sgvvv>2)std::cerr<<"Warning: Sound generation not yet set up to deal with object ids more than "<<NUM_OBJECTS<<" (got "<<objects[i]->track_id+1<<")"<<std::endl;
	    
	  }
	  else{
	    
	    uint oid=objects[i]->track_id;
	    SoundFeature sfeat;
	    pos_before_exaggeration=objects[oid]->centroid;

	    for(size_t ii=0; ii<2; ii++)
		pos_after_exaggeration[ii]=pos_before_exaggeration[ii]*this->exaggerate_pos;

	    sfeat.pos=pos_after_exaggeration;
	    //
	    float sizeBaseOctave=440; //The size will 1 octave from 440 to 880
	    float sizeMinValue=0.001;
	    float sizeMaxValue=0.1;
	    float SizePercentMinValue=1;
	    float SizePercentMaxValue=2;
	    float sizePercent = (((objects[oid]->size - sizeMinValue)*(SizePercentMaxValue-SizePercentMinValue)) / (sizeMaxValue-sizeMinValue)) + SizePercentMinValue;
	    
	    float curvatureBaseOctave=1760;
	    //float curvaturePercent=;
	    
	    
	    
	    if(sgvvv>2)std::cout<<"Size percent is: "<<sizePercent<<"\n";
	    float fw1=330/(2*objects[oid]->width);
	    float fh1=330/(2*objects[oid]->height);
	    float fd1=330/(2*objects[oid]->depth);
	    
	    
    // 	sfeat.frequencies.push_back(sizeBaseOctave * sizePercent);
	    sfeat.frequencies.push_back(fw1/2);
	    sfeat.frequencies.push_back(fw1/2+rand()*0.0001);
	    sfeat.frequencies.push_back(fh1+rand()*0.0001);
    // 	sfeat.frequencies.push_back(fd1);
    // 	sfeat.frequencies.push_back(fw1/2);
    // 	sfeat.frequencies.push_back(fh1/2);
	    sfeat.frequencies.push_back(fd1/4+rand()*0.0001);
	    
	    //sfeat.frequencies.push_back(1000/(objects[0]->centroid.norm() * objects[0]->centroid.norm()));
	    
	    
	    double ratio = log(objects[oid]->width / objects[oid]->height);
	    if(ratio  > 0){
		if(ratio >0.9)ratio=0.9;
		if(sgvvv>3)std::cout<<"ratio:"<<ratio<<std::endl;
		SoundFeature sfeatold=ssg[oid]->get_last_played_sound_feature();
		for(unsigned int fno=0;fno<sfeat.frequencies.size()&&fno<sfeatold.frequencies.size();fno++){
		    sfeat.frequencies[fno]=sfeat.frequencies[fno]*(1-ratio)+sfeatold.frequencies[fno]*ratio;
		}
	    }
	    
	    
	    double moved_dist;
	    if(last_pos[oid].norm()>0)
		moved_dist=(pos_before_exaggeration - last_pos[oid]).norm();
	    else
		moved_dist=5;
	    if(moved_dist<0.8)moved_dist=0.8;
	    
	    sfeat.energy_mult=0.1/moved_dist;
	    sfeat.energy_mult=0.05;

	    sfeat.spread_mult=1.0;

	    ssg[oid]->generate_sound(sfeat,full_buffer[oid]);
	    ssi[oid]->interpolate_sound(full_buffer[oid],consuming_buffer[oid],SoundSourceWaveformInterpolator::RestartFadeStartEnd);

	    if(sgvvv>0)std::cout<<"SoundGenerator::Execute - Playing "<<sfeat<<std::endl;
	    last_pos[oid]=pos_before_exaggeration;
	  }
// 	}
      }  
    } 
    
    for(unsigned int i=0;i<sso.size();i++)
      sso[i]->output(consuming_buffer[i],last_pos[i]);
}
