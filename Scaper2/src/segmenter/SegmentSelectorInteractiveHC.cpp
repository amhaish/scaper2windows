#include <SegmentSelectorInteractiveHC.h>
#include <boost/thread/thread.hpp>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/math/special_functions/fpclassify.hpp>
#include <pcl/common/centroid.h>
#include <limits>
#include <cmath>

int ssvvv=3;
bool isFirstTime = true;
bool selecting_helper = false;


SegmentSelectorInteractiveHC::SegmentSelectorInteractiveHC(uint units_divisor,boost::shared_ptr<pcl::visualization::PCLVisualizer> vis,boost::mutex& m){
  this->viewer_mutex=&m;
  this->units_divisor=units_divisor;
  last_size=0;
  this->viewer=vis;//new pcl::visualization::PCLVisualizer("Objects Tracker Visualizer");
  //this->viewer->setBackgroundColor(1.0,1.0,1.0); //test this: should look better when printed to paper
}

void SegmentSelectorInteractiveHC::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::mutex* viewer_mutex)
{
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};
  
  for (size_t i = 0; i < clouds.size (); i++)
  {
    viewer_mutex->lock();
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = "tr" + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
    viewer_mutex->unlock();
  }
  
//   for(unsigned int i=clouds.size();i<last_size;i++){
    
//    viewer_mutex->lock();
//     std::string s = "tr" + std::to_string(i);
//     viewer->removePointCloud(s.c_str());
//     viewer_mutex->unlock();
//   }
//   
  
  last_size=clouds.size();
}

inline void SegmentSelectorInteractiveHC::removePreviousDataFromScreen()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }
}

std::vector<PCLObject::Ptr> SegmentSelectorInteractiveHC::Execute(std::vector<PCLObject::Ptr> &segments){
  
  std::vector<PCLObject::Ptr> objects;
  std::vector<PCLObject::Ptr> sortedObjects;
  std::vector<PCLObject::Ptr> selectedObjects;

  int zoomPercent = 0;
  


  int nearestObjIndex;
  int biggestObjIndex;
  static int selectedObjIndex = -1;
  float nearestObjZ = std::numeric_limits<float>::max();
  float biggestObjSize = std::numeric_limits<float>::min();
  float objDist;
  float objSize;
  
  for (size_t i = 0; i < segments.size(); i++)
  {
    PCLObject::Ptr obj(new PCLObject(*segments[i]));
    
    // already calculated earlier ??
    pcl::compute3DCentroid(*obj->objectCloud,obj->centroid);
    
    // already calculated earlier ??
     objDist = sqrt(obj->centroid[0]*obj->centroid[0] + obj->centroid[1]*obj->centroid[1] + obj->centroid[2]*obj->centroid[2]);
     obj->distance=objDist;
    objSize = segments[i]->objectCloud->size();
    obj->isMatched = false;
    
    // dj this does not make sense to me but keeping it for faithfulness
    float zoomAmount = ((float)zoomPercent/100)*35;
    obj->centroid[0] = obj->centroid[0] - zoomAmount;
    obj->centroid[1] = obj->centroid[1] - zoomAmount;
    obj->centroid[2] = obj->centroid[2] - zoomAmount;
   
    if(obj->centroid[2] < nearestObjZ && obj->centroid[2] > 0.420*units_divisor){
      nearestObjZ = obj->centroid[2];
      nearestObjIndex = i;
    }
    if(objDist*objSize > biggestObjSize){
      biggestObjSize = objDist*objSize;
      biggestObjIndex = i;
    }
//     if(obj->centroid[2] > 4.20) //WHY????
//       objects.push_back(obj);
      
//!!!!!!!!!!!      20160808: Hakan says we need this instead... still discussing it:
//     float zoomAmount = ((float)zoomPercent/100)*35;
//     obj->centroid[2] = obj->centroid[2] - zoomAmount;
//     if(zoomPercent == 0){
//       if(objSize > 3000)
//       objects.push_back(obj);
//     }
//     else{
//       //DJD: parametrise this all???
//       if(objSize > 3000 && obj->centroid[2] > 0.8*units_divisor) // in depthsense
//       objects.push_back(obj);
//     }
//!!!!!!!!!!!!!

  }
  
  float x_axis;
  int index;
  
  // Sorting segments according to their position (left to right)
  while(sortedObjects.size() < objects.size()){
    x_axis = std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < objects.size(); i++){
      if(objects[i]->isMatched == true) continue;
      if(objects[i]->centroid[0] < x_axis){
	x_axis = objects[i]->centroid[0];
	index = i;
      }
    }
    objects[index]->isMatched = true;
    sortedObjects.push_back(objects[index]);
  }
  if(objects.size() > 0 && sortedObjects.size() > 0){

      if(objectSelectionType == 1){
	selecting_helper = false;
	isFirstTime = true;
	selectedObjIndex = nearestObjIndex;
	if(ssvvv>2)cout << "nearest segment selected" << endl;
	selectedObjects.push_back(objects[selectedObjIndex]);
      }
      else if(objectSelectionType == 2){
	selecting_helper = false;
	isFirstTime = true;
	selectedObjIndex = biggestObjIndex;
	if(ssvvv>2)cout << "biggest segment selected" << endl;
	selectedObjects.push_back(objects[selectedObjIndex]);
      }
      else if(objectSelectionType == 3){
	selecting_helper = true;
	if(isFirstTime) selectedObjIndex = sortedObjects.size() / 2;
	else{
	  if(selectedObjIndex > 0) selectedObjIndex--;
	}
	if(selectedObjIndex >= sortedObjects.size()-1) selectedObjIndex = sortedObjects.size()-1;
	if(ssvvv>2)cout << "the segment that is on the left of previous segment is selected" << endl;
	selectedObjects.push_back(sortedObjects[selectedObjIndex]);
	isFirstTime = false;
      }
      else if(objectSelectionType == 4){
	selecting_helper = true;
	if(isFirstTime) selectedObjIndex = sortedObjects.size() / 2;
	else{
	  if(selectedObjIndex >= sortedObjects.size()-1) selectedObjIndex = sortedObjects.size()-1;
	  else selectedObjIndex++;
	}
	if(ssvvv>2)cout << "the segment that is on the right of previous segment is selected" << endl;
	selectedObjects.push_back(sortedObjects[selectedObjIndex]);
	isFirstTime = false;
      }
      else{
	if(selectedObjIndex == -1) selectedObjIndex = nearestObjIndex;	// Default selection option
	if(selectedObjIndex >= sortedObjects.size()-1) selectedObjIndex = sortedObjects.size()-1;
	if(ssvvv>2)cout << "Default selecting option" << endl;
	cout << "selection type: " << objectSelectionType << endl; 
	if(selecting_helper == false)
	  selectedObjects.push_back(objects[selectedObjIndex]);
	else
	  selectedObjects.push_back(sortedObjects[selectedObjIndex]);
      }
  
      if(ssvvv>2)cout << "selectedObjIndex= " << selectedObjIndex << endl;
  }
  else { 
    if(ssvvv>2) cout << "There are no objects to select (probably because of zooming)." << endl;
  }
  
  if(ssvvv>2)cout << "-------------------------------------------------------------------------------------" << endl;
  
  if(this->viewer){
    viewer_mutex->lock();
    removePreviousDataFromScreen();
    viewer_mutex->unlock();
    if(selectedObjects.size()>0)
    {
      displayCloudsColoured(selectedObjects,viewer_mutex);
    }
  }
  
  return selectedObjects;
}