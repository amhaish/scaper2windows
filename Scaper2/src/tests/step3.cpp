
#include <AL/al.h>
#include <AL/alc.h>
#define _USE_MATH_DEFINES
#include <cmath>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#define sleep(seconds) Sleep(seconds * 1000)
#else
#include <unistd.h>
#endif


void CheckError();
void InitializeOpenAL();
void TerminateOpenAL();
void wave(double freq , double sec);


int main(int argc, char** argv)
{
  InitializeOpenAL();
  double f = 440;
  double s = 2.3;
  wave(f,s);
  
  /*float frequency = 440.f;
  int seconds = 2;
  unsigned sampleRate = 44100;
  size_t bufferSize = seconds * sampleRate;

  short* samples = new short[bufferSize];
  for(size_t i{0}; i < bufferSize; ++i)
  {
    float sineWave = std::sin((2.f*float(M_PI)*frequency)/sampleRate * i);
    short pitch = 32767;
    if(sineWave < 0)
    {
      pitch = -32768;
      sineWave = std::abs(sineWave);
    }
    samples[i] = (short)(pitch * sineWave);
  }*/

  
  
  TerminateOpenAL();
  
  return 0;
}
 
void wave(double frequency , double secound)
{
  double freq= frequency;
  double sec = secound;
  unsigned sampleRate = 44100;
  int s = sec;
  
  size_t bufferSize = (s+1)* sampleRate ;
  short* samples = new short[bufferSize];
  
  for(size_t i{ 0 }; i < bufferSize; ++i)
  {
      double sineWave = std::sin((2 * double(M_PI)*freq)/sampleRate * i);
      short pitch = 32767;
      if(sineWave < 0)
	{
	  pitch = -32768;
	  sineWave = std::abs(sineWave);
	}
     samples[i] = (double)(pitch * sineWave);
  }
  ALuint sampleBuffer;
  alGenBuffers(1, &sampleBuffer);
  //CheckError();

  alBufferData(sampleBuffer, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
  //CheckError();

  ALuint source = 0;
  alGenSources(1, &source);
 // CheckError();
  alSourcei(source, AL_BUFFER, sampleBuffer);
  alSourcePlay(source);
  fgetc(stdin);
  
  sleep(sec);

  alSourceStop(source);
  alDeleteSources(1, &source);
  alDeleteBuffers(1, &sampleBuffer);

  delete[] samples;
  
}




/*void CheckError()
{
  ALenum err = alGetError();

  if(AL_NO_ERROR != err)
  {
    exit(err);
  }
}*/


void InitializeOpenAL()
{
  ALCdevice* device{nullptr};
  ALCcontext* context{nullptr};

  const char* defaultDevice = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

  
  ///
  ALfloat SourcePos[] = { 0.0, 0.0, 0.0 };
  ALfloat SourceVel[] = { 0.0, 0.0, 0.0 };
  
  ///
  ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };
  ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };
  ALfloat ListenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
  ///
  alListenerfv(AL_POSITION,listenerPos);
  alListenerfv(AL_VELOCITY,listenerVel);
  alListenerfv(AL_ORIENTATION,listenerOri);
	
  
  device = alcOpenDevice(defaultDevice);
  context = alcCreateContext(device, NULL);
  alcMakeContextCurrent(context);
 // CheckError();
}

void TerminateOpenAL()
{
  ALCcontext* context = alcGetCurrentContext();
  ALCdevice* device = alcGetContextsDevice(context);

  alcMakeContextCurrent(NULL);
  alcDestroyContext(context);
  alcCloseDevice(device);
}
