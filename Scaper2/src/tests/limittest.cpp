#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <AL/al.h>


#include <AL/alc.h>


#include <AL/alut.h>

#include <memory>
#include <boost/shared_ptr.hpp>

#include <sstream>

#include <boost/concept_check.hpp>


int main(int argc,char ** argv) {

    printf("I tried this on Kubuntu 14.04 OpenALSoft and the number of buffers was unlimited but the number of sources maxxed out at 256");

    alutInit(0,NULL);
// alGetError();
    int NUM_BUFFERS=1024;
    int NUM_SOURCES=256;

    ALuint buffers[NUM_BUFFERS];
    std::cout<<"allocate buffers"<<std::endl;
    alGenBuffers(NUM_BUFFERS,buffers);
    std::cout<<"done"<<std::endl;
    ALenum error;

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("Could not generate buffers\n");
        return 1;
    }
    printf("ok\n");
//   /usr/share/skype/sounds/CallHold.wav
// sleep(1);




    ALenum format=AL_FORMAT_MONO16;
    int seconds = 4;
    unsigned sample_rate = 22050;
    size_t buf_size = seconds * sample_rate;
    int bytes_per_entry=2;
//     double med = buf_size/2;

    int freq=330;
    short *samples;
    samples = new short[buf_size];

    double entry=3200;

    for(unsigned int i=0; i<buf_size; ++i) {



        samples[i] = 32760 * sin( (2.f*float(M_PI)*freq)/sample_rate * i );
// 	if(i<entry)samples[i]=samples[i]*i/entry;
        if(i>buf_size-entry) {
//  	  printf("%d",samples[i]);
// 	  samples[i]=samples[i]*3;

            samples[i]=(samples[i]*1.0f/entry)*(buf_size-i-1);


//  	  printf("-->%d\n",samples[i]);

        }
//         if(i>buf_size-entry && i < buf_size-entry+10) {
// 	  samples[i]=32760;
// 	}
// 	samples[i]=(samples[i]*1.0f/buf_size)*(buf_size-i);

    }
    for(unsigned int i=0; i<buf_size; ++i) {
//       printf("%d\n",samples[i]);
    }

    std::cout<<"buffer data"<<std::endl;
    for(int i=0; i<NUM_BUFFERS; i++) {
        alBufferData(buffers[i], format, samples, buf_size*bytes_per_entry, sample_rate);
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            printf("Could not buffer data\n%d\n",i);
            return 1;
        }
    }
    std::cout<<"done"<<std::endl;



    ALuint src[NUM_SOURCES];
    std::cout<<"gen sources"<<std::endl;
    alGenSources(NUM_SOURCES, src);


    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("Could not gen sources\n");
        return 1;
    }

    std::cout<<"dine"<<std::endl;
//     alSourcei(src, AL_BUFFER, buf);
    int j = 0;
    for(int i=0; i<NUM_SOURCES; i++) {
        j++;
        if (j>=NUM_BUFFERS)j=0;
        std::cout<<"play source "<<i<<" from buffer "<<j<<std::endl;
        alSourceQueueBuffers(src[i], 1, &buffers[j]);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            printf("Could not queue buffers\n");
            return 1;
        }
        alSourcePlay(src[i]);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            printf("Could not play source\n");
            return 1;
        }
    }


//     double start=0;
//     double end=15;
//     double step=1;

    sleep(seconds*2);
    alutExit();

    exit(0);
}
