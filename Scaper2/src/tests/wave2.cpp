#include <AL/al.h>
#include <AL/alc.h>
#define _USE_MATH_DEFINES
#include <cmath>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#define sleep(seconds) Sleep(seconds * 1000)
#else
#include <unistd.h>
#endif


void CheckError();
void InitializeOpenAL();
void TerminateOpenAL();
void wave(double freq , double sec);


int main(int argc, char** argv)
{
  InitializeOpenAL();
  double f = 440;
  double s = 2.3;
  wave(f,s); 
  
  TerminateOpenAL();
  
  return 0;
}




 
void wave(double frequency , double secound)
{
  double freq= frequency;
  double sec = secound;
  unsigned sampleRate = 44100;
  int s = sec;
  
  size_t bufferSize = (s+1)* sampleRate ;
  short* samples = new short[bufferSize];
  
  for(size_t i{ 0 }; i < bufferSize; ++i)
  {
      double sineWave = std::sin((2 * double(M_PI)*freq)/sampleRate * i);
      short pitch = 32767;
      if(sineWave < 0)
	{
	  pitch = -32768;
	  sineWave = std::abs(sineWave);
	}
     samples[i] = (double)(pitch * sineWave);
  }


  ALuint sampleBuffer;
  alGenBuffers(1, &sampleBuffer);

  alBufferData(sampleBuffer, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);

  
  
  ALuint source = 0;
//
	ALfloat SourcePos[] = {2.0 , 0.0 , 2.0};
	ALfloat SourceVel[] ={0.0 , 0.0 , 1.0};
//  ALfloat SourceOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
	ALfloat Listenerpos[]={0.0 ,0.0 ,0.0};
	ALfloat ListenerVel[]={0.0 , 0.0 , 0.0};
	ALfloat ListenerOri[]={0.0 , 0.0 , -1.0, 0.0 , 1.0 , 0.0};


//     alSourceQueueBuffers(src[i], 1, &buffers[j]);

  alGenSources(1, &source);
  alListenerfv(AL_POSITION,listenerPos);
  alListenerfv(AL_VELOCITY,listenerVel);
  alListenerfv(AL_ORIENTATION,listenerOri);
  alSourcei(source, AL_BUFFER, sampleBuffer);
  alSourcei (Source, AL_LOOPING,  AL_TRUE  );
  alSourcePlay(source);
  fgetc(stdin);
  
  sleep(sec);

  alSourceStop(source);
  alDeleteSources(1, &source);
  alDeleteBuffers(1, &sampleBuffer);

  delete[] samples;
  
}



void InitializeOpenAL()
{
  ALCdevice* device{nullptr};
  ALCcontext* context{nullptr};

  const char* defaultDevice = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

  device = alcOpenDevice(defaultDevice);
  context = alcCreateContext(device, NULL);
  alcMakeContextCurrent(context);
}



void TerminateOpenAL()
{
  ALCcontext* context = alcGetCurrentContext();
  ALCdevice* device = alcGetContextsDevice(context);

  alcMakeContextCurrent(NULL);
  alcDestroyContext(context);
  alcCloseDevice(device);
}
