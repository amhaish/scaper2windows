#include <AL/al.h>
#include <AL/alc.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#define sleep(seconds) Sleep(seconds * 1000)
#else
#include <unistd.h>
#include <boost/graph/graph_concepts.hpp>
#endif

#include <math.h>

#define PI 3.14159


void CheckError();
void InitializeOpenAL();
void TerminateOpenAL();
void wave(double freq , double sec);
void ChangePosition(float alpha, ALfloat pos);
float FindTheta( float pos);

ALfloat new_pos;
ALuint source;
ALuint sampleBuffer;

int main(int argc, char** argv)
{
  InitializeOpenAL();
  double f = 440;
  double s = 2.3;
  wave(f,s); 
  TerminateOpenAL();
  
  return 0;
}




 
void wave(double frequency , double secound)
{
  float theta;
  double freq= frequency;
  double sec = secound;
  unsigned sampleRate = 44100;
  int s = sec;
  
  size_t bufferSize = (s+1)* sampleRate ;
  short* samples = new short[bufferSize];
  
  for(size_t i{ 0 }; i < bufferSize; ++i)
  {
      double sineWave = std::sin((2 * double(M_PI)*freq)/sampleRate * i);
      short pitch = 32767;
      if(sineWave < 0)
	{
	  pitch = -32768;
	  sineWave = std::abs(sineWave);
	}
     samples[i] = (double)(pitch * sineWave);
  }

  source=0;
  
  alGenBuffers(1, &sampleBuffer);

  alBufferData(sampleBuffer, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
   
 
//
	ALfloat SourcePos[] = {2.0 , 0.0 , 2.0};
	theta = FindTheta(SourcePos);
	
// 	ALfloat SourceVel[] ={0.0 , 0.0 , 1.0};
//      ALfloat SourceOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
// 	ALfloat listenerPos[]={0.0 ,0.0 ,0.0};
// 	ALfloat listenerVel[]={0.0 , 0.0 , 0.0};
// 	ALfloat listenerOri[]={0.0 , 0.0 , -1.0, 0.0 , 1.0 , 0.0};

//     alGetSourcei(source, AL_BUFFERS_PROCESSED, &val);
//     alSourceQueueBuffers(src[i], 1, &buffers[j]);

  alGenSources(1, &source);
//     alListenerfv(AL_POSITION,listenerPos);
//     alListenerfv(AL_POSITION,listenerPos);
//     alListenerfv(AL_VELOCITY,listenerVel);
//     alListenerfv(AL_ORIENTATION,listenerOri);
//     alSourcei(source, AL_BUFFER, sampleBuffer);
  
  alSourceQueueBuffers(source, 1, &sampleBuffer);
  alSourcefv (source, AL_POSITION, SourcePos);
  alSourcei (source, AL_LOOPING,  AL_TRUE  );
  alSourcePlay(source);
  ChangePosition(theta,SourcePos);
  
 
  
  
  
  
 // fgetc(stdin);
  
  sleep(sec);

  alSourceStop(source);
  alDeleteSources(1, &source);
  alDeleteBuffers(1, &sampleBuffer);

  delete[] samples;
  
}

float FindTheta(float s_pos[3])
{
  float theta;
  float x= s_pos[0];
  float y= s_pos[2];
  
  if( atan2(y,x) < 0 )
    theta=(2 * PI)+atan2(y,x);
  else
    theta=atan2(y,x);
  
  return theta;
  
}
  

void ChangePosition(float alpha, ALfloat pos)
{
  char k= 'i';
  while(k != 's')
 {
   
     new_pos[0]=pos[0]*cos(alpha);
     
     new_pos[2]=pos[2]*sin(alpha);
     
     alpha=+ 1/(PI * 10);
     
  alSourceQueueBuffers(source, 1, &sampleBuffer);
  alSourcefv (source, AL_POSITION, pos);
  alSourcei (source, AL_LOOPING,  AL_TRUE  );
  alSourcePlay(source);
 }

}

  

void InitializeOpenAL()
{
  ALCdevice* device{nullptr};
  ALCcontext* context{nullptr};

  const char* defaultDevice = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

  device = alcOpenDevice(defaultDevice);
  context = alcCreateContext(device, NULL);
  alcMakeContextCurrent(context);
}



void TerminateOpenAL()
{
  ALCcontext* context = alcGetCurrentContext();
  ALCdevice* device = alcGetContextsDevice(context);

  alcMakeContextCurrent(NULL);
  alcDestroyContext(context);
  alcCloseDevice(device);
}
