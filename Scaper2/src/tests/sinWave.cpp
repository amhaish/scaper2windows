/* =============================================================================
   The MIT License (MIT)
   
   Copyright (c) 2015 Anthony Smith
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
 ============================================================================= */

#include <AL/al.h>
#include <AL/alc.h>
#define _USE_MATH_DEFINES
#include <cmath>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#define sleep(seconds) Sleep(seconds * 1000)
#else
#include <unistd.h>
#endif

// declare our functions

void CheckError();
void InitializeOpenAL();
void TerminateOpenAL();

int main(int argc, char** argv)
{
  InitializeOpenAL();

  float frequency = 500.f;
  int seconds =4;
  unsigned sampleRate = 44100;
  size_t bufferSize = seconds * sampleRate;

  short* samples = new short[bufferSize];
  for(size_t i{0}; i < bufferSize; ++i)
  {
    float sineWave = std::sin((2.f*float(M_PI)*frequency)/sampleRate * i);
    short pitch = 32767;
    if(sineWave < 0)
    {
      pitch = -32768;
      sineWave = std::abs(sineWave);
    }
    samples[i] = (short)(pitch * sineWave);
  }
//add functon call here
      
  
  ALuint sampleBuffer;
  alGenBuffers(1, &sampleBuffer);
  //CheckError();

  alBufferData(sampleBuffer, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
  //CheckError();

  ALuint source = 0;
  alGenSources(1, &source);
  //CheckError();
  alSourcei(source, AL_BUFFER, sampleBuffer);
  alSourcePlay(source);
  
  sleep(seconds);

  alSourceStop(source);
  alDeleteSources(1, &source);
  alDeleteBuffers(1, &sampleBuffer);

  delete[] samples;
  
  TerminateOpenAL();
  
  return 0;
}
/*
void CheckError()
{
  ALenum err = alGetError();

  if(AL_NO_ERROR != err)
  {
    exit(err);
  }
}
*/
void InitializeOpenAL()
{
  ALCdevice* device{nullptr};
  ALCcontext* context{nullptr};

  const char* defaultDevice = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

  device = alcOpenDevice(defaultDevice);
  context = alcCreateContext(device, NULL);
  alcMakeContextCurrent(context);
  //CheckError();
}

void TerminateOpenAL()
{
  ALCcontext* context = alcGetCurrentContext();
  ALCdevice* device = alcGetContextsDevice(context);

  alcMakeContextCurrent(NULL);
  alcDestroyContext(context);
  alcCloseDevice(device);
}
