#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <AL/al.h>


#include <AL/alc.h>


#include <AL/alut.h>

#include <memory>
#include <boost/shared_ptr.hpp>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>

#include <sstream>


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/io/grabber.h>

#include <boost/concept_check.hpp>

#include <pcl/io/openni_grabber.h>
#include <boost/program_options.hpp>
// #include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_grabber.h>

// #include <AL/alu.h>


namespace po = boost::program_options;
po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Proto Viewer" );
    desc.add_options()
    ( "filename",po::value<std::string>()->default_value (""),"Read from a file." )
//     ( "k",po::value<int>()->default_value ( -1 ),"Number of points for calculation of normal." )
//     ( "max_surface_angle",po::value<double>()->default_value ( M_PI/4 ),"Don't consider points for triangulation
//  if their normal deviates more than this value from the query point's normal." )
//     ( "mesh_search_radius",po::value<double>()->default_value ( 0.025 ),"!" )
//     ( "mesh_search_points",po::value<int>()->default_value ( 100 ),"!" )
    ( "openni","Use the OpenNI grabber" )
    ( "depthsense","Use the DepthSense grabber" )
//     ( "radius",po::value<double>()->default_value ( -1 ),"Radius for normal calculation." )

    ;

    po::positional_options_description p;
//     p.add ( "filenames", 15 );
//   p.add("target_filename", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm ); 

    return vm;
}



int main(int argc,char ** argv) {
  
      po::variables_map vm=args ( argc,argv );

      
        bool openni=false;
    bool depthsense=false;
    bool fromfile=false;
    std::string filename;
    if ( !vm["openni"].empty() ) openni=true;
    if ( !vm["depthsense"].empty() ) depthsense=true;
    if ( vm["filename"].as<std::string>() !="" ) {
        fromfile=true;
        filename=vm["filename"].as<std::string>();
    }

    if(!openni && !depthsense && !fromfile) {
        std::cerr<<"Give me a point cloud source"<<std::endl;
        exit(1);
    }
    if(openni && depthsense ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(depthsense && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(openni && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }
    
    boost::shared_ptr<pcl::Grabber> grabber;
    if(openni) {
        
        grabber.reset(new pcl::OpenNIGrabber("",pcl::OpenNIGrabber::OpenNI_QQVGA_30Hz));
    } else if(fromfile) {

        grabber.reset(new pcl::PCDGrabber<pcl::PointXYZ>(filename));
    } else {
        std::cerr<<"Not implemented. Sorry."<<std::endl;
        exit(1);
    }

      
    alutInit(0,NULL);
// alGetError();
    int NUM_BUFFERS=5;
    ALuint buffers[NUM_BUFFERS];
    alGenBuffers(NUM_BUFFERS,buffers);
    ALenum error;

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("whoops\n");
//       return 1;
    }
    printf("ok\n");
//   /usr/share/skype/sounds/CallHold.wav
// sleep(1);

    ALuint buf;
    alGenBuffers(1, &buf);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("whoops222\n");
//       return 1;
    }


    ALenum format=AL_FORMAT_MONO16;
    int seconds = 4;
    unsigned sample_rate = 22050;
    size_t buf_size = seconds * sample_rate;
    int bytes_per_entry=2;
//     double med = buf_size/2;

    int freq=330;
    short *samples;
    samples = new short[buf_size];

    double entry=3200;

    for(int i=0; i<buf_size; ++i) {



        samples[i] = 32760 * sin( (2.f*float(M_PI)*freq)/sample_rate * i );
// 	if(i<entry)samples[i]=samples[i]*i/entry;
        if(i>buf_size-entry) {
//  	  printf("%d",samples[i]);
// 	  samples[i]=samples[i]*3;

            samples[i]=(samples[i]*1.0f/entry)*(buf_size-i-1);


//  	  printf("-->%d\n",samples[i]);

        }
//         if(i>buf_size-entry && i < buf_size-entry+10) {
// 	  samples[i]=32760;
// 	}
// 	samples[i]=(samples[i]*1.0f/buf_size)*(buf_size-i);

    }
    for(int i=0; i<buf_size; ++i) {
//       printf("%d\n",samples[i]);
    }


    alBufferData(buf, format, samples, buf_size*bytes_per_entry, sample_rate);
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("whoops333\n");
//       return 1;
    }


    ALuint src;
    alGenSources(1, &src);
//     alSourcei(src, AL_BUFFER, buf);
    alSourceQueueBuffers(src, 1, &buf);

    alSourcei( src,AL_LOOPING,AL_TRUE);
    double start=0;
    double end=15;
    double step=1;

    alSourcePlay(src);
    
//     float result;
//   alGetSourcef(src, AL_SEC_OFFSET, &result);
//   std::cout<<"result "<<result<<std::endl;
//     
    sleep(seconds);
    /*
    for(double i=start; i<=end; i+=step) {

        ALfloat sourcePos[] = { 0, 0, 0};
        ALfloat sourceVel[] = { (end-start)/seconds, 0.0, 0.0 };
// ALfloat sourcePos[] = { i, 0.0, 0.0 };

        ALfloat sourceOri[] = { 0.0, 0.0, 0.0 };


        alSourcefv( src,AL_POSITION,sourcePos);

        if(i==start)alSourcePlay(src);

        usleep(1000000*seconds/(end-start));
    }*/

//     sleep(seconds );

    alutExit();
//   alutSleep(5);
    exit(0);
}
