#include <AL/al.h>
#include <AL/alc.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#define sleep(seconds) Sleep(seconds * 1000)
#else
#include <unistd.h>
#include <boost/graph/graph_concepts.hpp>
#endif

#include <math.h>
#include <tgmath.h>

#define PI 3.14159

using namespace std;

const unsigned sampleRate = 44100;

void CheckError();
void InitializeOpenAL();
void TerminateOpenAL();
void wave(double freq , double sec);
void FillBuffer(size_t buf_size,short* sample,double starttime,double endtime,double startenv,double endenv,double freq);
void ChangePosition(ALfloat pos[]);
void ClearSample(short* samples, size_t bufferSize);

ALfloat new_pos[3];
ALuint Source=0;
ALuint sampleBuffer1;
ALuint sampleBuffer2;

    ALenum error; 

int main(int argc, char** argv)
{
    InitializeOpenAL();
    double f =400 ;
    double s = 5;
    wave(f,s);
    TerminateOpenAL();

    return 0;
}





void wave(double frequency , double secound)
{  
  
    ALint num_finishedbuf=1 ;
    ALuint unqueue_buffer;
    double freq= frequency;
    double sec = secound;

    int s = sec;

    size_t bufferSize = (s)* sampleRate ;
    short* samples = new short[bufferSize];


    ClearSample(samples,bufferSize);
   

    Source=0;
    alGenBuffers(1, &sampleBuffer1);
    alGenBuffers(1, &sampleBuffer2);
    
    ALfloat SourcePos[] = {10.0 , 10.0 , 5.0};

    alGenSources(1, &Source);
    
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
    
// void FillBuffer(bufferSize,samples,starttime,endtime,startenv,endenv,freq);
    FillBuffer(bufferSize,samples,0,sec,1,1,freq);
    FillBuffer(bufferSize,samples,sec,2*sec,1,1,freq);


   
    alBufferData(sampleBuffer1, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
    
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
    
    
    alBufferData(sampleBuffer2, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
    
//     alGetSourcei(Source, AL_BUFFERS_PROCESSED, &val);
//     alSourceQueueBuffers(src[i], 1, &buffers[j]);
    
//// alGetSourcei(source_id, AL_BUFFERS_PROCESSED, &num_finished);
//// alSourceUnqueueBuffers(source_id, 1, &unqueue_buffer);
    
    

//     alSourcei(Source, AL_BUFFER, sampleBuffer);

    alSourceQueueBuffers(Source, 1, &sampleBuffer1);
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
    alSourceQueueBuffers(Source, 1, &sampleBuffer2);
    
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
    while(freq>20)
 {
      
    alGetSourcei(Source, AL_BUFFERS_PROCESSED, &num_finishedbuf);
  //  cout << "I'm at line 108" << endl;
 //   CheckError();
if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
  //  cout << "no error on line 109" << endl;
    if(num_finishedbuf>=0)
    {
      cout << "trying queue buffer " << endl;
        alSourceUnqueueBuffers(Source, 1, &unqueue_buffer);
      //    cout << "I'm at line 114" << endl;
    //CheckError();
  //  cout << "no error on line 115" << endl;
	if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
      
        freq   -=   20;
	
        ClearSample(samples,bufferSize);
	
	FillBuffer(bufferSize,samples,0,sec,1,1,freq);
        FillBuffer(bufferSize,samples,sec,2*sec,1,1,freq);
	
	
	alBufferData(unqueue_buffer, AL_FORMAT_MONO16, samples, bufferSize, sampleRate);
	
	alSourceQueueBuffers(Source, 1, &unqueue_buffer);
     //      cout << "I'm at line 129" << endl;
   // CheckError();
	if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
       
   // cout << "no error on line 130" << endl;
	  
	sleep(0.03);
	ChangePosition(SourcePos);
    }
 }
   

// fgetc(stdin);

    sleep(sec);

    alSourceStop(Source);
   
    alDeleteSources(1, &Source);
    alDeleteBuffers(1, &sampleBuffer1);
    alDeleteBuffers(1, &sampleBuffer2);

   // void CheckError();

    delete[] samples;

}





void FillBuffer(size_t buf_size, short* sample,double starttime,double endtime,double startenv,double endenv,double freq)

{
    double env;

    for(size_t i = 0 ; i < buf_size ; ++i)
    {
        double t=1.0*i/sampleRate+starttime;

       
	//double sineWave = sin( ((2 * double(M_PI)*freq)/sampleRate * i) - starttime);
        
	double sineWave = sin ( 2 * 3.14 * freq * t);
        short pitch = 32767;
        if(sineWave < 0)
        {
            pitch = -32768;
            sineWave = std::abs(sineWave);
        }


        if(starttime==0)

            //calculate Env1
            //env= 1.0/(endtime-starttime)*(1.0/sampleRate)* i;
            env = 1.0*i/buf_size;

        else
            env = 1.0-1.0*i/buf_size;
        //calculate Env2
	//env =(-1.0/(endtime-starttime))*(1.0/sampleRate )* i + 1 ;


        sample[i] = sample[i]+(double)(pitch * sineWave * env);
    }


}





void ChangePosition(ALfloat pos[])
{
    float x= pos[0];
    float z= pos[2];
    float R= sqrt(x*x + z*z);

//    float sleep_time= 0.03;

    float theta=atan2(z,x);

    //std::cout << "enter key s to stop ";

//    int k=24;
//    while(k != 0)
//    {
        //  std:: cin>> k;

       theta= + (2 * PI * R /16);
    
        new_pos[0]=R * cos(theta);
        new_pos[2]=R * sin(theta);

        

//        k--;

//        sleep(sleep_time);
/**/	alSourcefv (Source, AL_POSITION, new_pos);
	//CheckError();
if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
//	if(k==23){
	alSourcei (Source, AL_LOOPING,  AL_FALSE  );
//	CheckError();
        alSourcePlay(Source);
  //      CheckError();
//	}
       
//        CheckError();


 //   }

}


void ClearSample(short* samples, size_t bufferSize)
{
for(size_t i=0; i < bufferSize; ++i)
    {
        samples[i]= 0;
    }
}




/*void CheckError()
{
    ALenum error;

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
}*/





void InitializeOpenAL()
{
    ALCdevice* device {nullptr};
    ALCcontext* context {nullptr};

    const char* defaultDevice = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

    device = alcOpenDevice(defaultDevice);
    context = alcCreateContext(device, NULL);
    alcMakeContextCurrent(context);

    //CheckError();
    
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
}






void TerminateOpenAL()
{
    ALCcontext* context = alcGetCurrentContext();
    ALCdevice* device = alcGetContextsDevice(context);

    alcMakeContextCurrent(NULL);
    alcDestroyContext(context);
    alcCloseDevice(device);

   // CheckError();
if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"I got the error at line "<<__LINE__<<": "<< std::hex <<error<<std::endl;
    }
    
}
