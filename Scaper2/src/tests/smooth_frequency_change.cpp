#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <AL/al.h>


#include <AL/alc.h>


#include <AL/alut.h>

#include <memory>
#include <boost/shared_ptr.hpp>

#include <sstream>

#include <boost/concept_check.hpp>




int main(int argc,char ** argv) {
  
    alutInit(0,NULL);
    
    ALuint buffers[1];
    std::cout<<"allocate buffers"<<std::endl;
    alGenBuffers(1,buffers);
    std::cout<<"done"<<std::endl;
    ALenum error;


    if ((error=alGetError()) != AL_NO_ERROR)
    {
        printf("whoops222\n");
//       return 1;
    }


    ALenum format=AL_FORMAT_MONO16;
    int seconds = 4;
    unsigned sample_rate = 22050;
    size_t buf_size = seconds * sample_rate;
        int bytes_per_entry=2;//short

    int freq1=330;
    int freq2=630;
    short *samples;
    samples = new short[buf_size];

    double entry=3200;

    for(unsigned int i=0; i<buf_size; ++i) {

       double thisfreq =  freq1 + (((double)i)/buf_size) * (freq2-freq1);
      

        samples[i] = 32760 * sin( (2.f*float(M_PI)*thisfreq)/sample_rate * i );

        if(i>buf_size-entry	) {

            samples[i]=(samples[i]*1.0f/entry)*(buf_size-i-1);

	}

//  	  printf("-->%d\n",samples[i]);

        }

    std::cout<<"buffer data"<<std::endl;
    
      alBufferData(buffers[0], format, samples, buf_size*bytes_per_entry, sample_rate);
      if ((error=alGetError()) != AL_NO_ERROR)
    {
//         printf("whoops333\n%d\n",i);
      printf("whoops333\n%d\n",1);

    }

    std::cout<<"done"<<std::endl;
    


    ALuint src[1];
    std::cout<<"gen sources"<<std::endl;
    alGenSources(1, src);
    std::cout<<"dine"<<std::endl;

      alSourceQueueBuffers(src[0], 1, &buffers[0]);
      alSourcePlay(src[0]);

    sleep(seconds*2);
    alutExit();

    exit(0);

}

}

