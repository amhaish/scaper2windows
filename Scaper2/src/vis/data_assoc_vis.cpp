#include <vis/data_assoc_vis.h>
#include <boost/graph/graph_concepts.hpp>

int vvv=6;

#define POINT_SIZE 6
#define TEXT_SCALE 0.01

DataAssocVisualizer::DataAssocVisualizer(boost::mutex& m, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis, 
		   std::vector<PCLObject::Ptr> &cl1,  std::vector<PCLObject::Ptr> &cl2,
 		   std::vector<std::pair<int,int> > &matches):
		   cloud1on(true),cloud2on(true),stopme(false) {
    //viewer(new pcl::visualization::PCLVisualizer("Data Association Visualizer"))
    this->viewer_mutex=&m;

    this->viewer=vis;
    last_size=matches.size()*2;
//     removePreviousDataFromScreen();
    if(vvv>5) {
        std::cout<<"Making view ports"<<std::endl;
    } 

usleep(100000);
//       int v1(0);
//     viewer->createViewPort(0.0, 0.0, 1.0, 0.5, v1);
//     viewer->setBackgroundColor (255, 255, 255,v1);
//
//     int v2(0);
//     viewer->createViewPort(0.0, 0.5, 1.0, 1.0, v2);
//     viewer->setBackgroundColor (255, 255, 255,v2);
// cout<<"t"<<cl1.size()<<" "<<cl2.size()<<endl;
    for(int i=0; i<cl1.size();i++){
//       cout<<"!!"<<endl;
      clouds1.push_back(cl1[i]->objectCloud);
    }
    for(int i=0; i<cl2.size();i++){
      clouds2.push_back(cl2[i]->objectCloud);
      for(int k=0;k<clouds2[i]->size();k++)
	clouds2[i]->points[k].x+=0.4;
    }
//     cout<<"u"<<clouds1.size()<<" "<<clouds2.size()<<endl;
//     clouds1=cl1;
//     clouds2=cl2;

 viewer_mutex->lock();
 usleep(100000);
    addLeftClouds();

    usleep(100000);
    addRightClouds();
    usleep(100000);

//     pcl::PointCloud<pcl::PointXYZ>::Ptr centroids1(new pcl::PointCloud<pcl::PointXYZ>);
//     pcl::PointCloud<pcl::PointXYZ>::Ptr centroids2(new pcl::PointCloud<pcl::PointXYZ>);

//     for(size_t i=0;i<clouds1.size();i++){
//       if(vvv>5){
// 	std::cout<<"Adding point cloud from clouds1 set "<<i<<std::endl;
//     }
//       pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color(clouds1[i],red1[i%12] ,grn1[i%12],blu1[i%12]);
//       std::stringstream ss,tt;
//       ss<<"C"<<i;
//       tt<<"c"<<i;
//       viewer->addPointCloud<pcl::PointXYZ> (clouds1[i], color, ss.str());
//       viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, ss.str());
// //       viewer->addPointCloud<pcl::PointXYZ> (clouds1[i], color, ss.str(), v1);
//       Eigen::Vector4f centroid;
//       pcl::compute3DCentroid(*clouds1[i],centroid);
//       pcl::PointXYZ centroidp;
//       centroidp.x=centroid[0];centroidp.y=centroid[1];centroidp.z=centroid[2];
//       viewer->addText3D(tt.str(), centroidp,0.05,0.0,0.0,1.0,tt.str());
// //       viewer->addText3D(tt.str(), centroidp,0.2,1.0,0.0,0.0,tt.str(),v1);
//       centroids1->points.push_back(centroidp);
//       centroids1->height++;
//
//
//     }
//     for(size_t  i=0;i<clouds2.size();i++){
//       if(vvv>5){
// 	std::cout<<"Adding point cloud from clouds2 set "<<i<<std::endl;
//     }
//       pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color(clouds2[i],red2[i%12] ,grn2[i%12],blu2[i%12]);
//       std::stringstream ss,tt;
//       ss<<"K"<<i;
//       tt<<"k"<<i;
//       viewer->addPointCloud<pcl::PointXYZ> (clouds2[i], color, ss.str());
// //       viewer->addPointCloud<pcl::PointXYZ> (clouds2[i], color, ss.str(), v2);
//       viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, ss.str());
//       Eigen::Vector4f centroid;
//       pcl::compute3DCentroid(*clouds2[i],centroid);
//       pcl::PointXYZ centroidp;
//       centroidp.x=centroid[0];centroidp.y=centroid[1];centroidp.z=centroid[2];
//       viewer->addText3D(tt.str(), centroidp,0.05,1.0,0.0,0.0,tt.str());
// //       viewer->addText3D(tt.str(), centroidp,0.2,1.0,0.0,0.0,tt.str(),v2);
//       centroids2->points.push_back(centroidp);
//       centroids2->height++;
//     }
                        std::cout << "we are in the visualization part	 "<<std::endl;

    if(vvv>5) {
        std::cout<<"Adding correspondences"<<std::endl;
    }

//     std::vector<int> corr;
    pcl::Correspondences corr;

    for(size_t i=0; i<matches.size(); i++) {
        corr.push_back(pcl::Correspondence(matches[i].first,matches[i].second,1));
    }
usleep(100000);
//     for(int i=0;i<matches.size();i++){
//       if(matches[i].first>=corr.size)corr.resize(matches[i].first,-1);
//     }
//     for(int i=0;i<matches.size();i++){
//       corr[matches[i].first]=matches[i].second;
//     }

//     viewer_mutex->lock();
    viewer->setBackgroundColor (255, 255, 255);
//     viewer/*-*/>updateCo
    usleep(100000);
    
    viewer->removeCorrespondences();
//                     std::cout << "we are in the visualization partddddddddddddddd	 "<<std::endl;

//     if(!viewer->updateCorrespondences<SystemPoint>(centroids1,centroids2,corr))
    viewer->addCorrespondences<SystemPoint>(centroids1,centroids2,corr);
usleep(100000);
    if(vvv>5) {
        std::cout<<"Spinning"<<std::endl;
    }

 
// usleep(10000000);
    viewer->registerMouseCallback(&DataAssocVisualizer::mouseCB,*this, NULL);
viewer_mutex->unlock();

    while(!this->stopme)usleep(100000);
    viewer_mutex->lock();
    remLeftClouds();
    remRightClouds();
    viewer_mutex->unlock();
// for(int i=0;i<1000;i++)
// while(!viewer->wasStopped())	
// {viewer->spinOnce();
//   usleep(1000);
// //   cout << "spinerising" << endl;
// }
//      viewer->spin();
//     
    
    

}
inline void DataAssocVisualizer::removePreviousDataFromScreen()
{
  std::string name;
  for (size_t i = 0; i < last_size; i++)
  {
    name="tr" + std::to_string(i);
    viewer->removePointCloud(name.c_str());
  }
}

void DataAssocVisualizer::addLeftClouds() {

    unsigned char red1 [12] = {155,   0,   0, 155, 155,   0,100,   0,   0, 100, 100,   0};
    unsigned char grn1 [12] = {  0, 155,   0, 155,   0, 155,0, 68,   0, 68,   0, 68};
    unsigned char blu1 [12] = {  100,   100, 255,   100, 255, 255, 100,   100, 168,   100, 168, 168};
remLeftClouds();usleep(10000);
    centroids1.reset(new pcl::PointCloud<SystemPoint>);

    if(vvv>3)std::cout<<clouds1.size()<<" left clouds"<<endl;
    
    for(size_t i=0; i<clouds1.size(); i++) {
        if(vvv>5) {
            std::cout<<"Adding point cloud from clouds1 set "<<i<<std::endl;
        }
        pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color(clouds1[i],red1[i%12] ,grn1[i%12],blu1[i%12]);
        std::stringstream ss,tt;
        ss<<"C"<<i;
        tt<<"c"<<i;
//  	if(!viewer->updatePointCloud<SystemPoint> (clouds1[i], color, ss.str())){
// 	    usleep(10000);
	  viewer->addPointCloud<SystemPoint> (clouds1[i], color, ss.str());
// 	}
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINT_SIZE, ss.str());
//       viewer->addPointCloud<pcl::PointXYZ> (clouds1[i], color, ss.str(), v1);
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*clouds1[i],centroid);
        SystemPoint centroidp;
        centroidp.x=centroid[0];
        centroidp.y=centroid[1];
        centroidp.z=centroid[2];
// 	std::cout<<"centroid "<<centroidp<<std::endl;
        viewer->addText3D(tt.str(), centroidp,TEXT_SCALE,0.0,0.0,1.0,tt.str());
//       viewer->addText3D(tt.str(), centroidp,0.2,1.0,0.0,0.0,tt.str(),v1);
        centroids1->push_back(centroidp);
    }

    cloud1on=true;
}

void DataAssocVisualizer::addRightClouds() {


    unsigned char red2 [12] = {255,   100,   100, 255, 255,   100,188,   100,   100, 188, 188,   100};
    unsigned char grn2 [12] = {  0, 255,   0, 255,   0, 255,0, 128,   0, 128,   0, 128};
    unsigned char blu2 [12] = {  0,   0, 200,   0, 200, 200, 0,   0, 108,   0, 108, 108};
// remRightClouds();usleep(10000);
    if(vvv>3)std::cout<<clouds2.size()<<" right clouds"<<endl;
    centroids2.reset(new pcl::PointCloud<SystemPoint>);
    
    for(size_t i=0; i<clouds2.size(); i++) {
        if(vvv>5) {
            std::cout<<"Adding point cloud from clouds2 set "<<i<<std::endl;
        }
        pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color(clouds2[i],red2[i%12] ,grn2[i%12],blu2[i%12]);
        std::stringstream ss,tt;
        ss<<"K"<<i;
        tt<<"k"<<i;
// 	if(!viewer->updatePointCloud<SystemPoint> (clouds2[i], color, ss.str())){
//         usleep(10000);
	  viewer->addPointCloud<SystemPoint> (clouds2[i], color, ss.str());
// 	}
//       viewer->addPointCloud<pcl::PointXYZ> (clouds2[i], color, ss.str(), v2);
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINT_SIZE, ss.str());
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*clouds2[i],centroid);
        SystemPoint centroidp;
        centroidp.x=centroid[0];
        centroidp.y=centroid[1];
        centroidp.z=centroid[2];
// 	std::cout<<"centroid "<<centroidp<<std::endl;
        viewer->addText3D(tt.str(), centroidp,TEXT_SCALE,1.0,0.0,0.0,tt.str());
//       viewer->addText3D(tt.str(), centroidp,0.2,1.0,0.0,0.0,tt.str(),v2);
	centroids2->push_back(centroidp);
    }
    cloud2on=true;

}

void DataAssocVisualizer::remLeftClouds() {
   for(size_t i=0; i<clouds1.size(); i++) {
        std::stringstream ss,tt;
        ss<<"C"<<i;
	tt<<"c"<<i;
        viewer->removePointCloud(ss.str());
	viewer->removeText3D(tt.str());
    }
    cloud1on=false;
}


void DataAssocVisualizer::remRightClouds() {
    for(size_t i=0; i<clouds2.size(); i++) {
         std::stringstream ss,tt;
        ss<<"K"<<i;
	 tt<<"k"<<i;
        viewer->removePointCloud(ss.str());
	viewer->removeText3D(tt.str());
    }
    cloud2on=false;
}

void DataAssocVisualizer::mouseCB(const pcl::visualization::MouseEvent &event,void* ignore) {

//    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);d

//     if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
//             event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease) {
//         if(cloud1on) {
//             remLeftClouds();
//             cloud1on=false;
//         } else {
//             addLeftClouds();
//             cloud1on=true;
//         }
//     }
//     if (event.getButton () == pcl::visualization::MouseEvent::RightButton &&
//             event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease) {
//         if(cloud2on) {
//             remRightClouds();
//             cloud2on=false;
//         } else {
//             addRightClouds();
//             cloud2on=true;
//         }
//     }
   if (event.getButton () == pcl::visualization::MouseEvent::RightButton )
  this->stopme=true;
//   this->stopme=true;
//   this->stopme=true;
}
/*

viewer->addText("Radius: 0.01", 10, 10, "v1 text", v1);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud1", v1);
*/