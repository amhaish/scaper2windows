#include <vis/data_assoc_vis.h>
#include <cstdlib>

void test(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr > &clouds1,std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr > &clouds2,std::vector<std::pair<int,int> > &matches){
  
  std::cout<<"Starting DataAssocVisualizer"<<std::endl;
  DataAssocVisualizer vis(clouds1,clouds2,matches);
  std::cout<<"Destroying DataAssocVisualizer"<<std::endl;
}

double rand01(){
    double r=rand();
    return r/(RAND_MAX+1.0);
}

int main(){
 
  std::cout<<"Building input data"<<std::endl;
  
  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> clouds1;
  for(int i=0;i<3;i++){
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
     for(int j=0;j<100;j++){
	pcl::PointXYZ p;
	p.x = rand01()+i;
	p.y = rand01();
	p.z = rand01();
	cloud->push_back(p);
     }
     clouds1.push_back(cloud);
  }
  
  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> clouds2;
  for(int i=0;i<2;i++){
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
     for(int j=0;j<100;j++){
	pcl::PointXYZ p;
	p.x = rand01()-1;
	p.y = rand01()+i;
	p.z = rand01();
	cloud->push_back(p);
     }
     clouds2.push_back(cloud);
  }
  
  std::vector <std::pair<int,int> > matches;
  matches.push_back(std::pair<int,int>(2,0));
  matches.push_back(std::pair<int,int>(0,1));
  
  
  test(clouds1,clouds2,matches);
 
  
}