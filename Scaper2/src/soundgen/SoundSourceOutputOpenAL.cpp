#include <al.h>
#include <alc.h>
#include <alut/alut.h>
// #include <boost/concept_check.hpp>
// #include <boost/graph/graph_concepts.hpp>
#include <soundgen/SoundSourceOutput.h>
#include <soundgen/SoundSourceOutputOpenAL.h>
#include <iostream> 

#include <Consts.h>
#include <common/Tools.h>
// #define SAMPLE_RATE 22050
#define FORMAT AL_FORMAT_MONO16


#define NUM_AL_BUFFERS_TO_KEEP 12

#define ADVANCE_SECS 0.01
#define BELIEVE_OPENAL_MULT 0.001
#define BELIEVE_CURR_FRAME_RATE_MULT 0.8

#define NUM_QUEUED_CORRECTION_SAMPLES 32

// #define OUTPUTAL_FILE 1

#ifdef OUTPUTAL_FILE
#include <fstream>
#endif //OUTPUTAL_FILE


int SoundSourceOutputImpl::verbosity=1;//VERBOSITY LEVEL

bool SoundSourceOutputImpl::si_alut_initialised=false;
int SoundSourceOutputImpl::si_num_sinterpolators=0;

SoundSourceOutput::SoundSourceOutput(double approx_hz) {
    impl.reset(new SoundSourceOutputImpl(approx_hz));
}
bool SoundSourceOutput::output(CircularBuffer<short>  &csb,Eigen::Vector4f &pos) {
	return impl->output(csb, pos);
}

bool SoundSourceOutput::output(CircularBuffer<short>  &csb) {
    return impl->output(csb);
}

SoundSourceOutput::~SoundSourceOutput() { }

bool SoundSourceOutputImpl::load_to_al_buffer(ALuint buffer_no,CircularBuffer<short> &csb,size_t num_to_load) {

    bool success_flag=false;
    assert(num_to_load>0);

//     short *samples=new short[num_to_load];
    std::unique_ptr<short[]> samples(new short[num_to_load]);
//     std::cout<<"num_to_load"<<num_to_load<<" num_updates: "<<rate_tracker.get_num_updates()<<std::endl;
    //Tools::check_debug_silliness(csb);
    for(size_t i=0; i<num_to_load; i++) {
        short consuming=csb.consume();
#ifdef OUTPUTAL_FILE
        if(i==0)
          *streaming_file<<"N,";
        else
          *streaming_file<<"X,";
        *streaming_file<<std::to_string(csb.get_num_consumed())<<",";
        *streaming_file<<std::to_string(rate_tracker.get_num_updates())<<",";
        *streaming_file<<std::to_string(buffer_no)<<",";
        *streaming_file<<std::to_string(consuming);
        *streaming_file<<std::endl;
#endif //#ifdef OUTPUTAL_FILE
	samples[i]=consuming;
    }
    //Tools::check_debug_silliness(csb);

    ALenum error;

    if (verbosity>4) {
        std::cout<<"Trying to buffer into buffer id #"<<buffer_no<<" an array of "<<num_to_load<<" shorts size "<<std::endl;//<<" into a buffer of size "<<(num_to_load*BYTES_PER_SAMPLE)<<" bytes"<<std::endl;// - t="<<this_time<<std::endl;
    }

    if(verbosity>6) {//VERBOSITY 7+
        std::cout<<"LOAD TO BUFFER: First few values: ";
        for(size_t i=0; i<20&&i<num_to_load; i++) {
            size_t arr_ind=i;
            if(i!=0)std::cout<<",";
            std::cout<<samples[arr_ind];
        }

        std::cout<<std::endl;

        std::cout<<"LOAD TO BUFFER: Last few values: ";
        for(size_t i=num_to_load-20; i<num_to_load; i++) {
            size_t arr_ind=i;
            std::cout<<samples[arr_ind];
            if(i!=num_to_load-1)std::cout<<",";
        }
        std::cout<<std::endl;
    }
    //Tools::check_debug_silliness(csb);
    alBufferData(buffer_no, FORMAT, &(samples[0]), num_to_load*BYTES_PER_SAMPLE, SAMPLE_RATE);
    //Tools::check_debug_silliness(csb);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"*********couldn't buffer data\n"<< std::hex <<error<<std::endl;;
        success_flag=false;
        exit(1);
    }
    else {
        //success
        if (verbosity>4) {
            std::cout<<"Buffered into buffer id #"<<buffer_no<<" an array of "<<num_to_load<<" shorts size"<<std::endl;//" into a buffer of size "<<(num_to_load*BYTES_PER_SAMPLE)<<" bytes"<<std::endl;
        }
        if (verbosity>6) {
            std::cout<<"Before queue "<<buffer_no<<std::endl;
        }

        alSourceQueueBuffers(source_id, 1, &buffer_no);

        if (verbosity>6) {
            std::cout<<"After queue "<<buffer_no<<std::endl;
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"*********couldn't queue buffer\n"<< std::hex <<error<<std::endl;;
            success_flag=false;
            exit(1);
        }
        else {
            //success
            if (verbosity>4) {
                std::cout<<"Queued buffer "<<buffer_no<<" for playing"<<std::endl;
            }
            success_flag=true;
            num_queued_or_playing_al++; //now playing 1 more!
            playing_al_buffer_ids.push_front(buffer_no);
            playing_al_buffer_queued_time.push_back(std::chrono::high_resolution_clock::now());
            playing_al_buffer_size.push_back(num_to_load);
            all_playing_al_buffer_size+=num_to_load;
        }
    }
//     if(delsam)delete[] samples;
//     delete[] samples;
    return success_flag;
}

// void fill_sample_add(short *samples,size_t start_ind,size_t buffer_size,wave_properties &wp)
// void queue_from_circular(short *samples,size_t start_ind,size_t buffer_size,size_t num_to_buffer);

/* Construct sound interpolation object, and intitialise OpenAL if necessary */
SoundSourceOutputImpl::SoundSourceOutputImpl(const double approx_hz):rate_tracker("SoundSourceOutputImpl.RateTracker",approx_hz) {

    num_queued_or_playing_al=0;
    played_last=false;
    last_offset=0;
    all_playing_al_buffer_size=0;
    
    ALenum error;

    if(!si_alut_initialised) {//intialise OpenAL

        alutInit(0,NULL);
		
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"You have asked me to initialise OpenAL. I'm sorry, Dave. I'm afraid I can't do that."<< std::hex <<error<<"\n"<<std::endl;
            exit(1);
        }
        
            /** The following is a bit weird but *it stops a crash* (which
     * happens approximately every 2 times starting an OpenAL context) that is
     * it even hapens when starting a new OpenAL context
     * there are sometimes buffers waiting to be unqueued!
     * I provide here the source_id I just generated so I wouldn't expect the following to work... but it does!
     */
// ////     alSourceStop(source_id);
//     for(size_t ii=0;ii<NUM_AL_BUFFERS_TO_KEEP*10;ii++){
//         ALuint unqueue_buffer = ii;
// //// 	ALuint unqueue_buffer = buffer_id[ii];//this->last_played_buffer_ind==0?1:0;      
//         alSourceUnqueueBuffers(source_id, 1, &unqueue_buffer);
//     }
    
    /* NOTE: putting this between calls to alGenBuffers also causes havoc.. so I probably won't do that. */

    }

    si_alut_initialised=true;
    si_num_sinterpolators++;

    ALuint buffer_id[NUM_AL_BUFFERS_TO_KEEP]; //should be enough
    for(int i=0;i<NUM_AL_BUFFERS_TO_KEEP;i++)buffer_id[i]=0;

    if(verbosity>3) { //VERBOSITY 4+
        std::cout<<"generating buffers...";

	/*
	   std::cout<<"generating buffers, overwriting:\n";
        for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++)
	      std::cout<<"  "<<buffer_id[ii]<<std::endl;*/
    }

    alGenBuffers(NUM_AL_BUFFERS_TO_KEEP,&buffer_id[0]); //break SoundSourceOutputOpenAL.cpp:169

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't gen buffers\n"<< std::hex <<error<<std::endl;;
        exit(1); // I can't go on like this anymore.
    }
    else {
        if(verbosity>3) { //VERBOSITY 4+
            std::cout<<"generated buffers, buffer ids:\n";
            for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++)
                std::cout<<"  "<<buffer_id[ii]<<std::endl;
        }
    }

    for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++) {

        available_al_buffer_ids.push_front(buffer_id[ii]);

    }
    
    alGenSources(1, &source_id);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't gen sources\n"<< std::hex <<error<<std::endl;;
        exit(1); // I can't go on like this anymore.
    }

#ifdef OUTPUTAL_FILE

    streaming_file=std::shared_ptr<std::ofstream>(new std::ofstream);
    streaming_file->open("streaming_output_openal"+std::to_string(si_num_sinterpolators-1)+".csv");
    *streaming_file<<"STATUS,CIRCULARBUFFER_USRCNTR,NUM_UPDATES,AL_BUFFER,SHORT_VALUE"<<std::endl;

#endif //OUTPUTAL_FILE

}


SoundSourceOutputImpl::~SoundSourceOutputImpl() {

    si_num_sinterpolators--;

    if(si_num_sinterpolators==0) {
        std::cerr<<"alutExit doesn't work... keeping OpenAL engine around."<<std::endl;
	std::cerr<<"You'll probably get an error about alc_cleanup or something."<<std::endl;

        //alutExit DOES NOT WORK!! So we never close the OpenAL engine. It will clean up when program exits.
//         if(!si_alut_exited) {
//             ALenum error;
//             alutExit();
//             if ((error=alGetError()) != AL_NO_ERROR)
//             {
//                 std::cerr<<"COULD NOT EXIT OPENAL. SORRY DAVE."<< std::hex <<error<<"\n"<<std::endl;
//                 exit(1);
//             }
//
//         }
//         si_alut_initialised=false;
    }
#ifdef OUTPUTAL_FILE
    streaming_file->close();
#endif //OUTPUTAL_FILE
}

/* Unqueue buffers if they need unqueuing */
uint SoundSourceOutputImpl::unqueue_al_buffers()
{
    unsigned int unqueued=0;
    ALenum error;

    ALint num_finished;

    alGetSourcei(source_id, AL_BUFFERS_PROCESSED, &num_finished);

    if (verbosity>4) {
        std::cout<<"Num finished is "<<(num_finished)<<std::endl;
    }

    if(verbosity>5) {

        ALfloat offset;
        alGetSourcef(source_id, AL_SEC_OFFSET, &offset);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
            exit(1);

        }

        offset=offset*SAMPLE_RATE;

        std::cout<<"Frames processed before unqueue check: "<<offset-this->last_offset<<std::endl;
    }

    while(num_finished>0) {


        ALuint unqueue_buffer = playing_al_buffer_ids.back();//this->last_played_buffer_ind==0?1:0;

        if (verbosity>5) {
            std::cout<<"Before unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples."<<std::endl;
        }
        
        alSourceUnqueueBuffers(source_id, 1, &unqueue_buffer);
        //NOTE: alSourceUnqueueBuffers overwrites its 3rd arg, probably with the id of the buffer unqueued
	
        if (verbosity>5) {
            std::cout<<"After unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples."<<std::endl;
// 	    std::cout<<"After unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples.";
            /*
                        ALfloat offset;
                        alGetSourcef(source_id, AL_BYTE_OFFSET, &offset);

                        if ((error=alGetError()) != AL_NO_ERROR)
                        {
                            std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
                            exit(1);

                        }
                        offset=offset/2; //num shorts offset not bytes
                        std::cout<<"(offset "<<offset<<")"<<std::endl;*/
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"*********couldn't unqueue buffer\n"<< std::hex <<error<<std::endl;;
            exit(1);

        }
        else {
            if (verbosity>4) {
                std::cout<<"Unqued buffer #"<<unqueue_buffer<<std::endl;
            }

        }

        unqueued++;
        playing_al_buffer_ids.pop_back();
        playing_al_buffer_queued_time.pop_back();
        all_playing_al_buffer_size-=playing_al_buffer_size.back();
        playing_al_buffer_size.pop_back();

        this->num_queued_or_playing_al--;
        available_al_buffer_ids.push_front(unqueue_buffer);

        alGetSourcei(source_id, AL_BUFFERS_PROCESSED, &num_finished);

	if (verbosity>4) {
            std::cout<<"===================="<<num_finished<<std::endl;
        }
        if(num_finished>0) {
            if(verbosity>1)
                std::cerr<<"BLIPZZ: Usually should only need to unqueue one buffer."<<std::endl;
            
        }

    }
    return unqueued;
}

bool SoundSourceOutputImpl::fill_buffers_to_al_buffers(CircularBuffer<short> &csb) {

    ALenum error;

    ALfloat curr_offset;
    alGetSourcef(source_id, AL_SEC_OFFSET, &curr_offset);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
        exit(1);

    }
    curr_offset=curr_offset*SAMPLE_RATE;

    assert(curr_offset>=0);//otherwise I don't know

    int num_to_finish;

    num_to_finish=all_playing_al_buffer_size-curr_offset;
    if(all_playing_al_buffer_size==0)assert(curr_offset==0);

    if(num_to_finish<0){
      std::cerr<<"!!WARNING:::num_to_finish="<<num_to_finish<<",curr_offset="<<curr_offset<<",all_playing_al_buffer_size:"<<all_playing_al_buffer_size<<std::endl;
//       assert(num_to_finish>=0);
    }

    int num_to_buffer_al=rate_tracker.get_tracked_period()*SAMPLE_RATE-num_to_finish+ADVANCE_SECS*SAMPLE_RATE;


    if(num_to_buffer_al<=0)num_to_buffer_al=ADVANCE_SECS*SAMPLE_RATE; //keep the rythm moving

    //Don't trust the offset reported by OpenAL - check it - it's a multiple of 1024 till the first buffer is finished - and is never accurate
    
    int num_to_buffer_timer=(BELIEVE_CURR_FRAME_RATE_MULT*rate_tracker.get_current_period() + (1-BELIEVE_CURR_FRAME_RATE_MULT)*rate_tracker.get_tracked_period())*SAMPLE_RATE;
    
    int num_to_buffer=BELIEVE_OPENAL_MULT*num_to_buffer_al+(1-BELIEVE_OPENAL_MULT)*num_to_buffer_timer;

    if(num_queued_or_playing_al==1) {
        //fine
    } else if(num_queued_or_playing_al>1) {
        //too slow
//       num_to_buffer=num_to_buffer/2;
//         num_to_buffer-=ADVANCE_SECS*SAMPLE_RATE;
//       uint correction=(ADVANCE_SECS*SAMPLE_RATE)/16;
      uint correction=0;
      if(num_queued_or_playing_al>2)
	correction=NUM_QUEUED_CORRECTION_SAMPLES/2;
      if(num_queued_or_playing_al>3)
	correction=NUM_QUEUED_CORRECTION_SAMPLES;
      
//       uint correction=0; // will get out of sync
//       uint correction=(num_queued_or_playing_al-1)*(ADVANCE_SECS*SAMPLE_RATE)/16;
        if(verbosity>1)
            std::cerr<<"BLIPYY: Outputting too much sound for OpenAL("<<num_queued_or_playing_al<<" queued). Correction: "<<std::dec<<((int)correction)<<std::endl;//"("<<num_queued_or_playing_al-1<<")*("<<ADVANCE_SECS<<"*"<<(SAMPLE_RATE)<<")/16"<<std::endl;
        if(num_queued_or_playing_al>=NUM_AL_BUFFERS_TO_KEEP) {
            std::cerr<<"!!!!!!!!!!!!!!!: NOT ENOUGH OPENAL BUFFERS."<<std::endl;
//             exit(1);//for debugging
            return false;
        }
        
         num_to_buffer-=correction; //don't want instability because this is after all a control system

    } else if(num_queued_or_playing_al==0) {
        ///bad error
        
	//don't do anything though, you already have the ADVANCE corrector
	uint correction=NUM_QUEUED_CORRECTION_SAMPLES;
	std::cerr<<"BLIPXX: Sound output can't keep up with OpenAL ("<<num_queued_or_playing_al<<" queued). Correction: "<<std::dec<<((int)correction)<<std::endl;
	num_to_buffer+=correction;
    }
    else {
        assert(0);
    }
    
    if(num_to_finish<0)num_to_buffer-=num_to_finish;

    if (verbosity>4) {
        std::cout<<"Offset is "<<curr_offset<<" and num to process is "<<num_to_finish;//<<std::endl;
        std::cout<<" Num to buffer is "<<num_to_buffer<<std::endl;
    }


//     uint num_to_buffer_main=num_to_buffer;

    //this contains most of the sound
    ALuint fill_al_buffer=available_al_buffer_ids.back();
    available_al_buffer_ids.pop_back();
    //Tools::check_debug_silliness(csb);
    bool loaded1 = load_to_al_buffer(fill_al_buffer,csb,num_to_buffer);
    //Tools::check_debug_silliness(csb);
    
    assert(loaded1);
// done by the circular buffer .consume()     zero_samples_circular(this->fill_buffer.data(),fill_buffer_start,MAX_NUM_SAMPLES,num_to_buffer_main);
//     fill_buffer_start+=num_to_buffer_main;
//     if(fill_buffer_start>=MAX_NUM_SAMPLES)fill_buffer_start-=MAX_NUM_SAMPLES;

    //next time we can see whether this was played or not and catch up accordingly
//     fill_al_buffer=available_al_buffer_ids.back();
//     available_al_buffer_ids.pop_back();
//     bool loaded2 = load_to_al_buffer(fill_al_buffer,this->fill_buffer.data(),MAX_NUM_SAMPLES,fill_buffer_start,num_to_buffer_check);
//     assert(loaded2);
//     zero_samples_circular(this->fill_buffer.data(),fill_buffer_start,MAX_NUM_SAMPLES,num_to_buffer_check);
//     fill_buffer_start+=num_to_buffer_check;
//     if(fill_buffer_start>=MAX_NUM_SAMPLES)fill_buffer_start-=MAX_NUM_SAMPLES;
//
//         if(loaded1||loaded2) {
//             played=true; //set return value

//                 this->last_sfeat=sfeat; //keep track of what was played last time, for interpolation
    played_last=true;
    // if for some reason the playing is stopped or if it is first time, start the source playing


    alGetSourcef(source_id, AL_SEC_OFFSET, &last_offset);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
        exit(1);

    }
    last_offset=last_offset*SAMPLE_RATE;

    return true; //assert takes care of this ;)


}

void SoundSourceOutputImpl::al_play() {

    ALenum error;

    ALenum source_state;
    alGetSourcei(source_id, AL_SOURCE_STATE, &source_state);

    if (source_state != AL_PLAYING) {
        std::cout<<"--------Warning:: starting/restarting OpenAL source play."<<std::endl;
        alSourcePlay(source_id);
        if (verbosity>4) {
            std::cout<<"Trying to play source: "<<source_id;
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"*********couldn't play source\n"<< std::hex <<error<<std::endl;;
            exit(1);

        } else {
            if (verbosity>4) {
                std::cout<<"...Playing."<<std::endl;
            }
        }
    }

}

bool SoundSourceOutputImpl::output(CircularBuffer<short> &csb,const Eigen::Vector4f pos){
      ALenum error;

    //move sound source location - do this first; the easiest part
    alSourcefv( source_id,AL_POSITION,pos.data());
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't set sound position\n"<< std::hex <<error<<std::endl;;
      return false;
    }
    //Tools::check_debug_silliness(csb);
    return output(csb);
    
}


bool SoundSourceOutputImpl::output(CircularBuffer<short> &csb)
{
    if(verbosity>4)std::cout<<"SoundSourceOutputImpl::Frame #"<<rate_tracker.get_num_updates()<<std::endl;

    rate_tracker.update();
    if(verbosity>3)rate_tracker.print_status();

    uint num_unqueued=unqueue_al_buffers();

    if (verbosity>4) {
        std::cout<<"Num unqueued is "<<(num_unqueued)<<std::endl;
        std::cout<<"Num queued now is "<<(num_queued_or_playing_al)<<std::endl;
    }
    //Tools::check_debug_silliness(csb);
    bool played=fill_buffers_to_al_buffers(csb);
    //Tools::check_debug_silliness(csb);
    if(played)al_play();

    return played;
}

// void SoundInterpolatorImpl::play_nothing()
// {
// 
//     if(verbosity>4)std::cout<<"Frame #"<<this->frames_processed<<std::endl;
// 
//     calc_timing();
// 
// //     ALenum error;
// 
//     uint num_unqueued=this->unqueue_al_buffers();
// 
//     if (verbosity>4) {
//         std::cout<<"Num unqueued is "<<(num_unqueued)<<std::endl;
//         std::cout<<"Num queued now is "<<(this->num_queued_or_playing_al)<<std::endl;
//     }
// 
//     bool played=this->fill_buffers_to_al_buffers();
//     if(played)this->al_play();
// 
//     //remove from the queue any buffers that have finished playing
//     this->frames_processed++;
//     return;
// }
