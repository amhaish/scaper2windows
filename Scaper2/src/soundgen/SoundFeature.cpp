#include <soundgen/SoundFeature.h>
#include <iomanip>

SoundFeature::SoundFeature():attack_duration(-1),hold_duration(-1),release_duration(-1){
    pos=Eigen::Vector4f(0,0,0,1);
    spread_mult=1.0;
    energy_mult=1.0;
}

std::ostream& operator<<(std::ostream& stream,
                         const SoundFeature& sfeat) {
    stream<<"<<SoundFeature: pos:"<<std::setw(5)<<sfeat.pos[0]<<","<<sfeat.pos[1]<<","<<sfeat.pos[2]<<" freq:";
    for (size_t i=0; i<sfeat.frequencies.size(); i++) {
        if (i>0)stream<<",";
        stream<<sfeat.frequencies[i];
	if(sfeat.multipliers.size()>=i+1)stream<<"("<<std::setw(4)<<sfeat.multipliers[i]<<")";
    }
    stream<<" spread:"<<sfeat.spread_mult<<" energy:"<<sfeat.energy_mult;
    stream<<">>";
    return stream;

}
