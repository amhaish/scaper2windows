#include <soundgen/SoundSourceOutput.h>
#include <soundgen/SoundSourceGenerator.h>
#include <boost/program_options.hpp>
#include <math.h>
#include <AL/alut.h>
#include <iostream>
#include <chrono>
#include <stdlib.h>


double randd() {
    return ((double)rand())/RAND_MAX;
}

namespace po = boost::program_options;

int vvv;

po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Test Sound Interpolator" );
    desc.add_options()
    ( "help,h","Get help. You probably need it. But not that kind of help." )
    ( "verbose,v",po::value<int>()->default_value ( 0 ),"Verbosity level." )

    ;

    po::positional_options_description p;
//     p.add ( "filenames", 15 );
//   p.add("target_filename", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm );

    return vm;
}

double frequency_of_note(double octave,double note/*starts with A*/) {
    double lowestA=27.50	;
    double a= pow(2.0,1.0/12);
//   std::cout<<"pow("<<lowestA<<","<<octave<<")*pow("<<a<<","<<note<<")="<<pow(lowestA,1.0*octave)<<"*"<<pow(a,1.0*note)<<std::endl;
    return lowestA*pow(2,1.0*octave)*pow(a,1.0*note);
}

// double middlec=261.6;



void test1() {


    SoundSourceGenerator ssg(15);
    SoundSourceOutput sso(15);
    CircularBuffer<short> csb(10000);

    std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;


        sfeat.frequencies.push_back(frequency_of_note(1,30));

        std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
        std::cout<<"------------------------------------"<<std::endl;
	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
        std::cout<<"------------------------------------"<<std::endl;
	ssg.generate_sound(sfeat,csb);
	sso.output(csb,sfeat.pos);
        alutSleep(1.0/30);


    }

}



void test2() {


    SoundSourceGenerator ssg(1);
    SoundSourceOutput sso(1);
    CircularBuffer<short> csb(50000);

    std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;


        sfeat.frequencies.push_back(frequency_of_note(1,30+randd()*0.8));

        std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
        std::cout<<"------------------------------------"<<std::endl;
	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
        std::cout<<"------------------------------------"<<std::endl;
	ssg.generate_sound(sfeat,csb);
	sso.output(csb,sfeat.pos);
        alutSleep(1.0/30);


    }

}

void test3() {


    std::vector<double> note_sequence;

    SoundSourceGenerator ssg(10);
    SoundSourceOutput sso(10);
    CircularBuffer<short> csb(6000);

    std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<720*30; angle++) {
//       for(double angle=0; angle<720; angle++) {
//           for(double angle=0; angle<360; angle++) {


        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;

        int num_frequencies=(((int)angle)	%720)/120 + 1;

        for (int f=0; f<num_frequencies; f++) {
//             int notenum=12+f*12;

//             sfeat.frequencies.push_back(frequency_of_note(1,notenum)+randd()*0.5);
// 	    sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
// 	  sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
// 	  sfeat.frequencies.push_back(frequency_of_note(2+f,0));
// 	  sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
            sfeat.frequencies.push_back(frequency_of_note(2+f,randd()));

        }
        std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
        std::cout<<"------------------------------------"<<std::endl;
	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
	std::cout<<"------------------------------------"<<std::endl;
	ssg.generate_sound(sfeat,csb);
	sso.output(csb,sfeat.pos);
        alutSleep(1.0/30);


    }

}

int main(int argc,char ** argv) {

//   SoundInterpolator sint;

    po::variables_map vm=args ( argc,argv );

    vvv = vm["verbose"].as<int>();

    // okay let's make a sound come around you once, building up frequencies as it goes.
    std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<"TEST 3"<<std::endl;
    test3();
    alutSleep(1.0);
    //simpler test
    std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<"TEST 2"<<std::endl;
    test2();
    alutSleep(1.0);
    std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<"TEST 1"<<std::endl;
    test1();
    alutSleep(2.0);

}
