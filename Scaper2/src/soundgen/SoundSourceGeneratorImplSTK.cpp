 

// #include <boost/concept_check.hpp>
// #include <boost/graph/graph_concepts.hpp>
#include <soundgen/SoundSourceGeneratorSTK.h>
#include <soundgen/SoundSourceGeneratorImplSTK.h>


#include <iostream> 
#include <boost/graph/graph_concepts.hpp>

#include <Consts.h>
#define ATTENUTATION_FUDGE 3

/////////////////////////////
using namespace stk;


//Stk::showWarnings( true );
//////////////////////////////////

int ssgvvvSTK=1;//VERBOSITY LEVEL


SoundSourceGeneratorSTK::SoundSourceGeneratorSTK(double approx_hz) {
    implSTK.reset(new SoundSourceGeneratorImplSTK(approx_hz));
}
void SoundSourceGeneratorSTK::generate_sound(SoundFeature &sfeat,CircularBuffer<short> &csb) {
    implSTK->generate_sound(sfeat,csb);
}
///////////////////////////////////////////////

SoundFeature SoundSourceGeneratorSTK::get_last_played_sound_feature() {
  return implSTK->get_last_played_sound_feature();
}
///////////////////////////////////////////////

// void SoundSourceGenerator::play_nothing() {
//     impl->play_nothing();
// }

SoundSourceGeneratorSTK::~SoundSourceGeneratorSTK() { }

/* Return just one sample of a wave frequency freq at time t*/
// double amplitude(double freq,double t) {
//     double toret=32760 * sin( (2.f*float(M_PI)*freq) * t );
//     if(ssgvvvSTK>8) {
//         std::cout<<"amplitude("<<freq<<","<<t<<")-->"<<toret<<std::endl;
//     }
//     return toret;
// }



// void zero_samples_circular(short *samples,size_t start_ind,size_t buffer_size,size_t num_to_zero) {
//     for(size_t i=0; i<num_to_zero; ++i) {
// 
//         size_t arr_ind=(start_ind+i)%buffer_size;
// 
//         samples[arr_ind] =0;
// 
//     }
// 
// }

/* Add wave into array (don't clear array) */
void SoundSourceGeneratorImplSTK::fill_sample_add(CircularBuffer<short> &csb,wave_properties &wp,size_t start_ind,string objectType,size_t freq_ind) 
{

    if(ssgvvvSTK>6) {//VERBOSITY 7+
        std::cout<<"fill_sample_add(CircularBuffer<short> &csb,wave_properties &wp,size_t start_ind="<<start_ind<<")"<<std::endl;
    }

    size_t num_steps=(wp.endtime-wp.starttime)*SAMPLE_RATE;

    
      if(num_steps!=wp.num_samples)
	  assert(num_steps==wp.num_samples);
    assert(num_steps<=csb.size());

    //double freq_step=0;//(wp.endfreq-wp.startfreq)/wp.num_samples;
    double freq=wp.startfreq;
    double time_step=(wp.endtime-wp.starttime)/wp.num_samples;
    double t=wp.starttime;
    double attenuation_step=(wp.end_attenuation-wp.start_attenuation)/wp.num_samples;
    double attenuation=wp.start_attenuation;

    if(ssgvvvSTK>5) {//VERBOSITY 6+
        std::cout<<"    wp.startfreq="<<wp.startfreq<<" wp.endfreq="<<wp.endfreq<<" wp.start_attenuation="<<wp.start_attenuation;
        std::cout<<" wp.end_attenuation="<<wp.end_attenuation<<" wp.starttime="<<wp.starttime<<" wp.endtime="<<wp.endtime;
        std::cout<<" wp.num_samples="<<wp.num_samples<<  std::endl;
    }
    
////////////////////////////////////////////////////////////////////////////////



//instrument.setRawwavePath("/usr/share/stk/rawwaves/"); //is not necessary because stk already knows this path
StkFloat frequency=freq/10;


// blowBotl.noteOn(frequency,0.5);
// bowed.noteOn(frequency,0.5);

 

  
/*
//instrument.noteOn(frequency,0.2);
// hvymtl.noteOn(frequency,0.6);
flut.setFrequency(freq);
flut.noteOn(freq,0.6);
rhodey.setFrequency(frequency);
rhodey.noteOn(frequency,0.6);

whistle.setFrequency(frequency);
whistle.startBlowing(0.9,1);
whistle.stopBlowing(5);
whistle.noteOn(frequency,0.8);
*/
// resonate.setResonance(frequency/5,0.8);
// resonate.noteOn(frequency,0.5);
// moog.setModulationDepth(10);
// moog.setModulationSpeed(100);
// moog.noteOn(frequency,0.5);

// simple.noteOn(frequency,0.5);
 
 //  
// //  sitar.noteOn(frequency/10,0.5);
//  sitar.pluck(50);
 
/*krap.setStretch(0.7);
krap.setPickupPosition(0.5);
krap.pluck(0.5);
krap.noteOn(frequency,0.5);*/


///////////////////////////////////////////////////////////////////////////////
/* if(aaa%100==0)hvymtl.noteOn(frequency,0.6);
 if(aaa%100==0)envlp.setValue(1);
 if(aaa%100==0)envlp.setTarget(0);
 if(aaa%100==0)envlp.setTime(4);
 aaa++;*/
 
// blit_saw.setFrequency(StkFloat(freq/5));
// blit_square.setFrequency(StkFloat(freq/5));
  
// modulate.setVibratoRate(StkFloat(freq/100));  //2
// modulate.setVibratoGain(10000);             //2
   
  std::vector<StkFloat> numerator( 5, 0.1 ); // create and initialize numerator coefficients
  std::vector<StkFloat> denominator;         // create empty denominator coefficients
  denominator.push_back( 1.0 );              // populate our denomintor values
  denominator.push_back( 0.2);
  denominator.push_back( -0.2);
  
//  Iir iir_fil( numerator, denominator );
  
// bi_fil.setCoefficients(1.0, 0.5,-0.3,0.9,false);//4
bi_fil.setResonance(220.0, 0.6, true);//3
// bi_fil.setB0(0.4);//5

 
//  onep_fil.setCoefficients(0.4,0.9);
////////////////////////////////////////////
  
  
  StkFrames input(wp.num_samples,1);
 
  
  
  
 //   bi_fil.tick(input);
    
//    for(size_t i=0; i<wp.num_samples; ++i) {
 
//      size_t arr_ind=(start_ind+i);
// 	size_t arr_ind=i;
///////////////////////////////////////////////////////////////////// fill the circular buffer with STK////////////////////////////////////////////////
// 	csb[arr_ind] += 32760*(noise.tick())*attenuation;  //1
//       csb[arr_ind] += 50*(iir_fil.tick(modulate.tick()))*attenuation;   //2 
	
	
	

///////////////////different instrument for width,hight,depth/////////////////////////inside for
/*if (freq_ind==0 || freq_ind==1)
        {
          sineWv.setFrequency(frequency);
          tick_value=sineWv.tick();}
	   
 else
   if(freq_ind==2)
	{
 	  
	  drummer.noteOn(44,0.8);
	  tick_value= drummer.tick();}

 else 
 if(freq_ind==3)
	  {
	 hvymtl.noteOn(frequency,0.6);	
	 tick_value=hvymtl.tick();}*/

///////////////////END OF  different instrument for width,hight,depth///////////////////	 
	

 
 if (objectType=="lightbulb") 
      {  
	std::cout<<"******************************** it is lightbulb ****************************************"<<std::endl;
// 	sineWv.setFrequency(frequency);
	beeThree.noteOn(frequency,0.5);
	
	for(size_t i=0; i<wp.num_samples; ++i) 
	    { 
//	      size_t arr_ind=(start_ind+i);
//	      csb[arr_ind] +=732760*sineWv.tick()*attenuation;
// 	      input[i] +=732760*sineWv.tick()*attenuation;
// 	      shakers.controlChange(10,120);
// 	      input[i] +=732760*(shakers.tick())*attenuation;
	      
	      input[i] +=732760*(beeThree.tick())*attenuation;
   
	      attenuation+=attenuation_step; 
	     }
	     
	}
 
 else if (objectType=="coffeemug") 
      {  
	std::cout<<"++++++++++++++++++++++++++++++++++it is coffeemug +++++++++++++++++++++++++++++++++++++++"<<std::endl;
// 	sineWv.setFrequency(frequency);
	bandWG.noteOn(frequency,0.5);
	for(size_t i=0; i<wp.num_samples; ++i) 
	    {
//	      size_t arr_ind=(start_ind+i);
//	      csb[arr_ind] +=732760*sineWv.tick()*attenuation;
// 	      input[i] +=732760*sineWv.tick()*attenuation;
// 	      shakers.controlChange(10,120);
// 	      input[i] +=732760*(shakers.tick())*attenuation;
	      input[i] +=732760*(bandWG.tick())*attenuation;
	      attenuation+=attenuation_step; 
	     }
	     
	}
 
 else if (objectType=="banana") 
      {  
	std::cout<<"-------------------------------- it is banana----------------------------------------"<<std::endl;
// 	sineWv.setFrequency(frequency);
	 shakers.noteOn(frequency,0.5);
	for(size_t i=0; i<wp.num_samples; ++i) 
	    {
//	      size_t arr_ind=(start_ind+i);
//	      csb[arr_ind] +=732760*sineWv.tick()*attenuation;
// 	      input[i] +=732760*sineWv.tick()*attenuation;
// 	      shakers.controlChange(10,120);
// 	      input[i] +=732760*(shakers.tick())*attenuation;
	      shakers.controlChange(9,80);
	      input[i] +=732760*(shakers.tick())*attenuation; 
	      attenuation+=attenuation_step; 
	     }
	     
	}
 
 
 
 else if (objectType =="cap")
     {
	std::cout<< "////////////////////  it is cap /////////////////////////////"<<std::endl;
// 	blit.setFrequency(frequency);
// 	drummer.noteOn(frequency,0.5);
// 	drummer.noteOn(90,0.5);
	for(size_t i=0; i<wp.num_samples; ++i) 
	    {
	      input[i] +=13300*noise.tick()*attenuation;
// 	     input[i] +=732760*(drummer.tick())*attenuation;
	     // size_t arr_ind=(start_ind+i);
// 	      csb[arr_ind] +=732760*(blit_saw.tick())*attenuation;
// 	      input[i] +=732760*(blit_saw.tick())*attenuation;
//  	     input[i] +=732760*(bandWG.tick())*attenuation;
//  	     input[i] +=732760*(drummer.tick())*attenuation;
// 	      input[i] +=732760*(beeThree.tick())*attenuation;
// 	      input[i] +=732760*(resonate.tick())*attenuation;
// 	      input[i] +=732760*(moog.tick())*attenuation;
// 	     shakers.controlChange(9,80);
// 	      input[i] +=732760*(shakers.tick())*attenuation;   
// 	      input[i] +=732760*(simple.tick())*attenuation;
// 	     input[i] +=732760*(sitar.tick())*attenuation; 
// 	      input[i] +=732760*(krap.tick())*attenuation;    //why it does not generate any sound??
// 	      input[i]+=732760*(whistle.tick())*attenuation;
// 	     input[i] +=732760*(blowBotl.tick())*attenuation;
// 
	 
	      attenuation+=attenuation_step;  
	    }
      }
  
  /* else if (objectType =="coffeemug")
  */ 
  
/*  
else if (objectType =="apple")
  
  {
	std::cout<< "-------------------------------it is apple ----------------------------------------"<<std::endl;
// 	blit.setFrequency(frequency);
	 static int blah=0;
        if(blah%50==0)sitar.noteOn(frequency/10,0.5);
        if(blah%50==0)sitar.pluck(20);
        blah++;

	for(size_t i=0; i<wp.num_samples; ++i) 
	    {
	     // size_t arr_ind=(start_ind+i);
// 	      csb[arr_ind] +=732760*(blit_saw.tick())*attenuation;
// 	      input[i] +=732760*(blit_saw.tick())*attenuation;
 	     
// 	      input[i] +=732760*(resonate.tick())*attenuation;
// 	      input[i] +=732760*(moog.tick())*attenuation;
  
// 	      input[i] +=732760*(simple.tick())*attenuation;
 	     input[i] +=732760*(sitar.tick())*attenuation; 
	      
// 	      input[i] +=732760*(krap.tick())*attenuation;    //why it does not generate any sound??
// 	      input[i]+=732760*(whistle.tick())*attenuation;
//            input[i] +=732760*(blowBotl.tick())*attenuation;
//               drummer.noteOn(90,0.5);
// 	     input[i] +=732760*(drummer.tick())*attenuation;
	    
	      
	     
	      //        whistle.setSampleRate(220);
//        whistle.setFrequency(freq/5000);
//        whistle.startBlowing(30,10);
//        whistle.stopBlowing(10);
//        whistle.noteOn(freq/5000,30);
//        whistle.noteOff(50);
	      //    input[i] +=733*whistle.tick()*attenuation;
	      attenuation+=attenuation_step;  
	    }
      }
  */
else
     {
       std::cout<<"///////////////////////////////// DON'T KNOW ///////////////////////////////////////////"<<std::endl;
       std::cout<<"/////////// result: "<< objectType<< "////////////"<<std::endl;
       for(size_t i=0; i<wp.num_samples; ++i) 
	    { 
// 	      size_t arr_ind=(start_ind+i);
// 	      csb[arr_ind] +=700*noise.tick()*attenuation;
	      input[i] +=132760*noise.tick()*attenuation;
	      attenuation+=attenuation_step; 
	      }
      }
   
      
      
//   bi_fil.tick(input);
 for(size_t j=0;j<wp.num_samples;++j)
 {
   size_t arr_ind=(start_ind+j);
   csb[arr_ind] +=input[j];
}
      


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

         
//	csb[arr_ind] += input[i];
 
   // csb[arr_ind] += 50*(bi_fil.tick(modulate.tick()))*attenuation;   //3 and 4 and 5
    // csb[arr_ind] += 732760*(tick_value)*attenuation; 
      
     //  csb[arr_ind] += iir_fil.tick(blit_saw.tick()+blit.tick())*attenuation;

	
/////////////////////////////////////////////////////////////////////END OF fill the circular buffer with STK////////////////////////////////////////////////
	//csb[arr_ind] += amplitude(freq,t)*attenuation;
	//samples[arr_ind] += attenuation*amplitude(freq,t);
      //  freq+=freq_step;
//          t+=time_step; ////////////// we don't need this line when we use stk
      
//      }
 
    if(ssgvvvSTK>6) {//VERBOSITY 6+
/*         std::cout<<"LOAD TO ARRAY: First few values: ";
         for(size_t i=start_ind; i<start_ind+20; i++) {
// //             size_t arr_ind=(start_ind+i)%buffer_size;
// 	    size_t arr_ind=start_ind+i;
if(i!=start_ind)std::cout<<",";
             std::cout<<csb[i];
// // 	    std::cout<<samples[arr_ind];
         }
         
                  std::cout<<"LOAD TO ARRAY: Last few values: ";
         
	   for(size_t i=start_ind+wp.num_samples-20; i<start_ind+wp.num_samples; i++) {
// //             size_t arr_ind=(start_ind+i)%buffer_size;
// 	    size_t arr_ind=start_ind+i;
if(i!=start_ind)std::cout<<",";
             std::cout<<csb[i];
// // 	    std::cout<<samples[arr_ind]*/;
//          }
// 
//         std::cout<<std::endl;
// 
//         std::cout<<"LOAD TO ARRAY: Last few values: ";
//         for(size_t i=wp.num_samples-20; i<wp.num_samples; i++) {
// //             size_t arr_ind=(start_ind+i)%buffer_size;
// 	    size_t arr_ind=start_ind+i;
//             std::cout<<csb[arr_ind];
// // 	    std::cout<<samples[arr_ind];
//             if(i!=wp.num_samples-1)std::cout<<",";
//         }
//         std::cout<<std::endl;
//     }
      
      std::cout<<"LOAD TO ARRAY - CURRENT ARRAY - FIRST 20:"<<std::endl<<csb.str(start_ind,start_ind+20)<<std::endl;
      std::cout<<"LOAD TO ARRAY - CURRENT ARRAY - LAST 20:"<<std::endl<<csb.str(start_ind+wp.num_samples-20,start_ind+wp.num_samples)<<std::endl;
    }
}


SoundSourceGeneratorImplSTK::SoundSourceGeneratorImplSTK(double approx_hz):rate_tracker("SoundSourceGeneratorImplSTK.RateTracker",approx_hz),played_last(false) {
Stk::setSampleRate(SAMPLE_RATE);
aaa=0;
}

SoundSourceGeneratorImplSTK::~SoundSourceGeneratorImplSTK() {

}

/////////////////////////////////////////////////
SoundFeature SoundSourceGeneratorImplSTK::get_last_played_sound_feature() {
  return last_played;
}
////////////////////////////////////////////////

void SoundSourceGeneratorImplSTK::generate_sound(SoundFeature sfeat,CircularBuffer<short> &csb) {
  
     if(ssgvvvSTK>4)std::cout<<"SoundSourceGeneratorImpl::Frame #"<<rate_tracker.get_num_updates()<<std::endl;

    rate_tracker.update();
  
    double rise_time=rate_tracker.get_tracked_period();

    double spread_mult;
    if((2*sfeat.spread_mult-1.0)*rise_time*SAMPLE_RATE>csb.size()) {
        std::cerr<<"WARNING: A sound is provided that is too long for our buffer."<<std::endl;;
        std::cerr<<"WARNING: Shortening sound."<<std::endl;
        spread_mult=(1.0*csb.size()/(SAMPLE_RATE*rise_time)+1)/2.0;
    }
    else spread_mult=sfeat.spread_mult;


    size_t num_freq=sfeat.frequencies.size();


    double starttime=0.0;
    double midtime=rise_time;
    double endtime=2*rise_time*spread_mult;

    size_t num_samples1=SAMPLE_RATE*(midtime-starttime);
    size_t num_samples2=SAMPLE_RATE*(endtime-midtime);//num_samples_all-num_samples1;

//     size_t start_sample1=0;
//     size_t start_sample2=num_samples1;

    double energy_mult=sfeat.energy_mult/spread_mult;


    for (size_t freqind = 0; freqind < num_freq; freqind++) { //assuming some kind of order

        wave_properties wp;

        double mid_atten=energy_mult/(num_freq+ATTENUTATION_FUDGE);

        // add in a waveform that fades in the next frequency over one grain
        wp.startfreq=sfeat.frequencies[freqind];
        wp.endfreq=sfeat.frequencies[freqind];
        wp.num_samples=num_samples1;
        //if it is spread then the peak power is lower
        wp.start_attenuation=0.0;
        wp.end_attenuation=mid_atten;
        wp.starttime = starttime;
        wp.endtime=midtime;
// 	  void fill_sample_add(short *samples,size_t start_ind,size_t buffer_size,wave_properties &wp) {
        fill_sample_add(csb,wp,0,sfeat.objectType,freqind);

        // now the tail of the waveform
        wp.num_samples=num_samples2;
        //if it is spread then the peak power is lower
        wp.start_attenuation=mid_atten;
        wp.end_attenuation=0.0;
        wp.starttime = midtime;
        wp.endtime=endtime;
        fill_sample_add(csb,wp,num_samples1,sfeat.objectType,freqind);

    }
    //////////////////////////////////////
 last_played=sfeat;
///////////////////////////////////////////
}


