#include <soundgen/SoundSourceFullWaveformGenerator.h>
#include <soundgen/SoundSourceWaveformInterpolator.h>
#include <soundgen/SoundSourceOutput.h>
#include <boost/program_options.hpp>
#include <math.h>
#include <AL/alut.h>
#include <iostream>
#include <chrono>
#include <stdlib.h>
#include <Consts.h>

#define BUFFER_SIZE 44100*10
//std::chrono::high_resolution_clock::now();
//std::chrono::high_resolution_clock::time_point start_time;
//std::chrono::duration<float>(now-released_time).count();

#define MAX_AMPLITUDE_SHORT 16000

double randd() {
    return ((double)rand())/RAND_MAX;
}

namespace po = boost::program_options;

int vvv;

po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Test Sound Waveform Interpolator" );
    desc.add_options()
    ( "help,h","Get help. You probably need it. But not that kind of help." )
    ( "verbose,v",po::value<int>()->default_value ( 0 ),"Verbosity level." )

//     ( "NormalSmoothingSize",po::value<double>()->default_value ( 10.0f ),"NormalSmoothingSize - integral image normal estimation parameter - see PCL API." )

    ;

    po::positional_options_description p;
//     p.add ( "filenames", 15 );
//   p.add("target_filename", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm );

    return vm;
}

double frequency_of_note(double octave,double note/*starts with A*/) {
    double lowestA=27.50	;
    double a= pow(2.0,1.0/12);
//   std::cout<<"pow("<<lowestA<<","<<octave<<")*pow("<<a<<","<<note<<")="<<pow(lowestA,1.0*octave)<<"*"<<pow(a,1.0*note)<<std::endl;
    return lowestA*pow(2,1.0*octave)*pow(a,1.0*note);
}

// double middlec=261.6;

double sine(double freq,double t) {

    double toret=MAX_AMPLITUDE_SHORT * sin( (2.f*float(M_PI)*freq) * t );
//     if(ssfwgvvv>8) {
//         std::cout<<"amplitude("<<freq<<","<<t<<")-->"<<toret<<std::endl;
//     }
    return toret;
}



void test0() {


// //     SoundSourceFullWaveformGenerator ssg;
    std::vector<short> full_buffer;
//     std::vector<short> full_buffer(BUFFER_SIZE);
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);

        const int soundlength=20000;
	
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq=frequency_of_note(1,30);
// 	double freq=frequency_of_note(1,30+2*randd());
        full_buffer.resize(soundlength);
	double sinstart = 500*((double)rand())/RAND_MAX;
	
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]=sine(freq,i*1.0/SAMPLE_RATE+sinstart);
        }
 
        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30+randd()/30);


    }

}



void test1(SoundSourceWaveformInterpolator::InterpolationApproach approach) {


// //     SoundSourceFullWaveformGenerator ssg;
    std::vector<short> full_buffer;
//     std::vector<short> full_buffer(BUFFER_SIZE);
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

//         SoundFeature sfeat;
//
        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
//         sfeat.pos[0]=x;
//         sfeat.pos[1]=y;
//         sfeat.pos[2]=0.0;
//
// 	sfeat.spread_mult=6;
// 	sfeat.energy_mult=6;
//
//
//         sfeat.frequencies.push_back(frequency_of_note(1,30));

// 	ssg.generate_sound(sfeat,full_buffer);

        
const int ramp1=1000;
const int ramp2=40000;
// 	 const int soundlength=44100;
        const int soundlength=60300;
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq=frequency_of_note(1,30+randd()*2);
        full_buffer.resize(soundlength+ramp1,0);
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]=sine(freq,i*1.0/SAMPLE_RATE);
        }
        for(int i=0; i<ramp1; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp1);
        }
        for(int i=0; i<ramp2; i++) {
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp2);
        }        



        ssi.interpolate_sound(full_buffer,consuming_buffer,approach);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}


void test2(SoundSourceWaveformInterpolator::InterpolationApproach approach) {

    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);


        const int ramp=3000;
        int soundlength=44100;
// 	  	 int soundlength=std::max(4000,60300*randd());
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq=frequency_of_note(1,30+randd());

        full_buffer.resize(soundlength);
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]=sine(freq,i*1.0/SAMPLE_RATE);
        }
        for(int i=0; i<ramp; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp);
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp);
        }



        ssi.interpolate_sound(full_buffer,consuming_buffer,approach);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/90);


    }

}




void test3() {

    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);


        const int ramp=1000;
//  	 int soundlength=44100;
        int soundlength=60300+angle*20;
// 		 int soundlength=60300;
// 		 int soundlength=60300-angle*20;
// 		 int soundlength=std::max(4000.0,60300*randd());
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq=frequency_of_note(1,30+randd()*3);

        full_buffer.resize(soundlength);
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]=sine(freq,i*1.0/SAMPLE_RATE);
        }
        for(int i=0; i<ramp; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp);
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp);
        }



        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}




void test4() {

    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);


        const int ramp=1000;
//  	 int soundlength=44100;
//  	  	 int soundlength=60300+angle*20;
// 		 int soundlength=60300;
        int soundlength=60300-angle*20;
// 		 int soundlength=std::max(4000.0,60300*randd());
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq=frequency_of_note(1,30+randd()*3);

        full_buffer.resize(soundlength);
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]=sine(freq,i*1.0/SAMPLE_RATE);
        }
        for(int i=0; i<ramp; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp);
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp);
        }



        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}





void test5() {

    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);


        const int ramp=1000;
//  	 int soundlength=44100;
//  	  	 int soundlength=60300+angle*20;
// 		 int soundlength=60300;
        int soundlength=std::max(100.0,60300+(randd()-0.5)*angle*100);
// 		 int soundlength=std::max(4000.0,60300*randd());
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double freq1=frequency_of_note(1,20+randd()*3);
        double freq2=frequency_of_note(1,30+randd()*6);
        double freq3=frequency_of_note(1,40+randd()*9);
        full_buffer.clear();
        full_buffer.resize(soundlength);
        for(int i=0; i<soundlength; i++) {
            full_buffer[i]+=sine(freq1,i*1.0/SAMPLE_RATE)/3;
            full_buffer[i]+=sine(freq2,i*1.0/SAMPLE_RATE)/3;
            full_buffer[i]+=sine(freq3,i*1.0/SAMPLE_RATE)/3;
        }
        for(int i=0; i<ramp; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp);
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp);
        }



        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}

double test6spectrum(double mean,double dev,double freq) {
//   double mean=30.0;
//   double dev=10.0;
    double exponent= (freq-mean)*(freq-mean)/(2*dev*dev);
    double multiplier=1.0/(dev*sqrt(2*3.14));
    double res = multiplier*exp(exponent);
    return res;
}


void test6() {

    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();

    for(double angle=0; angle<360; angle++) {

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);


        const int ramp=1000;
//  	 int soundlength=44100;
//  	  	 int soundlength=60300+angle*20;
// 		 int soundlength=60300;
        int soundlength=std::max(100.0,60300+(randd()-0.5)*angle*500);
// 		 int soundlength=std::max(4000.0,60300*randd());
        Eigen::Vector4f pos;
        pos[0]=x;
        pos[1]=y;
        pos[2]=0.0;
        double num_freq=0;


        full_buffer.clear();
        full_buffer.resize(soundlength);

        double start_freq=100.0;
        double end_freq=510.0;
        double step_freq=20.0;
        double peak_freq=500;
        double freq_spread=50.0;

        for(double freq=start_freq; freq<end_freq; freq+=step_freq) {

            num_freq++;
        }
        std::cout<<"Dealing with "<<num_freq<<" frequencies"<<std::endl;
// 	 std::cout<<"frequency "<<freq<<std::endl;
        double total_curve=0;
        for(double freq=start_freq; freq<end_freq; freq+=step_freq) {
            total_curve+=test6spectrum(peak_freq,freq_spread,freq);
        }

        std::cout<<"Total curve:"<<total_curve<<"."<<std::endl;

        for(double freq=start_freq; freq<end_freq; freq+=step_freq) {
            double multiplier=test6spectrum(peak_freq,freq_spread,freq)/total_curve;
// std::cout<<"Frequency "<<freq<<" multiplier "<<multiplier<<std::endl;
            for(int i=0; i<soundlength; i++) {

                full_buffer[i]+=sine(freq,i*1.0/SAMPLE_RATE)*multiplier;

            }
        }
// //

        for(int i=0; i<ramp; i++) {
            full_buffer[i]=full_buffer[i]*(i*1.0/ramp);
            full_buffer[soundlength-i-1]=full_buffer[soundlength-i-1]*(i*1.0/ramp);
        }



        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}




// void test2() {
//
//
//     SoundInterpolator si(15);
//
//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();
//
//     for(double angle=0; angle<360; angle++) {
//
//         SoundFeature sfeat;
//
//         double x=cos(3.14*angle/180);
//         double y=sin(3.14*angle/180);
//         sfeat.pos[0]=x;
//         sfeat.pos[1]=y;
//         sfeat.pos[2]=0.0;
//
//
//         sfeat.frequencies.push_back(frequency_of_note(1,30+randd()*0.8));
//
//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	si.play_feature(sfeat);
//         alutSleep(1.0/30);
//
//
//     }
//
// }
//
// void test3() {
//
//
//     std::vector<double> note_sequence;
//
//     SoundInterpolator si(10);
//
//     std::chrono::high_resolution_clock::time_point start_time=std::chrono::high_resolution_clock::now();
//
//     for(double angle=0; angle<720; angle++) {
// //           for(double angle=0; angle<360; angle++) {
//
//
//         SoundFeature sfeat;
//
//         double x=cos(3.14*angle/180);
//         double y=sin(3.14*angle/180);
//         sfeat.pos[0]=x;
//         sfeat.pos[1]=y;
//         sfeat.pos[2]=0.0;
//
//         int num_frequencies=angle/120 + 1;
//
//         for (int f=0; f<num_frequencies; f++) {
// //             int notenum=12+f*12;
//
// //             sfeat.frequencies.push_back(frequency_of_note(1,notenum)+randd()*0.5);
// // 	    sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
// // 	  sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
// // 	  sfeat.frequencies.push_back(frequency_of_note(2+f,0));
// // 	  sfeat.frequencies.push_back(frequency_of_note(2+f,randd()*0.1));
//             sfeat.frequencies.push_back(frequency_of_note(2+f,randd()));
//
//         }
//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
// 	std::cout<<"------------------------------------"<<std::endl;
//         si.play_feature(sfeat);
//         alutSleep(1.0/30);
//
//
//     }
//
// }


void test7() {


    SoundSourceFullWaveformGenerator ssg;
    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

    for(double angle=0; angle<360; angle++) {

        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;
//
        sfeat.spread_mult=1;
        sfeat.energy_mult=1;
        sfeat.frequencies.push_back(frequency_of_note(1,30+randd()));

        ssg.generate_sound(sfeat,full_buffer);

        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,sfeat.pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}


void test8() {


    SoundSourceFullWaveformGenerator ssg;
    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

    for(double angle=0; angle<360; angle++) {

        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;
//
        sfeat.spread_mult=randd()+0.5;
        sfeat.energy_mult=1;
        sfeat.frequencies.push_back(frequency_of_note(1,30+5*randd()));
        sfeat.frequencies.push_back(frequency_of_note(1,25+5*randd()));
        sfeat.frequencies.push_back(frequency_of_note(1,20+5*randd()));

        ssg.generate_sound(sfeat,full_buffer);

        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,sfeat.pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
        alutSleep(1.0/30);


    }

}


double test9spectrum(double peak,double dev,double freq) {
    if(freq>peak)return 0;
    if(freq<(peak-dev))return 0;
    double range=peak-dev;
    double toret=(freq-(peak-dev))/range;
    assert(toret>=0);
    return toret;
}
void test9() {


    SoundSourceFullWaveformGenerator ssg;
    std::vector<short> full_buffer;
    SoundSourceWaveformInterpolator ssi;
    CircularBuffer<short> consuming_buffer(BUFFER_SIZE);
    SoundSourceOutput sso;

    for(double angle=0; angle<360; angle+=2) {
        std::cout<<std::endl<<std::endl<<"***********"<<std::endl<<"***********test9 angle***********"<<angle<<std::endl<<"***********"<<std::endl;
        SoundFeature sfeat;

        double x=cos(3.14*angle/180);
        double y=sin(3.14*angle/180);
        sfeat.pos[0]=x;
        sfeat.pos[1]=y;
        sfeat.pos[2]=0.0;
//
//        sfeat.spread_mult=1;
	
        sfeat.spread_mult=randd()+0.05;
        sfeat.energy_mult=1;
// 	sfeat.spread_mult=0.1;//!!!
// if(angle==16)sfeat.spread_mult=1.0;
	

        double start_freq=200.0+angle/4+randd()*10;
//          double start_freq=400.0-5;
        
	double end_freq=450.0;
        double step_freq=5.3;
// 	double end_freq=start_freq+step_freq;
// 		double end_freq=start_freq+step_freq*3;
        double peak_freq=400;
        double freq_spread=150.0;

        int num_freq=0;
        double total_curve=0;
        for(double freq=start_freq; freq<end_freq; freq+=step_freq) {
            double thisspec=test9spectrum(peak_freq,freq_spread,freq);
            total_curve+=thisspec;
	    
            num_freq++;
        }

//         std::cout<<"Total curve:"<<total_curve<<"."<<std::endl;

        for(double freq=start_freq; freq<end_freq; freq+=step_freq) {
            double multiplier=test9spectrum(peak_freq,freq_spread,freq)/total_curve;
// std::cout<<"Frequency "<<freq<<" multiplier "<<multiplier<<std::endl;
//   std::cout<<"freq "<<freq<<": "<<multiplier<<std::endl;
            sfeat.frequencies.push_back(freq);
            sfeat.multipliers.push_back(multiplier*num_freq);
        }
std::cout<<"Playing "<<sfeat<<std::endl;
        ssg.generate_sound(sfeat,full_buffer);

        ssi.interpolate_sound(full_buffer,consuming_buffer);
        sso.output(consuming_buffer,sfeat.pos);

//         std::chrono::high_resolution_clock::time_point play_time=std::chrono::high_resolution_clock::now();
//         std::cout<<"------------------------------------"<<std::endl;
// 	std::cout<<"Playing "<<sfeat<<" at "<<std::chrono::duration<float>(play_time-start_time).count()<<std::endl;
//         std::cout<<"------------------------------------"<<std::endl;
// 	ssg.
//         if(angle==18) {
//             alutSleep(15.0);
//             std::cout<<"PAUSE"<<std::endl;
//         }
//         alutSleep(5.0);
	alutSleep(5.0/30);
  

    }

}

int main(int argc,char ** argv) {

//   SoundInterpolator sint;

    po::variables_map vm=args ( argc,argv );

    vvv = vm["verbose"].as<int>();

    // okay let's make a sound come around you once, building up frequencies as it goes.
//     test3();
//     alutSleep(1.0);
    //simpler test
//     test9();
//     alutSleep(1.0);
//     test8();
//     alutSleep(1.0);
//     test7();
//     alutSleep(1.0);
//     test6();
//     alutSleep(1.0);
//     test5();
//     alutSleep(1.0);
// 	        test4();
//     alutSleep(1.0);
//     test3();
//     alutSleep(1.0);
    test2(SoundSourceWaveformInterpolator::ChangeWaveAndSizeLayered);
    alutSleep(1.0);


    test2(SoundSourceWaveformInterpolator::GranulateNewIn);
    alutSleep(1.0);



    test2(SoundSourceWaveformInterpolator::RestartFadeStart);
    alutSleep(1.0);
    
    
//     test1(SoundSourceWaveformInterpolator::ChangeWaveAndSizeLayered);
//     alutSleep(2.0);
//     
//         test1(SoundSourceWaveformInterpolator::GranulateNewIn);
//     alutSleep(2.0);
    
//     test0();
//     alutSleep(2.0);

}
