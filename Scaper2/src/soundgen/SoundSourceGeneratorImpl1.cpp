
#include <soundgen/SoundSourceGenerator.h>
#include <soundgen/SoundSourceGeneratorImpl1.h>
#include <iostream> 
#include <boost/concept_check.hpp>

#include <Consts.h>
#define ATTENUTATION_FUDGE 3


int ssgvvv=1;//VERBOSITY LEVEL


SoundSourceGenerator::SoundSourceGenerator(double approx_hz) {
    impl.reset(new SoundSourceGeneratorImpl(approx_hz));
}
void SoundSourceGenerator::generate_sound(SoundFeature &sfeat,CircularBuffer<short> &csb) {
    impl->generate_sound(sfeat,csb);
}

SoundFeature SoundSourceGenerator::get_last_played_sound_feature() {
  return impl->get_last_played_sound_feature();
}

SoundSourceGenerator::~SoundSourceGenerator() { }

/* Return just one sample of a wave frequency freq at time t*/
double amplitude(double freq,double t) {
    double toret=32760 * sin( (2.f*float(M_PI)*freq) * t );
    if(ssgvvv>8) {
        std::cout<<"amplitude("<<freq<<","<<t<<")-->"<<toret<<std::endl;
    }
    return toret;
}

/* For passing to the fill_sample routine*/
struct wave_properties {
    double starttime;
    double endtime;
    double startfreq;
    double endfreq;
    double start_attenuation;
//     double attack_duration;
//     double release_onset;
    double end_attenuation;
    uint num_samples;
};

/* Add wave into array (don't clear array) */
void fill_sample_add(CircularBuffer<short> &csb,wave_properties &wp,size_t start_ind) {

    if(ssgvvv>6) {//VERBOSITY 7+
        std::cout<<"fill_sample_add(CircularBuffer<short> &csb,wave_properties &wp,size_t start_ind="<<start_ind<<")"<<std::endl;
    }

    size_t num_steps=(wp.endtime-wp.starttime)*SAMPLE_RATE;

    
      if(num_steps!=wp.num_samples)
	  assert(num_steps==wp.num_samples);
    assert(num_steps<=csb.size());

    double freq_step=(wp.endfreq-wp.startfreq)/wp.num_samples;
    double freq=wp.startfreq;
    double time_step=(wp.endtime-wp.starttime)/wp.num_samples;
    double t=wp.starttime;
    double attenuation_step=(wp.end_attenuation-wp.start_attenuation)/wp.num_samples;
    double attenuation=wp.start_attenuation;

    if(ssgvvv>5) {//VERBOSITY 6+
        std::cout<<"    wp.startfreq="<<wp.startfreq<<" wp.endfreq="<<wp.endfreq<<" wp.start_attenuation="<<wp.start_attenuation;
        std::cout<<" wp.end_attenuation="<<wp.end_attenuation<<" wp.starttime="<<wp.starttime<<" wp.endtime="<<wp.endtime;
        std::cout<<" wp.num_samples="<<wp.num_samples<<  std::endl;
    }

    for(size_t i=0; i<wp.num_samples; ++i) {

        size_t arr_ind=(start_ind+i);

        csb[arr_ind] += attenuation*amplitude(freq,t);
        freq+=freq_step;
        t+=time_step;
        attenuation+=attenuation_step;
    }

    if(ssgvvv>6) {//VERBOSITY 6+

      std::cout<<"LOAD TO ARRAY - CURRENT ARRAY:"<<std::endl<<csb.str(start_ind,start_ind+20)<<std::endl;
    }
}


SoundSourceGeneratorImpl::SoundSourceGeneratorImpl(double approx_hz):rate_tracker("SoundSourceGeneratorImpl.RateTracker",approx_hz),played_last(false) {

}

SoundSourceGeneratorImpl::~SoundSourceGeneratorImpl() {

}

SoundFeature SoundSourceGeneratorImpl::get_last_played_sound_feature() {
  return last_played;
}

void SoundSourceGeneratorImpl::generate_sound(SoundFeature sfeat,CircularBuffer<short> &csb) {
  
     if(ssgvvv>4)std::cout<<"SoundSourceGeneratorImpl::Frame #"<<rate_tracker.get_num_updates()<<std::endl;

    rate_tracker.update();
  
    double rise_time=rate_tracker.get_tracked_period();

    double spread_mult;
    if((2*sfeat.spread_mult-1.0)*rise_time*SAMPLE_RATE>csb.size()) {
        std::cerr<<"WARNING: A sound is provided that is too long for our buffer."<<std::endl;;
        std::cerr<<"WARNING: Shortening sound."<<std::endl;
        spread_mult=(1.0*csb.size()/(SAMPLE_RATE*rise_time)+1)/2.0;
    }
    else spread_mult=sfeat.spread_mult;


    size_t num_freq=sfeat.frequencies.size();


    double starttime=0.0;
    double midtime=rise_time;
    double endtime=2*rise_time*spread_mult;

    size_t num_samples1=SAMPLE_RATE*(midtime-starttime);
    size_t num_samples2=SAMPLE_RATE*(endtime-midtime);//num_samples_all-num_samples1;


    double energy_mult=sfeat.energy_mult/spread_mult;


    for (size_t freqind = 0; freqind < num_freq; freqind++) { //assuming some kind of order

        wave_properties wp;

        double mid_atten=energy_mult/(num_freq+ATTENUTATION_FUDGE);

        // add in a waveform that fades in the next frequency over one grain
        wp.startfreq=sfeat.frequencies[freqind];
        wp.endfreq=sfeat.frequencies[freqind];
        wp.num_samples=num_samples1;
        //if it is spread then the peak power is lower
        wp.start_attenuation=0.0;
        wp.end_attenuation=mid_atten;
        wp.starttime = starttime;
        wp.endtime=midtime;

	fill_sample_add(csb,wp,0);

        // now the tail of the waveform
        wp.num_samples=num_samples2;
        //if it is spread then the peak power is lower
        wp.start_attenuation=mid_atten;
        wp.end_attenuation=0.0;
        wp.starttime = midtime;
        wp.endtime=endtime;
        fill_sample_add(csb,wp,num_samples1);

    }
    last_played=sfeat;

}


