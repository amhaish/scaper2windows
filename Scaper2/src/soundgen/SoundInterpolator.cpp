#include <al.h>
#include <alc.h>
#include <alut/alut.h>
#include <boost/concept_check.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <soundgen/SoundInterpolator.h>
#include <soundgen/SoundInterpolatorImpl2.h>
#include <iostream> 

#define MAX_SOUND_PERIOD 5
#define SAMPLE_RATE 44100
// #define SAMPLE_RATE 22050
#define FORMAT AL_FORMAT_MONO16
#define BYTES_PER_SAMPLE 2
#define ATTENUTATION_FUDGE 3
// #define MAX_FILL_BUFFERS 10
#define MAX_NUM_SAMPLES (MAX_SOUND_PERIOD*SAMPLE_RATE)
#define NUM_AL_BUFFERS_TO_KEEP 10

#define AVG_FRAME_WEIGHT_MULTIPLIER 0.2
// #define BUFFER_TEST_SIZE_DIVISOR 16

#define ADVANCE_SECS 0.01
#define BELIEVE_OPENAL_MULT 0.001
#define BELIEVE_CURR_FRAME_RATE_MULT 0.8

// #define CATCHUP_MULT 0.1


bool si_alut_initialised=false;
int si_num_sinterpolators=0;

int sivvv=2;//VERBOSITY LEVEL


SoundInterpolator::SoundInterpolator(double approx_hz) {
    impl.reset(new SoundInterpolatorImpl(approx_hz));
}
bool SoundInterpolator::play_feature(SoundFeature& sfeat) {
    return impl->play_feature(sfeat);
}

void SoundInterpolator::play_nothing() {
    impl->play_nothing();
}

SoundInterpolator::~SoundInterpolator() { }

//#include <chrono>
//std::chrono::high_resolution_clock::now();
//std::chrono::high_resolution_clock::time_point start_time;
//std::chrono::duration<float>(now-released_time).count();

/* Return just one sample of a wave frequency freq at time t*/
double amplitude(double freq,double t) {
    double toret=32760 * sin( (2.f*float(M_PI)*freq) * t );
    if(sivvv>8) {
        std::cout<<"amplitude("<<freq<<","<<t<<")-->"<<toret<<std::endl;
    }
    return toret;
}


/* For passing to the fill_sample routine*/
struct wave_properties {
    double starttime;
    double endtime;
    double startfreq;
    double endfreq;
    double start_attenuation;
//     double attack_duration;
//     double release_onset;
    double end_attenuation;
    uint num_samples;
};

void zero_samples_circular(short *samples,size_t start_ind,size_t buffer_size,size_t num_to_zero) {
    for(size_t i=0; i<num_to_zero; ++i) {

        size_t arr_ind=(start_ind+i)%buffer_size;

        samples[arr_ind] =0;

    }

}

/* Add wave into array (don't clear array) */
void fill_sample_add(short *samples,size_t start_ind,size_t buffer_size,wave_properties &wp) {
// void fill_sample_add(short *samples,size_t buffer_size,wave_properties &wp) {

    if(sivvv>6) {//VERBOSITY 7+
        std::cout<<"fill_sample_add(short *samples,start_ind="<<start_ind<<",buffer_size="<<buffer_size<<",wave_properties &wp)"<<std::endl;
    }

    size_t num_steps=(wp.endtime-wp.starttime)*SAMPLE_RATE;

    
      if(num_steps!=wp.num_samples)
// 	if(false){
	  assert(num_steps==wp.num_samples);
// 	}
    assert(num_steps<=buffer_size);

    double freq_step=(wp.endfreq-wp.startfreq)/wp.num_samples;
    double freq=wp.startfreq;
    double time_step=(wp.endtime-wp.starttime)/wp.num_samples;
//     double time_step=1.0/SAMPLE_RATE;//(wp.endtime-wp.starttime)/num_steps;
    double t=wp.starttime;
    double attenuation_step=(wp.end_attenuation-wp.start_attenuation)/wp.num_samples;
    double attenuation=wp.start_attenuation;

    if(sivvv>5) {//VERBOSITY 6+
        std::cout<<"    wp.startfreq="<<wp.startfreq<<" wp.endfreq="<<wp.endfreq<<" wp.start_attenuation="<<wp.start_attenuation;
        std::cout<<" wp.end_attenuation="<<wp.end_attenuation<<" wp.starttime="<<wp.starttime<<" wp.endtime="<<wp.endtime;
        std::cout<<" wp.num_samples="<<wp.num_samples<<  std::endl;
    }

//     for(size_t i=0; i<num_steps; ++i) {
    for(size_t i=0; i<wp.num_samples; ++i) {

        size_t arr_ind=(start_ind+i)%buffer_size;

        samples[arr_ind] += attenuation*amplitude(freq,t);
        freq+=freq_step;
        t+=time_step;
        attenuation+=attenuation_step;
    }

    if(sivvv>6) {//VERBOSITY 6+
        std::cout<<"LOAD TO ARRAY: First few values: ";
        for(size_t i=0; i<20; i++) {
            size_t arr_ind=(start_ind+i)%buffer_size;
            if(i!=0)std::cout<<",";
            std::cout<<samples[arr_ind];
        }

        std::cout<<std::endl;

        std::cout<<"LOAD TO ARRAY: Last few values: ";
        for(size_t i=wp.num_samples-20; i<wp.num_samples; i++) {
            size_t arr_ind=(start_ind+i)%buffer_size;
            std::cout<<samples[arr_ind];
            if(i!=wp.num_samples-1)std::cout<<",";
        }
        std::cout<<std::endl;
    }

}


bool SoundInterpolatorImpl::load_to_al_buffer(ALuint buffer_no,short *samples_ptr,size_t buffer_size,size_t start_ind,size_t num_to_load) {

    short *samples;
    bool delsam;
    assert(num_to_load>0);
    //this next part is just to make this function interface nicer by dealing with circular buffers transparently
    if(start_ind+num_to_load>=buffer_size) {
        delsam=true;
        samples=new short[num_to_load];
        for(size_t i=0; i<num_to_load; i++) {
            size_t ind=(i+start_ind)%buffer_size;
            samples[i]=samples_ptr[ind];
        }

        start_ind=0;

    }
    else {
        delsam=false;
        samples=samples_ptr;
    }
    bool success_flag=false;
    ALenum error;

    if (sivvv>4) {
        std::cout<<"Trying to buffer into buffer id #"<<buffer_no<<" an array of "<<num_to_load<<" shorts size starting at ind "<<start_ind<<std::endl;//<<" into a buffer of size "<<(num_to_load*BYTES_PER_SAMPLE)<<" bytes"<<std::endl;// - t="<<this_time<<std::endl;
    }


    if(sivvv>6) {//VERBOSITY 7+
        std::cout<<"LOAD TO BUFFER: First few values: ";
        for(size_t i=0; i<20&&i<num_to_load; i++) {
            size_t arr_ind=start_ind+i;
            if(i!=0)std::cout<<",";
            std::cout<<samples[arr_ind];
        }

        std::cout<<std::endl;

        std::cout<<"LOAD TO BUFFER: Last few values: ";
        for(size_t i=num_to_load-20; i<num_to_load; i++) {
            size_t arr_ind=start_ind+i;
            std::cout<<samples[arr_ind];
            if(i!=num_to_load-1)std::cout<<",";
        }
        std::cout<<std::endl;
    }

    alBufferData(buffer_no, FORMAT, samples+start_ind, num_to_load*BYTES_PER_SAMPLE, SAMPLE_RATE);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"*********couldn't buffer data\n"<< std::hex <<error<<std::endl;;
        success_flag=false;
        exit(1);
    }
    else {
        //success
        if (sivvv>4) {
            std::cout<<"Buffered into buffer id #"<<buffer_no<<" an array of "<<num_to_load<<" shorts size starting at ind "<<start_ind<<std::endl;//" into a buffer of size "<<(num_to_load*BYTES_PER_SAMPLE)<<" bytes"<<std::endl;
        }
        if (sivvv>6) {
            std::cout<<"Before queue "<<buffer_no<<std::endl;
        }

        alSourceQueueBuffers(source_id, 1, &buffer_no);

        if (sivvv>6) {
            std::cout<<"After queue "<<buffer_no<<std::endl;
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"*********couldn't queue buffer\n"<< std::hex <<error<<std::endl;;
            success_flag=false;
            exit(1);
        }
        else {
            //success
            if (sivvv>4) {
                std::cout<<"Queued buffer "<<buffer_no<<" for playing"<<std::endl;
            }
            success_flag=true;
            this->num_queued_or_playing_al++; //now playing 1 more!
            playing_al_buffer_ids.push_front(buffer_no);
            playing_al_buffer_queued_time.push_back(std::chrono::high_resolution_clock::now());
            playing_al_buffer_size.push_back(num_to_load);
            all_playing_al_buffer_size+=num_to_load;
        }
    }
    if(delsam)delete[] samples;
    return success_flag;
}

// void fill_sample_add(short *samples,size_t start_ind,size_t buffer_size,wave_properties &wp)
// void queue_from_circular(short *samples,size_t start_ind,size_t buffer_size,size_t num_to_buffer);

/* Construct sound interpolation object, and intitialise OpenAL if necessary */
SoundInterpolatorImpl::SoundInterpolatorImpl(double approx_hz) {

    this->approx_hz=approx_hz;
//     this->grain_length=1.0/approx_hz;
//     this->grain_num_samples=SAMPLE_RATE/approx_hz; // number of samples in each grain (small set of samples)
    this->fill_buffer.resize(MAX_NUM_SAMPLES ); // We will use this as a circular buffer
    this->fill_buffer_start=0;
    //this->last_time=0; //don't yet have a need for accurate chronometer but if we do, I used one in donk.cpp
    this->num_queued_or_playing_al=0;
    this->played_last=false;

    this->avg_frame_rate=1.0/approx_hz;
    this->avg_frame_rate_err_secs=0.0; //set it to quite high is better but this is for testing
    this->last_time_play_request_received=std::chrono::high_resolution_clock::now();

    this->frames_processed=0;
    
    this->last_offset=0;

    ALenum error;

    if(!si_alut_initialised) {//intialise OpenAL

        alutInit(0,NULL);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"You have asked me to initialise OpenAL. I'm sorry, Dave. I'm afraid I can't do that."<< std::hex <<error<<"\n"<<std::endl;
            exit(1);
        }

    }

    si_alut_initialised=true;
    si_num_sinterpolators++;

    ALuint buffer_id[NUM_AL_BUFFERS_TO_KEEP]; //should be enough

    if(sivvv>3) { //VERBOSITY 4+
        std::cout<<"generating buffers...";

	/*
	   std::cout<<"generating buffers, overwriting:\n";
        for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++)
	      std::cout<<"  "<<buffer_id[ii]<<std::endl;*/
    }

    alGenBuffers(NUM_AL_BUFFERS_TO_KEEP,&buffer_id[0]);



    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't gen buffers\n"<< std::hex <<error<<std::endl;;
        exit(1); // I can't go on like this anymore, darling.
    }
    else {
        if(sivvv>3) { //VERBOSITY 4+
            std::cout<<"generated buffers, buffer ids:\n";
            for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++)
                std::cout<<"  "<<buffer_id[ii]<<std::endl;
        }
    }

    for(int ii=0; ii<NUM_AL_BUFFERS_TO_KEEP; ii++) {

        available_al_buffer_ids.push_front(buffer_id[ii]);
//     available_al_buffer_ids.push_front(buffer_id[1]);
    }

//     if(sivvv>3) { //VERBOSITY 4+
//         std::cout<<"generated two buffers, at "<<buffer_id[0]<<","<<buffer_id[1]<<std::endl;
//     }

    alGenSources(1, &source_id);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't gen sources\n"<< std::hex <<error<<std::endl;;
        exit(1); // I can't go on like this anymore, darling.
    }
}

SoundInterpolatorImpl::~SoundInterpolatorImpl() {

    si_num_sinterpolators--;



    if(si_num_sinterpolators==0) {
        std::cerr<<"alutExit doesn't work... keeping OpenAL engine around."<<std::endl;

        //alutExit DOES NOT WORK!! So we never close the OpenAL engine. It will clean up when program exits.
//         if(!si_alut_exited) {
//             ALenum error;
//             alutExit();
//             if ((error=alGetError()) != AL_NO_ERROR)
//             {
//                 std::cerr<<"COULD NOT EXIT OPENAL. SORRY DAVE."<< std::hex <<error<<"\n"<<std::endl;
//                 exit(1);
//             }
//
//         }
//         si_alut_initialised=false;
    }
}

/* Unqueue buffers if they need unqueuing */
uint SoundInterpolatorImpl::unqueue_al_buffers()
{
    unsigned int unqueued=0;
    ALenum error;

    ALint num_finished;

    alGetSourcei(source_id, AL_BUFFERS_PROCESSED, &num_finished);

    if (sivvv>4) {
        std::cout<<"Num finished is "<<(num_finished)<<std::endl;
    }

    if(sivvv>5) {

        ALfloat offset;
        alGetSourcef(source_id, AL_SEC_OFFSET, &offset);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
            exit(1);

        }
//             offset=offset/2; //num shorts offset not bytes
        offset=offset*SAMPLE_RATE;

        std::cout<<"Frames processed before unqueue check: "<<offset-this->last_offset<<std::endl;
    }

    while(num_finished>0) {


        ALuint unqueue_buffer = playing_al_buffer_ids.back();//this->last_played_buffer_ind==0?1:0;

        if (sivvv>5) {
            std::cout<<"Before unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples."<<std::endl;


        }
        alSourceUnqueueBuffers(source_id, 1, &unqueue_buffer);
        //NOTE: alSourceUnqueueBuffers overwrites its 3rd arg, probably with the id of the buffer unqueued
        if (sivvv>5) {
            std::cout<<"After unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples."<<std::endl;
// 	    std::cout<<"After unqueue "<<unqueue_buffer<<". of "<<playing_al_buffer_size.back()<<" samples.";
            /*
                        ALfloat offset;
                        alGetSourcef(source_id, AL_BYTE_OFFSET, &offset);

                        if ((error=alGetError()) != AL_NO_ERROR)
                        {
                            std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
                            exit(1);

                        }
                        offset=offset/2; //num shorts offset not bytes
                        std::cout<<"(offset "<<offset<<")"<<std::endl;*/
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cerr<<"*********couldn't unqueue buffer\n"<< std::hex <<error<<std::endl;;
            exit(1);

        }
        else {
            if (sivvv>4) {
                std::cout<<"Unqued buffer #"<<unqueue_buffer<<std::endl;
            }

        }

        unqueued++;
        playing_al_buffer_ids.pop_back();
        playing_al_buffer_queued_time.pop_back();
        all_playing_al_buffer_size-=playing_al_buffer_size.back();
        playing_al_buffer_size.pop_back();

        this->num_queued_or_playing_al--;
        available_al_buffer_ids.push_front(unqueue_buffer);


        alGetSourcei(source_id, AL_BUFFERS_PROCESSED, &num_finished);
        if (sivvv>4) {
            std::cout<<"===================="<<num_finished<<std::endl;
        }
        if(num_finished>0) {
            if(sivvv>1)
                std::cerr<<"BLIPZZ: Usually should only need to unqueue one buffer."<<std::endl;
            
        }



    }
    return unqueued;
}

void SoundInterpolatorImpl::calc_timing() {
    std::chrono::high_resolution_clock::time_point received_at=std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> dur = std::chrono::duration<double>(received_at-last_time_play_request_received);
    double durf=dur.count();
    this->curr_frame_rate=durf;
//     if(this->frames_processed>10)exit(0);

    double avg_frame_weight_multiplier;
    if (this->frames_processed==0)
        avg_frame_weight_multiplier=0.0;
    if (this->frames_processed<10)
        avg_frame_weight_multiplier=0.7;
    else
        avg_frame_weight_multiplier=AVG_FRAME_WEIGHT_MULTIPLIER ;
    
    double thiserr = std::fabs(durf-this->avg_frame_rate);
    this->avg_frame_rate= durf*avg_frame_weight_multiplier + this->avg_frame_rate*(1-avg_frame_weight_multiplier);
    this->avg_frame_rate_err_secs=thiserr*avg_frame_weight_multiplier + this->avg_frame_rate_err_secs*(1-avg_frame_weight_multiplier);
    this->approx_hz=1.0/this->avg_frame_rate;

    std::cout<<"  "<<durf<<"s frame proc,";
    std::cout<<", "<<(durf*SAMPLE_RATE)<<" short samples";
    std::cout<<", "<<this->avg_frame_rate<<"s avg fps";
    std::cout<<", "<<this->avg_frame_rate_err_secs<<"s avg fps err ";
    std::cout <<std::endl;

    last_time_play_request_received=received_at;
}


void SoundInterpolatorImpl::add_feature_to_fill_buffer(SoundFeature &sfeat) {
    rise_time=this->avg_frame_rate;
//     double grain_length=this->grain_length;

    double spread_mult;
    if((2*sfeat.spread_mult-1.0)*rise_time*SAMPLE_RATE>MAX_NUM_SAMPLES) {
        std::cerr<<"WARNING: A sound is provided that is too long for our buffer."<<std::endl;;
        std::cerr<<"WARNING: Shortening sound."<<std::endl;
        spread_mult=(1.0*MAX_NUM_SAMPLES/(SAMPLE_RATE*this->rise_time)+1)/2.0;
    }
    else spread_mult=sfeat.spread_mult;

//     uint num_samples=rise_time*SAMPLE_RATE;


    size_t num_freq=sfeat.frequencies.size();


    double starttime=0.0;
    double midtime=rise_time;
    double endtime=2*rise_time*spread_mult;

//     size_t num_samples_all=SAMPLE_RATE*(endtime-starttime);
    size_t num_samples1=SAMPLE_RATE*(midtime-starttime);
    size_t num_samples2=SAMPLE_RATE*(endtime-midtime);//num_samples_all-num_samples1;

    size_t start_sample1=fill_buffer_start;
    size_t start_sample2=(start_sample1+num_samples1)%MAX_NUM_SAMPLES;

    double energy_mult=sfeat.energy_mult/spread_mult;


    for (size_t freqind = 0; freqind < num_freq; freqind++) { //assuming some kind of order

        wave_properties wp;

        double mid_atten=energy_mult/(num_freq+ATTENUTATION_FUDGE);

        // add in a waveform that fades in the next frequency over one grain
        wp.startfreq=sfeat.frequencies[freqind];
        wp.endfreq=sfeat.frequencies[freqind];
        wp.num_samples=num_samples1;
        //if it is spread then the peak power is lower
        wp.start_attenuation=0.0;
        wp.end_attenuation=mid_atten;
        wp.starttime = starttime;
        wp.endtime=midtime;
// 	  void fill_sample_add(short *samples,size_t start_ind,size_t buffer_size,wave_properties &wp) {
        fill_sample_add(this->fill_buffer.data(),start_sample1,MAX_NUM_SAMPLES,wp);

        // now the tail of the waveform
        wp.num_samples=num_samples2;
        //if it is spread then the peak power is lower
        wp.start_attenuation=mid_atten;
        wp.end_attenuation=0.0;
        wp.starttime = midtime;
        wp.endtime=endtime;
        fill_sample_add(this->fill_buffer.data(),start_sample2,MAX_NUM_SAMPLES,wp);

    }


}
bool SoundInterpolatorImpl::fill_buffers_to_al_buffers() {

    ALenum error;

    ALfloat curr_offset;
    alGetSourcef(source_id, AL_SEC_OFFSET, &curr_offset);
//     alGetSourcef(source_id, AL_SE, &curr_offset);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
        exit(1);

    }
    curr_offset=curr_offset*SAMPLE_RATE;
//     curr_offset=curr_offset/2; //num shorts offset not bytes


    assert(curr_offset>=0);//otherwise I don't know

    int num_to_finish;
//     if(playing_al_buffer_size.size()>0)

    num_to_finish=all_playing_al_buffer_size-curr_offset;
    if(all_playing_al_buffer_size==0)assert(curr_offset==0);
//     else
//       num_to_finish=0;

    assert(num_to_finish>=0);
//     if(num_to_finish<=0)num_to_finish=0; //so you loaded up way too many buffers last time, because perhaps you were panicking. never mind, le
//     assert(this->avg_frame_rate*SAMPLE_RATE>num_to_finish);
    int num_to_buffer_al=this->avg_frame_rate*SAMPLE_RATE-num_to_finish+ADVANCE_SECS*SAMPLE_RATE;

//     if(num_to_buffer==0){
//      this->played_last=false;
//      return false;
//     }

    if(num_to_buffer_al<=0)num_to_buffer_al=ADVANCE_SECS*SAMPLE_RATE; //keep the rythm moving

    //Don't trust the offset reported by OpenAL - check it - it's a multiple of 1024 till the first buffer is finished - and is never accurate
    
    int num_to_buffer_timer=(BELIEVE_CURR_FRAME_RATE_MULT*curr_frame_rate + (1-BELIEVE_CURR_FRAME_RATE_MULT)*this->avg_frame_rate)*SAMPLE_RATE;
    
    int num_to_buffer=BELIEVE_OPENAL_MULT*num_to_buffer_al+(1-BELIEVE_OPENAL_MULT)*num_to_buffer_timer;
//     uint num_to_buffer_check=num_to_buffer/BUFFER_TEST_SIZE_DIVISOR;
//     uint num_to_buffer_main=num_to_buffer-num_to_buffer_check;


    if(num_queued_or_playing_al==1) {
        //fine
    } else if(num_queued_or_playing_al>1) {
        //too slow
//       num_to_buffer=num_to_buffer/2;
//         num_to_buffer-=ADVANCE_SECS*SAMPLE_RATE;
        if(sivvv>1)
            std::cerr<<"BLIPYY: Sound too fast for input."<<std::endl;
        if(num_queued_or_playing_al>=NUM_AL_BUFFERS_TO_KEEP) {
            std::cerr<<"!!!!!!!!!!!!!!!: NOT ENOUGH OPENAL BUFFERS."<<std::endl;
//             exit(1);//for debugging
            return false;
        }
         num_to_buffer-=num_queued_or_playing_al*ADVANCE_SECS/16;

    } else if(num_queued_or_playing_al==0) {
        ///bad error
        std::cerr<<"BLIPXX: Input can't keep up with sound."<<std::endl;
//       num_to_buffer=num_to_buffer*2;
//         num_to_buffer+=ADVANCE_SECS*SAMPLE_RATE;
    }
    else {
        assert(0);
    }

    if (sivvv>4) {
        std::cout<<"Offset is "<<curr_offset<<" and num to process is "<<num_to_finish;//<<std::endl;
        std::cout<<" Num to buffer is "<<num_to_buffer<<std::endl;
    }


    uint num_to_buffer_main=num_to_buffer;

    //this contains most of the sound
    ALuint fill_al_buffer=available_al_buffer_ids.back();
    available_al_buffer_ids.pop_back();
    bool loaded1 = load_to_al_buffer(fill_al_buffer,this->fill_buffer.data(),MAX_NUM_SAMPLES,fill_buffer_start,num_to_buffer_main);
    assert(loaded1);
    zero_samples_circular(this->fill_buffer.data(),fill_buffer_start,MAX_NUM_SAMPLES,num_to_buffer_main);
    fill_buffer_start+=num_to_buffer_main;
    if(fill_buffer_start>=MAX_NUM_SAMPLES)fill_buffer_start-=MAX_NUM_SAMPLES;

    //next time we can see whether this was played or not and catch up accordingly
//     fill_al_buffer=available_al_buffer_ids.back();
//     available_al_buffer_ids.pop_back();
//     bool loaded2 = load_to_al_buffer(fill_al_buffer,this->fill_buffer.data(),MAX_NUM_SAMPLES,fill_buffer_start,num_to_buffer_check);
//     assert(loaded2);
//     zero_samples_circular(this->fill_buffer.data(),fill_buffer_start,MAX_NUM_SAMPLES,num_to_buffer_check);
//     fill_buffer_start+=num_to_buffer_check;
//     if(fill_buffer_start>=MAX_NUM_SAMPLES)fill_buffer_start-=MAX_NUM_SAMPLES;
//
//         if(loaded1||loaded2) {
//             played=true; //set return value

//                 this->last_sfeat=sfeat; //keep track of what was played last time, for interpolation
    this->played_last=true;
    // if for some reason the playing is stopped or if it is first time, start the source playing


    alGetSourcef(source_id, AL_SEC_OFFSET, &last_offset);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"*********couldn't get some info from OpenAL -- I don't like this game. Error:\n"<< std::hex <<error<<std::endl;;
        exit(1);

    }
    last_offset=last_offset*SAMPLE_RATE;
//     last_offset=last_offset/2; //num shorts offset not bytes


    return true; //assert takes care of this ;)


}

void SoundInterpolatorImpl::al_play() {

    ALenum error;

    ALenum source_state;
    alGetSourcei(source_id, AL_SOURCE_STATE, &source_state);

    if (source_state != AL_PLAYING) {
        if (sivvv>4)std::cout<<"--------Warning:: starting/restarting OpenAL source play."<<std::endl;
        alSourcePlay(source_id);
        if (sivvv>4) {
            std::cout<<"Trying to play source: "<<source_id;
        }
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            if (sivvv>4)std::cout<<"*********couldn't play source\n"<< std::hex <<error<<std::endl;;
            exit(1);

        } else {
            if (sivvv>4) {
                std::cout<<"...Playing."<<std::endl;
            }
        }
    }

}


bool SoundInterpolatorImpl::play_feature(SoundFeature sfeat)
{

    if(sivvv>4)std::cout<<"Frame #"<<this->frames_processed<<std::endl;

    calc_timing();

    ALenum error;


    //move sound source location - do this first; the easiest part
    alSourcefv( source_id,AL_POSITION,sfeat.pos.data());
    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cerr<<"couldn't set sound position\n"<< std::hex <<error<<std::endl;;

    }

    uint num_unqueued=this->unqueue_al_buffers();

    if (sivvv>4) {
        std::cout<<"Num unqueued is "<<(num_unqueued)<<std::endl;
        std::cout<<"Num queued now is "<<(this->num_queued_or_playing_al)<<std::endl;
    }

    this->add_feature_to_fill_buffer(sfeat);


    //okay we have filled our sound array, now load it to the buffer and queue it up for playing

    bool played=this->fill_buffers_to_al_buffers();
    if(played)this->al_play();

    //remove from the queue any buffers that have finished playing
    this->frames_processed++;
    return played;
}

void SoundInterpolatorImpl::play_nothing()
{

    if(sivvv>4)std::cout<<"Frame #"<<this->frames_processed<<std::endl;

    calc_timing();

//     ALenum error;

    uint num_unqueued=this->unqueue_al_buffers();

    if (sivvv>4) {
        std::cout<<"Num unqueued is "<<(num_unqueued)<<std::endl;
        std::cout<<"Num queued now is "<<(this->num_queued_or_playing_al)<<std::endl;
    }

    bool played=this->fill_buffers_to_al_buffers();
    if(played)this->al_play();

    //remove from the queue any buffers that have finished playing
    this->frames_processed++;
    return;
}
