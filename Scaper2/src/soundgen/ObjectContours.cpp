#include "soundgen/ObjectContours.h"
#include <assert.h>
#include <bits/c++allocator.h>
#include <sstream>

using namespace contours;

Contour::Contour()
{

}

Contour::Contour(PCLObject::Ptr object)
{
    cloud=object->objectCloud;
    centroid.x=object->centroid[0];
    centroid.y=object->centroid[1];
    centroid.z=object->centroid[2];


    normalized_rcontour= NormalizedRotaryContour();
    normalized_rcontourX = NormalizedRotaryContourX();
    unnormalized_rcontour= UnnormalizedRotaryContour();
    normalized_hcontour= HorizontalContour();
    normalized_vcontour= VerticalContour();
    normalized_hcontour_neg= Negate(normalized_hcontour);
    normalized_vcontour_neg= Negate(normalized_vcontour);

    internal_contour_horizontal_bar = InternalContourHorizontalBar();
//     internal_contour_vertical_bar = InternalContourVerticalBar();


}

std::vector< float > Contour::UnnormalizedRotaryContour()
{

    rotaryContour.clear();
    normalizedRotaryDistances.clear();

    sumOfDistances=0;
    boundary_indices = extractor.BoundaryExtraction(cloud);

    for(size_t j=0 ; j<boundary_indices.indices.size(); j++)
    {
        distanceToTheCentroid=pcl::geometry::distance(cloud->points[boundary_indices.indices[j] ],centroid);
        rotaryContour.push_back(distanceToTheCentroid);
    }
    normalizedRotaryDistances= NormalizeValues1(rotaryContour);

    return normalizedRotaryDistances;
}




std::vector< float > Contour::NormalizedRotaryContour()
{

    rotaryContour.clear();
    normalizedRotaryDistances.clear();

    sumOfDistances=0;
    boundary_indices = extractor.BoundaryExtraction(cloud);

    for(size_t j=0 ; j<boundary_indices.indices.size(); j++)
    {
        distanceToTheCentroid=pcl::geometry::distance(cloud->points[boundary_indices.indices[j] ],centroid);
        rotaryContour.push_back(distanceToTheCentroid);
    }
    normalizedRotaryDistances= NormalizeValues(rotaryContour);

    return normalizedRotaryDistances;
}


std::vector< float > Contour::NormalizedRotaryContourX()
{

    rotaryContour.clear();
    normalizedRotaryDistances.clear();

    sumOfDistances=0;
    boundary_indices = extractor.BoundaryExtraction(cloud);

    for(size_t j=0 ; j<boundary_indices.indices.size(); j++)
    {
        double xval=cloud->points[boundary_indices.indices[j]].x-centroid.x;
//         SystemPoint centroidAxis = centroid;
//         centroidAxis.y=cloud->points[boundary_indices.indices[j]].y;
//         double distanceToTheCentreLine=pcl::geometry::distance(cloud->points[boundary_indices.indices[j] ],centroidAxis);
//         rotaryContour.push_back(distanceToTheCentreLine);
        rotaryContour.push_back(xval);
//         std::cout<<"rotary_x["<<j<<"]="<<rotaryContour[j]<<std::endl;

    }
    normalizedRotaryDistances= NormalizeValues1(rotaryContour);

    return NormalizeValues1(normalizedRotaryDistances);
}

//---------------------------------------------------------------------------------
std::vector <float> Contour::InternalContourHorizontalBar() //TopToBottom
// {
//   int index = 0;
//   float prev_depth = 0;
//   float value_of_first_element = 0;
//   float diff = 0;
//   float global_max =0;
//   depthWave.clear();
//   depthWaveProcessing.clear();
//
//
//
//   for( int i=0; i<(int)cloud->height; i++)
//    {
//      depth.clear();
//      uint indright = FindFinitePointInCloudLeftToRight(i);
//      uint indleft = FindFinitePointInCloudRightToLeft(i);
//      int row_width = indright - indleft;
//      assert(row_width>0 && "ffffffffffffffffffffffffffffffffffffFF");
//
//      for( uint j=indleft%cloud->width; j<indright%cloud->width; j++)
//      {
//       float val;
//       index=i*cloud->width+j;
//       if (pcl::isFinite<SystemPoint>(cloud->points[index]))
//       {
//         val = cloud->points[index].z-centroid.z;
//        }else
//        {
//          val = prev_depth;
//        }
//       depth.push_back(val);
//       prev_depth = val;
//      }
//       int row_num_samples=depth.size();
//       assert(row_num_samples==row_width);
//       if(row_width>1){
//
//
//         float mx = 0.0;
//
//         for( int j=row_width-1; j>=0; j--){
//           if (std::abs(depth[j])>mx)mx=depth[j];
// //           if (std::abs(depth[j])>mx)mx=v;
//           depth.push_back(-depth[j]);
//         }
//
//
//         for(int k=0; k< (int)depth.size(); k++)
//          depthWaveProcessing.push_back(depth[k]);
//       }
//
//   }
//
//   float mx=0;
//   for( int j=0; j<depthWaveProcessing.size(); j++){
//
//
//
// if(std::abs(depthWaveProcessing[j])>mx)mx=std::abs(depthWaveProcessing[j]);
//
//   }
//
//
//   prev_depth = 0;
//   for( int j=0; j<depthWaveProcessing.size(); j++){
//      float new_depth=depthWaveProcessing[j];
//
// // (new_depth-prev_depth)/std::abs(prev_depth-new_depth);
//
//
//     depthWave.push_back(new_depth);
//
//   }
//
//
//
//   normalizedInternalDepth= SquishTo1(depthWave);
//
//   for( int j=0; j<normalizedInternalDepth.size(); j++)
//     std::cout<<","<<normalizedInternalDepth[j];
//   std::cout<<std::endl;
//
//   return normalizedInternalDepth;
// }
{
  bool filedebug=false;
  int numrows=15;
  int index = 0;
  float prev_depth = 0;
  float value_of_first_element = 0;
  float diff = 0;
  float global_max =0;

  if(filedebug)debugfile2.open("fullwave.csv");
  depthWave.clear();

  // go through all the rows and generate a wave for each row

  for( int i=0; i<(int)cloud->height; i+=numrows)
//   for( int i=0; i<(int)cloud->height; i++)
   {


      //calculating window around square consisting of currrent row and next numrows rows
     int colright_window = -1;
     int colleft_window = cloud->width-1;

     for(int r=i;r<i+numrows;r++){
//      for(int r=i-numrows/2;r<i+numrows/2;r++){

          colright_window = std::max(int(FindFinitePointInCloudLeftToRight(r)%cloud->width),colright_window);
	  colleft_window = std::min(int(FindFinitePointInCloudRightToLeft(r)%cloud->width),colleft_window);

     }

     if(colright_window==-1)continue;

     int numcols_segment=colright_window-colleft_window;
     std::vector<float> colvals;
     colvals.clear();
     float mxx=0;
     float mnn=0;
     if(filedebug)debugfile1.open ("modulator.csv");
//      std::cout<<"modulator"<<std::endl;
     //calculate for each column in square the average value of that column (ends up in colvals)
     for(uint c=colleft_window;c<=colright_window;c++){

        float val = 0;

	for(int r=i;r<i+numrows;r++){
	  index=r*cloud->width+c;
	  if (pcl::isFinite<SystemPoint>(cloud->points[index]))
	  {
	    val += (cloud->points[index].z-centroid.z);

	  }
	}

	val/=numrows; //TODO
	if(filedebug)debugfile1<<","<<val;
// 	std::cout<<","<<val;
        mxx=std::max(std::abs(val),mxx);
        mnn=std::min(std::abs(val),mnn);
	colvals.push_back(val);

//         std::cout<<","<<val;
     }

     //calculating number of values in square inside the contour
//      std::cout<<std::endl;
     if(filedebug)debugfile1.close();
     float num_vals_sect=0;
     for(int r=i;r<i+numrows;r++)
     {
          int indright_row = FindFinitePointInCloudLeftToRight(r);
          int indleft_row = FindFinitePointInCloudRightToLeft(r);
          num_vals_sect+=indright_row-indleft_row;

     }
     float vals_per_col=num_vals_sect/colvals.size();

     for(uint modno=0;modno<colvals.size();modno++)colvals[modno]=(colvals[modno]-mnn)/(mxx-mnn);

     uint valno=0;

     //now go through each row in that square making a wave from the lot of them
     //except that the number of values here (num_vals_sect) is progressively
     //modified by the colvals above (so there are less of those colvals than there are
     //pixels here to be converted) so that the colvals acts as a modulator
     for(int r=i;r<i+numrows;r++)
     {
       depth.clear();

       //left and right values for this row
      int indright_row = FindFinitePointInCloudLeftToRight(r);
      int indleft_row = FindFinitePointInCloudRightToLeft(r);
      for( uint j=indleft_row%cloud->width; j<indright_row%cloud->width; j++)
      {


          float val;
          index=r*cloud->width+j;
          if (pcl::isFinite<SystemPoint>(cloud->points[index]))
          {
            val = (cloud->points[index].z);
//             val = (cloud->points[index].z-centroid.z);
          }else
          {
            val = prev_depth;
          }
          uint colno=valno/vals_per_col;
          if(colno==colvals.size())colno--; //will only happen due to slight numerical issues
          float modulator=colvals[colno]+0.5;//TODO
          depth.push_back(val*modulator);
          prev_depth = val;

  // 	  float depth_ratio= 0.0;
  // 	  if (prev_depth != 0)
  // 	    if (new_depth < prev_depth)
  // 	      depth_ratio =  new_depth / prev_depth;
  // 	    else
  // 	      depth_ratio =  prev_depth / new_depth;


  // 	  float noise_threshold = 0.6;
  // 	  if(std::abs(depth_ratio) < noise_threshold)
  // 	      new_depth = prev_depth + noise_threshold * (new_depth-prev_depth)/std::abs(prev_depth-new_depth);
          valno++;
        }
        int row_width=depth.size();
//         assert(row_num_samples==row_width);

        if(row_width>0){
        //if(row_width>1){
          diff=depth[row_width-1]-depth[0];

          value_of_first_element = depth[0];
//           std::cout<<"diff"<<diff<<std::endl;
//           std::cout<<"value_of_first_element"<<value_of_first_element<<std::endl;
//           std::cout<<"value_of_last_element"<<depth[row_width-1]<<std::endl;
//           std::cout<<"row_width"<<row_width<<std::endl;


//           std::stringstream ss;
//           for( int j=0; j<row_width; j++){
//             double one =  depth[j];
//             double proportion_through = j*1.0/(row_width-1);
//             depth[j]= (depth[j]-value_of_first_element);
//             depth[j]= depth[j] - diff * proportion_through;  //mapping start and end values to zero
//             double two =  depth[j];
//             ss << "("<<one<<"-->"<<two<<")"<<std::endl;
//           }
//           std::cout<<ss.str()<<std::endl;
//           std::cout<<"   "<<std::endl;

          float mx = 0.0;

          for( int j=row_width-1; j>=0; j--){
//           for( int j=row_width-2; j>0; j--){
            if (std::abs(depth[j]>mx))mx=depth[j];
            depth.push_back(-depth[j]);
          }


          for(int k=0; k< (int)depth.size(); k++)
          {
            depthWave.push_back(depth[k]);
            if(filedebug)debugfile2<<depth[k]<<std::endl;
          }
  // 	  depthWave.push_back(depth[k]*row_width/mx);
        }

      }
   }

//    for( int j=0; j<depthWave.size(); j++)
//      std::cout<<","<<depthWave[j];
//    std::cout<<std::endl;
   normalizedInternalDepth= SquishTo1(depthWave);

//   	for( int j=0; j<normalizedInternalDepth.size(); j++)
// 	  std::cout<<","<<normalizedInternalDepth[j];
// 	std::cout<<std::endl;
  if(filedebug)debugfile2.close();
  return normalizedInternalDepth;
}

/*
//---------------------------------------------------------------------------------
std::vector <float> Contour::InternalContourHorizontalBar() //TopToBottom
// {
//   int index = 0;
//   float prev_depth = 0;
//   float value_of_first_element = 0;
//   float diff = 0;
//   float global_max =0;
//   depthWave.clear();
//   depthWaveProcessing.clear();
//
//
//
//   for( int i=0; i<(int)cloud->height; i++)
//    {
//      depth.clear();
//      uint indright = FindFinitePointInCloudLeftToRight(i);
//      uint indleft = FindFinitePointInCloudRightToLeft(i);
//      int row_width = indright - indleft;
//      assert(row_width>0 && "ffffffffffffffffffffffffffffffffffffFF");
//
//      for( uint j=indleft%cloud->width; j<indright%cloud->width; j++)
//      {
//       float val;
//       index=i*cloud->width+j;
//       if (pcl::isFinite<SystemPoint>(cloud->points[index]))
//       {
//         val = cloud->points[index].z-centroid.z;
//        }else
//        {
//          val = prev_depth;
//        }
//       depth.push_back(val);
//       prev_depth = val;
//      }
//       int row_num_samples=depth.size();
//       assert(row_num_samples==row_width);
//       if(row_width>1){
//
//
//         float mx = 0.0;
//
//         for( int j=row_width-1; j>=0; j--){
//           if (std::abs(depth[j])>mx)mx=depth[j];
// //           if (std::abs(depth[j])>mx)mx=v;
//           depth.push_back(-depth[j]);
//         }
//
//
//         for(int k=0; k< (int)depth.size(); k++)
//          depthWaveProcessing.push_back(depth[k]);
//       }
//
//   }
//
//   float mx=0;
//   for( int j=0; j<depthWaveProcessing.size(); j++){
//
//
//
// if(std::abs(depthWaveProcessing[j])>mx)mx=std::abs(depthWaveProcessing[j]);
//
//   }
//
//
//   prev_depth = 0;
//   for( int j=0; j<depthWaveProcessing.size(); j++){
//      float new_depth=depthWaveProcessing[j];
//
// // (new_depth-prev_depth)/std::abs(prev_depth-new_depth);
//
//
//     depthWave.push_back(new_depth);
//
//   }
//
//
//
//   normalizedInternalDepth= SquishTo1(depthWave);
//
//   for( int j=0; j<normalizedInternalDepth.size(); j++)
//     std::cout<<","<<normalizedInternalDepth[j];
//   std::cout<<std::endl;
//
//   return normalizedInternalDepth;
// }
{
  int index = 0;
  float prev_depth = 0;
  float value_of_first_element = 0;
  float diff = 0;
  float global_max =0;

  depthWave.clear();
  for( int i=0; i<(int)cloud->height; i++)
   {
     depth.clear();
     uint indright = FindFinitePointInCloudLeftToRight(i);
     uint indleft = FindFinitePointInCloudRightToLeft(i);
     int row_width = indright - indleft;

     for( uint j=indleft%cloud->width; j<indright%cloud->width; j++)
     {
	float val;
	index=i*cloud->width+j;
	if (pcl::isFinite<SystemPoint>(cloud->points[index]))
	{
	  val = (cloud->points[index].z-centroid.z);
	}else
	{
	  val = prev_depth;
	}
	depth.push_back(val);
	prev_depth = val;

// 	  float depth_ratio= 0.0;
// 	  if (prev_depth != 0)
// 	    if (new_depth < prev_depth)
// 	      depth_ratio =  new_depth / prev_depth;
// 	    else
// 	      depth_ratio =  prev_depth / new_depth;


// 	  float noise_threshold = 0.6;
// 	  if(std::abs(depth_ratio) < noise_threshold)
// 	      new_depth = prev_depth + noise_threshold * (new_depth-prev_depth)/std::abs(prev_depth-new_depth);

      }
      int row_num_samples=depth.size();
      assert(row_num_samples==row_width);
      if(row_width>1){
	diff=depth[row_width-1]-depth[0];

	value_of_first_element = depth[0];
	std::cout<<"diff"<<diff<<std::endl;
	std::cout<<"value_of_first_element"<<value_of_first_element<<std::endl;
	std::cout<<"value_of_last_element"<<depth[row_width-1]<<std::endl;
	std::cout<<"row_width"<<row_width<<std::endl;


	std::stringstream ss;
	for( int j=0; j<row_width; j++){
	  double one =  depth[j];
	  double proportion_through = j*1.0/(row_width-1);
 	  depth[j]= (depth[j]-value_of_first_element);
 	  depth[j]= depth[j] - diff * proportion_through;  //mapping start and end values to zero
	  double two =  depth[j];
	  ss << "("<<one<<"-->"<<two<<")"<<std::endl;
	}
	std::cout<<ss.str()<<std::endl;
	std::cout<<"   "<<std::endl;

	float mx = 0.0;

	for( int j=row_width-2; j>0; j--){
	  if (std::abs(depth[j]>mx))mx=depth[j];
	  depth.push_back(-depth[j]);
	}


	for(int k=0; k< (int)depth.size(); k++)
	  depthWave.push_back(depth[k]);
// 	  depthWave.push_back(depth[k]*row_width/mx);
      }

}

	for( int j=0; j<depthWave.size(); j++)
	  std::cout<<","<<depthWave[j];
	std::cout<<std::endl;
  normalizedInternalDepth= SquishTo1(depthWave);

//   	for( int j=0; j<normalizedInternalDepth.size(); j++)
// 	  std::cout<<","<<normalizedInternalDepth[j];
// 	std::cout<<std::endl;

  return normalizedInternalDepth;
}*/

//---------------------------------------------------------------------------------

std::vector <float> Contour::InternalContourVerticalBar() //LeftToRight
{
  int index = 0;
  int prev_depth = 0;
  float value_of_first_element = 0;
  float diff = 0;
  float global_max =0;

  depthWave.clear();
  for( int i=0; i<(int)cloud->width; i++)
   {
     depth.clear();
     for( int j=0; j<(int)cloud->height; j++)
     {
	index= j*cloud->width+i;
	if (pcl::isFinite<SystemPoint>(cloud->points[index]))
	{
	  depth.push_back(cloud->points[index].z);
	  prev_depth = cloud->points[index].z;
	 }else
	  depth.push_back(prev_depth);
      }

      diff=depth[cloud->height-1]-depth[0];
      value_of_first_element = depth[0];
      for( int j=0; j<(int)cloud->height; j++)
	depth[j]= (depth[j]-value_of_first_element) -(j*diff/(cloud->height-1));  //mapping start and end values to zero

      for( int j=0; j<(int)cloud->height; j++)
	depth.push_back(-depth[j]);


      for(int k=0; k< (int)depth.size(); k++)
	depthWave.push_back(depth[k]);

}

  normalizedInternalDepth= SquishTo1(depthWave);
  return normalizedInternalDepth;
}

//----------------------------------------------------------------------------------
std::vector< float > Contour::VerticalContour()
{

    vertical_distance.clear();
    normalizedVerticalDistances.clear();

    for( int i=0; i<(int)cloud->width; i++)
        if(FindFinitePointInCloudToptoBottom(i)>=0) {
            int index1= FindFinitePointInCloudToptoBottom(i);
            int index2= FindFinitePointInCloudBottomToTop(i);
            float distance = pcl::geometry::distance(cloud->points[index1],cloud->points[index2]);
            vertical_distance.push_back(distance);

        }
        normalizedVerticalDistances2=CentreAtZero(vertical_distance);
    normalizedVerticalDistances=SquishTo1(normalizedVerticalDistances2);
//   normalizedVerticalDistances= NormalizeValues(vertical_distance);

    return normalizedVerticalDistances;
}

int Contour::FindFinitePointInCloudToptoBottom(int index) {
    for( int i=0; i< (int) cloud->height; i++)
        if (pcl::isFinite<SystemPoint>(cloud->points[i*cloud->width+index]))
            return i*cloud->width+index;
    return -1;
}

int Contour::FindFinitePointInCloudBottomToTop(int index) {
    for( int i=(int)cloud->height-1; i>=0; i--)
        if (pcl::isFinite<SystemPoint>(cloud->points[i*cloud->width+index]))
            return i*cloud->width+index;
    return -1;
}








std::vector<float> Contour::HorizontalContour()
{
    horizontal_distance.clear();
    bool started=false;
    bool finished=false;
//   normalizedHorizontalDistances.clear();

    for( int i=0; i<(int)cloud->height; i++)
        if(FindFinitePointInCloudLeftToRight(i)>=0) {
//             assert(!finished);
            int index1= FindFinitePointInCloudLeftToRight(i);
            int index2= FindFinitePointInCloudRightToLeft(i);
            assert(index2>=0);
//     std::cout<<"index1 and index2 are: "<< index1<<" and "<<index2<< std::endl;
            float distance = pcl::geometry::distance(cloud->points[index1],cloud->points[index2]);
//      std::cout<<"distance["<<i<<"] is: "<< distance<< std::endl;
            horizontal_distance.push_back(distance);
            started=true;
        }else{
	    horizontal_distance.push_back(horizontal_distance[horizontal_distance.size()-1]);
//                 if(started)finished=true;
        }
//     normalizedHorizontalDistances = SquishTo1(CentreAtZero(horizontal_distance));
    normalizedHorizontalDistances2 = CentreAtZero(horizontal_distance);
    normalizedHorizontalDistances = SquishTo1(normalizedHorizontalDistances2);
//   for(int i=0;i<normalizedHorizontalDistances.size();i++)
//     std::cout<<"normalised["<<i<<"] is: "<<normalizedHorizontalDistances[i]<<std::endl;
    return normalizedHorizontalDistances;
}

int Contour::FindFinitePointInCloudRightToLeft(int index) {
    for( int i=0; i< (int) cloud->width; i++)
        if (pcl::isFinite<SystemPoint>(cloud->points[index*cloud->width+i]))
            return index*cloud->width+i;
    return -1;
}

int Contour::FindFinitePointInCloudLeftToRight(int index) {
    for( int i=cloud->width-1; i>=0 ; i--)
        if (pcl::isFinite<SystemPoint>(cloud->points[index*cloud->width+i]))
            return index*cloud->width+i;
    return -1;
}


std::vector< float > Contour::CentreAtZero(std::vector< float > vals) {

//     normalizedContour.clear();
    normalizedContour.resize(vals.size(),0);

    double sumOfDistances=0;
    for (size_t i=0; i<vals.size(); i++)
    {
        sumOfDistances += vals[i];
    }

    double averageDistance=sumOfDistances/vals.size();

    for (size_t k=0; k< vals.size(); k++) {
        normalizedContour[k]=vals[k]-averageDistance;
    }
// for (size_t i=0; i<vals.size(); i++)
//  {
//   if(verbosep)std::cout<<"normalizedContour["<<i<<"]: "<<normalizedContour[i]<<std::endl;
//  }
    return normalizedContour;

}


std::vector< float > Contour::SquishTo1(std::vector< float > vals) {

    if(vals.size()==0)return vals;

    double max_distance=0;
    double min_distance=0;
    for (size_t i=0; i<vals.size(); i++)
    {
        if(vals[i]>max_distance)max_distance=vals[i];
        if(vals[i]<min_distance)min_distance=vals[i];
    }
    double max_worse = std::max(std::abs(max_distance),std::abs(min_distance));

    if(max_worse>=0.0f) {
        normalizedContour.resize(vals.size(),0);

        for(size_t j=0; j< vals.size(); j++)
        {
            double waveNewValue= vals[j]/max_worse;
            normalizedContour[j]=waveNewValue;
        }

      return normalizedContour;

    } else return vals;


}



std::vector< float > Contour::Negate(std::vector< float > vals) {

    normalizedContour.resize(vals.size(),0);

    for (size_t k=0; k< vals.size(); k++) {
        normalizedContour[k]=-vals[k];
    }

    return normalizedContour;

}





std::vector< float > Contour::NormalizeValues1(std::vector< float > contour,bool verbosep)
{
    for (size_t i=0; i<contour.size(); i++)
    {
        if(verbosep)std::cout<<"contour["<<i<<"]: "<<contour[i]<<std::endl;
    }

    normalizedContour.clear();
    double max_distance=0;
    double min_distance=0;
    for (size_t i=0; i<contour.size(); i++)
    {

        if(std::abs(contour[i])>max_distance)max_distance=std::abs(contour[i]);
        if(std::abs(contour[i])<min_distance)min_distance=std::abs(contour[i]);
    }
    for(size_t j=0; j< contour.size(); j++)
    {
        double waveNewValue= contour[j]/max_distance;
        normalizedContour.push_back(waveNewValue );
    }


    sumOfDistances=0;
    for (size_t i=0; i<contour.size(); i++)
    {
        sumOfDistances += normalizedContour[i];
    }
    averageDistance=sumOfDistances/contour.size();
    for (size_t k=0; k< contour.size(); k++) {
        normalizedContour[k]-=averageDistance;
    }
    for (size_t i=0; i<contour.size(); i++)
    {
        if(verbosep)std::cout<<"normalizedContour["<<i<<"]: "<<normalizedContour[i]<<std::endl;
    }
    return normalizedContour;

}




std::vector< float > Contour::NormalizeValues(std::vector< float > contour)
{
    normalizedContour.clear();
    sumOfDistances=0;
    for (size_t i=0; i<contour.size(); i++)
    {
        sumOfDistances += contour[i];
    }
    averageDistance=sumOfDistances/contour.size();
    for (size_t k=0; k< contour.size(); k++) {
        contour[k]-=averageDistance;
    }

    //Normalizing wave values between -1 and 1 (for first contour approach)
    auto it_max = std::max_element(std::begin(contour), std::end(contour)); // c++11
    auto it_min = std::min_element(std::begin(contour), std::end(contour)); // c++11
//     std::cout<<"######"<<   *(it_min ) << std::endl;
//     std::cout<<"######"<<  *(it_max) << std::endl;

    float waveMaxValue= *it_max;
    float waveMinValue= *it_min;
    float waveNewValue;
    for(size_t j=0; j< contour.size(); j++)
    {
        waveNewValue= ( ((contour[j]-waveMinValue)*2) / (waveMaxValue - waveMinValue))-1;
        normalizedContour.push_back(waveNewValue );
    }

    return normalizedContour;
}
