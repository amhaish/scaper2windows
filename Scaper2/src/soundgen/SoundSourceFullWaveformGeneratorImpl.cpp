
#include <soundgen/SoundSourceFullWaveformGenerator.h>
#include <soundgen/SoundSourceFullWaveformGeneratorImpl.h>
#include <iostream> 
#include <boost/concept_check.hpp>

#include <Consts.h>
#define ATTENUTATION_FUDGE 3

// #define MAX_RISE_SAMPLES 200

 #define MAX_AMPLITUDE_SHORT 32760
 
// #define MAX_AMPLITUDE_SHORT 8000

int SoundSourceFullWaveformGeneratorImpl::verbosity=1;//VERBOSITY LEVEL

double rand01() {
    return ((double)rand())/RAND_MAX;
}
SoundSourceFullWaveformGenerator::SoundSourceFullWaveformGenerator() {
    impl.reset(new SoundSourceFullWaveformGeneratorImpl());
}
void SoundSourceFullWaveformGenerator::generate_sound(SoundFeature &sfeat,std::vector<short> &output) {
    impl->generate_sound(sfeat,output);
}

SoundFeature SoundSourceFullWaveformGenerator::get_last_played_sound_feature() {
  return impl->get_last_played_sound_feature();
}

SoundSourceFullWaveformGenerator::~SoundSourceFullWaveformGenerator() { }

/* Return just one sample of a wave frequency freq at time t*/
double SoundSourceFullWaveformGeneratorImpl::amplitude_sine(double freq,double t) {
    
    double toret=MAX_AMPLITUDE_SHORT * sin( (2.f*float(M_PI)*freq) * t );
    if(verbosity>8) {
        std::cout<<"amplitude_sine("<<freq<<","<<t<<")-->"<<toret<<std::endl;
    }
    return toret;
}

/* Add wave into array (don't clear array) */
void SoundSourceFullWaveformGeneratorImpl::fill_sample_add(std::vector<short> &output,wave_properties &wp,size_t start_ind) {

    size_t num_steps=std::round((wp.endtime-wp.starttime)*SAMPLE_RATE);

    
      if(num_steps!=wp.num_samples)
	  assert(num_steps==wp.num_samples);
    assert(num_steps<=output.size());

    double freq_step=(wp.endfreq-wp.startfreq)/wp.num_samples;
    double freq=wp.startfreq;
    double time_step=(wp.endtime-wp.starttime)/wp.num_samples;
    double t=wp.starttime;
    double attenuation_step=(wp.end_attenuation-wp.start_attenuation)/wp.num_samples;
    double attenuation=wp.start_attenuation;

    if(verbosity>5) {//VERBOSITY 6+
      std::cout<<"fill_sample_add:"<<std::endl;
        std::cout<<"    wp.startfreq="<<wp.startfreq<<" wp.endfreq="<<wp.endfreq<<" wp.start_attenuation="<<wp.start_attenuation;
        std::cout<<" wp.end_attenuation="<<wp.end_attenuation<<" wp.starttime="<<wp.starttime<<" wp.endtime="<<wp.endtime;
        std::cout<<" wp.num_samples="<<wp.num_samples<<  std::endl;
    }

    double maxxy=0;
    for(size_t i=0; i<wp.num_samples; ++i) {

        size_t arr_ind=(start_ind+i);

	double value=attenuation*amplitude_sine(freq,t);
	if(value>maxxy)maxxy=value;
        output[arr_ind] += value;
        freq+=freq_step;
        t+=time_step;
        attenuation+=attenuation_step;
    }

    if(verbosity>5)
            std::cout<<"fill_sample_add: max value "<<maxxy<<std::endl;
//     if(verbosity>6) {//VERBOSITY 6+
//       std::cout<<"LOAD TO ARRAY - CURRENT ARRAY:"<<std::endl<<csb.str(start_ind,start_ind+20)<<std::endl;
//     }

  
}


SoundSourceFullWaveformGeneratorImpl::SoundSourceFullWaveformGeneratorImpl(){}

SoundSourceFullWaveformGeneratorImpl::~SoundSourceFullWaveformGeneratorImpl() {}

SoundFeature SoundSourceFullWaveformGeneratorImpl::get_last_played_sound_feature() {
  return last_played;
}

void SoundSourceFullWaveformGeneratorImpl::generate_sound(SoundFeature sfeat,std::vector<short> &output) {
  
//      if(verbosity>4)std::cout<<"SoundSourceFullWaveformGeneratorImpl::Frame #"<<rate_tracker.get_num_updates()<<std::endl;

//     rate_tracker.update();
  
//     double rise_time=rate_tracker.get_tracked_period();
    rate_tracker.update();

    double rise_time=rate_tracker.get_tracked_period();
    rise_time+=0.01; //now divorced from interpolator, need to ensure we get enough signal. Backwards compatibility.
    double spread_mult=sfeat.spread_mult; //ignoreed

    size_t num_freq=sfeat.frequencies.size();

//     double starttime=0.0;
    double attack_duration;
    double hold_duration;
    double release_duration;

    if(sfeat.attack_duration>=0)
      attack_duration=sfeat.attack_duration;
    else{
//       attack_duration=std::max((1.0*MAX_RISE_SAMPLES)/SAMPLE_RATE,spread_mult/2);
      attack_duration=rise_time;
      if(attack_duration<0)attack_duration=0;
    }

    if(sfeat.release_duration>=0)
      release_duration=sfeat.release_duration;
    else{
      release_duration=rise_time;//*spread_mult;;
      if(release_duration<0)release_duration=0;
    }

    if(sfeat.hold_duration>=0)
      hold_duration=sfeat.hold_duration;
    else{
      hold_duration=rise_time*2-release_duration-attack_duration;
      if(hold_duration<0)hold_duration=0;
    }

    size_t num_samples_attack=SAMPLE_RATE*attack_duration;
    size_t num_samples_hold=SAMPLE_RATE*hold_duration;
    size_t num_samples_release=SAMPLE_RATE*release_duration;

    //let's align our durations with the num samples - to make calculations easier
    attack_duration=((double)num_samples_attack)/SAMPLE_RATE;
    hold_duration=((double)num_samples_hold)/SAMPLE_RATE;
    release_duration=((double)num_samples_release)/SAMPLE_RATE;

    double attack_starttime=0;
    double hold_starttime=attack_duration;
    double release_starttime=hold_starttime+hold_duration;

    size_t sample_start_attack=0;
    size_t sample_start_hold=num_samples_attack;
    size_t sample_start_release=num_samples_attack+num_samples_hold;

    size_t sample_finish_attack=num_samples_attack;
    size_t sample_finish_hold=num_samples_attack+num_samples_hold;
    size_t sample_finish_release=num_samples_attack+num_samples_hold+num_samples_release;

    output.resize(sample_finish_release,0);
    memset(&output[0],0,output.size()*(sizeof output[0]));

    double energy_mult=sfeat.energy_mult;  
// was not doing any good when spread is less than 1.0     double energy_mult=sfeat.energy_mult/spread_mult;

    for (size_t freqind = 0; freqind < num_freq; freqind++) { //assuming some kind of order

	double rand_st=rand01();//interesting things can be done by making this less random. E.g. using same rand_st each time.
      
	attack_starttime+=rand_st;
	hold_starttime+=rand_st;
	release_starttime+=rand_st;
          
// 	num_samples1=SAMPLE_RATE*(midtime-starttime);
// 	num_samples2=SAMPLE_RATE*(endtime-midtime);
	
// 	while(num_samples1+num_samples2>output.size())output.push_back(0);
      
        double multipler=1.0;
	if(sfeat.multipliers.size()>freqind)multipler=sfeat.multipliers[freqind];
        wave_properties wp;

        double mid_atten=energy_mult/(num_freq+ATTENUTATION_FUDGE);

        // Attack
        wp.startfreq=sfeat.frequencies[freqind];
        wp.endfreq=sfeat.frequencies[freqind];
        wp.num_samples=num_samples_attack;
        wp.start_attenuation=0.0;
        //if it is spread then the peak power is lower
        wp.end_attenuation=mid_atten*multipler;
        wp.starttime = attack_starttime;
        wp.endtime=attack_starttime+attack_duration;
	fill_sample_add(output,wp,0);

        // Hold
        wp.num_samples=num_samples_hold;
        wp.start_attenuation=mid_atten*multipler;
        wp.end_attenuation=mid_atten*multipler;
        wp.starttime = hold_starttime;
        wp.endtime=hold_starttime+hold_duration;
        fill_sample_add(output,wp,sample_start_hold);

        // Release
        wp.num_samples=num_samples_release;
        wp.start_attenuation=mid_atten*multipler;
        wp.end_attenuation=0.0;
        wp.starttime = release_starttime;
        wp.endtime=release_starttime+release_duration;
        fill_sample_add(output,wp,sample_start_release);

    }
    last_played=sfeat;

}


