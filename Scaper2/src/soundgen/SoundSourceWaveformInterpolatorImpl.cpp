
#include <soundgen/SoundSourceWaveformInterpolator.h>
#include <soundgen/SoundSourceWaveformInterpolatorImpl2.h>
#include <iostream>
#include <boost/concept_check.hpp>

#include <Consts.h>
#include <common/Tools.h>
// #define ATTENUTATION_FUDGE 3
// #define END_MUTE_BUFFER_SAMPLES 100
// #define LEAD_IN_NUM_SAMPLES 100
#define AVG_GRAIN_FADE_SAMPLES 400

#define MAX_LENGTH_CHANGE 2000

// #define BREATH_SAMPLES 100

int SoundSourceWaveformInterpolatorImpl::verbosity=2;//VERBOSITY LEVEL


SoundSourceWaveformInterpolator::SoundSourceWaveformInterpolator() {
    impl.reset(new SoundSourceWaveformInterpolatorImpl());
}
void SoundSourceWaveformInterpolator::interpolate_sound(const std::vector<short> &full_sound,CircularBuffer<short> &csb,InterpolationApproach approach) {
    impl->interpolate_sound(full_sound,csb,approach);
}


SoundSourceWaveformInterpolator::~SoundSourceWaveformInterpolator() { }

SoundSourceWaveformInterpolatorImpl::SoundSourceWaveformInterpolatorImpl():prev_length(0),num_frames_played_sound(0) {
//   SoundSourceWaveformInterpolatorImpl::SoundSourceWaveformInterpolatorImpl(double approx_hz):played_last(false),num_frames_played_sound(0) {
//   SoundSourceWaveformInterpolatorImpl::SoundSourceWaveformInterpolatorImpl(double approx_hz):rate_tracker("SoundSourceWaveformInterpolatorImpl.RateTracker",approx_hz),played_last(false) {

}

SoundSourceWaveformInterpolatorImpl::~SoundSourceWaveformInterpolatorImpl() {

}

void SoundSourceWaveformInterpolatorImpl::interpolate_sound(const std::vector<short> &full_sound_p,CircularBuffer<short> &csb,SoundSourceWaveformInterpolator::InterpolationApproach approach) {

    auto tostr = [](const std::vector<short> &arr,size_t start_ind=0,int end_ind=-1) {
        std::stringstream ss;
        if(end_ind<0)end_ind=arr.size();
        ss<<"[vector<short>["<<start_ind<<"-"<<end_ind-1<<"](size="<<arr.size()<<"):";
        for (size_t i=start_ind; i<(size_t)end_ind; i++) {
            if(i!=start_ind)ss<<",";
            ss<<arr[i];
        }
        ss<<"]";
        return ss.str();
    };

    auto randAround0 = []() {

        return ((double)rand())/RAND_MAX * 2 - 1.0;
    };

//     //Tools::check_debug_silliness(csb);

    if(approach==SoundSourceWaveformInterpolator::RestartFadeStart || approach==SoundSourceWaveformInterpolator::RestartFadeStartEnd) {

        if(verbosity>4)std::cout<<"SoundSourceWaveformInterpolatorImpl::Frame #"<<rate_tracker.get_num_updates()<<std::endl;
        rate_tracker.update();
        double rise_time=rate_tracker.get_tracked_period();
        size_t fade_in_end = std::min<size_t>(rise_time*SAMPLE_RATE,full_sound_p.size());
//         double fade_out_end = full_sound_p.size();
        size_t fade_out_end;
        size_t fade_out_start;
        if(approach==SoundSourceWaveformInterpolator::RestartFadeStartEnd) {
//             fade_out_start = std::max<size_t>(full_sound_p.size()-grain_fade_samples ,0);
          fade_out_start = fade_in_end;
          fade_out_end=std::min<size_t>(rise_time*2*SAMPLE_RATE,full_sound_p.size());
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(RestartFadeStartEnd). fade_in_end="<<fade_in_end<<" fade_out_start="<<fade_out_start <<" fade_out_end="<<fade_out_end <<" full_sound_p.size()="<<full_sound_p.size()<<std::endl;

        } else {
            fade_out_start = full_sound_p.size();
            fade_out_end=full_sound_p.size();
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(RestartFadeStart). fade_in_end="<<fade_in_end<<" fade_out_start="<<fade_out_start <<" fade_out_end="<<fade_out_end <<" full_sound_p.size()="<<full_sound_p.size()<<std::endl;
        }

        for(size_t i=0; i<csb.size(); i++) {
            double old_prop;
            if(i<fade_in_end)old_prop = 1 - ((double)i)/fade_in_end;
            else old_prop=0;
            double remaining = 1-old_prop;
            double zero_prop;
            if(i>=fade_out_start) {
                if(i<fade_out_end)
                    zero_prop = remaining * (i-fade_out_start)/(fade_out_end-fade_out_start);
                else
                    zero_prop = remaining;
            } else {
                zero_prop=0;
            }
            remaining=1-old_prop-zero_prop;
            double new_sample;
            if(i<full_sound_p.size())new_sample=full_sound_p[i];
            else new_sample=0;
            csb[i] = csb[i] * old_prop + new_sample * remaining;
            //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
        }

        if(verbosity>5)std::cout<<"Here's the first 150 waiting on CSB:"<<std::endl;
        if(verbosity>5)std::cout<<csb.str(0,150)<<std::endl;

        if(verbosity>5)std::cout<<"Here's the first 150 in target waveform:"<<std::endl;
        if(verbosity>5)std::cout<<tostr(full_sound_p,0,150)<<std::endl;

        if(verbosity>5)std::cout<<std::endl ;

    }
    else if(approach==SoundSourceWaveformInterpolator::GranulateNewIn) {

        //could parametrise this lovely randomness
        size_t grain_fade_samples = AVG_GRAIN_FADE_SAMPLES  + AVG_GRAIN_FADE_SAMPLES * randAround0()/6;

        int num_consumed = csb.get_num_consumed();

        double prop_consumed;
        if(prev_length==0)prop_consumed = 1.0;
        else prop_consumed = ((double) num_consumed) / prev_length ;

        if(prop_consumed >= 1.0) {

            //Either we're starting afresh or we already consumed the last sound.
            //So fill the CSB with whatever was sent to us.
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(GranulateNewIn). Starting new sound."<<std::endl;
            csb.zero_all();
            for(size_t i=0; i<full_sound_p.size(); i++){
              csb[i]=full_sound_p[i]; //load new sound
              //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
            }
            prev_length=full_sound_p.size();//keep track of how long it is
            csb.reset_num_consumed();


        } else {

            //fade the sound according to how far through it is.

            size_t new_start = (size_t) ( full_sound_p.size() * prop_consumed );
            size_t remaining = full_sound_p.size() - new_start;
            size_t fade_length = std::min<size_t>(grain_fade_samples,remaining);

            for(size_t i=0; i<csb.size(); i++) {
                if(i<fade_length) {
                    //fade in from old sound
                    double new_prop = ((double)i)/fade_length;
                    csb[i]=new_prop * full_sound_p[new_start+i] + (1-new_prop)*csb[i];
                    //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
                } else if(i<remaining) {
                    //play new sound
                    csb[i]=full_sound_p[new_start+i];
                    //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
                } else {
                    //the rest is zero
                    csb[i]=0;
                    //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
                }

            }

        }


    }
    else if(approach==SoundSourceWaveformInterpolator::ChangeWaveAndSizeLayered) {

        //TOTALLY IS TESTED NOW std::cerr<<"WARNING: UNTESTED CODE. Experimental. Hope to get back to this soon."<<std::endl;

        //could parametrise this lovely randomness
        size_t grain_fade_samples = AVG_GRAIN_FADE_SAMPLES  + AVG_GRAIN_FADE_SAMPLES * randAround0()/6;

        if(verbosity>2)if(num_frames_played_sound==0)std::cout<<std::endl<<std::endl<<std::endl;
        std::vector<short> full_sound=full_sound_p;

        int max_length_buffer = csb.size();
        int num_consumed = csb.get_num_consumed();
        int target_length = full_sound.size();

        double old_prop = (1.0*num_frames_played_sound) / (1.0+num_frames_played_sound);
        double new_prop = (1.0) / (1.0+num_frames_played_sound);

        int new_size = old_prop * prev_length + new_prop * target_length;

        if(std::abs(new_size-prev_length)>MAX_LENGTH_CHANGE && num_frames_played_sound>0) {
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):Opted not to move size to "<<new_size<<std::endl;
            new_size = prev_length + MAX_LENGTH_CHANGE*Tools::sgn(new_size-prev_length);
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):Changed to "<<new_size<<std::endl;
        }

        //save us from later short fading - can sound funny
        if((size_t)std::abs(new_size-target_length)<50)new_size=target_length;
        if((size_t)std::abs(new_size-prev_length)<50)new_size=prev_length;

        if(verbosity>1)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):target_length="<<target_length<<" max_length_buffer="<<max_length_buffer<< " num_consumed="<<num_consumed<<" old_prop="<<old_prop <<" new_prop="<<new_prop<<" prev_length="<<prev_length<<" new_size="<<new_size<<" num_frames_played_sound="<<num_frames_played_sound<<std::endl;

        if(num_frames_played_sound==0) {
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):starting new sound"<<std::endl;
            // Nothing to alter, start again.
            csb.zero_all();
            csb.reset_num_consumed();
            for(int i=0; i<new_size; i++){
              csb[i] = full_sound[i];
              //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
            }
            num_frames_played_sound=1;
        } else if(num_consumed>=prev_length) {
            //should already have finished playing
            //so let's just start playing the new one
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):finishing old sound and starting new sound"<<std::endl;
            csb.zero_all();
            csb.reset_num_consumed();
            new_size=target_length;
            for(int i=0; i<new_size; i++){
              csb[i] = full_sound[i];
              //if(csb[i]>=DEBBB_VALLL)assert(0);//DEBUG
            }
            num_frames_played_sound=1;
        } else if(num_consumed>=new_size-(int)grain_fade_samples) {

            //nope -decided to do it here SHOULD NOT HAPPEN BECAUSE OF ABOVE CHANGE ( new_size = num_consumed; )
            if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):finishing old sound (don't want to start a new one yet)"<<std::endl;
// 	  assert(0);
            //so the currently playing sound is not finished but the target sound would be longer than it
            //so let's just wait for the old one to finish.
            new_size=prev_length;
            num_frames_played_sound++;

// 	    int num_to_zero = std::max(GRAIN_FADE_SAMPLES,prev_length-num_consumed);
//
//             for(int i=0; i<num_to_zero; i++) {

	    //                 double attenuation=1.0 - (i+1.0)/num_to_zero;
//                 csb[i]*=attenuation;
//             }
//         new_size=0;
            // okay we'll miss one frame of sound but probably needed a pause anyway
        }
        else {

//     assert(target_length<=max_length_buffer);
            assert(new_size<=max_length_buffer);

            //let's ensure it fades out before the end from the previous
//             double ramp_down_length_prev = prev_length - new_size;
//guaranteed to be more than GRAIN_FADE_SAMPLES - see above 	    double ramp_down_length_prev = std::min(prev_length - new_size,GRAIN_FADE_SAMPLES);
            if(prev_length - new_size > 0) {
                if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):previous sound squishing by "<<grain_fade_samples <<std::endl;
                for(int i=new_size-grain_fade_samples; i<prev_length; i++) {
                    double keep_prop;
                    if(i>=new_size)keep_prop=0;
                    else keep_prop = (new_size-i-1.0)/(grain_fade_samples);
                    if(verbosity>6) {
                        if(i==new_size-grain_fade_samples)std::cout<<"Here's what I'm doing to 10 or so samples from "<<grain_fade_samples<<" samples from new_size ("<<new_size<<") into the previous sound"<<std::endl;
                        if(i<new_size-grain_fade_samples+10) {
                            std::cout<<csb[i-num_consumed]<<" --> "<<(csb[i-num_consumed] *keep_prop)<<" keep_prop="<<keep_prop<<std::endl;
                        }
                        if(i==new_size-10)std::cout<<"Here's what I'm doing to the last 20 samples or so around new_size in the previous sound, to shorten it: "<<std::endl;
                        if(i>=new_size-10 && i<new_size+10) {
                            std::cout<<csb[i-num_consumed]<<" --> "<<(csb[i-num_consumed] *keep_prop)<<" keep_prop="<<keep_prop<<std::endl;
                        }
                    }
                    csb[i-num_consumed] = keep_prop * csb[i-num_consumed];
                    //if(csb[i-num_consumed]>=DEBBB_VALLL)assert(0);//DEBUG
                    assert(i-num_consumed>=0);
                }
            }

            //let's ensure it fades out before the end from the new
//             double ramp_down_length_new = target_length - new_size;
//guaranteed to be more than GRAIN_FADE_SAMPLES - see above 	    double ramp_down_length_new = std::min(target_length - new_size,GRAIN_FADE_SAMPLES);
            if(target_length - new_size > 0) {
                if(verbosity>2)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered):new sound squishing by "<<grain_fade_samples <<std::endl;
                for(int i=new_size-grain_fade_samples; i<target_length; i++) {
                    double keep_prop;
                    if(i>=new_size)keep_prop=0;
                    else keep_prop = (new_size-i-1.0)/grain_fade_samples;
                    if(verbosity>6) {
                        if(i==new_size-grain_fade_samples)std::cout<<"Here's what I'm doing to 10 or so samples from "<<grain_fade_samples<<" samples from from new_size ("<<new_size<<") into the new sound"<<std::endl;
                        if(i<new_size-grain_fade_samples+10) {
                            std::cout<<full_sound[i]<<" --> "<<(full_sound[i] *keep_prop)<<std::endl;
                        }
                        if(i==new_size-10)std::cout<<"Here's what I'm doing to the last 10 samples or so around new_size in the previous sound, to shorten it: "<<std::endl;
                        if(i>=new_size-10&&i<new_size+10) {
                            std::cout<<full_sound[i]<<" --> "<<(full_sound[i] *keep_prop)<<std::endl;
                        }
                    }
                    full_sound[i] = full_sound[i] * keep_prop;

                }
            }

            assert(new_size-num_consumed>=(int)grain_fade_samples);

            if(verbosity>1)std::cout<<"SoundSourceWaveformInterpolatorImpl::interpolate_sound(ChangeWaveAndSizeLayered) "<<(new_size-num_consumed)<<" samples yet to consume."<<std::endl;
            for(int i = 0 ; i < new_size-num_consumed; i++) {
                short new_sample;
                if(i+num_consumed>=full_sound.size())new_sample=0;
                else new_sample=full_sound[i+num_consumed];
                double targ_val = new_prop * new_sample + old_prop * csb[i];

                double change_prop;
                if(i<(int)grain_fade_samples)change_prop = ((double)i)/((double)grain_fade_samples);
                else change_prop=1.0;

                double new_val=change_prop * targ_val + (1-change_prop) * csb[i];
                if(new_val>=11000)assert(0);//DEBUG
                csb[i] = new_val;

                //if(csb[i-num_consumed]>=DEBBB_VALLL)assert(0);//DEBUG
            }

            num_frames_played_sound++;
            //Tools::check_debug_silliness(csb);

        }

        num_consumed=csb.get_num_consumed();
        
        if(verbosity>5)std::cout<<"Here's the 50 before current in waveform:"<<std::endl;
        if(verbosity>5)std::cout<<csb.str(-50,0)<<std::endl;

        if(verbosity>5)std::cout<<"Here's the first 50 in waveform:"<<std::endl;
        if(verbosity>5)std::cout<<csb.str(0,50)<<std::endl;

        if(verbosity>5)std::cout<<"Here's around #500 in to-play waveform:"<<std::endl;
        if(verbosity>5)std::cout<<csb.str(475,525)<<std::endl;

        if(verbosity>5)std::cout<<"Here's around #500 in incoming waveform:"<<std::endl;
        if(verbosity>5){
            for(int i=475;i<525&&i<full_sound_p.size();i++){
              if(i>0)std::cout<<", ";
              std::cout<<"["<<i<<"]: "<<full_sound_p[i];
            }
            std::cout<<std::endl;
        }

        if(verbosity>5)std::cout<<"Here's the 50 around prev_length ("<<prev_length<<") in waveform:"<<std::endl;
        if(verbosity>5)std::cout<<csb.str(std::max(0,prev_length-num_consumed-25),std::max(0,prev_length-num_consumed+25))<<std::endl;
//     if(verbosity>5)std::cout<<csb.str(prev_length-num_consumed-25,prev_length-num_consumed+25)<<std::endl;

        if(verbosity>5)std::cout<<"Here's the 50 around target_length ("<<target_length<<") in waveform:"<<std::endl;
//     if(verbosity>5)std::cout<<csb.str(target_length -num_consumed-25,target_length -num_consumed+25)<<std::endl;
        if(verbosity>5)std::cout<<csb.str(std::max(0,target_length -num_consumed-25),std::max(0,target_length -num_consumed+25))<<std::endl;

        if(verbosity>5)std::cout<<"Here's the 50 around new_size ("<<new_size<<") waveform:"<<std::endl;
//     if(verbosity>5)std::cout<<csb.str(new_size-num_consumed-25,new_size-num_consumed+25)<<std::endl;
        if(verbosity>5)std::cout<<csb.str(std::max(0,new_size-num_consumed-25),std::max(0,new_size-num_consumed+25))<<std::endl;
        if(verbosity>5)std::cout<<std::endl ;

        prev_length=new_size;

//         //Tools::check_debug_silliness(csb);
    } else {
        std::cerr<<"Amid the beaming of Love's stars, thou'lt greet her in Eastern sky. "<<std::endl;
        assert(0);
    }
//     usleep(500000);
}


