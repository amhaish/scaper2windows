#include "soundgen/SoundGenBlocks.h"
#include <cassert>

#include <assert.h>

using namespace SoundBlocks;

Sine::Sine(float freq):freq_(freq),t_(0.0f){}

float Sine::tick(){
  float toret = sin( (2.0f*float(M_PI)*freq_) * t_ );
  float increment = 1.0f/SAMPLE_RATE;
  t_ += increment;
  if(t_>1.0/freq_)t_-=1.0/freq_;
  return toret;
}

Noise::Noise(){}

float Noise::tick(){
  float sample = ( ((double)rand())/(RAND_MAX+1.0f) ) * 2.0f - 1.0f;
  return sample;
}




InterpolatorGenerator::InterpolatorGenerator(std::vector<float>& contour, float interpolator_scale)
{
           /*
                curr_index=0;

                scale=interpolator_scale;
                prev_size=contour.size();
                required_size= contour.size() * scale;
                interval_size= ( (required_size - prev_size)/ (prev_size - 1) )+1;

                float interpolated_value;
                for(size_t i=0; i< contour.size(); i++)
                {
                    int interval_begin= i*scale;
                    int interval_end= interval_begin+interval_size;
                    float begin_value= contour[i];
                    float end_value= contour[i+1];

                    for(size_t j=interval_begin ; j<interval_end ; j++)
                    {
                        interpolated_value= ((end_value - begin_value) * (j - interval_begin))/interval_size + begin_value;
                        interpolated_contour.push_back(interpolated_value);
                    }
                }
            */
           if(contour.size()==0)
           {
             std::cout<<"warning:empty contour."<<__FILE__<<":"<<__LINE__<<std::endl;
             interpolated_contour.clear();
             return;
           }

           curr_index=0;
	   interval_size= 1.0 /interpolator_scale;
           interpolated_size= 0;

           float interpolated_value;
           for( float i=0 ; i< contour.size()-1; i=i+interval_size)
           {
             interpolated_size++;
//              if(i== floor(i))
//                interpolated_contour.push_back(contour[i]);

//              else
//              {
               int interval_begin= floor(i);
               int interval_end= interval_begin + 1;
                if(i<=contour.size()-1){
                  float begin_value= contour[interval_begin];
                  float end_value;
                  if(interval_end<contour.size())end_value= contour[interval_end];
                  else end_value=begin_value; // could as easily be 0
                  interpolated_value= ((end_value - begin_value) * (i- interval_begin))/1  + begin_value;
                  interpolated_contour.push_back(interpolated_value);
                }
//              }

          }
//           if(contour.size()>0)interpolated_contour.push_back(contour[contour.size()-1]);

}




float InterpolatorGenerator::tick()
{
 if(curr_index >= interpolated_size)
    curr_index=0;
 tick_value=interpolated_contour[curr_index];
 curr_index++ ;

 return tick_value;
}

int InterpolatorGenerator::getSize()
{
return interpolated_contour.size();
}




//////////////////////////////////////////////
AmplitudeModulator::AmplitudeModulator(Tickable* carrier, Tickable* modulator, float modulator_effect):
carrier_(carrier),
modulator_(modulator),
modulator_effect_(modulator_effect)
{

}


float AmplitudeModulator::tick()
{
  assert(modulator_effect_>=0 && modulator_effect_<=1);
  carrier_value=carrier_->tick();
  modulator_value=modulator_->tick();
tick_value= (((modulator_value +1)/2 )* carrier_value)*modulator_effect_  + (1-modulator_effect_)*carrier_value;
return tick_value;
}



//////////////////////////////////////////////


int FrequencyModulator::verbosity__ = 0;

FrequencyModulator::FrequencyModulator(Tickable *carrier,Tickable *modulator,float peak_frequency_deviation):
  carrier_(carrier),
  modulator_(modulator),
  peak_frequency_deviation_(peak_frequency_deviation),
  curr_carrier_ind_(0.0f),
  prev_carrier_ind_(-2),
  prev_carrier_sample_(99999),
  next_carrier_sample_(99999)
{
//     if(peak_frequency_deviation_==1.0f)peak_frequency_deviation_=0.99f;
//     assert(peak_frequency_deviation_<1.0f&&peak_frequency_deviation_>=0.0f);
}

float FrequencyModulator::tick(){

  float old_carrier_ind=prev_carrier_ind_;

  // get the samples that we need for interpolation
  while (prev_carrier_ind_< floor(curr_carrier_ind_)){
    prev_carrier_sample_ = next_carrier_sample_;
    next_carrier_sample_ = carrier_->tick();
    prev_carrier_ind_++;
  }
  if(verbosity__>3)std::cout<<__FILE__<<":"<<__FUNCTION__<<":"<<__LINE__<<": prev carrier ind skips "<<(prev_carrier_ind_-old_carrier_ind)<<std::endl;

  float next_prop = (curr_carrier_ind_ - prev_carrier_ind_);

//   assert( next_prop <= 1.0f );
//   assert( next_prop >= 0.0f );
  if (next_prop<0 || next_prop>1)
    std::cout <<"next_prop : "<< next_prop <<std::endl;
  float prev_prop = 1.0f - next_prop;

  float curr_carrier_sample = next_prop * next_carrier_sample_+ prev_prop * prev_carrier_sample_;

  if(verbosity__>2)std::cout<<__FILE__<<":"<<__FUNCTION__<<":"<<__LINE__<<": "<<curr_carrier_sample<<" = "<<next_prop<<" * "<<next_carrier_sample_<<"+ "<<prev_prop<<" * "<<prev_carrier_sample_<<std::endl;

  // now we calculate the next sample pos

  float deviation = modulator_->tick();

  float old_period = 1.0f ; // sample index

//   if(deviation==1.0f){
//    std::cout<<"break SoundGenBlocks.cpp:61"<<std::endl;
//   }

  float old_sample_freq = 1.0f/old_period;

  float new_sample_freq =  old_sample_freq + peak_frequency_deviation_ * deviation ;

  float new_period = 1.0f / new_sample_freq;

  assert(!std::isinf(new_period));

//   float new_period = 1.0f / ( 1.0/old_period + peak_frequency_deviation_ * deviation );

  if(verbosity__>1)std::cout<<__FILE__<<":"<<__FUNCTION__<<":"<<__LINE__<<": new_period: "<<new_period<<std::endl;

  curr_carrier_ind_ += new_period;

  const int overflow_ind=SAMPLE_RATE*50;
  // prevent overflow
  if(prev_carrier_ind_>overflow_ind){
    prev_carrier_ind_-=overflow_ind;
    curr_carrier_ind_-=overflow_ind;
  }

  return curr_carrier_sample;

}

Add::Add(std::vector<Tickable*> addends,std::vector<float> mults):addends_(addends),mults_(mults){
  float tot=0;
  for(size_t i=0;i<mults.size();i++)tot+=mults[i];
  if(mults.size()==addends.size())assert(tot==1.0f);
  else if(mults.size()==addends.size()-1 && tot<=1.f && tot>=0.f)mults_.push_back(1.f-tot);
  else assert(false);
}

float Add::tick(){
  float sample=0;
  for(size_t i=0;i<addends_.size();i++)sample+=mults_[i]*addends_[i]->tick();
  assert(sample>=-1.0f&&sample<=1.0f);
  return sample;
}
