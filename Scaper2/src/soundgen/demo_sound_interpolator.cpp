#include <soundgen/SoundSourceGenerator.h>
#include <soundgen/SoundSourceOutput.h>
// #include <soundgen/SoundInterpolator.h>
#include <AL/alut.h>
#include <stdlib.h>
#include <iostream>

double randd() {
    return ((double)rand())/RAND_MAX;
}

int main(int argc,char ** argv) {

//     SoundInterpolator si(15);

    SoundFeature sfeat;
    SoundSourceGenerator ssg(15);
    SoundSourceOutput sso(15);
    CircularBuffer<short> csb(44100*3); // 3 second long buffer - should be enough

    for (double var=-3.14; var<3.14; var+=0.01) {
        sfeat.pos[0]=cos(var);
        sfeat.pos[1]=sin(var);
        sfeat.pos[2]=randd()*0.05;
        sfeat.frequencies.clear();
        double rr=randd()*50;
        sfeat.frequencies.push_back(150+40*var+rr);
        sfeat.frequencies.push_back(300+80*var+rr*2);
        std::cout<<"Playing "<<sfeat<<std::endl;
//         si.play_feature(sfeat);
	ssg.generate_sound(sfeat,csb);
	sso.output(csb,sfeat.pos);
        alutSleep(1.0/30);
    }

    alutSleep(1.0);

}
