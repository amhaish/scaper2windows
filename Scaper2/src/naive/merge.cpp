#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <AL/al.h>


#include <AL/alc.h>


#include <AL/alut.h>

#include <memory>
#include <boost/shared_ptr.hpp>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>

#include <sstream>


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/io/grabber.h>

#include <boost/concept_check.hpp>

#include <pcl/io/openni_grabber.h>
#include <boost/program_options.hpp>
// #include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_grabber.h>

#include <boost/timer.hpp>
// #include <time.h>
#include <chrono>

// #include <AL/alu.h>

#define SOUNDWIDTH 1
#define SOUNDHEIGHT 1
#define IMWIDTH 160
#define IMHEIGHT 120
#define FRAMERATE 30
#define FRAMETIME 1.0f/FRAMERATE

#define SOUND_PERIOD FRAMETIME*2
// #define SAMPLE_RATE 44100
#define SAMPLE_RATE 22050
#define FORMAT AL_FORMAT_MONO16
#define NUM_BUFFERS SOUNDWIDTH*SOUNDHEIGHT
#define NUM_SOURCES NUM_BUFFERS
#define BYTES_PER_SAMPLE 2
#define BUF_SIZE int(SAMPLE_RATE * SOUND_PERIOD)

const int buf_size = BUF_SIZE;



namespace po = boost::program_options;

// ALuint buffers[NUM_BUFFERS];
// ALuint sources[NUM_SOURCES];

boost::timer timer;
// time_t start_time;
// std::chrono::system_clock::time_point start_time;
std::chrono::high_resolution_clock::time_point start_time;


inline double calc_sample(double freq,double i, double attenuation) {
    return attenuation * 32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * i );

}

// void fill_sample_zeroes(short *samples) {
//     
//     std::fill(samples,samples+BUF_SIZE,0);
//     
// 
// }

void fill_sample_add(double freq,short *samples,double start_attenuation, double end_attenuation) {
    
    double attenuation_step = (end_attenuation-start_attenuation)/(BUF_SIZE-1);
    double attenuation = start_attenuation;    

    for(int i=0; i<BUF_SIZE; ++i) {

        samples[i] += calc_sample(freq,i,attenuation);//32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * i );
	attenuation+=attenuation_step;
    }

}

void fill_sample(double freq,short *samples,double attenuation,double &curval, int &curdir) {
//   double entry=3200;
//     std::chrono::high_resolution_clock::no
    bool found=false;
    int starti;

//let's try to make a smooth transition
    for(starti=0; starti<BUF_SIZE && !found; ++starti) {

        double sample1 = calc_sample(freq,starti,attenuation);//attenu32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * starti );
        double sample2 = calc_sample(freq,starti+1,attenuation);//32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * (starti+1) );

//     if(sample1!=sample2){
        if(sample1<=curval&&curval<=sample2) {
            int dir=0;
            if(sample1<sample2) {
                dir=-1;
            } else {
                dir=1;
            }
            if (dir ==curdir)found=true;
        }
//     }

    }

    for(int i=starti; i<starti+BUF_SIZE; ++i) {

        samples[i-starti] = calc_sample(freq,i,attenuation);//32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * i );

    }

    curval=samples[BUF_SIZE-1];
    curdir=samples[BUF_SIZE-1]<samples[BUF_SIZE-2]?-1:1;
}


    
pcl::PointXYZ close_point(const pcl::PointCloud<pcl::PointXYZ> &cloud,std::vector<int> &indices){
 
  if(indices.size()==0)return pcl::PointXYZ();
  
  pcl::PointXYZ close;
  double closestz=99999;
  int closesti=indices[0];
  
  int directions[4];
  directions[0] = -1*cloud.width + 0; 
  directions[1] =  1*cloud.width + 0;
  directions[2] =  0*cloud.width - 1;
  directions[3] =  0*cloud.width + 1;
  int siz=cloud.size();
  int indsiz=indices.size();
    //z is positive from openni, and general PCL grabbers
  for(int ind=0;ind<indsiz;ind++){
//   for(int i=0;i<siz;i++){
    int i=indices[ind];
    double neighbours_furthestz=cloud.points[i].z;
    for(int dir=0;dir<4;dir++){
      int j = i+directions[dir];
      if(j>=0&&j<siz){
	int thisz=cloud.points[j].z;
	if(thisz>neighbours_furthestz)neighbours_furthestz=thisz;
      }
    }
    if(neighbours_furthestz<closestz){
      closestz=neighbours_furthestz;
      closesti=i;
    }
  }
  
close=cloud.points[closesti];
return close;

  
//   print (is neg)
}


class SceneSource {

public:

    std::vector<short> samples[2];
    ALuint buffers[2];
    unsigned int nextbuffer;

    ALuint source;
    unsigned int width;
    unsigned int height;

    unsigned int xstart;
    unsigned int xend;
    unsigned int ystart;
    unsigned int yend;

//   double last_time;
//     double /*expec*/ted_finish;

    int num_started;
    ALint processed;

    double curval;
    int curdir;


    bool playing;

    Eigen::Matrix< double, 4, 1 > centroid;

    std::vector<int> indices;

    int i;

    SceneSource(unsigned int width, unsigned int height, unsigned int xstart,unsigned int xend, unsigned int ystart, unsigned int yend):width(width),height(height),xstart(xstart),xend(xend),ystart(ystart),yend(yend) {

        ALenum error;
        alGenBuffers(2,buffers);
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"couldn't gen buffers\n"<<error<<std::endl;;
//       return 1;
        }

        alGenSources(1, &source);

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"couldn't gen sources\n"<<error<<std::endl;;
//       return 1;
        }

        for(unsigned int x=xstart; x<=xend; x++) {
            for(unsigned int y=ystart; y<=yend; y++) {

                unsigned int ind = y * width + x;
                if(ind>=160*120)
                    std::cout<<"oops"<<std::endl;
                indices.push_back(ind);

            }
        }
//     last_time=-1;
//         expected_finish=-1;
        nextbuffer=0;

        samples[0].resize(BUF_SIZE);// = std::vector<short>(BUF_SIZE,0);
        samples[1].resize(BUF_SIZE);// = std::vector<short>(BUF_SIZE,0);
        
        std::fill(samples[0].begin(),samples[0].end(),0);
	std::fill(samples[1].begin(),samples[1].end(),0);

        curval=0;
        curdir=1;
        playing=false;

        num_started=false;

    }

    ~SceneSource() {
//       if(playing)
// 	alSou
    }

    
/*
    
    int proc_centroid(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud) {


        ALenum error;
	
        alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"could not get num buffers processed\n"<<error<<std::endl;;
        }

        std::cout<<"Finished "<<processed<<" buffers -- have started "<<num_started<<"."<<std::endl;


	if(true){
            if(num_started-processed<2) {

                pcl::compute3DCentroid<pcl::PointXYZ>(*cloud,indices,centroid);
            }else{
	      return 0;
	    }

        } else {
            return 0;
        }
        if (!std::isnan(centroid[0])&&!std::isnan(centroid[1])&&!std::isnan(centroid[2])&&!std::isnan(centroid[3])) {
            if (!std::isinf(centroid[0])&&!std::isinf(centroid[1])&&!std::isinf(centroid[2])&&!std::isinf(centroid[3])) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }*/

    int proc_sound(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud, double attenuation) {


        ALenum error;

        int played=0;

	  if(true){

            if(num_started-processed<2) {
	    pcl::compute3DCentroid<pcl::PointXYZ>(*cloud,indices,centroid);
                if (!std::isnan(centroid[0])&&!std::isnan(centroid[1])&&!std::isnan(centroid[2])&&!std::isnan(centroid[3])) {

                    if (!std::isinf(centroid[0])&&!std::isinf(centroid[1])&&!std::isinf(centroid[2])&&!std::isinf(centroid[3])) {

			
		     
		      
                        ALfloat sourcePos[3];
			played=1;
                        if(centroid[3]>0) {
                            sourcePos[0]=centroid[0]/centroid[3];
                            sourcePos[1]=centroid[1]/centroid[3];
                            sourcePos[2]=centroid[2]/centroid[3];
                        } else {
                            sourcePos[0]=centroid[0];
                            sourcePos[1]=centroid[1];
                            sourcePos[2]=centroid[2];
                        }

                        double dist=centroid.norm();

                        double freq = 340.29/dist ; // 340.29 = dist / time, time = dist/340.29 ,  freq = 1/time , freq = 340.29/dist


			int followingbuffer = nextbuffer==1?0:1;
                        //make and insert sound
                        std::fill(samples[followingbuffer].begin(),samples[followingbuffer].end(),0);
                        fill_sample_add(freq,samples[followingbuffer].data(),attenuation,0);
                        fill_sample_add(freq,samples[nextbuffer].data(),0,attenuation);

                        if(num_started>=2) {
			  std::cout<<"dequeuing buffer "<<nextbuffer<<std::endl;
                            alSourceUnqueueBuffers(source, 1, &(buffers[nextbuffer]));
			    if ((error=alGetError()) != AL_NO_ERROR)
			    {
			      std::cout<<"couldn't unqueue buffer\n"<<error<<std::endl;;
			      
			    }
			    else{
			     num_started--; 
			    }
			      
			    
                        }			
			std::cout<<"buffering buffer "<<nextbuffer<<std::endl;
                        alBufferData(buffers[nextbuffer], FORMAT, samples[nextbuffer].data(), BUF_SIZE*BYTES_PER_SAMPLE, SAMPLE_RATE);
                        if ((error=alGetError()) != AL_NO_ERROR)
                        {
                            std::cout<<"couldn't buffer data\n"<<error<<std::endl;;
			    played=0;
                        }
			else{

			  std::cout<<"enqueuing buffer "<<nextbuffer<<std::endl;
			  alSourceQueueBuffers(source, 1, &(buffers[nextbuffer]));

			  if ((error=alGetError()) != AL_NO_ERROR)
			  {
			      std::cout<<"couldn't queue buffer\n"<<error<<std::endl;;
			      played=0;
			  }
                        
			}

                        alSourcefv( source,AL_POSITION,sourcePos);
                        if ((error=alGetError()) != AL_NO_ERROR)
                        {
                            std::cout<<"couldn't set position\n"<<error<<std::endl;;

                        }

                        if(!playing) {

                            alSourcePlay(source);
                            playing=true;


                        }

                        nextbuffer = (nextbuffer+1)%2;

                        
			if(played)
			  num_started++;

                    }
                }
            }

            /*last_time=time;*/

        }
        return played;
    }

};

boost::shared_ptr<SceneSource> sources[SOUNDHEIGHT][SOUNDWIDTH];



void grabbed (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud) {

     int num_valid_centroids=SOUNDWIDTH*SOUNDHEIGHT;
//     std::cout<<"Centroid checks"<<std::endl;
    double st=timer.elapsed();
//     for(int sY=0; sY<SOUNDHEIGHT; sY++) {
//         for(int sX=0; sX<SOUNDWIDTH; sX++) {
// 
//             int valid=sources[sY][sX]->proc_centroid(cloud);
//             num_valid_centroids+=valid;
//         }
//     }
    int num_played=0;
    //double attenuation=1.0/num_valid_centroids;
    double attenuation=1.0/(SOUNDWIDTH*SOUNDHEIGHT);

    std::cout<<"Now start buffering sounds"<<std::endl;

    for(int sY=0; sY<SOUNDHEIGHT; sY++) {
        for(int sX=0; sX<SOUNDWIDTH; sX++) {
            int valid=sources[sY][sX]->proc_sound(cloud,attenuation);
            num_played+=valid;
        }
    }
    double en=timer.elapsed();

//     time_t curr_time;
//     time(&curr_time);
    std::chrono::high_resolution_clock::time_point now  = std::chrono::high_resolution_clock::now();

//     now.

    std::chrono::high_resolution_clock::duration diff = now-start_time;
//     diff.

    std::chrono::milliseconds diffmilli=std::chrono::duration_cast<std::chrono::milliseconds>(diff);
//     std::chrono::system_clock::duration so_far = now-start_time;
    double so_far_milliseconds=diffmilli.count();
//     double so_far_milliseconds=std::chrono::duration_cast<std::chrono::milliseconds>(now-start_time).count();
    double so_far_seconds = so_far_milliseconds/1000;
//     std::cout<<"so_far_milliseconds "<<so_far_milliseconds<<std::endl;
//     chrono::
//      double so_far = aaa.count()/1000.0;



    std::cout<<"Processing time: "<<std::setprecision(8)<<std::setw(12)<<(en-st)<<" seconds ---- Num of sectors processed: "<<std::setprecision(1)<<std::setw(5)<<(num_played)<<"/"<<std::setprecision(1)<<std::setw(5)<<(num_valid_centroids)<<" ---- elapsed: "<<std::setw(7)<<std::setprecision(3)<<so_far_seconds <<" seconds"<<std::endl;
std::cout<<"!!!"<<std::endl;
}


po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Proto Viewer" );
    desc.add_options()
    ( "filename",po::value<std::string>()->default_value (""),"Read from a file." )
//     ( "k",po::value<int>()->default_value ( -1 ),"Number of points for calculation of normal." )
//     ( "max_surface_angle",po::value<double>()->default_value ( M_PI/4 ),"Don't consider points for triangulation
//  if their normal deviates more than this value from the query point's normal." )
//     ( "mesh_search_radius",po::value<double>()->default_value ( 0.025 ),"!" )
//     ( "mesh_search_points",po::value<int>()->default_value ( 100 ),"!" )
    ( "openni","Use the OpenNI grabber" )
    ( "depthsense","Use the DepthSense grabber" )
//     ( "radius",po::value<double>()->default_value ( -1 ),"Radius for normal calculation." )

    ;

    po::positional_options_description p;
//     p.add ( "filenames", 15 );
//   p.add("target_filename", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm );

    return vm;
}


int main(int argc,char ** argv) {

//     time(&start_time);
//   start_time== std::chrono::system_clock::now();
    start_time= std::chrono::high_resolution_clock::now();

    po::variables_map vm=args ( argc,argv );


    bool openni=false;
    bool depthsense=false;
    bool fromfile=false;
    std::string filename;
    if ( !vm["openni"].empty() ) openni=true;
    if ( !vm["depthsense"].empty() ) depthsense=true;
    if ( vm["filename"].as<std::string>() !="" ) {
        fromfile=true;
        filename=vm["filename"].as<std::string>();
    }

    if(!openni && !depthsense && !fromfile) {
        std::cerr<<"Give me a point cloud source"<<std::endl;
        exit(1);
    }
    if(openni && depthsense ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(depthsense && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(openni && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    boost::shared_ptr<pcl::Grabber> grabber;
    if(openni) {

        grabber.reset(new pcl::OpenNIGrabber("",pcl::OpenNIGrabber::OpenNI_QQVGA_30Hz));
    } else if(fromfile) {

        grabber.reset(new pcl::PCDGrabber<pcl::PointXYZ>(filename));
    } else {
        std::cerr<<"Not implemented. Sorry."<<std::endl;
        exit(1);
    }

    ALenum error;
    alutInit(0,NULL);


    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"no init alut s"<<error<<"\n"<<std::endl;
        return 1;
    }
//     printf("ok\n");


    int impersX=IMWIDTH/SOUNDWIDTH;
    int impersY=IMHEIGHT/SOUNDHEIGHT;



    for(int sX=0; sX<SOUNDWIDTH; sX++) {
        for(int sY=0; sY<SOUNDHEIGHT; sY++) {
            int xstart=sX*impersX;
            int xend=(sX+1)*impersX-1;
            int ystart=sY*impersY;
            int yend=(sY+1)*impersY-1;
            sources[sY][sX].reset(new SceneSource(IMWIDTH,IMHEIGHT,xstart,xend,ystart,yend));
        }
    }

//     alSourcei(src, AL_BUFFER, buf);


//     alSourcei( src,AL_LOOPING,AL_TRUE);




    boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f = boost::bind (&grabbed, _1);
    grabber->registerCallback (f);

    grabber->start ();

    while(grabber->isRunning()) {
        alutSleep(1);
    }

    grabber->stop();

    alutExit();
//   alutSleep(5);
    exit(0);
}
