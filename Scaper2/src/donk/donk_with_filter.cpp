#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <memory>
#include <sstream>
#include <chrono>
#include <limits>

#include <boost/shared_ptr.hpp>
#include <boost/concept_check.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include <boost/timer.hpp>

//OpenAL includes
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

//Temporary Includes by Hakan
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>

//Point Cloud Library Includes
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/grabber.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/filters/statistical_outlier_removal.h>
#ifdef WITH_OPENNI2
#include <pcl/io/openni2_grabber.h>
#endif
#include <pcl/io/pcd_grabber.h>
#ifdef WITH_DEPTHSENSE_DS
#include "depth_sense_grabber.h"
#endif

#define IMWIDTH 160
#define IMHEIGHT 120
#define FRAMESPERSEC 30
#define FRAMETIME 1.0f/FRAMESPERSEC
#define MAX_SOURCES 256 //actually implementation dependent - on my platform 256

#define MAX_SOUND_PERIOD 2
#define SAMPLE_RATE 22050
#define FORMAT AL_FORMAT_MONO16
#define BYTES_PER_SAMPLE 2
#define MAX_BUF_SIZE int(SAMPLE_RATE * MAX_SOUND_PERIOD)
//#define PROJECTILE_VELOCITY_METERSPERSEC 0.4
#define MAX_SOURCES_PER_GRID 10
#define CURVATURE_K 20

#define RAMP_TIME 0.01
#define RAMP_SAMPLES (int) (SAMPLE_RATE * RAMP_TIME)

double MaxDepthChangeFactor;
double NormalSmoothingSize;
bool DepthDependentSmoothing;
int verbose;
int soundwidth;
int soundheight;
double projectile_velocity;
double use_curvature_histogram;

boost::timer timer;
std::chrono::high_resolution_clock::time_point start_time;
class SceneSource; // fully defined below
std::vector< std::vector< boost::shared_ptr<SceneSource> > > sources; //2D grid of sources in this attempt

/** defines the curvature-frequency mapping */
double curvature_to_freq(double curvature) {

    //let us assume that the biggest object we will ever encounter is 5m
    double lowest_freq = 340.29/5;

    //let us assume that the smallest object we will ever encounter is 0.02m
    double highest_freq = 340.29/0.02;

    //these are roughly empirically determined
    double lowest_curvature=0;
    double highest_curvature=0.328;

    double cnormed = (curvature-lowest_curvature)/(highest_curvature-lowest_curvature);
    double clogged = std::log(cnormed);

    double logfreq = std::log(lowest_freq)+cnormed*(std::log(highest_freq)-std::log(lowest_freq));

    double freq = std::exp(logfreq);

    if(verbose>5) {
        std::cout<<"cnormed: "<<cnormed<<" clogged: "<<clogged<<" logfreq: "<<logfreq<<" freq: "<<freq<<std::endl;
    }

    return freq;

}

/** For making a nice sine wave */
inline double calc_sample(double freq,double i, double attenuation) {
    return attenuation * 32760 * sin( (2.f*float(M_PI)*freq)/SAMPLE_RATE * i );

}

/** Add a sine wave to buffer */
void fill_sample_add(double freq,short *samples,int size,double start_attenuation, double end_attenuation) {

    double attenuation_step = (end_attenuation-start_attenuation)/(size-1);
    double attenuation = start_attenuation;

    for(int i=0; i<size; ++i) {
        double this_attenuation=attenuation;
        if(i<RAMP_SAMPLES)this_attenuation*= ((double)i)/RAMP_SAMPLES;

        samples[i] += calc_sample(freq,i,this_attenuation);
        attenuation+=attenuation_step;
    }

}

/** Put a sine wave in a buffer */
void fill_sample(double freq,short *samples,int size,double start_attenuation, double end_attenuation) {

    double attenuation_step = (end_attenuation-start_attenuation)/(size-1);
    double attenuation = start_attenuation;

    for(int i=0; i<size; ++i) {
        double this_attenuation=attenuation;
        if(i<RAMP_SAMPLES)this_attenuation*= ((double)i)/RAMP_SAMPLES;

        samples[i] = calc_sample(freq,i,this_attenuation);
        attenuation+=attenuation_step;
    }

}

// THERE IS A PROBLEM WITH THIS CODE
// double region_curvature(const pcl::PointCloud<pcl::PointXYZ> &cloud,int ind,int k) {
//
//     pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud_ptr;
//     cloud_ptr.reset(&cloud);
//
//     pcl::NormalEstimation<pcl::PointXYZ,pcl::Normal> ne;
//     ne.setInputCloud(cloud_ptr);
//     ne.setSearchSurface(cloud_ptr);
//     ne.setViewPoint(0,0,0);
//     ne.setKSearch(30);
//     std::vector<int> inds;
//     inds.push_back(ind);
//     pcl::PointCloud<pcl::Normal> out;
//     ne.compute(out);
//
//     assert(out.size()<2);
//     if(out.size()>0)return out[0].curvature;
//     else return std::numeric_limits<double>::quiet_NaN();
//
// }


/** Look at a subcloud (using indices) find the closest point. 
 * But assume some noise and look for largest distance of neighbours of each point and use that distance when deciding which point is closest.
 * Quite naive, and could be a lot simpler. Might divorce point finding from signature calculation also, should improve it.
 */
void close_point(const pcl::PointCloud<pcl::PointXYZ> &cloud,const pcl::PointCloud<pcl::Normal> &normal_cloud,std::vector<int> &indices,pcl::PointNormal &close,std::vector<double> &curvatures) {

    if(indices.size()==0) {
        // what you make me do
        close.x=std::numeric_limits<double>::infinity();
        close.y=std::numeric_limits<double>::infinity();
        close.z=std::numeric_limits<double>::infinity();
        close.normal_x=std::numeric_limits<double>::infinity();
        close.normal_y=std::numeric_limits<double>::infinity();
        close.normal_z=std::numeric_limits<double>::infinity();
        close.curvature=std::numeric_limits<double>::infinity();
        curvatures.clear();
        return;
    }

    const int num_directions = 4;

    int directions[num_directions];
    directions[0] = -1*cloud.width + 0;
    directions[1] =  1*cloud.width + 0;
    directions[2] =  0*cloud.width - 1;
    directions[3] =  0*cloud.width + 1;

    if(num_directions>4) { //haven't tested, might help
        directions[4] = -1*cloud.width + 1;
        directions[5] =  1*cloud.width + 1;
        directions[6] = -1*cloud.width - 1;
        directions[7] =  1*cloud.width - 1;
    }

    int siz=cloud.size(); //size of cloud
    int indsiz=indices.size(); //size of indices under consideration

    //z is positive from openni, and general PCL grabbers
    int closesti=indices[0];
    pcl::PointXYZ closest_point;
    pcl::Normal closest_normal;
    double closest_curvature;
    closest_point.z=99999;
    std::vector<double> local_curvatures;

    for(int ind=0; ind<indsiz; ind++) {

        int i=indices[ind];

//         if(i%cloud.width>70&&i%cloud.width<90&&i/cloud.width>50&&i/cloud.width<70) {
//      std::cout<<"mid"<<std::endl;
//         }

        if (pcl::isFinite(cloud.points[i])&&!std::isnan(normal_cloud.points[i].curvature)) { //ignore NAN points

            pcl::PointXYZ closest_point_check;
            pcl::Normal closest_normal_check;
            double avg_curvature=0;
            int avg_curvature_cnt=0;
            local_curvatures.clear();
            closest_point_check=cloud.points[i];
bool neighbours_have_nan=false;
            closest_normal_check=normal_cloud.points[i];
            avg_curvature+=normal_cloud.points[i].curvature;
            avg_curvature_cnt++;
            local_curvatures.push_back(normal_cloud.points[i].curvature);

          //  if(false) { //our work here is done
//
  //              static double lowest_curv=999999999;
    //            static double heighest_curv=-99999999;
//
  //              if(normal_cloud.points[i].curvature>0&&normal_cloud.points[i].curvature<lowest_curv) {
    //                lowest_curv=normal_cloud.points[i].curvature;
      //              std::cout<<"Lowest curvature so far "<<lowest_curv<<std::endl;
        //        }
          //      if(normal_cloud.points[i].curvature>0&&normal_cloud.points[i].curvature>heighest_curv) {
            //        heighest_curv=normal_cloud.points[i].curvature;
             //       std::cout<<"Heighest curvature so far "<<heighest_curv<<std::endl;
              //  }
            //}

            for(int dir=0; dir<4; dir++) {

                int j = i+directions[dir];

                if(pcl::isFinite(cloud.points[j])){

                    if(j>=0&&j<siz) {

                        if(cloud.points[j].z>closest_point_check.z) {
                            closest_point_check=cloud.points[j];
                            if(!std::isnan(normal_cloud.points[j].curvature)) {
                                closest_normal_check=normal_cloud.points[j];
                            }
                        }
                        if(std::isnan(closest_normal_check.curvature)&&!std::isnan(normal_cloud.points[j].curvature))closest_normal_check=normal_cloud.points[j];
                        if(!std::isnan(normal_cloud.points[j].curvature)) {
                            avg_curvature+=normal_cloud.points[j].curvature;
                            avg_curvature_cnt++;
                            local_curvatures.push_back(normal_cloud.points[j].curvature);
                        }
                
                    }
                }else{
                        neighbours_have_nan=true;

                }
            }

            if(!neighbours_have_nan && closest_point_check.z < closest_point.z && !std::isnan(closest_normal_check.curvature)) {

                closest_point=closest_point_check;
                closest_normal=closest_normal_check;
                closest_curvature=avg_curvature/avg_curvature_cnt;
                closesti=i;
                curvatures=local_curvatures;

            }
        }
    }

    if(std::isnan(closest_point.z)||closest_point.z>100) {
        close.x=std::numeric_limits<double>::infinity();
        close.y=std::numeric_limits<double>::infinity();
        close.z=std::numeric_limits<double>::infinity();
        close.normal_x=std::numeric_limits<double>::infinity();
        close.normal_y=std::numeric_limits<double>::infinity();
        close.normal_z=std::numeric_limits<double>::infinity();
        close.curvature=std::numeric_limits<double>::infinity();
    }
    else {
        close.x=closest_point.x;
        close.y=closest_point.y;
        close.z=closest_point.z;
        close.normal_x=closest_normal.normal_x;
        close.normal_y=closest_normal.normal_y;
        close.normal_z=closest_normal.normal_z;
// close.curvature=closest_normal.curvature;
        close.curvature=closest_curvature;
// 		double temp_curv=region_curvature(cloud,closesti,CURVATURE_K);

        if(verbose>3)std::cout<<"closesti: "<<closesti<<"..."<<closesti/cloud.width<<"x"<<closesti%cloud.width<<" curvature: "<<close.curvature<<"("<<")"<<std::endl;
//         if(verbose>3)std::cout<<"closesti: "<<closesti<<"..."<<closesti/cloud.width<<"x"<<closesti%cloud.width<<" curvature: "<<close.curvature<<"("<<temp_curv<<")"<<std::endl;
// 	if(!std::isnan(temp_curv))close.curvature=temp_curv;
    }

    return;
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer ("PCL Visualizer"));
bool isFirst = true;
bool isClosestPointChanged = false;

/** One sound source per grid entry in this code - but could frame it in other ways. */
class SceneSource {

public:

    int numsources;

    std::vector<short> samples; //for making sounds
    std::vector<ALuint> buffers;
    std::vector<ALuint> sources;
    unsigned int nextbuffer;
    std::vector<ALint> processed;

    // sound grid
    unsigned int width;
    unsigned int height;

    // depth image pixel grid
    unsigned int xstart;
    unsigned int xend;
    unsigned int ystart;
    unsigned int yend;

    std::vector<int> indices; // calculated from grid definition

    std::chrono::high_resolution_clock::time_point released_time;

    pcl::PointNormal representative_point;
    std::vector<double> curvatures;
	
    std::list<double> moving_average_list;
    static double sum_of_curvatures;
    static int curvature_cnt;

    SceneSource(unsigned int numsources,unsigned int width, unsigned int height, unsigned int xstart,unsigned int xend, unsigned int ystart, unsigned int yend):numsources(numsources),width(width),height(height),xstart(xstart),xend(xend),ystart(ystart),yend(yend) {

        buffers.resize(numsources);
        sources.resize(numsources);

        ALenum error;
        alGenBuffers(numsources,buffers.data());
        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"couldn't gen buffers\n"<<error<<std::endl;;
            exit(1); // I can't go on like this anymore, darling.
        }

        alGenSources(numsources, sources.data());

        if ((error=alGetError()) != AL_NO_ERROR)
        {
            std::cout<<"couldn't gen sources\n"<<error<<std::endl;;
            exit(1); // I can't go on like this anymore, darling.
        }

        for(unsigned int x=xstart; x<=xend; x++) {
            for(unsigned int y=ystart; y<=yend; y++) {

                unsigned int ind = y * width + x;
                if(ind>=160*120)
                    std::cout<<"oops"<<std::endl;

                indices.push_back(ind);

            }
        }

        nextbuffer=0;

        samples.resize(MAX_BUF_SIZE);// = std::vector<short>(BUF_SIZE,0);
        std::fill(samples.begin(),samples.end(),0);

        // start those projectiles flying
        released_time = std::chrono::high_resolution_clock::now();

    }

    ~SceneSource() {

    }

    /** Take a point cloud, take this object's part of the point cloud (according to indices), and generate a spatial sound from it */
    int proc_sound(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud,const pcl::PointCloud<pcl::Normal>::ConstPtr &normal_cloud, double attenuation) {

        ALenum error;
        int played=0;

        close_point(*cloud,*normal_cloud,indices,representative_point,curvatures);
        //pcl::compute3DCentroid<pcl::PointXYZ>(*cloud,indices,centroid);

        if(!pcl::isFinite(representative_point)) {

            // if we don't reset the time we'll get a donk right when we get a signal
            released_time = std::chrono::high_resolution_clock::now();
            played=0;

        } else if(std::isnan(representative_point.curvature)) {

            // just leave it till next time.
            //TODO: only let this happen for a few cycles (not that important)
            // also may not happen anymore depending on close_point
            played=0;

        } else {

            std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
            double seconds_passed = std::chrono::duration<float>(now-released_time).count();
            double dist_travelled = seconds_passed * projectile_velocity;
            double dist=sqrt(representative_point.z*representative_point.z+representative_point.y*representative_point.y+representative_point.x*representative_point.x);

            if(verbose>3)std::cout<<"dist_travelled: "<<dist_travelled<<" dist: "<<dist<<std::endl;

            if(dist_travelled>=dist) { // collision

                ALfloat sourcePos[3];
                played=1;

                sourcePos[0]=representative_point.x;
                sourcePos[1]=representative_point.y;
                sourcePos[2]=representative_point.z;

                //make and insert sound

                std::fill(samples.begin(),samples.end(),0); // clear buffer

                //TODO: fix this calculation
                int buf_size=(int)std::min(1.0*MAX_BUF_SIZE,std::max(340.29*dist*SAMPLE_RATE,SAMPLE_RATE/5.0)); //if we assume that sounds 1 metre away will last one second, and then keep the same number of waves
                //TODO: above calculation is weird

		pcl::PointCloud<pcl::PointXYZ>::Ptr sound_sources (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointXYZ point;
		point.x = representative_point.x;
		point.y = representative_point.y;
		point.z = representative_point.z;
		double r = 0.0005/representative_point.curvature;
		double red = 0.5;
		double green = 0.5;
		double blue = 0.0;
		pcl::PointXYZ controlPoint;
		controlPoint.x = point.x;
		controlPoint.y = point.y;
		controlPoint.z = point.z;
		
		sound_sources->points.push_back(point);
		if(isFirst){
		  viewer->addSphere(sound_sources->points[0], r, red, green, blue, "sphere");
		}
		else{
		  viewer->updateSphere(sound_sources->points[0], r, red, green, blue, "sphere");
		}
		
		if(!(controlPoint.x == point.x && controlPoint.y == point.y && controlPoint.z == point.z)){
		  isClosestPointChanged = true;
		}
		
		isFirst = false;
		
                if(!use_curvature_histogram) { //single frequency
                    
		    double freq=curvature_to_freq(average_curvature(representative_point.curvature));
		    //double freq=curvature_to_freq(representative_point.curvature);
                    if(verbose>0) {

                        std::cout<<"Location: "<<std::setprecision(3)<<sourcePos[0]<<","<<std::setprecision(3)<<sourcePos[1]<<","<<std::setprecision(3)<<sourcePos[2];

                        std::cout<<"   curvature "<<representative_point.curvature<<" -- > "<<freq<<" freq";


                        std::cout <<"   seconds passed:"<<seconds_passed;
                        std::cout<<"   dist_travelled: "<<dist_travelled<<" dist: "<<dist<<std::endl;
                    }


                    fill_sample(freq,samples.data(),buf_size,attenuation,0);
                }
                else {//more complex sounds
                    double num_freqs=curvatures.size();
//                     double new_attenuation=attenuation/num_freqs;
                    if(verbose>0) {

                        std::cout<<"Location: "<<std::setprecision(3)<<sourcePos[0]<<","<<std::setprecision(3)<<sourcePos[1]<<","<<std::setprecision(3)<<sourcePos[2];


                        std::cout <<"   seconds passed:"<<seconds_passed;
                        std::cout<<"   dist_travelled: "<<dist_travelled<<" dist: "<<dist;
                    }

                    for(int f=0; f<num_freqs; f++) {
                        double freq=curvature_to_freq(curvatures[f]);
                        fill_sample_add(freq,samples.data(),buf_size,attenuation/num_freqs,0);

                        std::cout<<" [curvature "<<curvatures[f]<<" -- > "<<freq<<" freq]";

                    }
                    std::cout<<std::endl;
                }

                //stop it if it's already playing
                alSourceStop(sources[nextbuffer]);
                if ((error=alGetError()) != AL_NO_ERROR)
                {
                    std::cout<<"couldn't stop playing\n"<<error<<std::endl;;
                    //TODO: suppress this error for the first few calls as there won't be anything queued

                }

// 			alSourcei(sources[nextbuffer], AL_BUFFER, NULL);
// 			if ((error=alGetError()) != AL_NO_ERROR)
// 			    {
// 			      std::cout<<"couldn't remove buffers from source\n"<<error<<std::endl;;
// 			      //TODO: suppress this error for the first few calls as there won't be anything queued
//
// 			    }
//                         if(num_started>=2) {
// 			  std::cout<<"dequeuing buffer "<<nextbuffer<<std::endl;


                // should produce an error on the first time through
                alSourceUnqueueBuffers(sources[nextbuffer], 1, &(buffers[nextbuffer]));
                if ((error=alGetError()) != AL_NO_ERROR)
                {
                    std::cout<<"couldn't unqueue buffer\n"<<error<<std::endl;;
                    //TODO: suppress this error for the first few calls as there won't be anything queued

                }

                alBufferData(buffers[nextbuffer], FORMAT, samples.data(), buf_size*BYTES_PER_SAMPLE, SAMPLE_RATE);
                if ((error=alGetError()) != AL_NO_ERROR)
                {
                    std::cout<<"couldn't buffer data\n"<<error<<std::endl;;
                    played=0;
                }
                else {

                    if(verbose>15)std::cout<<"enqueuing buffer "<<nextbuffer<<std::endl;
                    alSourceQueueBuffers(sources[nextbuffer], 1, &(buffers[nextbuffer]));

                    if ((error=alGetError()) != AL_NO_ERROR)
                    {
                        std::cout<<"couldn't queue buffer\n"<<error<<std::endl;;
                        played=0;
                    }

                }
                // POSITION OF THE SOUND SOURCE
                alSourcefv( sources[nextbuffer],AL_POSITION,sourcePos);
                if ((error=alGetError()) != AL_NO_ERROR)
                {
                    std::cout<<"couldn't set position\n"<<error<<std::endl;;

                }

                alSourcePlay(sources[nextbuffer]);
                if ((error=alGetError()) != AL_NO_ERROR)
                {
                    std::cout<<"couldn't play source\n"<<error<<std::endl;;

                }

                nextbuffer = (nextbuffer + 1)%numsources;

                released_time = std::chrono::high_resolution_clock::now(); // send a new projectile

            }
        }

        return played;
    }

    float average_curvature(float representative_point_curvature) {
	if(isClosestPointChanged == true){
	  sum_of_curvatures = 0;
	  curvature_cnt = 0;
	  moving_average_list.clear();
	  isClosestPointChanged = false;
	}
      
	if(curvature_cnt < 200) {
	    moving_average_list.push_back(representative_point_curvature);
	    sum_of_curvatures += representative_point_curvature;
	    curvature_cnt++;
	    cout << endl << "representative_point_curvature: " << representative_point_curvature << endl << "sum_of_curvatures: " << sum_of_curvatures << endl << "curvature_cnt: " << curvature_cnt << endl;
	    cout << "moving_average_list last added: " << moving_average_list.back() << endl;
	}
	else {
	    sum_of_curvatures = sum_of_curvatures + representative_point_curvature - moving_average_list.front();
	    moving_average_list.pop_front();
	    moving_average_list.push_back(representative_point_curvature);
	    cout << endl << "representative_point_curvature: " << representative_point_curvature << endl << "sum_of_curvatures: " << sum_of_curvatures << endl << "curvature_cnt: " << curvature_cnt << endl;
	    cout << "moving_average_list last added: " << moving_average_list.back() << endl << "moving_average_list deleted element: " << moving_average_list.front() << endl;
	}
	cout << "average_curvature: " << sum_of_curvatures/curvature_cnt << endl;
	return sum_of_curvatures/curvature_cnt;
    }

};
double SceneSource::sum_of_curvatures = 0;
int SceneSource::curvature_cnt = 0;

//boost::shared_ptr<pcl::visualization::PCLVisualizer> pclVis ( boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer ){
 // return viewer;
//}

bool first_time = true;

/** process a cloud, calculate normals, send it to all the sound sources to generate a source*/
void grabbed (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud) {

    double st=timer.elapsed();
  
    pcl::PointCloud<pcl::Normal>::Ptr normal_cloud(new pcl::PointCloud<pcl::Normal>);    

    //------------------- statistical outlier filter starts here ------------------
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setKeepOrganized(true);
    sor.setInputCloud(cloud);
    sor.setMeanK(10);
    sor.setStddevMulThresh(1.0);
    sor.filter(*cloud_filtered);
    //-----------------------------------------------------------------------------
    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
    ne.setMaxDepthChangeFactor(MaxDepthChangeFactor);
    ne.setNormalSmoothingSize(NormalSmoothingSize);
    ne.setInputCloud(cloud_filtered);
    ne.compute(*normal_cloud);
    
    pcl::PointCloud<pcl::PointNormal>::Ptr point_normal_ptr(new pcl::PointCloud<pcl::PointNormal>);
    pcl::copyPointCloud(*cloud_filtered,*point_normal_ptr);
    pcl::copyPointCloud(*normal_cloud,*point_normal_ptr);
    
    viewer->setBackgroundColor (0, 0, 0);
   // viewer->addCoordinateSystem (0.001);
    viewer->setSize(1024,768);
    
    pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointNormal> normal_cloud_color_handler (point_normal_ptr, "curvature");
    if(!normal_cloud_color_handler.isCapable()){
      PCL_WARN ("Cannot create curvature color handler!");
    }
    
    if(first_time == true){
	viewer->addPointCloud<pcl::PointXYZ> (cloud_filtered, "cloud");
//	viewer->addPointCloud<pcl::PointNormal>(point_normal_ptr, normal_cloud_color_handler, "curvature");
    }
    else{
	viewer->updatePointCloud<pcl::PointXYZ>(cloud_filtered, "cloud");
//	viewer->updatePointCloud<pcl::PointNormal>(point_normal_ptr, normal_cloud_color_handler, "curvature");
    }
    
    if (!viewer->wasStopped ())
    {
	viewer->spinOnce (1);
	//boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
    else{
	exit(0);
    }
    
    first_time = false;
    
    //pclVis(viewer);
    
    if(verbose>1) {
        int num_nan = 0;
        int num_number=0;
        int num_number_c=0;
        int num_nan_c=0;
        for(unsigned int i=0; i<normal_cloud->size(); i++) {
            if(verbose>10)std::cout<<"raw points: "<<cloud_filtered->points[i]<<std::endl;;
            if(pcl::isFinite(normal_cloud->points[i]))num_number++;
            else num_nan++;
            if(pcl::isFinite(cloud_filtered->points[i]))num_number_c++;
            else num_nan_c++;
        }
        std::cout<<" normal estimation: num_number: "<<num_number<<" num_nan: "<<num_nan<<" num_number_c: "<<num_number_c<<" num_nan_c: "<<num_nan_c<<std::endl;
    }

    int num_valid_centroids=soundwidth*soundheight; //leftover var name, used to be 2-step process

    int num_played=0;
    double attenuation=1.0/std::max(16,soundwidth*soundheight);
    //double attenuation=1.0;

    for(int sY=0; sY<soundheight; sY++) {
        for(int sX=0; sX<soundwidth; sX++) {

            int pld=sources[sY][sX]->proc_sound(cloud_filtered,normal_cloud,attenuation);
            num_played+=pld;
        }
    }
    double en=timer.elapsed();
    std::chrono::high_resolution_clock::time_point now  = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::duration diff = now-start_time;
    std::chrono::milliseconds diffmilli=std::chrono::duration_cast<std::chrono::milliseconds>(diff);
    double so_far_milliseconds=diffmilli.count();
    double so_far_seconds = so_far_milliseconds/1000;
    if(verbose>1)std::cout<<"Processing time: "<<std::setprecision(8)<<std::setw(12)<<(en-st)<<" seconds ---- Num of sectors processed: "<<std::setprecision(1)<<std::setw(5)<<(num_played)<<"/"<<std::setprecision(1)<<std::setw(5)<<(num_valid_centroids)<<" ---- elapsed: "<<std::setw(7)<<std::setprecision(3)<<so_far_seconds <<" seconds"<<std::endl;

}


/** ds grabber does not support lower resolutions but I have fixed resolution to 160x120 for this app. ds supports 320x240. 
 * I wonder if there is a PCL class that already does this... 
 **/
void grabbed_downsample2 (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2;
    cloud2.reset(new pcl::PointCloud<pcl::PointXYZ>);
    cloud2->resize(cloud->height/2 * cloud->width/2);
    cloud2->width=cloud->width/2;
    cloud2->height=cloud->height/2;

    for(unsigned int y=0; y<cloud2->height; y++) {
        for(unsigned int x=0; x<cloud2->width; x++) {

            unsigned int original_ind[]= {
                (y*2+0)*cloud->width+x*2+0,
                (y*2+0)*cloud->width+x*2+1,
                (y*2+1)*cloud->width+x*2+0,
                (y*2+1)*cloud->width+x*2+1
            };

            pcl::PointXYZ sum;
            sum.x=0;
            sum.y=0;
            sum.z=0;
            int num_used=0;
            for(int ind=0; ind<4; ind++) {
                int i = original_ind[ind];
                if (pcl::isFinite(cloud->points[i])) {
                    sum.x+=cloud->points[i].x;
                    sum.y+=cloud->points[i].y;
                    sum.z+=cloud->points[i].z;
                    num_used++;
                }
            }
            int newind=y*cloud2->width+x;
            cloud2->points[newind].x=sum.x/num_used;
            cloud2->points[newind].y=sum.y/num_used;
            cloud2->points[newind].z=sum.z/num_used;

        }
    }
    grabbed(cloud2); // go ahead as per normal with the downsampled cloud

}

po::variables_map args ( int argc, char** argv )
{
    po::options_description desc ( "Proto Viewer" );
    desc.add_options()
    ( "help,h","Get help. You probably need it. But not that kind of help." )
    ( "verbose,v",po::value<int>()->default_value ( 0 ),"Verbosity level." )
    
    ( "filename",po::value<std::string>()->default_value (""),"Input grabber option: Read from a file." )
    ( "openni","Input grabber option: Use the OpenNI grabber (Xtion/Kinect)." )
    ( "openni2","Input grabber option: Use the OpenNI2 grabber (Xtion/Kinect)." )
    ( "depthsense","Input grabber option: Use the DepthSense grabber." )
    
    ( "soundwidth,sw",po::value<int>()->default_value ( 1 ),"Width of sound grid." )
    ( "soundheight,sh",po::value<int>()->default_value ( 1 ),"Height of sound grid." )
    ( "projectile_velocity",po::value<double>()->default_value ( 1.0 ),"Projectile velocity in meters per second." )
    ( "use_curvature_histogram","Multi sounds" )
    //---ADD OPTION FOR FILTER AS ABOVE
    ( "MaxDepthChangeFactor",po::value<double>()->default_value ( 0.02f ),"MaxDepthChangeFactor - integral image normal estimation parameter - see PCL API." )
    ( "NormalSmoothingSize",po::value<double>()->default_value ( 10.0f ),"NormalSmoothingSize - integral image normal estimation parameter - see PCL API." )
    ( "DepthDependentSmoothing",po::value<bool>()->default_value ( true ),"DepthDependentSmoothing - integral image normal estimation parameter - see PCL API." )

    ;

    po::positional_options_description p;
//     p.add ( "filenames", 15 );
//   p.add("target_filename", 1);
//   p.add("keypoint_type", 1);
//   p.add("descriptor_type", 1);

    po::variables_map vm;
    po::store ( po::command_line_parser ( argc, argv ).options ( desc ).positional ( p ).run(),vm );

    if ( vm.count ( "help" ) )
    {
        std::cout << desc << "\n";
    }
    po::notify ( vm );

    return vm;
}


int main(int argc,char ** argv) {

    start_time= std::chrono::high_resolution_clock::now();

    po::variables_map vm=args ( argc,argv );

    bool openni=false;
    bool openni2=false;
    bool depthsense=false;
    bool fromfile=false;

    std::string filename;

    verbose = vm["verbose"].as<int>();
    if ( !vm["openni"].empty() ) openni=true;
    if ( !vm["openni2"].empty() ) openni2=true;
    if ( !vm["depthsense"].empty() ) depthsense=true;
    if ( !vm["use_curvature_histogram"].empty()) use_curvature_histogram=true;
    else use_curvature_histogram=false;
      //---ADD OPTION FOR FILTER AS ABOVE
    if ( !vm["DepthDependentSmoothing"].empty()) DepthDependentSmoothing=true;
    else DepthDependentSmoothing=false;

    if ( vm["filename"].as<std::string>() !="" ) {
        fromfile=true;
        filename=vm["filename"].as<std::string>();
    }

    MaxDepthChangeFactor = vm["MaxDepthChangeFactor"].as<double>();
    NormalSmoothingSize = vm["NormalSmoothingSize"].as<double>();
    soundwidth = vm["soundwidth"].as<int>();
    soundheight = vm["soundheight"].as<int>();
    projectile_velocity = vm["projectile_velocity"].as<double>();

    if(!openni && !depthsense && !fromfile && !openni2) {
        std::cerr<<"Give me a point cloud source"<<std::endl;
        exit(1);
    }
    if(openni && depthsense ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }
    
    if(openni2 && depthsense ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }
    
    if(openni2 && openni) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }
    
    if(openni2 && fromfile) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(depthsense && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    if(openni && fromfile ) {
        std::cerr<<"Give me only one point cloud source"<<std::endl;
        exit(1);
    }

    MaxDepthChangeFactor = vm["MaxDepthChangeFactor"].as<double>();
    NormalSmoothingSize = vm["NormalSmoothingSize"].as<double>();

    boost::shared_ptr<pcl::Grabber> grabber;
    boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f ;
    
    if(openni) {
      
        grabber.reset(new pcl::OpenNIGrabber("",pcl::OpenNIGrabber::OpenNI_QQVGA_30Hz));
        f= boost::bind (&grabbed, _1);

      
    }else if(openni2) {
#ifdef WITH_OPENNI2
      
        grabber.reset(new pcl::io::OpenNI2Grabber("",pcl::io::OpenNI2Grabber::OpenNI_QQVGA_30Hz));
        f= boost::bind (&grabbed, _1);	

#else
        std::cerr<<"Openni2 not linked."<<std::endl;
        exit(1);
#endif

	
    } else if(fromfile) {

        grabber.reset(new pcl::PCDGrabber<pcl::PointXYZ>(filename));
        f= boost::bind (&grabbed, _1);
	
    } else if(depthsense) {
      
#ifdef WITH_DEPTHSENSE_DS
        grabber.reset(new pcl::DepthSenseGrabber(""));
        f = boost::bind (&grabbed_downsample2, _1);
#else
        std::cerr<<"Depthsense grabber (ds) not linked."<<std::endl;
        exit(1);
#endif
	
    } else {
        std::cerr<<"Not implemented. Sorry."<<std::endl;
        exit(1);
    }

    ALenum error;
    alutInit(0,NULL);

    if ((error=alGetError()) != AL_NO_ERROR)
    {
        std::cout<<"no init alut s"<<error<<"\n"<<std::endl;
        return 1;
    }

    int impersX=IMWIDTH/soundwidth;
    int impersY=IMHEIGHT/soundheight;
    int numgrids=soundwidth*soundheight;
    int sources_per_grid=std::min(MAX_SOURCES/numgrids,MAX_SOURCES_PER_GRID);

    sources.resize(soundheight);

    for(int sY=0; sY<soundheight; sY++) {

      sources[sY].resize(soundwidth);
      
      for(int sX=0; sX<soundwidth; sX++) {

            int xstart=sX*impersX;
            int xend=(sX+1)*impersX-1;
            int ystart=sY*impersY;
            int yend=(sY+1)*impersY-1;
            sources[sY][sX].reset(new SceneSource(sources_per_grid,IMWIDTH,IMHEIGHT,xstart,xend,ystart,yend));

        }
    }

    grabber->registerCallback (f);
    grabber->start ();
   
    while(grabber->isRunning()) {
        alutSleep(1);
    }

    grabber->stop();

    alutExit();

    exit(0);
}
