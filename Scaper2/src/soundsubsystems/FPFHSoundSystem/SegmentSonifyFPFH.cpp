#include <soundsubsystems/FPFHSoundSystem/SegmentSonifyFPFH.h>


SegmentSonifyFPFH::SegmentSonifyFPFH(double fpfh_r, boost::shared_ptr< pcl::visualization::PCLHistogramVisualizer > viewer, boost::mutex& mutex, std::string path_to_fpfh_database,double multiplier):fpfh_radius(fpfh_r)
{
 this->fpfh_radius= fpfh_r;
 this->hist_viewer= viewer;
 this->viewer_mutex= &mutex;
 this->multiplier= multiplier;
 
 extractor=new PCLFeaturesExtractorFPFH(fpfh_radius,hist_viewer,*viewer_mutex);
 soundGen=new SoundGeneratorFPFH(path_to_fpfh_database,hist_viewer,*viewer_mutex);
}


void SegmentSonifyFPFH::Execute(std::vector< PCLObject::Ptr > objects)
{ 
  extractor->Execute(objects);
  soundGen->setExaggeration(multiplier);
  soundGen->Execute(objects);
  
}