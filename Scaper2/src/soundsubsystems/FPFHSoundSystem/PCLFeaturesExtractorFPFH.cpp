#include <soundsubsystems/FPFHSoundSystem/PCLFeaturesExtractorFPFH.h>
#include <pcl/point_types.h>
#include <pcl/features/fpfh.h>
// #include <PCLObjectsTracker.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/histogram_visualizer.h>
#include<stdlib.h>
#include <ctime>

int fevvvFPFH=4;

PCLFeaturesExtractorFPFH::PCLFeaturesExtractorFPFH(double fpfh_radius,boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> v,boost::mutex& mutex):fpfh_radius(fpfh_radius)
{
    this->prev_width=0;
    this->prev_height=0;
    this->prev_depth=0;
    this->hist_viewer=v;
    this->viewer_mutex=&mutex;

}

void PCLFeaturesExtractorFPFH::Execute(std::vector<PCLObject::Ptr> objects) {


    for (size_t i = 0; i < objects.size(); i++)
    {

        int start_a=clock();
        objects[i]->fpfh= CalculateFPFH(objects[i]);
        int stop_a=clock();
        if(fevvvFPFH>3)cout << "PCLFeaturesExporterFPFH: time total: " << (stop_a-start_a)/double(CLOCKS_PER_SEC)*1000 << endl;

        DisplayHistogram(objects[i]->fpfh);

        float maxLeftPoint=0;
        float maxRightPoint=0;
        float maxTopPoint=0;
        float maxBottomPoint=0;
        float maxNearestPoint=0;
        float maxFurthestPoint=0;
        bool firstTime=true;


        for(size_t j=0; j < objects[i]->objectCloud->points.size (); j++)
        {

            if(pcl::isFinite(objects[i]->objectCloud->points[j])) {
                if(firstTime==true) {
                    maxLeftPoint=objects[i]->objectCloud->points[j].x;
                    maxRightPoint=objects[i]->objectCloud->points[j].x;
                    maxTopPoint=objects[i]->objectCloud->points[j].y;
                    maxBottomPoint=objects[i]->objectCloud->points[j].y;
                    maxNearestPoint=objects[i]->objectCloud->points[j].z;
                    maxFurthestPoint=objects[i]->objectCloud->points[j].z;
                    firstTime=false;
                    continue;
                }
                if(objects[i]->objectCloud->points[j].x < maxRightPoint) {
                    maxRightPoint=objects[i]->objectCloud->points[j].x;
                }
                if(objects[i]->objectCloud->points[j].x > maxLeftPoint) {
                    maxLeftPoint=objects[i]->objectCloud->points[j].x;
                }
                if(objects[i]->objectCloud->points[j].y < maxBottomPoint) {
                    maxBottomPoint=objects[i]->objectCloud->points[j].y;
                }
                if(objects[i]->objectCloud->points[j].y > maxTopPoint) {
                    maxTopPoint=objects[i]->objectCloud->points[j].y;
                }
                if(objects[i]->objectCloud->points[j].z < maxNearestPoint) {
                    maxNearestPoint=objects[i]->objectCloud->points[j].z;
                }
                if(objects[i]->objectCloud->points[j].z > maxFurthestPoint) {
                    maxFurthestPoint=objects[i]->objectCloud->points[j].z;
                }
            }
        }
        if(prev_depth==0) {
            this->prev_width=objects[i]->width=maxLeftPoint - maxRightPoint;
            this->prev_height=objects[i]->height=maxTopPoint - maxBottomPoint;
            this->prev_depth=objects[i]->depth=maxFurthestPoint - maxNearestPoint;
        } else {
            this->prev_width=objects[i]->width=(maxLeftPoint - maxRightPoint)* 0.2 + 0.8 * this->prev_width;
            this->prev_height=objects[i]->height=(maxTopPoint - maxBottomPoint)* 0.2 + 0.8 * this->prev_height;
            this->prev_depth=objects[i]->depth=(maxFurthestPoint - maxNearestPoint) * 0.2 + 0.8 * this->prev_depth;
        }
        objects[i]->size = objects[i]->width * objects[i]->height * objects[i]->depth;

    }

}



pcl::FPFHSignature33 PCLFeaturesExtractorFPFH::CalculateFPFH(PCLObject::Ptr object)
{
    pcl::PointCloud<SystemPoint>::Ptr modified_cloud (new pcl::PointCloud<SystemPoint>);
    if(fevvvFPFH>3)std::cout<<"PCLFeaturesExporterFPFH: Cloudsize: "<<object->objectCloud->points.size() <<std::endl;
    pcl::PointCloud<pcl::Normal>::Ptr modified_normals(new pcl::PointCloud<pcl::Normal>);

    if(object->objectCloud->points.size()<450)
    {
        ///without downSampling////////

        for (size_t i = 0; i < object->objectCloud->points.size(); i++)

        {
            if (pcl::isFinite<pcl::Normal>(object->normalCloud->points[i]) && pcl::isFinite<SystemPoint>(object-> objectCloud->points[i]))

            {
                modified_normals->points.push_back(object->normalCloud->points[i]);
                modified_cloud->points.push_back(object->objectCloud->points[i]);
            }
        }
    }
    else
    {
        /////////////// down sampling////////////////
        pcl::PointCloud<SystemPoint>::ConstPtr input (new pcl::PointCloud<SystemPoint>);
        input=object->objectCloud;
        std::vector<int> indices;
        // int sample=object->objectCloud->points.size()/3;
        int sample=150;
        pcl::RandomSample<SystemPoint> randomSample;
        randomSample.setInputCloud(input);
        randomSample.setSample(sample);
        randomSample.setSeed(rand());
        randomSample.filter(indices);

        for(size_t i=0; i<indices.size(); i++)
        {
            if (pcl::isFinite<pcl::Normal>(object->normalCloud->points[indices[i]]) && pcl::isFinite<SystemPoint>(object-> objectCloud->points[indices[i]]))
            {
                modified_normals->points.push_back(object->normalCloud->points[indices[i]]);
                modified_cloud->points.push_back(object->objectCloud->points[indices[i]]);
            }
        }


        if(fevvvFPFH>3)std::cout <<"PCLFeaturesExporterFPFH: after down sampling: "<<indices.size() <<std::endl;
    }

    Eigen::Vector4f modified_centroid;
    pcl::compute3DCentroid(*modified_cloud, modified_centroid);

    /////fpfh//////
    pcl::FPFHEstimation<SystemPoint, pcl::Normal, pcl::FPFHSignature33> fpfh;
    fpfh.setSearchSurface(modified_cloud);
    fpfh.setInputNormals (modified_normals);
    pcl::PointCloud<SystemPoint>::Ptr keypoint (new pcl::PointCloud<SystemPoint>);
    SystemPoint key;
    key.x=modified_centroid[0];
    key.y=modified_centroid[1];
    key.z=modified_centroid[2];
    keypoint->points.push_back(key);
    fpfh.setInputCloud (keypoint);

    pcl::PointCloud<pcl::FPFHSignature33>::Ptr fpfh_output (new pcl::PointCloud<pcl::FPFHSignature33> ());
    fpfh.setRadiusSearch (fpfh_radius);

    int start_s=clock();

    fpfh.compute (*fpfh_output);

    int stop_s=clock();
    if(fevvvFPFH>3)std::cout <<"PCLFeaturesExporterFPFH: time compute: " << (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000 << endl;


    return fpfh_output->points[0];
}

void PCLFeaturesExtractorFPFH::DisplayHistogram(pcl::FPFHSignature33 object_fpfh)
{   if(this->hist_viewer)
    {
        pcl::PointCloud< pcl::FPFHSignature33 >::Ptr fpfh (new pcl::PointCloud<pcl::FPFHSignature33>);
        fpfh->points.push_back(object_fpfh);
        viewer_mutex->lock();
        const std::string id="fpfh of scene";
//   const std::string field="FPFH hist";
        hist_viewer->setBackgroundColor(255,255,255);
        if(!hist_viewer->updateFeatureHistogram(*fpfh,33,id))
            hist_viewer->addFeatureHistogram(*fpfh, 33 , id, 640, 200);
        hist_viewer-> updateWindowPositions();
        viewer_mutex->unlock();
    }
}


