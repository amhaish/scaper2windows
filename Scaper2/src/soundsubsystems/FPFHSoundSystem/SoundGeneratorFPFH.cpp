#include <soundsubsystems/FPFHSoundSystem/SoundGeneratorFPFH.h>
#include <pcl/visualization/histogram_visualizer.h>
// #include <boost/graph/graph_concepts.hpp>
#include <iostream>
#include <string>
#include <fstream>

int sgvvvFPFH = 4;

#define DEFAULT_FPS 15
#define BUFFER_SIZE 44100*10

#define NUM_OBJECTS 1


//TODO: put it somewhere better
/*std::ostream& operator<<(std::ostream& stream,
                         const PCLObject& obj) {
    stream<<"<<PCLObject: key:"<<std::setw(5)<<obj.key;
    stream<<" width:"<<obj.width;
    stream<<" height:"<<obj.height;
    stream<<" depth:"<<obj.depth;
    stream<<" size:"<<obj.size;
    stream<<" distance:"<<obj.distance;
    stream<<" centroid:"<<obj.centroid[0]<<","<<obj.centroid[1]<<","<<obj.centroid[2];
    stream<<" cloud-size:"<<obj.objectCloud->size();
    stream<<">>";
    return stream;

}*/

SoundGeneratorFPFH::SoundGeneratorFPFH(std::string path_to_fpfh_database,boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> vis,boost::mutex& mutex):path_to_fpfh_database(path_to_fpfh_database) { // 44100*10 = 10 sec worth
//   SoundGenerator::SoundGenerator():si(5),last_objects(0){

    this->hist_viewer=vis;
    this->viewer_mutex=&mutex;

    while(ssg.size()<NUM_OBJECTS) {
        ssg.push_back(SoundSourceGeneratorSTK(DEFAULT_FPS));
        csb.push_back(CircularBuffer<short>(BUFFER_SIZE));
        last_pos.push_back(Eigen::Vector4f());
    }

    while(sso.size()<NUM_OBJECTS) {
        sso.push_back(SoundSourceOutput(DEFAULT_FPS));
    }
    
    ifstream file(path_to_fpfh_database+"/list.txt");
    if(!file)
    {
	cerr<<"file not found!"<<std::endl;
	exit(1);
    }
    std::string ObjectName;
    std::vector<std::string> ObjectsNameList;
    while(std::getline(file,ObjectName))
    {
	object_names_list.push_back(ObjectName);
    }
    file.close();
    
    pcl::PCDReader reader;
    FPFHDataset.reset(new pcl::PointCloud<pcl::FPFHSignature33> ());
    
    reader.read (path_to_fpfh_database+"/CloudOfFPFHs.pcd", *FPFHDataset);    
    kdtree.setInputCloud (FPFHDataset);
    

}
void SoundGeneratorFPFH::setExaggeration(double multiplier) {
    exaggerate_pos=multiplier;
}

double SoundGeneratorFPFH::randd() {
    return ((double)rand())/RAND_MAX;
}
void SoundGeneratorFPFH::Execute(std::vector<PCLObject::Ptr> objects) {
    Eigen::Vector4f pos_before_exaggeration(0,0,0,0);
    Eigen::Vector4f pos_after_exaggeration(0,0,0,0);

//     while(objects.size()>ssg.size() && ssg.size()<10){
//       ssg.push_back(SoundSourceGenerator(DEFAULT_FPS));
//       sso.push_back(SoundSourceOutput(DEFAULT_FPS));
//       csb.push_back(CircularBuffer<short>(BUFFER_SIZE));
//       last_pos.push_back(Eigen::Vector4f());
//     }
//     std::vector<SoundFeature> new_last_played;
    if(objects.size()>0) {
        for(unsigned int i=0; i<objects.size() ; i++)
        {
                      //             if(sgvvvFPFH>2)std::cout<<"Object[" << i << "]:"<<*objects[i]<<std::endl;
// 	if(objects[i]->width * objects[i]->height > 0.00001 && objects[i]->size < 0.1)
// 	{
            if(objects[i]->track_id>=NUM_OBJECTS) {

                std::cerr<<"Warning: Sound generation not yet set up to deal with object ids more than "<<NUM_OBJECTS<<" (got "<<objects[i]->track_id+1<<")"<<std::endl;

            }
            else {

                uint oid=objects[i]->track_id;
                SoundFeature sfeat;
                pos_before_exaggeration=objects[oid]->centroid;

                for(size_t ii=0; ii<2; ii++)
                    pos_after_exaggeration[ii]=pos_before_exaggeration[ii]*this->exaggerate_pos;

                sfeat.pos=pos_after_exaggeration;
                //
                float sizeBaseOctave=440; //The size will 1 octave from 440 to 880
                float sizeMinValue=0.001;
                float sizeMaxValue=0.1;
                float SizePercentMinValue=1;
                float SizePercentMaxValue=2;
                float sizePercent = (((objects[oid]->size - sizeMinValue)*(SizePercentMaxValue-SizePercentMinValue)) / (sizeMaxValue-sizeMinValue)) + SizePercentMinValue;

                float curvatureBaseOctave=1760;
                //float curvaturePercent=;

                if(sgvvvFPFH>3)std::cout<<"Size percent is: "<<sizePercent<<"\n";
                float fw1=330/(2*objects[oid]->width);
                float fh1=330/(2*objects[oid]->height);
                float fd1=330/(2*objects[oid]->depth);




                /* objects[oid]->fpfhs->points[0].histogram[28]>9
                objects[oid]->fpfhs->points[0].histogram[32]<0.5
                objects[oid]->fpfhs->points[0].histogram[31]<5 */
///////////////////////////////////////////////////////7basic search/////////////////////////
                /*    if(objects[oid]->fpfhs->points[0].histogram[16]>65)
                    {
                      sfeat.objectType= "box";

                    }
                    else if(objects[oid]->fpfhs->points[0].histogram[28]<8)
                    {
                      sfeat.objectType="cup";

                    }
                    else
                    {
                       sfeat.objectType="dd";

                    }*/
/////////////////////////////////////////////////////////////////////////////////////////////////


                pcl::FPFHSignature33 searchPoint;
                searchPoint = objects[oid]->fpfh;
                pcl::PointCloud<pcl::FPFHSignature33>::Ptr SearchPointFPFH (new pcl::PointCloud<pcl::FPFHSignature33> ());
                SearchPointFPFH->points.push_back(searchPoint);

                // K nearest neighbor search
                // for K values more than 1, we need to change code!(because just the last neighbor will be sent to SoundSourceGenerator)

                int K = 1;
                std::vector<int> pointIdxNKNSearch(K);
                std::vector<float> pointNKNSquaredDistance(K);

                std::cout << "K nearest neighbor search for QueryFPFH with K=" << K << std::endl;

                if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
                {
                    for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
                    {
//                         pcl::PointCloud<pcl::FPFHSignature33>::Ptr FPFH (new pcl::PointCloud<pcl::FPFHSignature33> ());
                        if(sgvvvFPFH>4)std::cout<< "********************** the index is:" << pointIdxNKNSearch[i] << std::endl;

//                         FPFH->points.push_back(FPFHDataset->points[ pointIdxNKNSearch[i]]);

                        if(pointNKNSquaredDistance[0]>2500)
                            sfeat.objectType= "unknown";
                        else
                            sfeat.objectType=object_names_list[pointIdxNKNSearch[i]];

                        if(sgvvvFPFH>2)std::cout<< "the object is : " <<object_names_list[pointIdxNKNSearch[i]]<<std::endl;
                        /*
                              if(this->hist_viewer)
                               { viewer_mutex->lock();
                        	 DisplayHistogram(SearchPointFPFH,"object","");
                                 viewer_mutex->unlock();
                               }

                              if(this->hist_viewer)
                               { viewer_mutex->lock();
                                 DisplayHistogram(FPFH,"result",ObjectsList[5]);
                                 viewer_mutex->unlock();
                               }
                        */
                    }
                }else sfeat.objectType= "unknown";


                // 	sfeat.frequencies.push_back(sizeBaseOctave * sizePercent);
                sfeat.frequencies.push_back(fw1/2);
                sfeat.frequencies.push_back(fw1/2+rand()*0.0001);
                sfeat.frequencies.push_back(fh1+rand()*0.0001);
                // 	sfeat.frequencies.push_back(fd1);
                // 	sfeat.frequencies.push_back(fw1/2);
                // 	sfeat.frequencies.push_back(fh1/2);
                sfeat.frequencies.push_back(fd1/4+rand()*0.0001);

                //sfeat.frequencies.push_back(1000/(objects[0]->centroid.norm() * objects[0]->centroid.norm()));


                double ratio = log(objects[oid]->width / objects[oid]->height);
                if(ratio  > 0) {
                    if(ratio >0.9)ratio=0.9;
                    if(sgvvvFPFH>3)std::cout<<"ratio:"<<ratio<<std::endl;
                    SoundFeature sfeatold=ssg[oid].get_last_played_sound_feature();
                    for(unsigned int fno=0; fno<sfeat.frequencies.size()&&fno<sfeatold.frequencies.size(); fno++) {
                        sfeat.frequencies[fno]=sfeat.frequencies[fno]*(1-ratio)+sfeatold.frequencies[fno]*ratio;
                    }
                }


                double moved_dist;
                if(last_pos[oid].norm()>0)
                    moved_dist=(pos_before_exaggeration - last_pos[oid]).norm();
                else
                    moved_dist=5;
                if(moved_dist<0.8)moved_dist=0.8;

                sfeat.energy_mult=0.1/moved_dist;
                sfeat.energy_mult=0.05;

                sfeat.spread_mult=1.0;

                ssg[oid].generate_sound(sfeat,csb[oid]);

                if(sgvvvFPFH>0)std::cout<<"SoundGenerator::Execute - Playing "<<sfeat<<std::endl;
                last_pos[oid]=pos_before_exaggeration;
            }
// 	}
        }
    }

    for(unsigned int i=0; i<sso.size(); i++)
        sso[i].output(csb[i],last_pos[i]);
}



void SoundGeneratorFPFH::DisplayHistogram(pcl::PointCloud< pcl::FPFHSignature33 >::Ptr fpfh, std::string fpfhID,std::string name)
{
    std::string id_1 ("result");
//   std::string id_2 ("object");

    if (fpfhID.compare(id_1) != 0)
    {   const std::string id= "Search result :"+ name;
// //       const std::string field="nearest object fpfh histogram";
        this->hist_viewer->setBackgroundColor(255,255,255);
        if(!hist_viewer->updateFeatureHistogram(*fpfh,33,id))
            hist_viewer->addFeatureHistogram(*fpfh, 33 , id, 640, 200);
        hist_viewer-> updateWindowPositions();
    }

    else
    {   const std::string id= "detected";
//       const std::string field="search point fpfh histogram";
        this->hist_viewer->setBackgroundColor(255,255,255);
        if(!hist_viewer->updateFeatureHistogram(*fpfh,33,id))
            hist_viewer->addFeatureHistogram(*fpfh, 33 , id, 640, 200);
        hist_viewer-> updateWindowPositions();
    }
}
