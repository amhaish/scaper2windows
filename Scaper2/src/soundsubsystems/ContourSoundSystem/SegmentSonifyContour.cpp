#include<soundsubsystems/ContourSoundSystem/SegmentSonifyContour.h>

SegmentSonifyContour::SegmentSonifyContour(double multiplier,double sound_z_offset, bool depth_normalization)
{

  multiplier_= multiplier;
  sound_z_offset_=sound_z_offset;
  depth_normalization_=depth_normalization;
  extractor=new PCLFeaturesExtractorContour();
  soundGen=new SoundGeneratorContour(depth_normalization_);
  soundGen->setExaggeration(multiplier_);
  soundGen->setZOffset(sound_z_offset_);
}


void SegmentSonifyContour::Execute(std::vector< PCLObject::Ptr > objects)
{
  extractor->Execute(objects);
  soundGen->Execute(objects);
}
