
 #include <soundsubsystems/ContourSoundSystem/PCLFeaturesExtractorContour.h>
#include <pcl/point_types.h>
#include <pcl/features/fpfh.h>
// #include <PCLObjectsTracker.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/histogram_visualizer.h>
#include<stdlib.h>
#include <ctime>

int fevvvContour=4;

// PCLFeaturesExtractorContour::PCLFeaturesExtractorContour(boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> v,boost::mutex& mutex)
PCLFeaturesExtractorContour::PCLFeaturesExtractorContour()
{
    this->prev_width=0;
    this->prev_height=0;
    this->prev_depth=0;
//     this->hist_viewer=v;
//     this->viewer_mutex=&mutex;

}


void PCLFeaturesExtractorContour::Execute(std::vector<PCLObject::Ptr> objects) {


    for (size_t i = 0; i < objects.size(); i++)
    {

        float maxLeftPoint=0;
        float maxRightPoint=0;
        float maxTopPoint=0;
        float maxBottomPoint=0;
        float maxNearestPoint=0;
        float maxFurthestPoint=0;
        bool firstTime=true;


        for(size_t j=0; j < objects[i]->objectCloud->points.size (); j++)
        {

            if(pcl::isFinite(objects[i]->objectCloud->points[j])) {
                if(firstTime==true) {
                    maxLeftPoint=objects[i]->objectCloud->points[j].x;
                    maxRightPoint=objects[i]->objectCloud->points[j].x;
                    maxTopPoint=objects[i]->objectCloud->points[j].y;
                    maxBottomPoint=objects[i]->objectCloud->points[j].y;
                    maxNearestPoint=objects[i]->objectCloud->points[j].z;
                    maxFurthestPoint=objects[i]->objectCloud->points[j].z;
                    firstTime=false;
                    continue;
                }
                if(objects[i]->objectCloud->points[j].x < maxRightPoint) {
                    maxRightPoint=objects[i]->objectCloud->points[j].x;
                }
                if(objects[i]->objectCloud->points[j].x > maxLeftPoint) {
                    maxLeftPoint=objects[i]->objectCloud->points[j].x;
                }
                if(objects[i]->objectCloud->points[j].y < maxBottomPoint) {
                    maxBottomPoint=objects[i]->objectCloud->points[j].y;
                }
                if(objects[i]->objectCloud->points[j].y > maxTopPoint) {
                    maxTopPoint=objects[i]->objectCloud->points[j].y;
                }
                if(objects[i]->objectCloud->points[j].z < maxNearestPoint) {
                    maxNearestPoint=objects[i]->objectCloud->points[j].z;
                }
                if(objects[i]->objectCloud->points[j].z > maxFurthestPoint) {
                    maxFurthestPoint=objects[i]->objectCloud->points[j].z;
                }
            }
        }
        if(prev_depth==0) {
            this->prev_width=objects[i]->width=maxLeftPoint - maxRightPoint;
            this->prev_height=objects[i]->height=maxTopPoint - maxBottomPoint;
            this->prev_depth=objects[i]->depth=maxFurthestPoint - maxNearestPoint;
        } else {
            this->prev_width=objects[i]->width=(maxLeftPoint - maxRightPoint)* 0.2 + 0.8 * this->prev_width;
            this->prev_height=objects[i]->height=(maxTopPoint - maxBottomPoint)* 0.2 + 0.8 * this->prev_height;
            this->prev_depth=objects[i]->depth=(maxFurthestPoint - maxNearestPoint) * 0.2 + 0.8 * this->prev_depth;
        }
        objects[i]->size = objects[i]->width * objects[i]->height * objects[i]->depth;

    }

}