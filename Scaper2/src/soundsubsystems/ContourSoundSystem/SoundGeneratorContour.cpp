#include<soundsubsystems/ContourSoundSystem/SoundGeneratorContour.h>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <vector>

// int sgvvvContour = 4;

// #include <boost/graph/graph_concepts.hpp>
#define FADE_AMT 100 //only necessary when using R contour for slowest AM modulator

#define DEFAULT_FPS 0.1 //start off slow for IUI
#define BUFFER_SIZE 44100*10 //10 seconds worth... should be enough! (!)
#define MAX_NUM_OBJECTS 10 //testin'

#define MAX_AMPLITUDE_SHORT 32760

#define interpolate_scale1 1
#define interpolate_scale2 20
#define interpolate_scale3 300

#define POS_SMOOTH 3

#include <common/Tools.h>
#include <AL/alut.h>

//TODO: put it somewhere better


SoundGeneratorContour::SoundGeneratorContour(bool normalization) {


//    while(ssg.size()<NUM_OBJECTS) {
//          std::shared_ptr<SoundSourceFullWaveformGenerator> ssgx(new SoundSourceFullWaveformGenerator);
//          ssg.push_back(ssgx);


   created_objects=0;
   depth_normalization=normalization;

   while (created_objects<MAX_NUM_OBJECTS) {
        std::shared_ptr<SoundSourceWaveformInterpolator> ssix(new SoundSourceWaveformInterpolator);
        ssi.push_back(ssix);
        csb.push_back(CircularBuffer<short>(BUFFER_SIZE));
        std::shared_ptr<SoundSourceOutput> ssox(new SoundSourceOutput(DEFAULT_FPS));
        sso.push_back(ssox);
        last_pos.push_back(Eigen::Vector4f(0,0,0,0));
        free_buffers.insert(created_objects);
        created_objects++;
    }

//     timer.reset();

}
void SoundGeneratorContour::setExaggeration(double multiplier) {
    exaggerate_pos=multiplier;
}

Eigen::Vector4f SoundGeneratorContour::exaggerate(Eigen::Vector4f pos){

      Eigen::Vector4f pos_after_exaggeration=pos;
    //OLD WAY 1
//     pos_after_exaggeration*=exaggerate_pos;

    //OLD WAY 2
    pos_after_exaggeration[0]*=exaggerate_pos*pos_after_exaggeration[3];
    pos_after_exaggeration[2]+=z_offset_*pos_after_exaggeration[3];
    assert(abs(pos_after_exaggeration[3] - 1)<0.0001f);

    // new way dosent work good yet
//     double angle = (1.0 - exaggerate_pos) * atan2(pos_after_exaggeration[0],pos_after_exaggeration[3]);
//
//     pos_after_exaggeration[0] = pos_after_exaggeration[0] * cos(angle) + pos_after_exaggeration[2] - sin(angle);
//     pos_after_exaggeration[2] = pos_after_exaggeration[0] * sin(angle) + pos_after_exaggeration[2] + cos(angle);

    return pos_after_exaggeration;
}


void SoundGeneratorContour::Execute(std::vector<PCLObject::Ptr> objects) {
//
//     Eigen::Vector4f pos_before_exaggeration(0,0,0,0);
//     Eigen::Vector4f pos_after_exaggeration(0,0,0,0);
    debug();

    int num_of_repetitions= 3;
    std::set<int> seen_track_ids;
    
    struct{
      bool operator()(PCLObject::Ptr a,PCLObject::Ptr b) const
      {
	return a->track_id<b->track_id;
      }
    }sort_by_track_id_comparator;
    
    std::sort(objects.begin(),objects.end(),sort_by_track_id_comparator);

    std::set<int> curr_frame_track_ids;

    for(int i=0;i<objects.size();i++){

      curr_frame_track_ids.insert(objects[i]->track_id);
      times_seen[objects[i]->track_id]++;
    }
    for(std::map<int,uint>::iterator it=times_seen.begin(); it!=times_seen.end(); it++){

      if (curr_frame_track_ids.find(it->first) == curr_frame_track_ids.end())
	times_seen[it->first]=0;
    }

    if(objects.size()>0) {
//       for(unsigned int i=2; i<objects.size() && i<3 ; i++) //for debugging: only do third object
//         for(unsigned int i=1; i<objects.size() && i<2 ; i++) //for debugging: only do second object
// 	  for(unsigned int i=0; i<objects.size() && i<1 ; i++) //for debugging: only do one object
	  for(unsigned int i=0; i<objects.size() ; i++)
        {
// 	  if(times_seen[objects[i]->track_id] > num_of_repetition){
	      PCLObject::Ptr &obj = objects[i];
	      if(verbosity>2)std::cout<<"Object["<<i<<"]:"<<*obj<<std::endl;
  // 	if(objects[i]->width * objects[i]->height > 0.00001 && objects[i]->size < 0.1)
  // 	{

	      uint oid=obj->track_id;

	      assert(oid>=0);

	      if(times_seen[oid] < num_of_repetitions){
		std::cerr<<"SoundGeneratorContour: Warning: can't sonify it."<<std::endl;
	      }else{
		debug();

	      	if(trackid2buffer.find(oid)==trackid2buffer.end()){
		  if(free_buffers.size()>0){
		    assert(trackid2buffer.size()<MAX_NUM_OBJECTS);
		      size_t sid=*(free_buffers.begin());
		      trackid2buffer[oid]=sid;
		      free_buffers.erase(sid);
		      last_pos[sid]=exaggerate(obj->centroid);
		      debug();
		  }
		  else{
		      std::cerr<<"SoundGeneratorContour: Warning: Sound generation not yet set up to deal with object ids more than "<<MAX_NUM_OBJECTS<<" (object id "<<obj->track_id<<")"<<std::endl;
		      assert(trackid2buffer.size()==MAX_NUM_OBJECTS);
		    }
		}
  //                 uint oid=objects[i]->track_id;
		  seen_track_ids.insert(oid);
		  int sid=trackid2buffer[oid];
  ///////////////////////////////////////////////
		  contours::Contour objContour(obj);

		obj->distance= sqrt(pow(obj->centroid[0],2.0)+pow(obj->centroid[1],2.0)+pow(obj->centroid[2],2.0));

		if (depth_normalization == true){
		  scale1 = interpolate_scale1 * pow(obj->distance,1.5);
		  scale2 = interpolate_scale2 * pow(obj->distance,1.5);
		  scale3 = interpolate_scale3 * pow(obj->distance,1.5);
		}else{
		  scale1 = interpolate_scale1;
		  scale2 = interpolate_scale2;
		  scale3 = interpolate_scale3;
		}

// 		  SoundBlocks::InterpolatorGenerator interpolated_rcontour_fast(objContour.normalized_rcontourX,scale1);
// 		  SoundBlocks::InterpolatorGenerator interpolated_hcontour_med(objContour.normalized_hcontour,scale2);
// 		  SoundBlocks::InterpolatorGenerator interpolated_vcontour_slow(objContour.normalized_vcontour,scale3);
// 		  SoundBlocks::InterpolatorGenerator interpolated_hcontour_slow(objContour.normalized_hcontour,scale3);
// 		  SoundBlocks::InterpolatorGenerator interpolated_vcontourNeg_slow(objContour.normalized_vcontour_neg,scale3);
// 		  SoundBlocks::InterpolatorGenerator interpolated_hcontourNeg_slow(objContour.normalized_hcontour_neg,scale3);
// 		  SoundBlocks::InterpolatorGenerator interpolated_rcontour_slow(objContour.unnormalized_rcontour,scale3);
// 		  SoundBlocks::InterpolatorGenerator interpolated_rcontourX_slow(objContour.normalized_rcontourX,scale3);

		  SoundBlocks::InterpolatorGenerator interpolated_icontourhb_fast(objContour.internal_contour_horizontal_bar,scale1);
// 		  SoundBlocks::InterpolatorGenerator interpolated_icontourhb_med(objContour.internal_contour_horizontal_bar,scale2);
// 		  SoundBlocks::InterpolatorGenerator interpolated_icontourhb_slow(objContour.internal_contour_horizontal_bar,scale3);
//                 SoundBlocks::InterpolatorGenerator interpolated_icontourvb_fast(objContour.internal_contour_vertical_bar,scale1);
//                 SoundBlocks::InterpolatorGenerator interpolated_icontourvb_med(objContour.internal_contour_vertical_bar,scale2);
//                 SoundBlocks::InterpolatorGenerator interpolated_icontourvb_slow(objContour.internal_contour_vertical_bar,scale3);


// 		  SoundBlocks::Sine sin1(440);
// 		  SoundBlocks::Sine sin2(360);
// 		  SoundBlocks::AmplitudeModulator ampModulator1(&sin2, &sin1 , 1);
//
// 		  SoundBlocks::FrequencyModulator fm2(&sin1,&interpolated_hcontour_med,0.78f);
// 		  SoundBlocks::FrequencyModulator fm3(&fm2,&interpolated_vcontour_slow,0.78f);
// 		  SoundBlocks::AmplitudeModulator out4(&fm3,&interpolated_rcontour_slow,0.78f);
//
// 		  SoundBlocks::AmplitudeModulator out5(&sin2,&interpolated_rcontour_slow,1);
// 		  SoundBlocks::AmplitudeModulator out10(&sin2,&interpolated_rcontour_fast,1);
//
// 		  SoundBlocks::FrequencyModulator out6(&sin2,&interpolated_rcontour_slow,1);
// 		  SoundBlocks::FrequencyModulator out6b(&interpolated_rcontour_fast,&interpolated_rcontour_slow,1);
// 		  SoundBlocks::FrequencyModulator out9(&sin2,&interpolated_rcontour_fast,1);
// 		  SoundBlocks::FrequencyModulator out7(&sin1,&interpolated_rcontour_slow,0.78f);
//
// 		  SoundBlocks::FrequencyModulator fm5(&sin1,&interpolated_rcontour_slow,0.78f);
//
// 		  SoundBlocks::AmplitudeModulator out11(&out6,&interpolated_rcontour_fast,1);
// 		  SoundBlocks::AmplitudeModulator out12(&out6,&interpolated_rcontour_slow,1);
// 		  SoundBlocks::AmplitudeModulator out12b(&out6b,&interpolated_rcontour_slow,1);
//
// 		  SoundBlocks::FrequencyModulator out13(&interpolated_rcontour_fast,&interpolated_hcontour_med,0.78f);
//
// 		  SoundBlocks::FrequencyModulator out14(&out12,&interpolated_hcontour_med,0.78f);
//
// 		  SoundBlocks::FrequencyModulator out15 (&interpolated_rcontour_fast, &interpolated_vcontour_slow,0.78f );
//
//  		  SoundBlocks::AmplitudeModulator out16(&out6, &interpolated_hcontour_med,1);
//
// 		  SoundBlocks::FrequencyModulator out6c(&interpolated_rcontour_fast,&interpolated_hcontourNeg_slow,1);
// 		  SoundBlocks::AmplitudeModulator out12c(&out6c,&interpolated_hcontour_slow,3);

//               SoundBlocks::FrequencyModulator fm2(&sin1,&interpolated_vcontour,0.78f);
//          	 int target_size=interpolated_rcontour_slow.getSize();
//               int target_size=interpolated_vcontour_slow.getSize()*2;
		 int target_size=interpolated_icontourhb_fast.getSize(); //TODO was *2
                 bool apply_fade=false;
//               int target_size=interpolated_hcontour_med.getSize();
//	         std::vector<short> full_buffer(44100*2,0);
///****************************************
		 std::vector<short> full_buffer(target_size+2000);
///****************************************

// 		  if(verbosity>1)std::cout<< "SoundGeneratorContour: oid="<<oid<<" sid="<<sid<<" target size="<<target_size<<" fast size="<< interpolated_rcontour_fast.getSize()<<std::endl;

//                 int k=0;
  //               int j=0;
  //               float fw1=330/(2*objects[oid]->width);

		double fade_in=FADE_AMT;
		double fade_out=target_size-FADE_AMT;
		if(fade_out<0)fade_out=0;
		
		std::cout<<"number of samples orig:"<<objContour.internal_contour_horizontal_bar.size()<<std::endl;
		std::cout<<"number of samples fast:"<<interpolated_icontourhb_fast.getSize()<<std::endl;
// 		std::cout<<"number of samples med:"<<interpolated_icontourhb_med.getSize()<<std::endl;
// 		std::cout<<"number of samples slow:"<<interpolated_icontourhb_slow.getSize()<<std::endl;


		  for(int i=0 ; i<target_size ; i++)
		  {

//                     tick_value=out12.tick();
                    tick_value=interpolated_icontourhb_fast.tick();

//              if(i<40)tick_value=i/40 * tick_value;
//              if(i>target_size-40)tick_value=(target_size-i)/40*tick_value;
//              tick_value=sin.tick();
//              tick_value=fm2.tick();

//                     if(tick_value==0)

//                         std::cout<< "wave[" <<i <<"] value:"<< tick_value<<std::endl;
                    double value= tick_value * (MAX_AMPLITUDE_SHORT-5); //-5 to avoid overflow

//                     if(i<FADE_AMT&&apply_fade)value=(i*1.0/FADE_AMT)*value;
//                     if(i<fade_in&&apply_fade)value=(i*1.0/fade_in)*value;
//                     if(i>target_size-fade_out&&apply_fade)value=((target_size-i)*1.0/fade_out)*value;
//                     if(i>target_size-FADE_AMT&&apply_fade)value=((target_size-i)*1.0/FADE_AMT)*value;

                    full_buffer[i] = value;

		  }
                

 //                 pos_before_exaggeration=objects[oid]->centroid;

//                 for(size_t ii=0; ii<2; ii++)
//                     pos_after_exaggeration[ii]=pos_before_exaggeration[ii]*this->exaggerate_pos;

                ssi[sid]->interpolate_sound(full_buffer,csb[sid],SoundSourceWaveformInterpolator::FinishOld);
//                 ssi[sid]->interpolate_sound(full_buffer,csb[sid],SoundSourceWaveformInterpolator::GranulateNewIn);
//                ssi[oid]->interpolate_sound(full_buffer[oid],csb[oid],SoundSourceWaveformInterpolator::FinishOld);

                last_pos[sid]=(last_pos[sid]*POS_SMOOTH+exaggerate(obj->centroid))/(POS_SMOOTH+1);
//                 last_pos[sid]=pos_before_exaggeration;

     }
  // 	}

// 	  }
	}
    }

    for(unsigned int i=0; i<sso.size(); i++) {
        //Tools::check_debug_silliness(csb[i]);
        sso[i]->output(csb[i],last_pos[i]);
        if(verbosity>1)std::cout<< "SoundGeneratorContour: outputting sid="<<i<<" at "<<last_pos[i][0]<<","<<last_pos[i][1]<<","<<last_pos[i][2]<<","<<last_pos[i][3]<<std::endl;
        //Tools::check_debug_silliness(csb[i]);
    }
    debug();
    for(auto it=trackid2buffer.begin();it!=trackid2buffer.end();it++){
        if(seen_track_ids.find(it->first)==seen_track_ids.end()){
            free_buffers.insert(it->second);
            trackid2buffer.erase(it->first);
        }
    }
    debug();

}

void SoundGeneratorContour::setZOffset(double offset){
    z_offset_=offset;
}

void SoundGeneratorContour::debug(){
     for(auto it=trackid2buffer.begin();it!=trackid2buffer.end();it++){
        assert(it->second<MAX_NUM_OBJECTS);
    }

}