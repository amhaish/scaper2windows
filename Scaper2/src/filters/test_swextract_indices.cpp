#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <filters/sw_extract_indices.h>

int
main (int argc, char** argv)
{
//   pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>), cloud_bg (new pcl::PointCloud<pcl::PointXYZRGB>), cloud_fg (new pcl::PointCloud<pcl::PointXYZRGB>);

  // Fill in the cloud data
  
  int full_width=640;
  int full_height=480;
  cloud->resize(full_width*full_height);
  
  for(int i=0;i<full_height;i++){
      for(int j=0;j<full_width;j++){
	pcl::PointXYZRGB p;
	p.x=((double)j-320)/160;
	p.y=((double)i-240)/120;
	p.z=sqrt(sqrt(4+p.x*p.x+p.y*p.y));
	p.r=p.x*10;
	p.g=p.x*10;
	p.b=p.x*10;
	cloud->points[i*full_width+j]=p;
      }
  }
  
  cloud->width=full_width;
  cloud->height=full_height;
  
  
  std::cerr << "PointCloud before filtering: " << cloud->width * cloud->height << " data points." << std::endl;

  // Write the original version to disk
  pcl::PCDWriter writer;
  writer.write<pcl::PointXYZRGB> ("original_swtest.pcd", *cloud, false);


  boost::shared_ptr<std::vector<int> > circle_inds(new std::vector<int>);
  int center_row=240;
  int center_col=320;
  int rad=30;
  for(int i=0;i<full_height;i++){
      for(int j=0;j<full_width;j++){
	   if((i-center_row)*(i-center_row) + (j-center_col)*(j-center_col) < rad*rad){
	      circle_inds->push_back(i*full_width+j);
	   }
      }
  }
  

  // Create the filtering object
  pcl::SWExtractIndices<pcl::PointXYZRGB> extract;

 extract.setSubWindow(true);
    // Extract the inliers
    extract.setInputCloud (cloud);
    extract.setKeepOrganized(true);
//     extract.set
    extract.setIndices (circle_inds);
    extract.setNegative (false);
    extract.filter (*cloud_fg);
    std::cerr << "PointCloud representing the circle part: " << cloud_fg->width * cloud_fg->height << " data points." << std::endl;

    std::stringstream ss;
    ss << "filter_circle.pcd";
    writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_fg, false);

    // Create the filtering object
    extract.setNegative (true);
    extract.filter (*cloud_bg);
    
    std::stringstream ss2;
    ss2 << "filter_neg.pcd";
    writer.write<pcl::PointXYZRGB> (ss2.str (), *cloud_bg, false);

  return (0);
}
