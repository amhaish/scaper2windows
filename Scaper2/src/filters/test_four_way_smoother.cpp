
#include "filters/four_way_smoother.h"
#include <string>     // std::string, std::stof
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/image_viewer.h>

void checkcompile() {
    pcl::PointCloud<pcl::PointXYZ> cloud;
    FourWaySmoother<pcl::PointXYZ> smoother;
    smoother.applyFilter(cloud);
}




void viewportsVis (
    pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud1,pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud2)
{
    // --------------------------------------------------------
    // -----Open 3D viewer and add point cloud and normals-----
    // --------------------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->initCameraParameters ();

    int v1(0);
    viewer->createViewPort(0.0, 0.0, 0.5, 1.0, v1);
    viewer->setBackgroundColor (0, 0, 0, v1);
//   viewer->addText("Radius: 0.01", 10, 10, "v1 text", v1);
//   pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZ> gf1(cloud1,"z");
    viewer->addPointCloud<pcl::PointXYZ> (cloud1, gf1, "sample cloud1", v1);

    int v2(0);
    viewer->createViewPort(0.5, 0.0, 1.0, 1.0, v2);
    viewer->setBackgroundColor (0, 0, 0, v2);
//   viewer->addText("Radius: 0.1", 10, 10, "v2 text", v2);
    pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZ> gf2(cloud2,"z");
    viewer->addPointCloud<pcl::PointXYZ> (cloud2, gf2, "sample cloud2", v2);

    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud1");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud2");
    viewer->addCoordinateSystem (1.0);

//   viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (cloud, normals1, 10, 0.05, "normals1", v1);
//   viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (cloud, normals2, 10, 0.05, "normals2", v2);

    viewer->spin();
}

void double_size_img(unsigned char *orig,unsigned char *after,size_t width,size_t height) {
//     size_t oldsize=width*heigh	t;

//     size_t newsize=width*height*4;
  size_t newwidth=width*2;
//   size_t newheight=height*2;

    for(size_t i =0 ; i<height*2; i++) {
        for(size_t j=0; j<width*2; j++) {
            size_t oldi=i/2;
            size_t oldj=j/2;
            for(size_t c=0; c<3; c++) {
                after[i*newwidth*3+j*3+c]=orig[oldi*width*3+oldj*3+c];
            }
        }
    }
}

void imviewerVis (
    pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud1,pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud2)
{
    size_t height=cloud1->height;
    size_t width=cloud1->width;
    assert(height==cloud2->height);
    assert(width==cloud2->width);
    size_t imsize=height*width;

    double maxz=0;
    for(size_t ii=0; ii<imsize; ii++) {
        if(cloud1->points[ii].z>maxz)maxz=cloud1->points[ii].z;
        if(cloud2->points[ii].z>maxz)maxz=cloud2->points[ii].z;
    }

    pcl::visualization::ImageViewer viewer;
    unsigned char *img = new unsigned char[imsize*2*3];
    unsigned char *imgn = new unsigned char[imsize*2*3*4];

    for(size_t i=0; i<height; i++) {
        for(size_t  j=0; j<width; j++) {
            if(pcl::isFinite(cloud1->points[i*width+j])) {
                img[i*width*3 + j*3 + 0]=0;
                img[i*width*3 + j*3 + 1]=255*cloud1->points[i*width+j].z/maxz;
                img[i*width*3 + j*3 + 2]=255-255*cloud1->points[i*width+j].z/maxz; //max out at 5
            }
            else {
                img[i*width*3 + j*3 + 0]=255;
                img[i*width*3 + j*3 + 1]=0;
                img[i*width*3 + j*3 + 2]=0;

            }

            if(pcl::isFinite(cloud2->points[i*width+j])) {
                img[imsize*3 + i*width*3 + j*3 + 0]=0;
                img[imsize*3 + i*width*3 + j*3 + 1]=255*cloud2->points[i*width+j].z/maxz;
                img[imsize*3 + i*width*3 + j*3 + 2]=255-255*cloud2->points[i*width+j].z/maxz; //max out at 5
            }
            else {
                img[imsize*3 + i*width*3 + j*3 + 0]=255;
                img[imsize*3 + i*width*3 + j*3 + 1]=0;
                img[imsize*3 + i*width*3 + j*3 + 2]=0;

            }

        }
    }
    double_size_img(img,imgn,width,height*2);
    viewer.addRGBImage(imgn,width*2,height*4,"layer");
    viewer.spin();
    viewer.removeLayer("layer");
    usleep(100);
    delete[] img;
    delete[] imgn;
    usleep(100);
}



int main(int argc,char *argv[]) {
//   checkcompile();
    
    if(argc<3) {
        std::cerr<<"Nope... "<<argc<<std::endl;
        return -1;
    }
    std::string fname (argv[1]);
    std::string maxdepth(argv[2]);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1 (new pcl::PointCloud<pcl::PointXYZ>);
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (fname, *cloud1) == -1) //* load the file
    {
        std::cerr<<"Nope... "<<fname<<std::endl;
        return -1;
    }
    double max_depth_change= std::stof(maxdepth);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2(new pcl::PointCloud<pcl::PointXYZ>);;
    FourWaySmoother<pcl::PointXYZ> alg(max_depth_change);
    alg.setInputCloud(cloud1);
    alg.applyFilter(*cloud2);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> intermediates=alg.getTClouds();
    pcl::PointCloud<pcl::PointXYZ>::Ptr added_cloud = alg.getAddedCloud();
    for(int i=0; i<4; i++) {
        std::cout<<"#"<<i<<std::endl;
//    viewportsVis(cloud1,intermediates[i]);
        imviewerVis(cloud1,intermediates[i]);
    }
    std::cout<<"added"<<std::endl;
    imviewerVis(cloud1,added_cloud);
    std::cout<<"#final"<<std::endl;
    imviewerVis(cloud1,cloud2);
//     viewportsVis(cloud1,cloud2);
}