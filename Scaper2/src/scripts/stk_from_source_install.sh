#!/bin/bash

echo
echo --------------------------------------------------
echo If this script finishes successfully, will write:
echo SUCCESFFULLY FINISHED
echo --------------------------------------------------
echo
echo You may need to input your superuser password
echo
echo Press enter to proceed

read line

sudo apt-get install autoconf build-essential
sudo apt-get remove stk libstk0-dev libstk0c2a stk-doc
sudo apt-get build-dep stk libstk0-dev libstk0c2a stk-doc

mkdir -p ~/software

cd ~/software/ || exit 1

git clone https://github.com/thestk/stk.git

cd stk || exit 1

git pull || exit 1

autoconf || exit 1

./configure  || exit 1

make  || exit 1

echo From SOURCE via script stk_from_source_install.sh in directory ~/software/stk > description-pak

sudo checkinstall -y --nodoc --pkgname stk_from_source --provides stk,libstk0-dev,libstk0c2a,stk-doc --replaces stk,libstk0-dev,libstk0c2a,stk-doc --pkgversion 4.4.X --exclude /home || exit 1

echo SUCCESFFULLY FINISHED