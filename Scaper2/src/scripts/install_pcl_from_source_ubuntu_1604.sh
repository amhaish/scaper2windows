#!/bin/bash

echo
echo Read all this I reckon:
echo
echo README: You will need an internet connectin probably.
echo README: You will probably be prompted for super user password.
echo README: You may need to read a license agreement and type \"yes\".
echo
echo README: Only run this script for Ubuntu 16.04 \(trusty\).
echo
echo README: If the script does not write \"Install PCL from source script succeeded.\" at the end, it probably did not work.
echo
echo Now press enter to proceed:
read line

# echo Adding ROS repos just used to get OpenNI etc...
# add ros
# sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'

# echo Getting key for ROS....
# add ros keys
# wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - || exit 1

echo Asking APT to download the latest source list....
# update APT
sudo apt-get -y update || exit 1

echo Asking APT to upgrade what it has not yet upgraded
# upgrade what is there
sudo apt-get -y upgrade
# sudo apt-get -y dist-upgrade

# qt

echo Now installing tools

sudo apt-get -y install build-essential clang git gitk git-cola mercurial || exit 1

sudo apt-get -y install tortoisehg dos2unix geany kdevelop kate || exit 1

sudo apt-get -y install libusb-1.0-0-dev libeigen3-dev build-essential cmake-curses-gui || exit 1

echo Now installing sound dependencies

sudo apt-get -y install sox libsox-dev stk libstk0-dev libstk0c2a stk-doc # STK - might yet be of use

sudo apt-get -y install sox libstk0-dev libstk0c2a libsox-dev stk stk-doc # STK - might yet be of use

sudo apt-get -y install alsoft-conf libopenal-dev libalut-dev libalut0 libopenal1 # OpenAL

sudo apt-get -y install libeigen3-dev libopenni2-dev openni2-utils || exit 1

sudo apt-get -y install openni2-utils  libqt4-opengl  libqtcore4 libqtgui4 || exit 1

echo Now installing PCL dependencies

sudo apt-get -y build-dep libpcl1 libpcl1-dev

sudo apt-get -y install libboost-all-dev libspnav-dev || exit 1

sudo apt-get -y install libqt4-dev libflann-dev libvtk5-dev || exit 1

sudo apt-get -y install libvtk5-qt4-dev libusb-1.0-0-dev || exit 1

sudo apt-get -y install libusb-1.0-0-dev libeigen3-dev || exit 1

sudo apt-get -y install libopenni-dev libopenni-sensor-primesense0 libopenni-sensor-primesense-dev libopenni0 openni-utils || exit 1

sudo apt-get -y install libeigen3-dev libopenni2-dev openni2-utils || exit 1

sudo apt-get -y install openni2-utils  libqt4-opengl  libqtcore4 libqtgui4 || exit 1
# sudo apt-get -y install ros-indigo-pcl-ros ros-indigo-perception-pcl ros-indigo-ecto-pcl ros-indigo-openni-launch ros-indigo-openni-camera openni-utils ros-indigo-openni2-launch openni2-utils  libqt4-opengl  libqtcore4 libqtgui4 || exit 1

###Ahmet Seha needed to do this to get it working. Doing it last to ensure it takes effect:
sudo apt-get -y install apt-get install librtaudio-dev librtmidi-dev 
sudo apt-get -y install libstk0-dev || exit 1

echo Now getting DepthSense Drivers

if [ ! -f /opt/softkinetic/DepthSenseSDK/bin/DepthSenseViewer ]
then


    cd /tmp
    DS_FNAME=DepthSenseSDK-1.9.0-5-amd64-deb.run
#     wget http://files.djduff.net/iisu/IISUy/DepthSenseSDK-1.4.5-2151-amd64-deb.run
    wget http://files.djduff.net/Download/DepthSense/$DS_FNAME
    chmod u+x $DS_FNAME
    sudo ./$DS_FNAME || exit 1 # accept agreement
    rm $DS_FNAME
    
fi

sudo apt-get install libudev1:i386 || exit 1

sudo ln -sf /lib/x86_64-linux-gnu/libudev.so.1 /lib/x86_64-linux-gnu/libudev.so.0 || exit 1

# olddir=`pwd`

mkdir -p $HOME/software/
cd $HOME/software || exit 1

git clone https://github.com/damienjadeduff/pcl.git # https://github.com/PointCloudLibrary/pcl.git

cd pcl || exit 1

git pull

echo NOW BUILDING PCL - TWICE!

mkdir build_rel
mkdir build_deb

cd $HOME/software/pcl/build_rel || exit 1
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_DSSDK=ON -DBUILD_simulation=ON -DBUILD_apps=ON -DBUILD_segmentation=ON -DBUILD_features=on -DBUILD_io=ON -DBUILD_stereo=ON -DBUILD_recognition=ON -DPCL_QT_VERSION=4 -DBUILD_filters=ON -DBUILD_search=ON -DBUILD_kdtree=ON -DWITH_OPENNI=ON -DWITH_OPENNI2=ON -DWITH_QT=ON -DWITH_VTK=ON -DWITH_ENSENSO=OFF -DWITH_DAVIDSDK=OFF .. || exit 1
make || exit 1

cd $HOME/software/pcl/build_deb || exit 1
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_DSSDK=ON -DBUILD_simulation=ON -DBUILD_apps=ON -DBUILD_segmentation=ON -DBUILD_features=on -DBUILD_io=ON -DBUILD_stereo=ON -DBUILD_recognition=ON -DPCL_QT_VERSION=4 -DCMAKE_BUILD_TYPE=Debug -DBUILD_filters=ON -DBUILD_search=ON -DBUILD_kdtree=ON -DWITH_OPENNI=ON -DWITH_OPENNI2=ON -DWITH_QT=ON -DWITH_VTK=ON -DWITH_ENSENSO=OFF -DWITH_DAVIDSDK=OFF .. || exit 1
make || exit 1

echo NOW GETTING SCAPER

cd $HOME/software || exit 1

echo IF YOU HAVE YOUR BITBUCKET USERNAME AND PASSWORD USE IT HERE TO GET ACCESS TO OUR REPO

git clone https://bitbucket.org/damienjadeduff/scaper2.git

mkdir $HOME/software/scaper2/build_deb

mkdir $HOME/software/scaper2/build_rel

cd $HOME/software/scaper2/build_deb || exit 1

cmake -DCMAKE_BUILD_TYPE=Debug -DTEST_SOUND_INTERPOLATORS=ON -DPCL_DIR=$HOME/software/pcl/build_deb .. || exit 1

make || exit 1

cd $HOME/software/scaper2/build_rel || exit 1

cmake -DCMAKE_BUILD_TYPE=Release  -DTEST_SOUND_INTERPOLATORS=ON -DPCL_DIR=$HOME/software/pcl/build_rel .. || exit 1

make || exit 1

# cd $olddir

echo Install PCL from source script succeeded.
