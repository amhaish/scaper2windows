#include <common/CircularBuffer.h>
#include <boost/concept_check.hpp>
#include <iostream>

int main(int argc,char **argv){
 
  CircularBuffer<short> buf1(10);
  std::cout<<"Buffer of size 10, pre-initialise:"<<buf1.str()<<std::endl;
  for(int i=0;i<5;i++)buf1[i]=i;
  std::cout<<"Buffer of size 10, 5 added:"<<buf1.str()<<std::endl;
  for(int i=5;i<12;i++)buf1[i]=i;
  std::cout<<"Buffer of size 10, 7 more added:"<<buf1.str()<<std::endl;
  for(int i=0;i<4;i++)std::cout<<"Consuming "<<buf1.consume()<<std::endl;
  std::cout<<"Buffer of size 10, after consuming:"<<buf1.str()<<std::endl;
  
}