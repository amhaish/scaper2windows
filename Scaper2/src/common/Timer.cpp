#include <common/Timer.h>
#include <iomanip>
#include <iostream>

Timer::Timer(std::string namep,uint print_precisionp) : start(clock::now()),name(namep),accum_sec(0),accum_cnt(0),print_precision(print_precisionp) {}
void Timer::reset() {
    start = clock::now();
}
double Timer::elapsed() const {
    return std::chrono::duration_cast<second>
           (clock::now() - start).count();
}

Timer::Timer (const Timer &timer):start(timer.start),name(timer.name),accum_sec(timer.accum_sec),accum_cnt(timer.accum_cnt) {

}


void Timer::print_elapsed() {
    double sec = elapsed();
    std::cout<<"Timer "<<name<<" elapsed: "<<std::fixed<< std::setprecision(print_precision)<<sec<<std::endl;
}

void Timer::accumulate() {
    double sec = elapsed();

    accum_sec += sec;
    accum_cnt ++;
    if(accum_cnt==0) {
        std::cerr<<__FILE__<<":"<<__LINE__<<": OVERFLOW."<<std::endl;
    }
}

void Timer::accumulate_and_print_elapsed() {
    double sec = elapsed();

    accum_sec += sec;
    accum_cnt ++;
    if(accum_cnt==0) {
        std::cerr<<__FILE__<<":"<<__LINE__<<": OVERFLOW."<<std::endl;
    }
    std::cout<<"Timer "<<name<<" elapsed: "<<std::fixed<< std::setprecision(print_precision)<<sec<<std::endl;
}

void Timer::accumulate_and_print_accumulated_average() {
    double sec = elapsed();

    accum_sec += sec;
    accum_cnt ++;
    if(accum_cnt==0) {
        std::cerr<<__FILE__<<":"<<__LINE__<<": OVERFLOW."<<std::endl;
    }
    print_accumulated_average();
}

double Timer::accumulated_average() const {
    return accum_sec/accum_cnt;
}

void Timer::print_accumulated_average() {
    double avg=accumulated_average();
    std::cout<<"Timer "<<name<<" average: "<<std::fixed<< std::setprecision(print_precision)<<avg<<std::endl;
}

