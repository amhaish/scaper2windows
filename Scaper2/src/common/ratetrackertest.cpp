#include <common/RateTracker.h>
#include <iostream>
#include <chrono>
#include <boost/thread.hpp>

int main(int argc,char *argv[]){
  RateTracker r1("MyTracker1");
  RateTracker r2("MyTracker2",9999999,0.0001);
  RateTracker r3("MyTracker3",0.001,0.8);
  RateTracker r4("MyTracker4",1000,0.3);
  RateTracker* trackers[]{&r1,&r2,&r3,&r4};
  
  
  for(uint ti=0;ti<4;ti++){
      RateTracker *rt=trackers[ti];
      rt->print_status();
      for(size_t i = 0 ; i<20;i++){

	  rt->update();
	  rt->print_status();
	  boost::this_thread::sleep_for( boost::chrono::milliseconds(100) );
	  
      }      
      
  }
}