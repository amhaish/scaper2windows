#include <common/RateTracker.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <math.h>

RateTracker::RateTracker(std::string namep,double initial_hzp,double avg_frame_weight_multiplierp){

    last_time_point=std::chrono::high_resolution_clock::now();
    current_period=0;
    tracked_period=1.0/initial_hzp;
    tracked_hz=1.0/tracked_period;
    tracked_period_error=tracked_period;
    num_updates=0;
    
    if (namep.size()==0){
      static uint name_cntr=0;
      std::stringstream ss;
      ss << "RateTracker#" << name_cntr;
      name = ss.str();
      name_cntr++;
    }else{
      name=namep;
    }
    
    avg_frame_weight_multiplier=avg_frame_weight_multiplierp;
    
    
}

void RateTracker::update() {
    std::chrono::high_resolution_clock::time_point received_at=std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> dur = std::chrono::duration<double>(received_at-last_time_point);
    current_period=dur.count();

    // tasty hidden functionality
    // purpose is to ignore bad data from first frames, relying on provided rate
    // thereafter, for a few frames, try to force the tracked data closer
    double update_avg_frame_weight_multiplier;
    if (num_updates<1)
        update_avg_frame_weight_multiplier=0.0;
    else if (num_updates<2)
        update_avg_frame_weight_multiplier=avg_frame_weight_multiplier+(1-avg_frame_weight_multiplier)/2;
    else
        update_avg_frame_weight_multiplier=avg_frame_weight_multiplier;
    
//     std::cout<<"num_updates:"<<num_updates<<" "<<"update_avg_frame_weight_multiplier:"<<update_avg_frame_weight_multiplier<<std::endl;
    
//     if(current_period*update_avg_frame_weight_multiplier + tracked_period*(1-update_avg_frame_weight_multiplier) < 0){
// 	std::cout<<"("<<current_period<<"*"<<update_avg_frame_weight_multiplier<<" + "<<tracked_period<<"*(1-"<<update_avg_frame_weight_multiplier<<")"<<std::endl;
// 	
//     }
    tracked_period = current_period*update_avg_frame_weight_multiplier + tracked_period*(1-update_avg_frame_weight_multiplier);
    
    tracked_hz=1.0/tracked_period;
    
    double updateerr = fabs(current_period-tracked_period);
    tracked_period_error=updateerr*avg_frame_weight_multiplier + tracked_period_error*(1-avg_frame_weight_multiplier );
    

    num_updates++;
    last_time_point=received_at;
}

void RateTracker::print_status(){    

    std::cout<<"["<<name<<" "<<std::fixed<<std::setprecision(4)<<current_period<<"s frame proc";
//     std::cout<<", "<<(durf*SAMPLE_RATE)<<" short samples";
    std::cout<<", "<<std::fixed<<std::setprecision(4)<<tracked_period<<"s avg fps";
    std::cout<<", "<<std::fixed<<std::setprecision(2)<<tracked_hz<<"s avg hz";
    std::cout<<", "<<std::fixed<<std::setprecision(4)<<tracked_period_error<<"s avg fps err ";
    std::cout <<"]"<<std::endl;

}