#include "common/PCLTools.h"
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

void PCLTools::displayCloudsColoured (std::vector<PCLObject::Ptr> &clouds,boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer,boost::mutex* viewer_mutex,std::string basename,int last_size){
  
  if(!viewer){
      std::cerr<<"Requested to do something strange. File "<<__FILE__<<", line "<<__LINE__<<std::endl;
      return;
  }
  viewer_mutex->lock();
  std::string name;
//   for (size_t i = 0; i < last_size; i++)//weirdness fix?
  for (int i = (int)clouds.size(); i < last_size; i++)
  {
    name=basename + std::to_string(i);
    if(verbosity>3)std::cout<<"PCLTools::displayCloudsColoured: Removing cloud "<<name<<"("<<i<<") from visualiser."<<std::endl;
//       viewer_mutex->lock();
    viewer->removePointCloud(name.c_str());
//       viewer_mutex->unlock();
  }
  
  unsigned char red [12] = {255, 255,   0,   0,   0, 255, 128,   0,   0, 128, 128,   0};
  unsigned char grn [12] = {  0, 255, 255, 255,   0,   0,0, 128,   0, 128,   0, 128};
  unsigned char blu [12] = {  0,   0,   0, 255, 255, 255, 0,   0, 128,   0, 128, 128};
  
  for (int i = 0; i < (int)clouds.size(); i++)
  {
    
    pcl::PointCloud<SystemPoint>::Ptr ccc=boost::make_shared<pcl::PointCloud<SystemPoint> >(*(clouds[i]->objectCloud));
    std::string s = basename + std::to_string(i);
    char const *pchar = s.c_str();
    pcl::visualization::PointCloudColorHandlerCustom<SystemPoint> color0(ccc,red[i%12] ,grn[i%12],blu[i%12]);
    
//       viewer_mutex->lock();
    if(verbosity>3)std::cout<<"PCLTools::displayCloudsColoured: Adding point cloud "<<name<<"("<<i<<") to visualiser."<<std::endl;
    if (!viewer->updatePointCloud(ccc,color0,pchar))
      viewer->addPointCloud (ccc,color0,pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, pchar);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.3, pchar);
//       viewer_mutex->unlock();
  }
  viewer_mutex->unlock();
}

