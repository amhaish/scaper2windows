#include <common/Timer.h>
#include <iostream>
#include <chrono>
#include <boost/thread.hpp>

int main(int argc,char *argv[]){
  Timer t1("Timer 1");
  Timer t2("Timer 2");
  for(size_t i = 0 ; i<12;i++){
      Timer *t;
      uint sl;
      if (i%3==0){
	t=&t1;
	sl=2;
      }
      else {
	t=&t2;
	sl=1;
      }
      
      t->reset();
      boost::this_thread::sleep_for( boost::chrono::seconds(sl) );
      t->accumulate_and_print_elapsed();
//       t->accumulate();
//       t->print_elapsed();
       t->print_accumulated_average();
      
  }
  
  t1.print_accumulated_average();
  t2.print_accumulated_average();
}