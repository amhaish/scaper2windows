#include <pcl/visualization/histogram_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <driver/depth_sense_grabber.h>

#include <PCLEuclideanSegmenter.h>
#include <PCLStatisticalOutlierFilter.h>
#include <PCLPlaneModelsFilter.h>
#include <PCLNormalCalculator.h>
#include <SegmentTrackerBaseline.h>
#include <ExecutionPipeline.h>
#include <soundsubsystems/ContourSoundSystem/SegmentSonifyContour.h>

#pragma once

#ifdef ISDLL
#define DLL __declspec(dllexport)
#endif

#ifdef USEDLL
#define DLL __declspec(dllimport)
#endif

namespace Scaper2
{
	enum DLL GrabberType { DepthSenseGrabber = 0, OpenNIGrabber = 1, OpenNI2Grabber = 2, PCDGrabber = 3 };
	class DLL ScaperEngine
	{
	public:
		ScaperEngine(GrabberType);
		ScaperEngine(GrabberType, bool);
		void ConfigureEngineVisualizers(bool, bool, bool, bool, bool, bool, bool, bool, bool);
		void ConfigurePipelineComponents(bool,bool,char,char,char);
		void ConfigurePipelineParameters(float,float,float,float,float,float,int,bool,double,double,float,float);
		void StartPipeline();
		void StopPipeline();
		~ScaperEngine();
	private:
		void ViewersCheker();

		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_orig;
		boost::shared_ptr<pcl::visualization::ImageViewer> viewer_orig_img;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_planar;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_seg;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_tracker;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_tracker_matches;
		boost::shared_ptr<pcl::visualization::PCLHistogramVisualizer> viewer_hist;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_selector;
		//Visualizer options
		bool viz_orig;
		bool viz_orig_img;
		bool viz_planar;
		bool viz_seg;
		bool viz_tracker;
		bool viz_tracker_matches;
		bool viz_hist;
		bool viz_selector;
		bool displayNormals;

		//Pipline components
		bool enableStatistcalOutlierFilter;
		bool enablePlaneFilter;
		char selection_approach;
		char tracking_approach;
		char sound_system_approach;

		//Normal Calculation parameters
		float  normalCalcMaxDepthChangeFactor;
		float  normalSmoothingSize;

		//Planes filtering parameters
		double planeFilterDistanceThreshold;
		double planeFilterAngularThreshold;
		float  planeFilterMaximumCurvature;
		float  planeFilterMinInliers;

		//Segmentation parameters
		float segmenterDistanceThreshold;
		float  segmenterAngularThreshold;
		float  segmenterCurvatureThreshold;
		float segmentYThreshold;
		int segmentSizeThreshold;
		bool segment_nan_normals_are_edges;


		double filterStddevMulThresh;
		int filterMeanK;
		double selector_scan_distance, selector_scan_time;
		double fpfh_radius;
		double sound_x_offset;
		double sound_y_offset;
		double sound_z_offset;
		double exaggerate_pos;

		//Performance parameters
		bool gpu;

		std::string path_to_fpfh_database;
		pcl::Grabber *interface;
		boost::mutex viewer_mutex;
		ExecutionPipeline *executionPipeline;
	};
}

