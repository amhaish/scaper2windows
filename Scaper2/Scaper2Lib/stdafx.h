// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define ISDLL
// Windows Header Files:
/*#include <windows.h>*/
#include <stdio.h>
#include <tchar.h>

#include <Consts.h>
#include <algorithm>
#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
