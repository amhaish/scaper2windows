#include "stdafx.h"
#include "ScaperEngine.h"
using namespace Scaper2;

ScaperEngine::ScaperEngine(GrabberType grabberType):ScaperEngine(grabberType, false) { }
ScaperEngine::ScaperEngine(GrabberType grabberType, bool timing) {
	switch (grabberType)
	{
	case GrabberType::DepthSenseGrabber:
		this->interface = new pcl::DepthSenseGrabber("");
		break;
	default:
		throw new std::exception("Grabber type wasn't chosen correctly");
		break;
	}
	executionPipeline = new ExecutionPipeline();
	executionPipeline->setTiming(timing);
}


void ScaperEngine::ConfigureEngineVisualizers(bool viz_orig, bool viz_orig_img, bool viz_planar, bool viz_seg, bool viz_tracker,bool viz_tracker_matches, bool viz_hist, bool viz_selector, bool dispalyNormals)
{
	this->viz_orig = viz_orig;
	this->viz_orig_img = viz_orig_img;
	this->viz_planar = viz_planar;
	this->viz_seg = viz_seg;
	this->viz_tracker = viz_tracker;
	this->viz_tracker_matches = viz_tracker_matches;
	this->viz_hist = viz_hist;
	this->viz_selector = viz_selector;
	this->displayNormals = dispalyNormals;
}
void ScaperEngine::ConfigurePipelineComponents(bool statistcalOutlierFilter, bool planeFilter, char selection_approach, char tracking_approach, char sound_system_approach)
{
	this->enableStatistcalOutlierFilter = statistcalOutlierFilter;
	this->enablePlaneFilter = planeFilter;
	this->selection_approach = selection_approach;
	this->tracking_approach = tracking_approach;
	this->sound_system_approach = sound_system_approach;
}

void ScaperEngine::ConfigurePipelineParameters(
	float maxDepthChangeFactor,
	float normalSmoothingSize, 
	float segmenterDistanceThreshold,
	float  segmenterAngularThreshold,
	float segmentYThreshold,
	float segmenterCurvatureThreshold,
	int segmentSizeThreshold,
	bool segment_nan_normals_are_edges,
	double planeFilterDistanceThreshold,
	double planeFilterAngularThreshold,
	float  planeFilterMaximumCurvature, 
	float  planeFilterMinInliers
	)
{
	this->normalCalcMaxDepthChangeFactor = maxDepthChangeFactor;
	this->normalSmoothingSize = normalSmoothingSize;
	this->segmenterDistanceThreshold = segmenterDistanceThreshold;
	this->segmenterAngularThreshold = segmenterAngularThreshold;
	this->segmentYThreshold = segmentYThreshold;
	this->segmentSizeThreshold = segmentSizeThreshold;
	this->segmenterCurvatureThreshold = segmenterCurvatureThreshold;
	this->segment_nan_normals_are_edges = segment_nan_normals_are_edges;
	this->planeFilterDistanceThreshold = planeFilterDistanceThreshold;
	this->planeFilterAngularThreshold = planeFilterAngularThreshold;
	this->planeFilterMaximumCurvature = planeFilterMaximumCurvature;
	this->planeFilterMinInliers = planeFilterMinInliers;
}

void ScaperEngine::StartPipeline()
{

	if (viz_orig)
		viewer_orig.reset(new pcl::visualization::PCLVisualizer("Original cloud"));
	else
		viewer_orig = NULL;
	if (viz_orig_img)
		viewer_orig_img.reset(new pcl::visualization::ImageViewer("Original cloud depth image"));
	else
		viewer_orig_img = NULL;
	if (viz_planar)
		viewer_planar.reset(new pcl::visualization::PCLVisualizer("Planar segmentation tester"));
	else
		viz_planar = NULL;
	if (viz_seg)
		viewer_seg.reset(new pcl::visualization::PCLVisualizer("Proto-object segmentation tester"));
	else
		viz_seg = NULL;
	if (viz_tracker)
		viewer_tracker.reset(new pcl::visualization::PCLVisualizer("Tracker tester"));
	else
		viz_tracker = NULL;
	if (viz_tracker_matches)
		viewer_tracker_matches.reset(new pcl::visualization::PCLVisualizer("Tracker matches"));
	else
		viewer_tracker_matches = NULL;
	if (viz_selector)
		viewer_selector.reset(new pcl::visualization::PCLVisualizer("Selector tester"));
	else
		viz_selector = NULL;
	if (viz_hist)
		viewer_hist.reset(new pcl::visualization::PCLHistogramVisualizer());
	else
		viz_hist = NULL;

	if (viz_orig)executionPipeline->AddOrigViewer(viewer_orig, viewer_mutex);
	if (viz_orig_img)executionPipeline->AddOrigImageViewer(viewer_orig_img, viewer_mutex);
	//Configuring pipline components

	IPCLNormalCalculator* normalCalculator = new PCLNormalCalculator(normalCalcMaxDepthChangeFactor, normalSmoothingSize);
	executionPipeline->SetPCLNormalCalculator(normalCalculator);

	if (enableStatistcalOutlierFilter)
	{
		IPCLFilter* filter = new PCLStatisticalOutlierFilter(NULL, filterMeanK, filterStddevMulThresh);
		executionPipeline->AddPCLFilter(filter);
	}

	if (enablePlaneFilter)
	{
		IPCLFilter* filter;
		if (viz_planar)
			filter = new PCLPlaneModelsFilter(viewer_planar, planeFilterDistanceThreshold, planeFilterMaximumCurvature, planeFilterMinInliers, planeFilterAngularThreshold, gpu, viewer_mutex);
		else
			filter = new PCLPlaneModelsFilter(NULL, planeFilterDistanceThreshold, planeFilterMaximumCurvature, planeFilterMinInliers, planeFilterAngularThreshold, gpu, viewer_mutex);
		executionPipeline->AddPCLFilter(filter);
	}

	IPCLSegmenter* segmenter;
	if (viz_seg)
		segmenter = new PCLEuclideanSegmenter(viewer_seg, segmenterDistanceThreshold, segmenterAngularThreshold, segmenterCurvatureThreshold,segmentYThreshold, segmentSizeThreshold, segment_nan_normals_are_edges, viewer_mutex);
	else
		segmenter = new PCLEuclideanSegmenter(NULL, segmenterDistanceThreshold, segmenterAngularThreshold, segmenterCurvatureThreshold,segmentYThreshold, segmentSizeThreshold, segment_nan_normals_are_edges, viewer_mutex);
	executionPipeline->SetPCLSegmenter(segmenter);

	//ISegmentTracker* tracker= new SegmentTrackerBaseline();
	//executionPipeline->SetSegmentTracker(tracker);
	//ISegmentSelector* selector;

	ISegmentSonify* soundSystem;//ContourSoundSystem or for basic things SIU

	/*if (viz_hist)
		soundSystem = new SegmentSonifyContour(fpfh_radius, viewer_hist, viewer_mutex, path_to_fpfh_database, exaggerate_pos);
	else
		soundSystem = new SegmentSonifyContour(fpfh_radius, NULL, viewer_mutex, path_to_fpfh_database, exaggerate_pos);*/

	//executionPipeline->SetSoundSonificationApproach(soundSystem);
	boost::function<void(const pcl::PointCloud<SystemPoint>::ConstPtr&)> callBackFunction = boost::bind(&ExecutionPipeline::ExecutePipeline, executionPipeline, _1);
	interface->registerCallback(callBackFunction);
	interface->start();
	ViewersCheker();
}

void ScaperEngine::StopPipeline()
{
	interface->stop();
	viewer_orig.reset();
	viewer_orig_img.reset();
	viewer_planar.reset();
	viewer_seg.reset();
	viewer_tracker.reset();
	viewer_hist.reset();
	viewer_selector.reset();
}

void ScaperEngine::ViewersCheker()
{
	bool st = false;
	while (!st) {
		if (viz_orig) {
			if (viewer_orig->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_orig->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_orig_img) {
			if (viewer_orig_img->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_orig_img->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_planar) {
			if (viewer_planar->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_planar->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_seg) {
			if (viewer_seg->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_seg->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_tracker) {
			if (viewer_tracker->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_tracker->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_selector) {
			if (viewer_selector->wasStopped())st = true;
			if (viewer_mutex.try_lock()) {
				viewer_selector->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		if (viz_hist) {
			if (viewer_mutex.try_lock()) {
				viewer_hist->spinOnce(0.0001);
				viewer_mutex.unlock();
			}
		}
		Sleep(100);
	}
}

ScaperEngine::~ScaperEngine()
{
	viewer_orig.reset();
	viewer_orig_img.reset();
	viewer_planar.reset();
	viewer_seg.reset();
	viewer_tracker.reset();
	viewer_hist.reset();
	viewer_selector.reset();
	delete interface;
	delete executionPipeline;
}
