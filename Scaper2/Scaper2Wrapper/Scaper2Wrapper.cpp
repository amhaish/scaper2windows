// This is the main DLL file.

#include "stdafx.h"
#include <string>
#include <msclr\marshal_cppstd.h>
#include "Scaper2Wrapper.h"



#include <vtkTextActor.h>
//For fixing a problem in vtk Visualizer Library with C++/CLI
void
pcl::visualization::PCLVisualizer::FPSCallback::Execute(vtkObject* caller, unsigned long, void*)
{
	vtkRenderer *ren = reinterpret_cast<vtkRenderer *> (caller);
	float fps = 1.0f / static_cast<float> (ren->GetLastRenderTimeInSeconds());
	char buf[128];
	sprintf(buf, "%.1f FPS", fps);
	actor->SetInput(buf);
}

using namespace Scaper2Wrapper;

// Constructor implementaion
Scaper2Engine::Scaper2Engine(WrapperGrabberType grabberType) : ManagedObject(new Scaper2::ScaperEngine(static_cast<Scaper2::GrabberType>(grabberType))) {}

Scaper2Engine::Scaper2Engine(WrapperGrabberType grabberType, bool timing) : ManagedObject(new Scaper2::ScaperEngine(static_cast<Scaper2::GrabberType>(grabberType), timing)) {  }

void Scaper2Engine::ConfigureEngineVisualizers(bool viz_orig, bool viz_orig_img, bool viz_planar, bool viz_seg, bool viz_tracker, bool viz_tracker_matches, bool viz_hist, bool viz_selector, bool dispalyNormals)
{
	this->m_Instance->ConfigureEngineVisualizers(viz_orig, viz_orig_img, viz_planar, viz_seg, viz_tracker, viz_tracker_matches, viz_hist, viz_selector, dispalyNormals);
}

void Scaper2Engine::ConfigurePipelineComponents(bool statistcalOutlierFilter, bool planeFilter, char selection_approach, char tracking_approach, char sound_system_approach)
{
	this->m_Instance->ConfigurePipelineComponents(statistcalOutlierFilter, planeFilter, selection_approach, tracking_approach, sound_system_approach);
}

void Scaper2Engine::ConfigurePipelineParameters(
	float maxDepthChangeFactor, 
	float normalSmoothingSize, 
	float segmenterDistanceThreshold, 
	float  segmenterAngularThreshold, 
	float segmenterCurvatureThreshold,
	float segmentYThreshold, 
	int segmentSizeThreshold, 
	bool segment_nan_normals_are_edges,
	double planeFilterDistanceThreshold,
	double planeFilterAngularThreshold,
	float  planeFilterMaximumCurvature,
	float  planeFilterMinInliers
)
{
	this->m_Instance->ConfigurePipelineParameters(maxDepthChangeFactor, normalSmoothingSize, segmenterDistanceThreshold, segmenterAngularThreshold, segmenterCurvatureThreshold, segmentYThreshold, segmentSizeThreshold, segment_nan_normals_are_edges, planeFilterDistanceThreshold, planeFilterAngularThreshold, planeFilterMaximumCurvature, planeFilterMinInliers);
}

void Scaper2Engine::StartPipeline()
{
	this->m_Instance->StartPipeline();
}

void Scaper2Engine::StopPipeline()
{
	this->m_Instance->StopPipeline();
}
