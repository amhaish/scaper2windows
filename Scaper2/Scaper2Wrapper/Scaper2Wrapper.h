#pragma once
#define generic GenericPath
#undef max
#undef min
#include "..\Scaper2Lib\ScaperEngine.h"
#undef generic
#include "ManagedObject.h"
using namespace System;

namespace Scaper2Wrapper {
	public enum class WrapperGrabberType
	{
		DepthSenseGrabber = 0, OpenNIGrabber = 1, OpenNI2Grabber = 2, PCDGrabber = 3
	};

	public ref class Scaper2Engine :public ManagedObject<Scaper2::ScaperEngine>
	{
	public:
		Scaper2Engine(WrapperGrabberType);
		Scaper2Engine(WrapperGrabberType, bool);
		void ConfigureEngineVisualizers(bool, bool, bool, bool, bool, bool, bool, bool,bool);
		void ConfigurePipelineComponents(bool,bool,char,char,char);
		void ConfigurePipelineParameters(float,float,float,float,float,float,int,bool,double,double,float,float);
		void StartPipeline();
		void StopPipeline();
	};
}


